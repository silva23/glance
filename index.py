# coding: utf-8

import sys
import cgi
import cgitb

import view.html_base as html
from view.html_etc import getParamDic

cgitb.enable()
form = cgi.FieldStorage()

DEBUG = False  # 전달된 Parameter 를 보고싶을경우 True

if __name__ == "__main__":
	
	# 넘어온 PARAM 을 Dic 으로 받음(자동 unquote)
	param_dic = getParamDic(form, [])

	# 실제 컨텐츠 작성
	html_content = """
	<div class="box" >
		<h2>Welcome to Glance</h2>
		<br/>
		<p>Some contents comes here!</p>
	</div>
	"""

	site_path = ["MAIN"]
	print "Content-type: text/html; charset=utf-8\n"
	print html.getHead(title_list=(), js="", css="", js_paths=(), css_paths=())
	print html.getTopMenuBar()

	# CONTENT HTML이 위치하는 곳
	if DEBUG:
		print str(form)+html_content  # Parameter DEBUG 시 주석해제
	else:
		print html_content

	print html.getLeftMenuBar(menu_type="")
