#!/usr/bin/env python2.7
# coding:utf-8

import urllib2
import cgi

TIME_FORMAT = "%Y-%m-%d %H:%M:%S"

################## JAVA SCRIPT ##########################################################

# 해당페이지로 유입되는 Parameter를 HTML 형태로 전부 출력한다
def showAllParams(form):
	keyList = form.keys()
	ret = ""
	for key in keyList:
		ret += "<pre>"+key+":"+str(form[key].value)+"</pre><br/>"
	return ret

def getParamDic(form, keyList):
	ret = dict()
	for key in keyList:
		if form.has_key(key):
			ret[key] = urllib2.unquote(form.getvalue(key)).strip()
		else:
			ret[key] = ""
	return ret

def getGetParamStr(dic):
	paramStr = ""
	for key, val in dic.items():
		paramStr += "%s=%s&"%(key, urllib2.quote(str(val)))
	return paramStr

def getOrderingLinkHtml(url, param_dic, orderby_field):
	param_dic["orderby"] = orderby_field
	param_dic["ordering"] = "ASC"
	get_params1 = getGetParamStr(param_dic)
	param_dic["ordering"] = "DESC"
	get_params2 = getGetParamStr(param_dic)
	html = """
	<a href='%s?%s'>▲</a><a href='%s?%s'>▼</a>
	"""%(url, get_params1, url, get_params2)
	return html

# 선택형 Dialog를 띄우는 Javascript 코드를 반환
# 입력 데이터 앞뒤에 표시할 문자열을 지정할 수 있다.
def getJSConfirmPopup(msgFront="",msgBack="",funcName="confirmPopup",openNewWindow=True):
	js_script = """
	function %s(data, redirURL) {
		if(confirm("%s"+data+"%s") == true){
			window.location.href=redirURL;
		}
	}
	"""%(funcName, msgFront, msgBack)

	if openNewWindow:
		js_script = """
		function %s(data, redirURL) {
			if(confirm("%s"+data+"%s") == true){
				window.open(redirURL, '_blank');
			}
		}
		"""%(funcName, msgFront, msgBack)

	return js_script

def getJSCloseWindow(msg=""):
	msg = msg.replace("\"", "")
	js_script = """
	<script>
		alert(\"%s\");
		window.close();
	</script>
	"""%msg
	return js_script

################## HTML FRAME ##########################################################

# msg를 띄운뒤 특정 페이지로 리다이렉트
def getHtmlRedirect(url, msg=""):
	if len(msg) != 0:
		msg = "alert(\"%s\");"%msg
	redir_code = " window.location.href = '"+url+"';"
	html_redir = """
<html>
	<head>
		<script>
			"""+msg+redir_code+"""
		</script>
	</head>
	<body>REDIRECTING...</body>
</html>
	"""
	return html_redir


def getSelectRow(rowName, formID, name, optionInfoList) :
	html_select = """
	<tr>
		<td>%s</td>
		<td>
			<select form="%s" name="%s">
	"""

	for optionInfo in optionInfoList:  # val, viewVal
		html_select += "<option value='%s'>%s</option>"%optionInfo

	html_select += """
			</select>
		</td>
	</tr>
	"""


# 해당 <input> 구성정보를 바탕으로 Form 내부 HTML을 생성하여 리턴
def getForm(formInfoList, rowAddFront="", rowAddEnd=""):
	html_form = """
	<input type='hidden' name='cmd' value='%s' />
	<table>
	"""%formInfoList[0] + rowAddEnd

	listLen = len(formInfoList)
	for i in range(1, listLen) :
		rowTitle, typeAttr, nameAttr, valAttr = formInfoList[i]

		if len(typeAttr) > 0 :
			html_form += """
				<tr>
					<td>%s</td>
					<td>
						<input type='%s' name='%s' value='%s'/>
					</td>
				</tr>
			"""%(rowTitle, typeAttr, nameAttr, valAttr)
		else:
			html_form += "<tr> <td>%s</td> <td>%s</td> </tr>"%(rowTitle, valAttr)
	
	return html_form+rowAddEnd+"</table>"


# 해당 listData를 가지고 Table을 만들어 준다.
def getTableTR(dataList, rowType="", style=""):
	ret = ""
	if rowType == "title":
		ret += "<tr class='titleRow' style='%s'>"%style
		for colName in dataList:
			ret += "<th>%s</th>"%colName
		ret += "</tr>"

	else:
		ret += "<tr style='%s'>"%style
		for data in dataList:
			ret += "<td>%s</td>"%cgi.escape(str(data))
		ret += "</tr>"

	return ret

