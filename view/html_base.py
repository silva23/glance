#!/usr/bin/env python2.7
# coding:utf-8

from glance.config.html_info import FAVICON_PATH, BASE_CSS_FILES, BASE_JS_FILES, TOP_TITLE, TOP_DESC, VIRTUAL_HOST_PATH


def getHead(title_list=(), js="", css="", js_paths=(), css_paths=()):
	"""
	:param title_list: Depth에 따른 타이틀 목록
	:param js: 삽입할 javascript 코드
	:param css: 삽입할 style 코드
	:param js_paths: 추가할 js파일 path 리스트
	:param css_paths: 추가할 css파일 path 리스트
	:return: <head>~</head> 부분의 html
	"""
	title = ""
	if len(title_list) > 0:  # 상위 > 하위 메뉴 리스트
		title = title_list[-1]  # 최하위메뉴가 타이틀 태그가 됨.

	html_imports = []

	for js_path in BASE_JS_FILES:
		html_imports.append("<script src='%s'></script>"%js_path)
	for js_path in js_paths:
		html_imports.append("<script src='%s'></script>"%js_path)

	for css_path in BASE_CSS_FILES:
		html_imports.append("<link rel='stylesheet' type='text/css' href='%s' />"%css_path)
	for css_path in css_paths:
		html_imports.append("<link rel='stylesheet' type='text/css' href='%s' />"%css_path)

	html_head = """
	<html>
		<head>
			<title>"""+title+"""</title>
			%s
			<link href='%s' rel='shortcut icon' type='image/ico'/>
			<script>%s</script>
			<style>%s</style>
		</head>
	"""%(''.join(html_imports), FAVICON_PATH, js, css)

	return html_head

def getTopMenuBar(site_path=()):

	html_site_path_list = []
	for i in range(0, len(site_path)):
		path = site_path[i]
		if i != 0:
			html_site_path_list.append("&nbsp;%gt;&nbsp;")
		html_site_path_list.append("<strong>%s</strong>"%path)
	html_site_path = "".join(html_site_path_list)

	html_top_menus = ""

	if site_path:  # site_path 가 있을때에만 표시
		# TODO : Edit here to modift top menu bar
		html_top_menus = """
				<li style='background:#ffffcc; font-weight:800; color:red;'>
					Sample<br/>
					<a href='#'>link1</a>&nbsp;|
					<a href='#'>link2</a>
				</li>

				<li><a href='#'>Sample<span>link3</span></a></li>
		"""

	# 최상단 탭바
	html_body = """
	<body>
	<div id='header'>
		<h1>
			<a href="%s/index.py"><span>%s</span></a>
		</h1>
		<h2>%s</h2>
		<ul class='ulList'>
			%s
		</ul>
		<p id='layoutdims'>%s</p>
	</div>
	<div class='colmask leftmenu'>
		<div class='colleft'>
			<div class='col1'>
				<!-- Column 1 start -->
	"""%(VIRTUAL_HOST_PATH, TOP_TITLE, TOP_DESC, html_top_menus, html_site_path)

	return html_body


def getLeftMenuBar(menu_type=""):  # 좌측 세로 메뉴

	body_common_start = """
				<!-- Column 1 end -->
			</div>
			<div class='col2'>
				<!-- Column 2 start -->
	"""

	# TODO : Edit here to modify left menu bar

	left_col = "<h2>Need<br/>menu_type<h2>"
	if menu_type == "menu_type1":
		left_col = """
				<h2>LEFT MENU 1</h2>
				<p>Sample left menu : using type 1</p>

				<h3>Sub menu1</h3>
				<ul>
					<li><a href='#'>title1</a></li>
					<li><a href='#'>title2</a></li>
				</ul>
		"""

	elif menu_type == "menu_type2":
		left_col = """
				<h2>LEFT MENU 2</h2>
				<p>Sample left menu : using type 2</p>

				<h3>Sub menu1</h3>
				<ul>
					<li><a href='#'>title1</a></li>
					<li><a href='#'>title2</a></li>
				</ul>

				<h3>Sub menu2</h3>
				<ul>
					<li><a href='#'>title1</a></li>
					<li><a href='#'>title2</a></li>
				</ul>
		"""

	# TODO : Edit here to modify bottom area
	body_common_end = """
				<!-- Column 2 end -->
			</div>
		</div>
	</div>
	<div id='footer'>
		<p>
			Copyright ⓒ  All Rights Reserved. <br/>
			<span>Glance</span><br/>
			- Made by Shiwoo,Park 2014 -
		</p>
	</div>
	</body>
</html>
	"""
	return body_common_start + left_col + body_common_end