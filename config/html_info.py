# coding:utf-8

"""
html의 각종 속성을 정의할 수 있는 파일
"""

# Apache 에서 설정한 프로젝트 접근 path
VIRTUAL_HOST_PATH = "/glance"

TOP_TITLE = "TOP TITLE (config/html_base.py)"
TOP_DESC = "TOP_DESC (config/html_base.py)"

FAVICON_PATH = "/resources/favicon.png"
BASE_JS_FILES = ["/js/jquery-1.10.2.js", "/js/jquery-ui.js"]
BASE_CSS_FILES = ["/css/jquery-ui.css", "/css/screen.css", "/css/default.css"]