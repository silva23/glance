#coding:utf-8

DATA_VIEW_INFO = dict()

"""
각 Table의 필드값을 표시용 값으로 1:1 매핑하기 위한 정보 지정

<예제>
'hr.person' 이라는 테이블에 'attendence'라는 필드(Y or N)가 있다고 할때,
'Y' 를 '참석', 'N' 을 '불참' 이라고 표시하고자 한다면,
아래와같은 코드를 입력한다.

DATA_VIEW_INFO["hr.person"] = dict()  # 테이블에 대한 사전 생성(이미 생성한 경우 생성X)
DATA_VIEW_INFO["hr.person"]["attendence"] = dict()  # 해당 테이블의 필드에 대한 사전생성
DATA_VIEW_INFO["hr.person"]["attendence"]["Y"] = "참석"
DATA_VIEW_INFO["hr.person"]["attendence"]["N"] = "불참"
"""
