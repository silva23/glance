#coding:utf-8

DATA_INFO = dict()

"""
<INTRO>

각 Table에 대해 필드별 용법 정의를 위한 모듈

1. 테이블 DATA를 대표할 key를 지정한다. (여기서는 예를들어 person 이라고 해보자.)
2. 테이블의 각 컬럼을 속성 및 용도에 따라 분류한다.
3. 각 컬럼을 목적에 맞도록 아래의 코드를 복사하여 필요에 따라 여러개를 작성한다.

data_info_key = "person"  # 테이블 DATA를 대표할 key
DATA_INFO[data_info_key] = dict()
DATA_INFO[data_info_key]["table"] = "테이블명"
DATA_INFO[data_info_key]["select"] = [View에서 보여줄 select 필드 나열]
DATA_INFO[data_info_key]["unique"] = [해당 테이블의 단일 UNIQUE 필드 나열]
DATA_INFO[data_info_key]["where_equal"] = [해당 테이블에서 동일 where 조건을 사용할 필드 나열]
DATA_INFO[data_info_key]["where_like"] = [해당 테이블에서 LIKE where 조건을 사용할 필드 나열 (항상 필드 마지막에 '_like'또는 '_notlike' 를 추가로 붙일것)]
DATA_INFO[data_info_key]["essential"] = [해당 테이블에 데이터 INSERT 시에 반드시 값이 존재해야하는 필드 나열]
"""
