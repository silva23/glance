#!/usr/bin/env python
#coding: utf8

import dao
from glance.utils.log import getLogger
from glance.config.data_info import DATA_INFO
from dao import getDBConnectionByName

"""
<1. INTRO>

데이터의 INSERT, UPDATE, SELECT 를 통합 관리하는 모듈

- 입력받은 dictionary의 특정 key로 모든 수행 로직이 결정되며,
  해당 dictionary에는 작업수행에 필요한 데이터도 포함되어있어야 한다.

- addUpdateData() 함수의 where 조건은 반드시 unique 필드로만 where 조건을 걸어 데이터를 조회하며,
  특정 필드값이 새로 입력되지 않았을 경우에는 Load한 OLD 데이터가 그대로 유지된다.

<2. 각 함수의 input_dic 에 반드시 필요한 key, val 값>

KEY : VALUE 설명
※아래의 KEY 는 예약된 KEY 로서 DB 컬럼명으로 사용불가

db_info_key : 작업하고자 하는 DB 정보를 가진 KEY (~/config/db_info.py 참고)
data_info_key : 작업하고자 하는 데이터의 테이블명 (~/config/data_info.py 참고)
{해당 테이블의 컬럼명} : {해당 테이블의 컬럼값}
"""

class DBUtil:

	def __init__(self, _cursor=None):
		self.cursor = _cursor
		self.logger = getLogger()

	def getFieldInfo(self, data_info_key):
		if data_info_key in DATA_INFO:
			ret = DATA_INFO[data_info_key]
		else:
			self.logger.error("Failed to find field information")
			ret = None
		return ret

	def addData(self, input_dic, _cursor=None):
		cursor = None
		if self.cursor:
			cursor = self.cursor
		if _cursor:
			cursor = _cursor
		need_close = False
		if not cursor:
			cursor = getDBConnectionByName(input_dic["db_info_key"])
			need_close = True

		ret = self.__addData(input_dic, cursor)

		if need_close:
			cursor.close()
		return ret

	def addUpdateData(self, input_dic, _cursor=None):
		cursor = None
		if self.cursor:
			cursor = self.cursor
		if _cursor:
			cursor = _cursor
		need_close = False
		if not cursor:
			cursor = getDBConnectionByName(input_dic["db_info_key"])
			need_close = True

		ret = self.__addUpdateData(input_dic, cursor)

		if need_close:
			cursor.close()
		return ret

	def delData(self, input_dic, _cursor=None):
		cursor = None
		if self.cursor:
			cursor = self.cursor
		if _cursor:
			cursor = _cursor
		need_close = False
		if not cursor:
			cursor = getDBConnectionByName(input_dic["db_info_key"])
			need_close = True

		ret = self.__delData(input_dic, cursor)

		if need_close:
			cursor.close()
		return ret

	def getDataList(self, input_dic, mode="", row_count=15, _cursor=None):
		cursor = None
		if self.cursor:
			cursor = self.cursor
		if _cursor:
			cursor = _cursor
		need_close = False
		if not cursor:
			cursor = getDBConnectionByName(input_dic["db_info_key"])
			need_close = True

		ret = self.__getDataList(cursor, input_dic, mode, row_count)

		if need_close:
			cursor.close()
		return ret

	def __addData(self, input_dic, cursor):
		field_info_dic = self.getFieldInfo(input_dic["data_info_key"])
		if not field_info_dic:
			return "Failed to  getFieldInfo(): key [%s]"%input_dic["data_info_key"]

		total_keys = field_info_dic["select"]

		# 입력 데이터 제작
		set_dic = dict()
		for i in range(len(total_keys)):
			key = total_keys[i]
			if key in input_dic:
				set_dic[key] = input_dic[key]

		for key in field_info_dic["essential"]:
			if key not in set_dic:
				return "Failed to addUpdateData : essential field [%s] data is missing"%key

		return dao.insertData(cursor, "%s Data"%(field_info_dic["table"]), field_info_dic["table"], set_dic)

	def __addUpdateData(self, input_dic, cursor):
		field_info_dic = self.getFieldInfo(input_dic["data_info_key"])
		if not field_info_dic:
			return "Failed to  getFieldInfo(): key [%s]"%input_dic["data_info_key"]

		total_keys = field_info_dic["select"]
		where_dic = dict()

		# 데이터 존재 여부 확인용 LOAD
		loaded_data = None
		for key in field_info_dic["unique"]:
			if key in input_dic:
				if input_dic[key]:
					where_dic[key] = input_dic[key]
					loaded_data = self.getDataList(input_dic)
					if loaded_data:
						loaded_data = loaded_data[0]
					break

		# 입력 데이터 제작
		set_dic = dict()
		for i in range(len(total_keys)):
			key = total_keys[i]
			if key in input_dic:
				set_dic[key] = str(input_dic[key]).strip()
			else:
				if loaded_data:  # 미지정값은 기존 값으로 할당
					if loaded_data[i]:
						set_dic[key] = loaded_data[i]

		for key in field_info_dic["essential"]:
			if key not in set_dic:
				return "Failed to addUpdateData : essential field [%s] data is missing"%key

		if where_dic:
			if loaded_data:
				ret = dao.updateData(cursor, "%s Data"%(field_info_dic["table"]), field_info_dic["table"], set_dic, where_dic)
			else:
				ret = dao.insertData(cursor, "%s Data"%(field_info_dic["table"]), field_info_dic["table"], set_dic)
			return ret
		else:
			return "Failed to addUpdateData : %s"%(field_info_dic["table"])

	def __delData(self, input_dic, cursor):
		field_info_dic = self.getFieldInfo(input_dic["data_info_key"])
		if not field_info_dic:
			return "Failed to  getFieldInfo(): key [%s]"%input_dic["data_info_key"]

		where_dic = dict()
		for key in field_info_dic["unique"]:
			if key in input_dic:
				if input_dic[key]:
					where_dic[key] = input_dic[key]
					break

		if not where_dic:
			for key in field_info_dic["where_equal"]:
				if key in input_dic:
					if input_dic[key]:
						where_dic[key] = input_dic[key]
						break

		if where_dic:
			return dao.deleteData(cursor, "%s Data"%(field_info_dic["table"]), field_info_dic["table"], where_dic)
		else:
			return "Failed to delData : No where clause : Input dic=" + str(input_dic)


	def __getDataList(self, cursor, input_dic, mode="", row_count=15):
		"""
		:param input_dic : where 조건데이터 및 필드 정보 취득용 데이터
			input_dic['_service_type'] : 얻고자하는 service 종류
			input_dic['_data_type'] : 데이터 타입
			input_dic['sc_type'] : SC명
			input_dic['@page'] : 페이지 번호
			input_dic['@row_count'] : 페이지 번호
		:param mode: count(개수), web(페이징), default(일반)
		:param row_count: web 모드일때만 사용됨, 한개 페이지당 리턴할 데이터 개수
		:return: selected data(tuple) list 또는 integer (count 모드)
		"""
		field_info_dic = self.getFieldInfo(input_dic["data_info_key"])
		if not field_info_dic:
			return "Failed to  getFieldInfo(): key [%s]"%input_dic["data_info_key"]

		select_keys = field_info_dic["select"]

		# 단일 데이터 선택
		unique_keys = field_info_dic["unique"]
		for key in unique_keys:
			if key in input_dic:
				if input_dic[key]:
					ret_data = dao.selectData(cursor, select_keys, field_info_dic["table"], {key:input_dic[key]})
					if mode == "count":
						if ret_data:
							return 1
						else:
							return 0
					else:
						return ret_data

		if mode == "unique":
			return None

		# WHERE 절 제작
		whereList = []
		paramList = []
		where_equal_keys = field_info_dic["where_equal"]
		for key in where_equal_keys:
			if key not in input_dic:
				continue
			if input_dic[key]:
				whereList.append(key+" = %s")
				paramList.append(input_dic[key])

		where_like_keys = field_info_dic["where_like"]
		for key in where_like_keys:
			if key not in input_dic:
				continue
			if input_dic[key]:
				not_str = ""
				if key.endswith("NotLike"):
					not_str = " NOT"
					rkey = key[:-7]
				elif key.endswith("_notlike"):
					not_str = " NOT"
					rkey = key[:-8]
				elif key.endswith("Like"):
					rkey = key[:-4]
				elif key.endswith("_like"):
					rkey = key[:-5]
				else:
					rkey = key
				whereList.append(rkey+not_str+" LIKE %s")
				paramList.append("%"+input_dic[key]+"%")

		where_clause = ""
		for i in range(0, len(whereList)):
			if i == 0:
				where_clause += " WHERE " + whereList[i]
			else:
				where_clause += " AND " + whereList[i]

		if mode == "count":
			ret_data = dao.selectCount(cursor, field_info_dic["table"], None, where_clause, paramList)
			return ret_data

		ordering = "DESC"
		if "ordering" in input_dic:
			if input_dic["ordering"]:
				ordering = input_dic["ordering"]
		orderby_field = unique_keys[0]
		if "orderby" in input_dic:
			if input_dic["orderby"]:
				if input_dic["orderby"] in field_info_dic["orderby"]:
					orderby_field = input_dic["orderby"]
		orderby_clause = " ORDER BY %s %s "%(orderby_field, ordering)

		# LIMIT 절 제작
		limit_clause = ""
		if mode == "web":
			page = 1
			if "page" in input_dic:
				page = int(input_dic["page"])

			if page == 1:
				limit_clause += " LIMIT %s"%row_count
			else:
				limit_clause += " LIMIT %s, %s"%((page-1)*row_count, row_count)

		ret_data = dao.selectData(cursor, select_keys, field_info_dic["table"], None, where_clause, paramList, orderby_clause, limit_clause)
		return ret_data

	def closeCursor(self):
		if self.cursor:
			self.cursor.close()