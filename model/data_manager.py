#!/usr/bin/python
#coding:utf-8

# MAKE DATA
def getFieldInfoDic(service_type, data_type, sc_type=""):
	"""
	:param service_type : 얻고자하는 service 종류
		(ex) domanager, pado, crawl
	:param data_type : 데이터 타입
		ex) domain, urlpt, parser, testurl, seed
	:param sc_type : SC명
		ex) news, oi, blog
	:return: 각 필드에 대한 정보 DIC (DB 쿼리 구분별 필드명)
	"""

	# data 타입, sc 타입에 따라 필드결정
	ret_dic = dict()
	ret_dic["table"] = ""
	ret_dic["select"] = []
	ret_dic["where_equal"] = []
	ret_dic["unique"] = []  # 여러개의 필드에 대해 AND 조건으로 유니크를 걸었을때는 &를 사용하여 여러개 지정가능.
	ret_dic["where_like"] = []
	ret_dic["essential"] = []  # 첫번째 element 는 반드시 primary_key 이어야 함.
	ret_dic["orderby"] = []

	if service_type == "domanager":  # DOMANAGER database
		
		if data_type == "domain":
			ret_dic["table"] = "domanager.domains"
			ret_dic["select"] = ["id", "domain", "mainPage", "ip", "generatorCode", "domainTypeCode", "robotBlockYN", "linkCount", "httpCode", "updateTime"]
			ret_dic["unique"] = ["id", "domain"]
			ret_dic["where_equal"] = ["generatorCode", "domainTypeCode", "robotBlockYN", "httpCode", "ip"]
			ret_dic["essential"] = ["domain"]

		elif data_type == "domainsc":
			ret_dic["table"] = "domanager.domain_sctype"
			ret_dic["select"] = ["domainScTypeID", "domainID", "scCode", "crawlStatusCode"]
			ret_dic["unique"] = ["domainScTypeID"]
			ret_dic["where_equal"] = ["domainID", "scCode", "crawlStatusCode"]
			ret_dic["essential"] = ["domainID", "scCode"]

		elif data_type == "sitemap":
			ret_dic["table"] = "domanager.sitemap_urls"
			ret_dic["select"] = ["id", "domain_id", "sitemap_url", "last_check", "seed_count"]
			ret_dic["unique"] = ["id"]
			ret_dic["where_equal"] = ["id","domain_id", "domain", "sitemap_url"]
			ret_dic["essential"] = ["domain_id", "sitemap_url"]

		elif data_type == "urlpt":
			ret_dic["table"] = "domanager.url_patterns"
			ret_dic["select"] = ["idx", "domainID", "domain", "priority", "urlType", "scCode", "inputPattern", "guidPattern"]
			ret_dic["unique"] = ["idx", "inputPattern"]
			ret_dic["where_equal"] = ["domainID","scCode","urlType","domain"]
			ret_dic["where_like"] = ["domainLike", "inputPatternLike"]
			ret_dic["essential"] = ["inputPattern"]

		elif data_type == "urlbpt":
			ret_dic["table"] = "domanager.url_build_patterns"
			ret_dic["select"] = ["idx", "domainID", "buildTypeCode", "guidPattern", "buildPattern"]
			ret_dic["unique"] = ["idx"]
			ret_dic["where_equal"] = ["idx", "domainID", "domain", "buildTypeCode"]
			ret_dic["where_like"] =["guidPatternLike", "buildPatternLike"]
			ret_dic["essential"] = ["domainID", "buildTypeCode", "guidPattern", "buildPattern"]

		elif data_type == "urlptparser":
			ret_dic["table"] = "domanager.url_pattern_parser"
			ret_dic["select"] = ["idx", "urlpatternID", "parserID"]
			ret_dic["unique"] = ["idx"]
			ret_dic["where_equal"] = ["urlpatternID", "parserID"]
			ret_dic["essential"] = ["urlpatternID", "parserID"]

		elif data_type == "manualseed":
			ret_dic["table"] = "domanager.manual_seed_urls"
			ret_dic["select"] = ["idx", "domain_id", "scCode", "domain", "seed_url", "down_url", "url_type"]
			ret_dic["unique"] = ["idx", "seed_url"]
			ret_dic["where_equal"] = ["domain_id", "domain", "url_type"]
			ret_dic["where_like"] = ["domainLike"]
			ret_dic["essential"] = ["down_url"]

	elif service_type == "pado":  # PADO database

		if data_type == "parser":
			ret_dic["table"] = "pado.parsers"
			ret_dic["select"] = ["idx","domainID", "domain", "name", "memo", "urlType", "scCode", "generatorCode","activeYN","validationStatus","errTestUrlCount","testUrlCount","modified","validated"]
			ret_dic["unique"] = ["idx"]
			ret_dic["where_equal"] = ["domainID", "scCode", "generatorCode", "activeYN", "validationStatus", "urlType"]
			ret_dic["where_like"] = ["domainLike", "nameLike", "memoLike"]
			ret_dic["essential"] = []

		elif data_type == "rule":
			ret_dic["table"] = "pado.parser_rules"
			ret_dic["select"] = ["idx","parserID","parentRuleID","field","ruleCode","ruleVal"]
			ret_dic["unique"] = ["idx"]
			ret_dic["where_equal"] = ["parserID","ruleCode"]
			ret_dic["where_like"] = ["fieldLike","ruleValLike"]
			ret_dic["essential"] = ["field", "ruleCode", "ruleVal"]

		elif data_type == "testurl":
			ret_dic["table"] = "pado.validation_testurls"
			ret_dic["select"] = ["guidHash", "guid", "downloadURL", "parserID", "domainID", "errorYN", "modified"]
			ret_dic["unique"] = ["guidHash"]
			ret_dic["where_equal"] = ["guidHash", "guid", "parserID", "domainID", "errorYN"]
			ret_dic["where_like"] = ["guidLike"]
			ret_dic["essential"] = ["guid", "downloadURL", "parserID", "domainID"]

		elif data_type == "history":
			ret_dic["table"] = "pado.validation_history"
			ret_dic["select"] = ["guid", "checkinfo", "checkStatus", "checkDate"]
			ret_dic["unique"] = ["guid"]
			ret_dic["where_equal"] = ["guid", "checkStatus"]

		elif data_type == "answer":
			ret_dic["table"] = "pado.validation_answers"
			ret_dic["select"] = ["fieldName", "fieldValue", "fieldType"]
			ret_dic["where_equal"] = ["guidHash"]

		elif data_type == "log":
			ret_dic["table"] = "pado.validation_log"
			ret_dic["select"] = ["idx", "summary", "checkDate"]
			ret_dic["where_equal"] = ["idx"]

	elif service_type == "crawl":  # 크롤 관리 테이블

		if data_type == "domain":
			ret_dic["table"] = "%sdb.%s_domains"%(sc_type, sc_type)
			ret_dic["select"] = ["domain_id", "domain", "name", "memo", "status", "server", "pid", "seed_url_count", "daily_count", "crawl_count_history", "last_visit"]
			ret_dic["unique"] = ["domain_id", "domain"]
			ret_dic["where_equal"] = ["name", "status", "server", "pid", "seed_url_count", "daily_count"]
			ret_dic["where_like"] = ["name_like", "domain_like","crawl_count_history_notlike"]
			ret_dic["essential"] = ["domain_id", "domain"]
			ret_dic["orderby"] = ["seed_url_count", "daily_count", "last_visit"]
			if sc_type == "oi":
				ret_dic["select"] += ["url", "mobile_url"]

		elif data_type == "seed":
			ret_dic["table"] = "%sdb.%s_seed_urls"%(sc_type, sc_type)
			ret_dic["select"] = ["seed_id", "domain_id", "seed_guid", "down_url", "seed_name", "seed_type", "anchor_name", "crawl_status", "view_url_count", "no_new_count", "max_no", "last_visit"]
			ret_dic["unique"] = ["seed_id", "seed_guid"]
			ret_dic["where_equal"] = ["seed_id","domain_id", "down_url","seed_type","crawl_status"]
			ret_dic["where_like"] = ["seed_guid_like", "seed_name_like"]
			ret_dic["essential"] = ["domain_id", "seed_guid", "down_url"]
			if sc_type == "oi":
				ret_dic["select"] += ["mobile_url","topic"]
			elif sc_type == "blog":
				ret_dic["select"] += ["generator_code", "blog_id", "link", "author", "total_count", "regdate", "language", "image", "description", "recent_write"]
				ret_dic["where_equal"] += ["generator_code"]
				ret_dic["where_like"] += ["blog_id_like"]

	if ret_dic["table"]:
		return ret_dic
	else:
		return None


def getFieldInfoByDic(input_dic):
	field_info_dic = getFieldInfoDic(input_dic["_service_type"], input_dic["_data_type"], input_dic["sc_type"])
	if not field_info_dic:
		print "Failed to getFieldInfoByDic() : Invalid information"
		return None
	return field_info_dic


def getTableName(service_type, data_type, sc_type=""):
	field_info_dic = getFieldInfoDic(service_type, data_type, sc_type)
	if field_info_dic:
		return field_info_dic["table"]
	return None


def getDataDicByData(data_list, input_dic, ret_dic=None):
	"""
	:param input_dic : _service_type, _data_type, sc_type 지정 필요
	:param data_list : data가 들어있는 list
	"""

	if not ret_dic:
		ret_dic = dict()

	all_fields = getFieldInfoByDic(input_dic)
	all_fields = all_fields["select"]

	if len(all_fields) != len(data_list):
		# Data count doesn't match
		return None

	for i in range(len(all_fields)):
		field = all_fields[i]
		data = data_list[i]
		ret_dic[field] = data

	return ret_dic


def getDataMappingDic(service_type, data_type, field_name):
	ret_dic = dict()

	if service_type == "crawl":

		if data_type == "domain":
			if field_name == "status":
				ret_dic["W"] = "준비중(W)"
				ret_dic["N"] = "크롤대기(N)"
				ret_dic["R"] = "크롤중(R)"

		elif data_type == "seed":
			if field_name == "crawl_status":
				ret_dic["N"] = "허용(N)"
				ret_dic["B"] = "차단(B)"
			elif field_name == "seed_type":
				ret_dic["N"] = "숫자(N)"
				ret_dic["F"] = "피드(F)"
				ret_dic["H"] = "HBASE(H)"

	elif service_type == "pado":

		if data_type == "parser":
			if field_name == "activeYN":
				ret_dic["Y"] = "활성화(Y)"
				ret_dic["T"] = "테스트(T)"
				ret_dic["N"] = "비활성(N)"

			elif field_name == "validationStatus":
				ret_dic["S"] = "성공(S)"
				ret_dic["E"] = "에러(E)"
				ret_dic["N"] = "미체크(N)"

		elif data_type == "testurl":
			if field_name == "errorYN":
				ret_dic["Y"] = "에러(Y)"
				ret_dic["N"] = "정상(N)"

	if ret_dic:
		return ret_dic
	else:
		return None