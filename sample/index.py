#!/usr/bin/env python2.7
#coding:utf-8

import sys
import cgi
import cgitb

# MOON Framework
sys.path.insert(0,"/home/moon/web")
import html
#import dao

cgitb.enable()
form = cgi.FieldStorage()

if __name__ == "__main__":
	
	# 넘어온 PARAM을 Dic으로 받음
	param_dic = html.getParamDic(form, [])

	# 실제 컨텐츠 작성
	sc_list = ["news", "bbs", "know", "oi", "video", "blog"]
	sc_title_list = ["", "뉴스", "BBS", "지식", "오픈인터넷", "비디오", "블로그"]
	title_row = html.getTableTR(sc_title_list, "title")

	content_rows = ""

	content_row = "<td>데이터 관리</td>"
	for sc in sc_list:
		content_row += """<td><a href="/moon/crawl/data_add.py?sc_type=%s">추가/수정</a></td>"""%sc
	content_rows += "<tr>%s</tr>"%content_row

	content_row = "<td>도메인 목록</td>"
	for sc in sc_list:
		content_row += """<td><a href="/moon/crawl/domain_list.py?sc_type=%s">도메인</a></td>"""%sc
	content_rows += "<tr>%s</tr>"%content_row

	content_row = "<td>씨드URL 목록</td>"
	for sc in sc_list:
		content_row += """<td><a href="/moon/crawl/seedurl_list.py?sc_type=%s">씨드</a></td>"""%sc
	content_rows += "<tr>%s</tr>"%content_row

	html_content = """
	<div class="box" >
		<h2>크롤 관리 사이트맵</h2><br/>
		<table>
			%s
			%s
		</table>
	</div>
	"""%(title_row, content_rows)

	site_path = ["MAIN"]
	print "Content-type: text/html; charset=utf-8\n"
	print html.getHead(titleList=site_path, script="", style="")
	print html.getBodyStart(site_path)
	#print str(form)+html_content  # DEBUG용
	print html_content
	print html.getBodyEnd(menu="main")
