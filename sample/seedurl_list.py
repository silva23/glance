#!/usr/bin/python2.7
# coding:utf-8

import urllib2
import sys
import cgi
import cgitb

sys.path.insert(0, "/home/moon/web")
import html
import dao
from common_util import getDateByStamp
from dao_basic import DBUtil
from data_manager import getDataMappingDic, getFieldInfoDic
from config import GENERATORS

cgitb.enable()
form = cgi.FieldStorage()


def getFormHtml(param_dic, dao_util):
	if not dao_util:
		return ""

	type_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "seed_type")
	html_seed_type = "<option value='%s'>%s</option>"%(param_dic["seed_type"], param_dic["seed_type"])
	for seedTypeData in type_dic.items():
		html_seed_type += "<option value='%s'>%s</option>"%seedTypeData

	status_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "crawl_status")
	html_status = "<option value='%s'>%s</option>"%(param_dic["crawl_status"], param_dic["crawl_status"])
	for statusData in status_dic.items():
		html_status += "<option value='%s'>%s</option>"%statusData

	html_generator = "<option value='%s'>%s</option>"%(param_dic["generator"], param_dic["generator"])
	html_generator += "<option value=''>ALL</option>"
	for gen in GENERATORS:
		html_generator += "<option value='%s'>%s</option>"%(gen, gen)

	added_title_tds = ""
	added_input_tds = ""
	if param_dic["sc_type"] == "blog":  # TODO : SC별 검색조건 변경
		added_title_tds = "<td>블로그ID</td>"

		added_input_tds = """
		<td><input type='text' name='blog_id_like' class='textInput' value='%s' style="width:100px;"/></td>
		"""%param_dic["blog_id_like"]

	html = """
	<table>
		<tr style='color:red; font-weight:800;'>
			<td>씨드ID</td> <td>도메인ID</td> <td>씨드타입</td> <td>크롤상태</td> <td>Generator</td> <td>씨드GUID</td> <td>씨드명</td> %s <td></td>
		</tr>
		<tr>
			<td><input type='text' name='seed_id' class='textInput' value='%s' style='width:50px;' /></td>
			<td><input type='text' name='domain_id' class='textInput' value='%s' style='width:50px;' /></td>
			<td><select form='search_form' name='seed_type'>%s</select></td>
			<td><select form='search_form' name='crawl_status'>%s</select></td>
			<td><select form='search_form' name='generator'>%s</select></td>
			<td><input type='text' name='seed_guid_like' class='textInput' value='%s' style="width:100px;"/></td>
			<td><input type='text' name='seed_name_like' class='textInput' value='%s' style="width:100px;"/></td>
			%s
			<td><input type="submit" value="조회" style=" width:60px; font-weight:800; color:red;"/></td>
		</tr>
	</table>
	"""%(added_title_tds, param_dic["seed_id"], param_dic["domain_id"], html_seed_type, html_status, html_generator, param_dic["seed_guid_like"], param_dic["seed_name_like"], added_input_tds)

	return html


def getTableAndPagingHtml(param_dic, dao_util):
	if not dao_util:
		return "", ""
	if not param_dic["sc_type"]:
		return "", ""

	if param_dic["page"]:
		page = int(param_dic["page"])
		param_dic["page"] = page+1
		next_page_get_params = html.getGetParamStr(param_dic)
		param_dic["page"] = page-1
		if (page-1) < 1:
			param_dic["page"] = 1
		prev_page_get_params = html.getGetParamStr(param_dic)
		param_dic["page"] = page
	else:
		param_dic["page"] = 2
		next_page_get_params = html.getGetParamStr(param_dic)
		param_dic["page"] = 1
		prev_page_get_params = html.getGetParamStr(param_dic)

	data_list = dao_util.getDataList(param_dic, "web")
	sc_type = param_dic["sc_type"]

	col_name_list = ["ID", "씨드 URL 정보", "씨드명<br/>(앵커값)", "Generator", "크롤상태<br/>(씨드타입)",
					"view URL수<br/><b style='color:#6495ed;'>신규URL미발견 횟수</b><br/>INT_ID최대값","마지막 방문일"]

	if sc_type == "blog":  # TODO : SC별 TITLE 행 표시 데이터 CUSTOMIZING
		col_name_list[2] = "채널명<br/>(작성자)"  # 씨드명 대신 표시
		col_name_list[3] = "Generator<br/>(블로그ID)"  # 앵커TEXT 대신 표시
		#col_name_list.append("새컬럼명")

	col_name_list.append("관리")
	html_title_row = html.getTableTR(col_name_list, "title")
	html_table = "<table style='table-layout:fixed; word-break:break-all;'>"+html_title_row

	# MAKE TABLE HTML
	type_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "seed_type")
	status_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "crawl_status")
	row_count = 0
	for row_data in data_list:
		seed_id = row_data[0]
		domain_id = row_data[1]
		seed_guid = row_data[2]
		down_url = row_data[3]
		seed_name = "<b>%s</b>"%row_data[4]
		seed_type = row_data[5]
		if seed_type:
			seed_type = type_dic[seed_type]
		anchor_name = row_data[6]
		if anchor_name:
			seed_name = "%s<br/>(%s)"%(seed_name, anchor_name)
		crawl_status = row_data[7]
		status = ""
		if crawl_status:
			status = "크롤:<b>%s</b>"%status_dic[crawl_status]
		status += "<br/>타입:<b>%s</b>"%seed_type
		view_url_count = "뷰수:<b>%s</b><br/>"%row_data[8]
		no_new_count = "<b style='color:blue;'>노뉴:%s</b><br/>"%row_data[9]
		max_no = "맥스:<b>%s</b>"%row_data[10]
		numbers = view_url_count + no_new_count + max_no
		last_visit = row_data[11]
		generator = row_data[12]

		if last_visit:
			last_visit = getDateByStamp(last_visit)
		else:
			last_visit = ""

		# TODO : SC별 데이터 행 표시 데이터 CUSTOMIZING
		added_url_info = ""
		added_td_tags = ""
		if sc_type == "blog":
			if row_data[13]:  # 블로그 ID
				generator += "<br/>(%s)"%row_data[13]

			if row_data[14]:  # 블로그 링크
				added_url_info = "<a href='%s' target='_blank'>해당 블로그 바로가기</a><br/>"%row_data[14]

			if row_data[15]:  # author
				seed_name += "<br/>작성자 : %s"%row_data[15]

		# ROW 데이터 표시 시작
		if (row_count % 2) == 0:
			html_row = "<tr class='evenRow'>"
		else:
			html_row = "<tr>"

		html_row += """
			<td>%s</td>
			<td>
				<b style="color:red;">%s</b><br/>
				%s
				도메인ID:<a href="/moon/domanager/domain_add.py?domainIdx=%s">%s</a>&nbsp;&nbsp;
				<a href="%s" target="_blank" >다운로드URL</a>
			</td>
			"""%(seed_id, seed_guid.replace("&", "&amp;"), added_url_info, domain_id, domain_id, down_url)+"""
			<td>%s</td>
			<td>%s</td>
			<td>%s</td>
			<td>%s</td>
			<td>%s</td>
			"""%(seed_name, generator, status, numbers, last_visit) + added_td_tags +"""
			<td>
				<a href="/moon/crawl/data_add.py?sc_type=%s&seed_id=%s" target="_blank" >수정</a> &nbsp;|&nbsp;
				<a href="/moon/action/sc_common.py?sc_type=%s&cmd=blockSeed&seed_id=%s" target="_blank" >차단</a> &nbsp;|&nbsp;
				<a href="#" onclick="confirmPopup('%s','/moon/action/sc_common.py?cmd=del&sc_type=%s&_data_type=seed&seed_id=%s')">삭제</a>
				<br/><a href="/moon/action/sc_common.py?cmd=resetNoNewCount&sc_type=%s&seed_id=%s" target="_blank" >신규URL수 초기화</a>
				<br/><a href="/moon/domanager/pattern_add.py?urlTypeTestUrl=%s" target="_blank">URL추출 TEST</a>
			</td>
		</tr>
		"""%(sc_type, seed_id, sc_type, seed_id, seed_guid, sc_type, seed_id, sc_type, seed_id, urllib2.quote(down_url))
		html_table += html_row
		row_count += 1

	# MAKE PAGING HTML
	total_count = dao_util.getDataList(param_dic, "count")
	total_page = total_count / 15
	if (total_count % 15) > 0:
		total_page += 1
	html_paging = """
	<p>
		<b>페이지 [%s/%s]</b> : <b style='color:red;'>%s</b> 개중 <b style='color:red;'>%s</b> 개 표시
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href='/moon/crawl/seedurl_list.py?%s#tableDiv'>&lt;&lt;이전</a> || <a href='/moon/crawl/seedurl_list.py?%s#tableDiv'>다음&gt;&gt;</a>
	</p>
	"""%( param_dic["page"], total_page, total_count, len(data_list), prev_page_get_params, next_page_get_params)

	return html_table+"</table>", html_paging


if __name__ == "__main__":
	sc_type = ""
	if form.has_key("sc_type"):
		sc_type = form.getvalue("sc_type")
	field_info_dic = getFieldInfoDic("crawl", "seed", sc_type)
	check_params = ["seed_id","orderby", "ordering", "sc_type", "page"] + field_info_dic["where_equal"] + field_info_dic["where_like"]
	param_dic = html.getParamDic(form, check_params)
	param_dic["_service_type"] = "crawl"
	param_dic["_data_type"] = "seed"

	dao_util = None
	if param_dic["sc_type"]:
		sc_type = param_dic["sc_type"]
		dao_util = DBUtil()

	html_search_form = getFormHtml(param_dic, dao_util)
	html_table, html_paging = getTableAndPagingHtml(param_dic, dao_util)

	html_content = """
	<div class="box" >
		<h2>%s SC 씨드 검색</h2>
		<form id='search_form' action='/moon/crawl/seedurl_list.py' method='post'>
			<input type="hidden" name="sc_type" value="%s" />
	"""%(sc_type.upper(), sc_type) + html_search_form +"""
		</form>
	</div>

	<div class="box" id="tableDiv">
		<h2>씨드 정보 조회 결과</h2><br/>
		<p>
			<b>- 신규URL 미발견 횟수</b> : list_top을 방문하였을때 크롤러가 신규URL을 하나도 발견하지 못했을때 1씩 증가한다. (값이 높을수록 재방문 주기가 늘어남.)<br/>
			<b>- 씨드타입</b> : 크롤 시 다음페이지를 방문하기위한 로직을 정의하며, <span style='color:red;font-weight:800;'>값이 변경되었을때 '신규URL 미발견 횟수'가 초기화</span>된다.<br/>
			&nbsp;&nbsp;<b>* F (= FEED)</b> : list패턴을 잡기 힘든 경우에 대용으로 RSS FEED URL을 사용하는 방식<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LINK의 추출 방식이 XML Base이기 때문에 XML 파서가 사용된다.<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;페이징을 통한 전체크롤을 할수 없다는 단점이 있다.<br/>
			&nbsp;&nbsp;<b>* H (= HBASE)</b> : 각 URL의 크롤 여부를 Hbase를 직접 조회하여 일일히 확인하여 체크하는 방식<br/>
			&nbsp;&nbsp;<b>* N (= NUMBER)</b> : URL의 INT_ID값이 글의 날짜(최신성)과 비례하여 증가함을 확인할 수 있을 경우 사용한다.<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 즉, 현재까지 크롤된 문서중 INT_ID가 가장 높았던 것보다 더 높은 경우 신규URL로 간주할 수 있다.
		</p>
	""" + html_paging + html_table + html_paging +"""
	</div>
	"""

	js_popup = html.getJSConfirmPopup("%s 도메인 ["%sc_type.upper(), "] 과 Seed URL 데이터를 모두 삭제하시겠습니까?")
	js_popup += html.getJSConfirmPopup("%s 도메인 ["%sc_type.upper(), "] 의 Seed URL 데이터를 모두 삭제하시겠습니까?","confirmPopup2")
	site_path_list = ["%s SC"%sc_type.upper(), "%s 도메인 리스트"%sc_type.upper()]

	print "Content-type: text/html; charset=utf-8\n"
	print html.getHead(site_path_list, js_popup)
	print html.getBodyStart(site_path_list)
	print html_content
	print html.getBodyEnd(sc_type)