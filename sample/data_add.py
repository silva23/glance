#!/usr/bin/python2.7
# coding:utf-8

import sys
import cgi
import cgitb

sys.path.insert(0, "/home/moon/web")
import html
import dao
from data_manager import getFieldInfoDic, getDataMappingDic
from dao_basic import DBUtil
from common_util import getDateByStamp

cgitb.enable()
form = cgi.FieldStorage()


def getDataDic(load_data, total_fields):
	ret_dic = dict()

	if load_data:
		if len(load_data) == 1:
			load_data = load_data[0]
			for i in range(len(total_fields)):
				if load_data[i]:
					ret_dic[total_fields[i]] = load_data[i]
				else:
					ret_dic[total_fields[i]] = ""

	if not ret_dic:
		for i in range(len(total_fields)):
			ret_dic[total_fields[i]] = ""

	return ret_dic


def getDomainAddForm(param_dic, dao_util):
	sc_type = param_dic["sc_type"]
	field_info_dic = getFieldInfoDic("crawl", "domain", sc_type)
	param_dic["_data_type"] = "domain"
	load_data = dao_util.getDataList(param_dic, "unique")
	data_dic = getDataDic(load_data, field_info_dic["select"])
	param_dic["domain_id"] = data_dic["domain_id"]

	status_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "status")
	html_status = "<option value='%s'>%s</option>"%(data_dic["status"], data_dic["status"])
	for statusData in status_dic.items():
		html_status += "<option value='%s'>%s</option>"%statusData

	added_form = ""
	if sc_type == "oi":
		added_form += """
		<tr> <td>URL</td> <td><input type='text' name='url' class='textInput' value='%s' style="width:200px;"/></td> </tr>
		<tr> <td>모바일URL</td> <td><input type='text' name='mobile_url' class='textInput' value='%s' style="width:200px;"/></td> </tr>
		"""%(data_dic["url"],data_dic["mobile_url"])

	html_form = """
	<form id='domainAddForm' action='/moon/action/sc_common.py' method='post'>
		<input type='hidden' name='cmd' value='addUpdate' />
		<input type='hidden' name='_data_type' value='domain' />
		<input type='hidden' name='sc_type' value='%s' />
		<input type='hidden' name='domain_id' value='%s' />
		<input type='hidden' name='server' value='%s' />
		<input type='hidden' name='pid' value='%s' />
		<input type='hidden' name='seed_url_count' value='%s' />
		<input type='hidden' name='daily_count' value='%s' />
		<input type='hidden' name='last_visit' value='%s' />
		<input type='hidden' name='crawl_count_history' value='%s' />
	"""%(sc_type, data_dic["domain_id"],data_dic["server"],data_dic["pid"],data_dic["seed_url_count"],data_dic["daily_count"], data_dic["last_visit"], data_dic["crawl_count_history"])+"""
		<table>
			<tr style='color:red; font-weight:800;'>
				<td>도메인ID</td> <td>서버</td> <td>PID</td>
				<td>씨드URL수</td> <td>신규URL수</td> <td>마지막방문</td>
			</tr>
			<tr>
				<td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td>
			</tr>
		</table>
	"""%(data_dic["domain_id"], data_dic["server"],data_dic["pid"],data_dic["seed_url_count"],data_dic["daily_count"],data_dic["last_visit"])+"""
		<table class="formTable">
			<tr> <td>도메인</td> <td><input type='text' name='domain' class='textInput' value='%s' style="width:200px;"/></td> </tr>
			<tr> <td>사이트명</td> <td><input type='text' name='name' class='textInput' value='%s' style="width:200px;"/></td> </tr>
			<tr> <td>메모</td> <td><input type='text' name='memo' class='textInput' value='%s' style="width:200px;"/></td> </tr>
			<tr> <td>크롤상태</td> <td><select form='domainAddForm' name='status'>%s</select></td>
			<tr> <td>최근크롤현황</td> <td>%s</td> </tr>
			%s
		</table>
		<input type="submit" value="등록 (혹은 수정)" style=" width:420px; font-weight:800; color:red;"/>
	</form>
	"""%(data_dic["domain"], data_dic["name"], data_dic["memo"], html_status, data_dic["crawl_count_history"], added_form)

	return html_form

def getSeedAddForm(param_dic, dao_util):
	sc_type = param_dic["sc_type"]
	field_info_dic = getFieldInfoDic("crawl", "seed", sc_type)
	param_dic["_data_type"] = "seed"
	load_data = dao_util.getDataList(param_dic, "unique")
	data_dic = getDataDic(load_data, field_info_dic["select"])
	if data_dic["domain_id"]:
		param_dic["domain_id"] = data_dic["domain_id"]
	elif param_dic["domain_id"]:
		data_dic["domain_id"] = param_dic["domain_id"]

	seed_type_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "seed_type")
	html_seed_type = "<option value='%s'>%s</option>"%(data_dic["seed_type"], data_dic["seed_type"])
	for seedTypeData in seed_type_dic.items():
		html_seed_type += "<option value='%s'>%s</option>"%seedTypeData

	status_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "crawl_status")
	html_status = "<option value='%s'>%s</option>"%(data_dic["crawl_status"], data_dic["crawl_status"])
	for statusData in status_dic.items():
		html_status += "<option value='%s'>%s</option>"%statusData

	added_form = ""
	if sc_type == "oi":
		added_form += """
		<tr> <td>모바일URL</td> <td><input type='text' name='mobile_url' class='textInput' value='%s' style="width:200px;"/></td> </tr>
		<tr> <td>토픽</td> <td><input type='text' name='topic' class='textInput' value='%s' style="width:200px;"/></td> </tr>
		"""%(data_dic["mobile_url"], data_dic["topic"])
	elif sc_type == "blog":
		added_form += """
		<tr> <td>블로그 ID</td> <td><input type='text' name='blog_id' class='textInput' value='%s' style="width:100px;"/></td> </tr>
		<tr> <td>블로그 링크</td> <td><input type='text' name='link' class='textInput' value='%s' style="width:200px;"/></td> </tr>
		"""%(data_dic["blog_id"], data_dic["link"])

	html_form = """
	<form id='seedAddForm' action='/moon/action/sc_common.py' method='post'>
		<input type='hidden' name='cmd' value='addUpdate' />
		<input type='hidden' name='_data_type' value='seed' />
		<input type='hidden' name='sc_type' value='%s' />
		<input type='hidden' name='seed_id' value='%s' />
		<input type='hidden' name='domain_id' value='%s' />
	"""%(sc_type, data_dic["seed_id"], data_dic["domain_id"])+"""
		<table>
			<tr style='color:red; font-weight:800;'>
				<td>씨드ID</td> <td>도메인ID</td> <td>뷰URL 수</td>
				<td>신규URL<br/>미발견 횟수</td> <td>마지막방문</td> <td>Generator</td>
			</tr>
			<tr>
				<td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td>
			</tr>
		</table>
	"""%(data_dic["seed_id"],data_dic["domain_id"],data_dic["view_url_count"],data_dic["no_new_count"],getDateByStamp(data_dic["last_visit"]),data_dic["generator"])+"""
		<table class="formTable">
			<tr> <td>씨드URL GUID</td> <td><input type='text' name='seed_guid' class='textInput' value='%s' style="width:300px;"/></td> </tr>
			<tr> <td>다운로드URL</td> <td><input type='text' name='down_url' class='textInput' value='%s' style="width:300px;"/></td> </tr>
			<tr> <td>씨드명</td> <td><input type='text' name='seed_name' class='textInput' value='%s' style="width:200px;"/></td> </tr>
			<tr> <td>INT_ID최대값</td> <td><input type='text' name='max_no' class='textInput' value='%s' style="width:200px;"/></td> </tr>
			<tr> <td>씨드타입</td> <td><select form='seedAddForm' name='seed_type'>%s</select> 기본값 : 숫자(N)</td>
			<tr> <td>크롤상태</td> <td><select form='seedAddForm' name='crawl_status'>%s</select> 기본값 : 준비중(W)</td>
			%s
		</table>
		<input type="submit" value="등록 (혹은 수정)" style=" width:420px; font-weight:800; color:red;"/>
	</form>
	"""%(data_dic["seed_guid"], data_dic["down_url"], data_dic["seed_name"], data_dic["max_no"], html_seed_type, html_status, added_form)

	return html_form


if __name__ == "__main__":
	check_params = ["domain_id","domain","seed_id","sc_type"]
	param_dic = html.getParamDic(form, check_params)
	param_dic["_service_type"] = "crawl"

	sc_type = ""
	dao_util = None
	if param_dic["sc_type"]:
		sc_type = param_dic["sc_type"]
		dao_util = DBUtil()

	#
	html_seed_add_form = getSeedAddForm(param_dic, dao_util)
	html_domain_add_form = getDomainAddForm(param_dic, dao_util)

	# HTML PRINT
	html_content = """
		<div class="box" >
			<h2>%s 도메인 정보 등록</h2><br/>
			<p>
			- 도메인ID : 전체도메인 테이블의 ID 자동입력(전체 테이블에 정보가 없으면 등록불가능)<br/>
			</p>
	"""%sc_type.upper() + html_domain_add_form+"""
		</div>
		<div class="box" >
			<h2>%s SEED URL 정보 등록</h2><br/>
			<p>
			- 필수입력 : Domain ID, Download URL<br/>
			- GUID : Download URL로 GUID 자동추출(실패 시, Download URL과 동일하게 입력)
			</p>
	"""%sc_type.upper() + html_seed_add_form+"""
		</div>
	"""

	sitePathList = ["%s SC"%sc_type.upper(),"%s DATA 등록/수정"%sc_type.upper()]
	print "Content-type: text/html; charset=utf-8\n"
	print html.getHead(sitePathList)
	print html.getBodyStart(sitePathList)
	print html_content
	print html.getBodyEnd(sc_type)