#!/usr/bin/python2.7
# coding:utf-8

import sys
import cgi
import cgitb

sys.path.insert(0, "/home/moon/web")
import html
from common_util import getDateByStamp
from dao_basic import DBUtil
from dao_action import updateSeedURLCount
from data_manager import getDataMappingDic

cgitb.enable()
form = cgi.FieldStorage()


def getFormHtml(param_dic, dao_util):
	if not dao_util:
		return ""

	status_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "status")

	html_status = "<option value='%s'>%s</option>"%(param_dic["status"], param_dic["status"])
	for statusData in status_dic.items():
		html_status += "<option value='%s'>%s</option>"%statusData

	html_statistics = "<option value='%s'>%s</option>"%(param_dic["crawl_count_history_notlike"], param_dic["crawl_count_history_notlike"])
	html_statistics += "<option value=''>정상</option>"
	html_statistics += "<option value='정상'>비정상</option>"

	html_form = """
	<table>
		<tr style='color:red; font-weight:800;'>
			<td>도메인ID</td> <td>크롤상태</td> <td>통계정보</td> <td>사이트명</td> <td>도메인</td> <td></td>
		</tr>
		<tr>
			<td><input type='text' name='domain_id' class='textInput' value='%s' style='width:50px;' /></td>
			<td><select form='search_form' name='status'>%s</select></td>
			<td><select form='search_form' name='crawl_count_history_notlike'>%s</select></td>
			<td><input type='text' name='name_like' class='textInput' value='%s' style="width:100px;"/></td>
			<td><input type='text' name='domain_like' class='textInput' value='%s' style='width:150px;' /></td>
			<td><input type="submit" value="조회" style=" width:60px; font-weight:800; color:red;"/></td>
		</tr>
	</table>
	"""%(param_dic["domain_id"], html_status, html_statistics, param_dic["name_like"], param_dic["domain_like"])

	return html_form


def getTableAndPagingHtml(param_dic, dao_util):
	if not dao_util:
		return "", ""
	if not param_dic["sc_type"]:
		return "", ""

	if param_dic["page"]:
		page = int(param_dic["page"])
		param_dic["page"] = page+1
		next_page_get_params = html.getGetParamStr(param_dic)
		param_dic["page"] = page-1
		if (page-1) < 1:
			param_dic["page"] = 1
		prev_page_get_params = html.getGetParamStr(param_dic)
		param_dic["page"] = page
	else:
		param_dic["page"] = 2
		next_page_get_params = html.getGetParamStr(param_dic)
		param_dic["page"] = 1
		prev_page_get_params = html.getGetParamStr(param_dic)

	sc_type = param_dic["sc_type"]
	status_dic = getDataMappingDic(param_dic["_service_type"], param_dic["_data_type"], "status")
	data_list = dao_util.getDataList(param_dic, "web")

	col_name_list = ["도메인ID<br/>(클릭 시 수정)", "도메인", "사이트명<br/>(메모)", "상태", "서버", "PID",
					 "씨드URL수%s"%(html.getOrderingLinkHtml("/moon/crawl/domain_list.py", param_dic, "seed_url_count")),
					 "신규URL수%s<br/>(최근7일 현황)"%(html.getOrderingLinkHtml("/moon/crawl/domain_list.py", param_dic, "daily_count")),
					 "마지막 크롤일%s"%(html.getOrderingLinkHtml("/moon/crawl/domain_list.py", param_dic, "last_visit"))]

	if param_dic["sc_type"] == "user":  # TODO SC별 테이블 CUSTOMIZING
		col_name_list.append("새컬럼명")
	col_name_list.append("관리")

	html_title_row = html.getTableTR(col_name_list, "title")
	html_table = "<table style='table-layout:fixed; word-break:break-all;'>"+html_title_row
	row_count = 0

	# MAKE TABLE HTML
	for row_data in data_list:
		domain_id = row_data[0]
		domain = row_data[1]
		name = row_data[2]
		memo = row_data[3]
		if memo:
			name = "%s<br/>(%s)"%(name, memo)
		status = row_data[4]
		if status:
			status = status_dic[status]
		server = row_data[5]
		pid = row_data[6]
		seed_url_count = row_data[7]
		daily_count = row_data[8]
		crawl_count_history = row_data[9]
		last_visit = row_data[10]
		if last_visit:
			last_visit = getDateByStamp(last_visit)
		else:
			last_visit = ""

		added_td_tags = ""
		if sc_type == "user":  # TODO 표시 데이터 SC별 CUSTOMIZING
			added_td_tags += "<td>%s</td>"%row_data[11]

		if (row_count % 2) == 0:
			html_row = "<tr class='evenRow'>"
		else:
			html_row = "<tr>"

		document_view_link = "http://topicplus.zuminternet.com/recent_documents.py?sc=%s&channelId=%s"%(sc_type, domain_id)
		if sc_type == "bbs":
			document_view_link = "http://topicplus.zuminternet.com/recent_documents.py?sc=%s&siteId=%s"%(sc_type, domain_id)

		html_row += """
			<td><a href="/moon/domanager/domain_add.py?domainIdx=%s" target="_blank">%s</a></td>
			<td><a href="%s" target="_blank">%s</a><br/>
				<a href="/moon/domanager/urlpt_list.py?domainLike=%s" target='_blank'>URL패턴</a> |
				<a href="/moon/crawl/seedurl_list.py?sc_type=%s&domain_id=%s" target='_blank'>씨드URL</a><br/>
				<a href="/moon/domanager/sitemapurl_list.py?domain_id=%s" target='_blank'>사이트맵URL</a>
			</td>
			"""%(domain_id, domain_id, "http://"+domain, domain,  domain, sc_type, domain_id, domain_id)+"""
			<td>%s</td>
			<td>%s</td>
			<td>%s</td>
			<td>%s</td>
			<td>%s</td>
			<td>
				<a href="%s" target="_blank" >%s</a>
				<br/>%s
			</td>
			<td>%s</td>
			"""%(name, status, server, pid, seed_url_count, document_view_link, daily_count, crawl_count_history, last_visit)+ added_td_tags +"""
			<td>
				<a href="/moon/crawl/data_add.py?sc_type=%s&domain_id=%s" target="_blank" >수정</a> &nbsp;|&nbsp;
				<a href="#" onclick="confirmPopup('%s','/moon/action/sc_common.py?cmd=del&sc_type=%s&_data_type=domain&domain_id=%s')">삭제</a><br/>
				<a href="/moon/crawl/data_add.py?sc_type=%s&domain_id=%s#seedAddForm" target="_blank" >씨드등록</a> &nbsp;|&nbsp;
				<a href="#" onclick="confirmPopup2('%s','/moon/action/sc_common.py?cmd=del&sc_type=%s&_data_type=seed&domain_id=%s')">씨드삭제</a><br/>
				<a href="/moon/action/sc_common.py?cmd=resetNoNewCount&sc_type=%s&domain_id=%s" target="_blank" >씨드전체 신규문서<br/>미발견 횟수 초기화</a>
			</td>
		</tr>
		"""%(sc_type, domain_id, domain, sc_type, domain_id, sc_type, domain_id, domain, sc_type, domain_id, sc_type, domain_id)
		html_table += html_row
		row_count += 1

	# MAKE PAGING HTML
	total_count = dao_util.getDataList(param_dic, "count")
	total_page = total_count / 15
	if (total_count % 15) > 0:
		total_page += 1
	html_paging = """
	<p>
		<b>페이지 [%s/%s]</b> : <b style='color:red;'>%s</b> 개중 <b style='color:red;'>%s</b> 개 표시
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href='/moon/crawl/domain_list.py?%s#tableDiv'>&lt;&lt;이전</a> || <a href='/moon/crawl/domain_list.py?%s#tableDiv'>다음&gt;&gt;</a>
	</p>
	"""%( param_dic["page"], total_page, total_count, len(data_list), prev_page_get_params, next_page_get_params)

	return html_table+"</table>", html_paging


if __name__ == "__main__":
	check_params = ["domain_id", "domain_like", "name_like", "crawl_count_history_notlike", "status", "orderby", "ordering", "sc_type", "cmd", "page"]
	param_dic = html.getParamDic(form, check_params)
	param_dic["_service_type"] = "crawl"
	param_dic["_data_type"] = "domain"

	sc_type = ""
	dao_util = None
	if param_dic["sc_type"]:
		sc_type = param_dic["sc_type"]
		dao_util = DBUtil()

	if form.has_key("cmd"):
		cmd = form.getvalue("cmd")
		if cmd == "updateSeedURLCount":
			updateSeedURLCount(param_dic)

	html_search_form = getFormHtml(param_dic, dao_util)
	html_table, html_paging = getTableAndPagingHtml(param_dic, dao_util)

	html_content = """
	<div class="box" >
		<h2>%s SC 도메인 검색</h2>
		<form id='search_form' action='/moon/crawl/domain_list.py' method='post'>
			<input type="hidden" name="sc_type" value="%s" />
	"""%(sc_type.upper(), sc_type) + html_search_form +"""
		</form>
	</div>

	<div class="box" id="tableDiv">
		<h2>도메인 정보 조회 결과</h2><br/>
		<p>
			<a href='/moon/crawl/domain_list.py?sc_type=%s&cmd=updateSeedURLCount'>SeedURL 개수 업데이트</a><br/>
		</p>
	"""%(sc_type) + html_paging + html_table + html_paging +"""
	</div>
	"""

	js_popup = html.getJSConfirmPopup("%s 도메인 ["%sc_type.upper(), "] 과 Seed URL 데이터를 모두 삭제하시겠습니까?")
	js_popup += html.getJSConfirmPopup("%s 도메인 ["%sc_type.upper(), "] 의 Seed URL 데이터를 모두 삭제하시겠습니까?","confirmPopup2")
	site_path_list = ["%s SC"%sc_type.upper(), "%s 도메인 리스트"%sc_type.upper()]

	print "Content-type: text/html; charset=utf-8\n"
	print html.getHead(site_path_list, js_popup)
	print html.getBodyStart(site_path_list)
	print html_content
	print html.getBodyEnd(sc_type)