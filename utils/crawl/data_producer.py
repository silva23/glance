#/usr/bin/env python
# coding: utf8

# type 이 SC를 정하니 그때그때 맞춰서 프린트 하는걸로.
# Dir 없으면 생성도 해주셈 (입력한 Path자체를 무조건 가능하게)

import os
from datetime import datetime

from utils.log import getLogger

"""
Module to make XML file

Made by. shiwoo park (silva23@naver.com)
Created on 2015. 3. 23.
"""

class XMLPrinter():
	''' Make XML file by input parse result dictionary
		SC category decides XML Fields
	'''

	def __init__(self, _outputDirPath=os.getcwd(), _documentCountLimit=1000):
		self.fieldListDic = dict()
		self.outputDirPath = _outputDirPath
		self.documentCountLimit = _documentCountLimit
		self.logger = getLogger()
		self.setupOutputDir()
		self.initSCFieldListDic()

	def initSCFieldListDic(self):
		self.fieldListDic["NEWS"] =["mode","type","guid","createTime","crawlTime","channelIdentifier","channelName","webLink","mobileLink","title","subTitle", "body","bodyHtml","imageThumbnail78x78", "imageThumbnail126x126", "imageThumbnailSignature","imageCount","videoCount","authorName","authorEmail", "sourceType"] 
		self.fieldListDic["OI"] =["mode","type","guid","createTime","crawlTime","channelIdentifier","channelIdentifierMobile","channelName","siteIdentifier","siteIdentifierMobile","siteName", "webLink","mobileLink","title","subTitle", "body","bodyHtml","imageList", "imageThumbnailList160x78", "imageThumbnailList240x126", "imageCount", "documentKeywords", "documentCategory"]
		self.fieldListDic["KNOW"] =["mode","type","guid","createTime","crawlTime","channelIdentifier","channelName","webLink","mobileLink","title","body","bodyHtml", "documentCategoryName", "imageCount", "answerCount", "answerRecommendCount", "isSolved", "linkCount", "readCount", "recommendCount"] 
		self.fieldListDic["BLOG_POST"] = ["mode", "type", "guid", "registrationTime", "crawlTime", "channelIdentifier", "createTime", "updateTime", "siteIdentifier", "siteName", "channelTitle", "channelLink", "author", "image", "description", "language", "generator", "sourceType"]
		self.fieldListDic["BLOG_CHANNEL"] = ["mode","type","guid","registrationTime","crawlTime","channelIdentifier","createTime","updateTime","siteIdentifier","siteName","channelTitle","channelLink","author","image","description","language","generator","sourceType"]
		self.fieldListDic["BBS"] = ["mode","type","guid","createTime","crawlTime","channelIdentifier","channelName","url","title","body","authorName","readCount","bodyHtml", "replyCount","videoCount", "imageCount"]
		self.fieldListDic["BBS_OLD"] = ["mode","type","guid","registrationTime","write_time","visit_time","site_id","bbs_id","url","alt_title","org_title","body","first_visit","read_count","reply_count","withVideo","is_dead"]

	def setupOutputDir(self):
		if not os.path.exists(self.outputDirPath) :
			try:
				os.makedirs(self.outputDirPath)
				self.logger.info("Made Dir :%s"%self.outputDirPath)
			except Exception,msg:
				self.logger.error(msg)

	def getXMLStr(self, document_data):

		dataDic = makeOutputDict(document_data)
		keyList = None
		if "type" in dataDic :
			scType = dataDic["type"]
			if scType in self.fieldListDic :
				keyList = self.fieldListDic[scType]
		ret_xml = ""
		contents_xml = ""
		if keyList : # 필드명 지정
			for field in keyList : 
				if field in dataDic :
					contents_xml += "		<%s><![CDATA[%s]]></%s>\n"%(field, dataDic[field], field)
		else : # 필드명 미지정(모두출력)
			for key,val in dataDic.items() :
				contents_xml += "		<%s><![CDATA[%s]]></%s>\n"%(key, val, key)

		if len(contents_xml) > 0:
			ret_xml = "	<DOCUMENT>\n"
			ret_xml += contents_xml
			ret_xml += "	</DOCUMENT>\n"

		return ret_xml


	def printXML(self, dataDicList, fileName=None, type=None):
		if not fileName :
			nowDate = datetime.now()
			fileName = nowDate.strftime('document_%Y%m%d_%H%M%S')+"_%s"%os.getpid()

		counter = 0
		xmlFile = None
		dataCount = len(dataDicList)
		fileName = self.outputDirPath +"/"+ fileName
		fileName = fileName.replace("//","/")
		
		for i in range(0, dataCount) :
			try:
				document_data = dataDicList[i]
				dataDic = makeOutputDict(document_data)
				if (i % self.documentCountLimit) == 0 : # Data limit 도달
					if xmlFile :
						xmlFile.write("</DOCUMENTS>")
						xmlFile.close()
						self.logger.info( "Write [%s] finished"%xmlFile.name)
						xmlFile = None

					if counter == 0 :
						xmlFile = open(fileName+".xml",'w')
						xmlFile.write("<DOCUMENTS>\n")
					else :
						xmlFile = open(fileName+"_%s.xml"%counter,'w')
						xmlFile.write("<DOCUMENTS>\n")

					counter += 1
				if type == "test":
					self.writeEachDocumentData(xmlFile, dataDic, type)
				else:
					xmlFile.write("	<DOCUMENT>\n")
					self.writeEachDocumentData(xmlFile, dataDic, type)
					xmlFile.write("	</DOCUMENT>\n")
			except Exception, msg:
				self.logger.error(msg)
		if xmlFile :
			xmlFile.write("</DOCUMENTS>")
			xmlFile.close()
			self.logger.info("Write [%s] finished"%xmlFile.name)
		

	def writeEachDocumentData(self, f, dataDic, type):

		try:
			if type == "test":
				f.write("%s\t%s\t%s\n"%(dataDic["guid"], dataDic["title"], dataDic["bodyHtml"]))
			else:
				keyList = None
				if "type" in dataDic :
					scType = dataDic["type"]
					if scType in self.fieldListDic :
						keyList = self.fieldListDic[scType]

				if keyList : # 필드명 지정
					for field in keyList : 
						if field in dataDic and dataDic[field] and dataDic[field] != "":
							f.write("		<%s><![CDATA[%s]]></%s>\n"%(field, dataDic[field], field))
				else : # 필드명 미지정(모두출력)
					for key,val in dataDic.items() :
						f.write("		<%s><![CDATA[%s]]></%s>\n"%(key, val, key))
		except Exception, msg:
			getLogger().error("%s %s FILE WRITE ERROR", msg, dataDic)


def checkDead(title):
	if title.find("삭제된 게시글 입니다") >= 0:
		return True
	return False


def makeOutputDict(document_data):

	try:

		if document_data.parsing_result:
			result_dict = document_data.parsing_result
		else:
			result_dict = dict()

		result_dict["mode"] = document_data.mode
		result_dict["type"] = document_data.type
		result_dict["guid"] = document_data.guid
		result_dict["crawlTime"] = document_data.crawl_time
	
		if "body" in result_dict:
			result_dict["body"] = result_dict["body"].replace("]","&#93;")
		if "bodyHtml" in result_dict:
			result_dict["bodyHtml"] = result_dict["bodyHtml"].replace("]","&#93;")
		if "title" in result_dict:
			result_dict["title"] = " ".join(result_dict["title"].replace("]","&#93;").split())

		result_dict["webLink"] = document_data.down_url
		result_dict["mobileLink"] = document_data.mobile_url

		if document_data.type == "NEWS":
			result_dict["channelName"] = document_data.domain_data.name
			result_dict["channelIdentifier"] = document_data.domain_data.url
			result_dict["sourceType"] = 4
			try:
				image_data = document_data.image_data
				if "78x78" in image_data:
					result_dict["imageThumbnail78x78"] = image_data["78x78"]
				if "126x126" in image_data:
					result_dict["imageThumbnail126x126"] = image_data["126x126"]
				if "signature" in image_data:
					result_dict["imageThumbnailSignature"] = image_data["signature"]
			except Exception, msg:
				getLogger().error(msg)
		elif document_data.type == "BBS":
			for int_field in ["readCount","replyCount", "recommendCount", "videoCount", "imageCount"]:
				if int_field not in result_dict:
					result_dict[int_field] = 0
			result_dict["siteName"] = document_data.domain_data.name
			result_dict["siteIdentifier"] = document_data.domain_data.url
			outLinks = list()
			for link in result_dict["bodyLinks"]:
				l_data = result_dict["bodyLinks"][link]
				outLinks.append("%s\t%s"%(link, l_data.text) )
			result_dict["outLinks"] = "\n".join(outLinks)

		elif document_data.type == "KNOW":
			result_dict["channelName"] = document_data.domain_data.name
			result_dict["channelIdentifier"] = document_data.domain_data.url

			if "title" in result_dict:
				if result_dict["title"].find("삭제된 게시글 입니다") >= 0:
					result_dict["mode"] = "D"

			for int_field in ["readCount","answerCount", "recommendCount", "answerRecommendCount" ]:
				if int_field not in result_dict:
					result_dict[int_field] = 0
			if "answer_list" in result_dict:
				for  t_dict in result_dict["answer_list"]:
					if "answer" in t_dict:
						result_dict["body"] += " "+t_dict["answer"].replace("]","&#93;")
					
			if "solved" in result_dict and len(result_dict["solved"]) > 0:
				result_dict["isSolved"] = 1
			else:
				result_dict["isSolved"] = 0

			if "bodyLinks" in result_dict:
				result_dict["linkCount"] = len(result_dict["bodyLinks"])
			else:
				result_dict["linkCount"] = 0

		elif document_data.type == "OI":
			result_dict["channelName"] = document_data.seed_data.name
			result_dict["channelIdentifier"] = document_data.seed_data.url
			result_dict["channelIdentifierMobile"] = document_data.seed_data.mobile_url
			result_dict["documentCategory"] = document_data.seed_data.topic

			result_dict["siteName"] = document_data.domain_data.name
			result_dict["siteIdentifier"] = document_data.domain_data.url
			result_dict["siteIdentifierMobile"] = document_data.domain_data.mobile_url

			image_data = document_data.image_data
			if "160x78" in image_data:
				result_dict["imageThumbnailList160x78"] = image_data["160x78"]
			if "240x126" in image_data:
				result_dict["imageThumbnailList240x126"] = image_data["240x126"]
			if "imageList" in image_data:
				result_dict["imageList"] = image_data["imageList"]

		return result_dict
	except Exception, msg:
		getLogger().error(msg)
		return result_dict

def makeHbaseDict(document_data):

	result_dict = dict()
	
	result_dict["urlinfo:mode"] = document_data.mode
	result_dict["urlinfo:type"] = document_data.type
	result_dict["urlinfo:visit_count"] = str(document_data.visit_count)
	result_dict["urlinfo:down_url"] = document_data.down_url
	result_dict["urlinfo:first_visit"] = str(document_data.first_visit)
	result_dict["urlinfo:last_visit"] = str(document_data.crawl_time)
	result_dict["urlinfo:write_time"] = str(document_data.write_time)
	result_dict["urlinfo:title"] = document_data.title
	result_dict["urlinfo:domain"] = document_data.domain_data.domain
	result_dict["urlinfo:domain_name"] = document_data.domain_data.name
	result_dict["urlinfo:domain_url"] = document_data.domain_data.url
	result_dict["urlinfo:domain_id"] = str(document_data.domain_data.id)

	return result_dict




if __name__ == '__main__':
	# 예제 데이터 생성
	dataList = []
	dic = dict()
	dic["mode"] = "N"
	dic["type"] = "BLOG_POST"
	dic["guid"] = "http://abc.com?a=1&seconed=3"
	dic["registrationTime"] = "1004246518"
	dic["sourceType"] = "lalalala"
	dic["channelIdentifier"] = "blog.naver.com"
	dic["channelType"] = "economy"
	dic["authorEmail"] = "silva23@naver.com"
	dic["name"] = "shiwoo"
	dataList.append(dic)

	dic2 = dict()
	dic2["mode"] = "N"
	dic2["type"] = "NEWS"
	dic2["guid"] = "http://abc.com?a=1&seconed=3"
	dic2["registrationTime"] = "1004246518"
	dic2["sourceType"] = "lalalala"
	dic2["channelIdentifier"] = "blog.naver.com"
	dic2["channelType"] = "economy"
	dic2["authorEmail"] = "silva23@naver.com"
	dic2["name"] = "shiwoo"
	dataList.append(dic2)
	
	dic3 = dict()
	dic3["mode"] = "N"
	dic3["type"] = "NEWS"
	dic3["guid"] = "http://abc.com?a=1&seconed=3"
	dic3["registrationTime"] = "1004246518"
	dataList.append(dic3)

	dic4 = dict()
	dic4["mode"] = "N"
	dic4["type"] = "NEWS"
	dic4["guid"] = "http://abc.com?a=1&seconed=3"
	dic4["registrationTime"] = "1004246518"
	dataList.append(dic4)

	dic5 = dict()
	dic5["mode"] = "N"
	dic5["type"] = "NEWS"
	dic5["guid"] = "http://abc.com?a=1&seconed=3"
	dic5["registrationTime"] = "1004246518"
	dataList.append(dic5)

	
	# 데이터 SC 구분하여 출력
	#dirPath = "C:\\Users\\jsilva\\data_produce_test\\output"
	dirPath = "C:/Users/jsilva/data_produce_test/output"
	dirPath = "/home/moon/tmp/"
	printer = XMLPrinter(dirPath, 2)
	printer.printXML(dataList, "testXmlData")
