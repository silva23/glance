#!/usr/bin/env python
# coding: utf-8
"""
DIAGNOSTICS : Process Inofrmation Diagnostics 

usage :
	pl = ProcessList()
	process_list = pl.getChildByProcName("Dali")
	for process in porcess_list:
		print process["PID"]
		print process["VSS"]
		....

"""
import time
import glob
import socket
import binascii

from basetree import *

PROC_CWD = "/proc/%s/cwd"
PROC_STATUS = "/proc/%s/status"
PROC_CMDLINE = "/proc/%s/cmdline"
PROC_STAT = "/proc/%s/stat"
PROC_FD = "/proc/%s/fd"
PROC_UPTIME = "/proc/uptime"
PROC_NET_TCP = "/proc/net/tcp"
PROC_NET_UNIX = "/proc/net/unix"
ROOT_PROC = "/proc/1"

TWO_SIZE_TOKEN = 2
THREE_SIZE_TOKEN = 3

UTIME_INDEX = 13
STIME_INDEX = 14
START_TIME_INDEX = 21
HZ = 100

FORMAT_DISPLAY = "%(PID)8s %(PPID)8s %(ETIME)13s %(PCPU)5s %(RSS)10s %(VSS)10s %(CMDLINE)s"

SOCKTYPE_TCP = 1
SOCKTYPE_UNIX = 2


class ProcessInfo:
	"""
	Information of One process
	Information:
		PID, PPID, VSS, RSS, PCPU, UID, ETIME, CMD, ARGS
	"""

	def __init__(self, pid):
		"""
		Initialize process information
		Make Dictionary of process information
		"""
		self.PID = pid
		self.PPID = 0
		self.VSS = 0
		self.RSS = 0
		self.PCPU = 0
		self.UID = None
		self.ETIME = None
		self.CMD = None 
		self.ARGS = None
		self.FD = {}
		self.SSOCKETS = []
		self.USOCKETS = []
		self.CSOCKETS = []
		self._makeProcInfo(pid)
		self.DIC_DATA = self._makeDicData()


	def _makeProcInfo(self,pid):
		"""
		Parsing the proc file data
		Calculate some information
		"""
		proc_status_info = self._readProcFile(PROC_STATUS % self.PID)
		proc_stat_info = self._readProcFile(PROC_STAT % self.PID)
		proc_uptime_info = self._readProcFile(PROC_UPTIME)
		proc_cmdline = self._readProcFile(PROC_CMDLINE % self.PID)
		proc_status_info = self._getStatusInfoDict(proc_status_info)
		self.PPID = proc_status_info.get("PPid", "Unknown")
		self.VSS = proc_status_info.get("VmSize", "Unknown")
		self.RSS = proc_status_info.get("VmRSS", "Unknown")
		self.UID = proc_status_info.get("Uid", "Unknown")
		self.FD = self._getFileDescripters()

		process_jiffy = (self._getStatInfo(proc_stat_info, UTIME_INDEX) + self._getStatInfo(proc_stat_info, STIME_INDEX)) 
		total_jiffy = (self._getUptime(proc_uptime_info) - self._getStatInfo(proc_stat_info, START_TIME_INDEX))
		self.PCPU = round((process_jiffy / total_jiffy) * 100, 1) # percent
 		self.ETIME = self._transformEtime(total_jiffy / HZ) # second
		
		self.CMD = self._getCmdLine(proc_cmdline)[:-1]
		if len(self.CMD)>1:
			self.ARGS = self.CMD[1:]


	def _getFileDescripters(self):
		"""
		/proc/{PID}/fd/*의 번호:이름 쌍을 읽어 dict로 만든다.
		"""
		fds = {}
		fddir = PROC_FD % self.PID
		try:
			for fdid in os.listdir(fddir):
				try:
					fds[fdid] = os.path.realpath(os.path.join(fddir, fdid))
				except:
					pass
		except:
			pass
		return fds


	def _transformEtime(self, etime):
		"""
		Transform elapsed time ( second )
		For human being ( dd-HH:MM:SS )
		"""
		dd = int(etime / (24 * 3600))
		hour = time.strftime("%H:%M:%S", time.gmtime(etime))
		if dd > 0:
			trans_etime = "%4d-%s" % (dd, hour)
		else:
			trans_etime = "%s%s" % (" " * 5, hour)
		return trans_etime


	def _makeDicData(self):
		"""
		Make Dictionary process data
		"""
		dic_data = {}
		dic_data["PID"] = self.PID
		dic_data["PPID"] = self.PPID
		dic_data["VSS"] = self.VSS
		dic_data["RSS"] = self.RSS
		dic_data["UID"] = self.UID
		dic_data["ETIME"] = self.ETIME
		dic_data["PCPU"] = self.PCPU
		dic_data["CMD"] = self.CMD
		dic_data["ARGS"] = self.ARGS
		return dic_data


	def _readProcFile(self, proc_path):
		"""
		Read Process File System
		File:
			/proc/[0-9]*/status
			/proc/[0-9]*/stat
			/proc/[0-9]*/cmdline
			/proc/uptime
		"""
		try:
			proc_fd = open(proc_path)
			proc_file_content = proc_fd.read()
			proc_fd.close()
		except:
			#getLogger("error").error("Exception:", exc_info=sys.exc_info())
			proc_file_content = ""
		
		return proc_file_content


	def _getStatusInfoDict(self, proc_status_info, verbose=False):
		"""
		parsing process status file 
		file : /proc/[0-9]*/status
		"""
		proc_status_info = proc_status_info.replace("\r","\n")
		lines = proc_status_info.split("\n")
		dict_items = []
		for line in lines:
			try:
				status_info = line.split(None, 1)
				status_info[0] = status_info[0].strip(":")
				if len(status_info) != 2:
					status_info = [status_info[0], ""]
			except:
				continue
			dict_items.append(tuple(status_info))

		return dict(dict_items)


	def _getStatInfo(self, proc_stat_info, key_index):
		"""
		Parsing process stat file 
		File : /proc/[0-9]*/stat
		"""
		stat_list = proc_stat_info.split()
		try:
			stat_value = float(stat_list[key_index])
		except:
			#getLogger("error").error("Exception:", exc_info=sys.exc_info())
			return 0
		return stat_value


	def _getCmdLine(self, proc_cmdline):
		"""
		Parsing process cmdline file 
		File : /proc/[0-9]*/cmdline
		"""
		return proc_cmdline.split("\x00")


	def _getUptime(self, proc_uptime_info):
		"""
		Parsing process uptime file 
		File : /proc/uptime
		"""
		uptime_list = proc_uptime_info.split()
		uptime = float(uptime_list[0])
		return uptime * HZ
	


class ProcInfoNode(BaseTreeNode):
	"""
	Information tree node for process list
	Use BaseTreeNode class
	"""

	def __init__(self, node_name, parent_name=""):
		"""
		Initialize tree node
		Make ProcessInfo class
		"""
		self.node_name = node_name
		if node_name.isdigit():
			self.pid = node_name
			proc_attr = ProcessInfo(self.pid)
			BaseTreeNode.__init__(self, self.pid, proc_attr.PPID, proc_attr)
		else:
			BaseTreeNode.__init__(self, node_name, parent_name)


	def __str__(self):
		return self.formatString(self.attrs.DIC_DATA)


	def formatString(self, proc_info_dict, fmt=FORMAT_DISPLAY):
		proc_info_dict["CMDLINE"] = " ".join(proc_info_dict["CMD"])	
		return fmt % proc_info_dict
	formatString = classmethod(formatString)



class ProcInfoTree(BaseTree):
	"""
	Process information tree
	Use BaseTree class

	"""
	
	def __init__(self):
		"""
		Initialize Tree
		Delete BaseTree's root for ProcInfoNode's root
		Make process information tree
		"""
		BaseTree.__init__(self, "")
		self.sock_dict = {}
		self.usock_dict = {}
		self.valid = True
		try:
			self.refresh()
		except AssertionError, msg:
			sys.stderr.write("Warning: %s" % msg)
			self.valid = False


	def __str__(self):
		return self.fullProcessText(self.root)


	def fullProcessText(self, pnode):
		if not self.valid:
			return ""
		if pnode is self.root:
			retstr = FORMAT_DISPLAY % {"PID":"PID", "PPID":"PPID", "ETIME":"ETIME", "PCPU":"PCPU", "RSS":"RSS", "VSS":"VSS", "CMDLINE":"CMDLINE"}
			retstr += "\n"
		else:
			retstr = ""
		retstr += "%s\n" % str(pnode)
		for child in pnode.children:
			retstr += self.fullProcessText(child)
		return retstr


	def addChildByPID(self, pid):
		"""
		Add child by process id
		Make ProcInfoNode with process id 
		"""
		new_node = ProcInfoNode(pid)
		self.addProcInfo(new_node)
		for fd_path in new_node.attrs.FD.values():
			fd_basename = os.path.basename(fd_path)
			if fd_basename.startswith("socket"):
				sock_inode = fd_basename.strip("socket:[]")
				sock_type = None
				try:
					sock_info = self.sock_dict[sock_inode]
					sock_type = SOCKTYPE_TCP
				except:
					try:
						sock_info = self.usock_dict[sock_inode]
						sock_type = SOCKTYPE_UNIX
					except:
						sock_info = ()
						sock_type = None
				if sock_type == SOCKTYPE_TCP:
					if len(sock_info) > 0 and sock_info[4] == 0x0A:
						new_node.attrs.SSOCKETS.append(sock_info)
					elif len(sock_info) > 0 and sock_info[4] == 0x01:
						new_node.attrs.CSOCKETS.append(sock_info)
				elif sock_type == SOCKTYPE_UNIX:
					try:
						sock_info_list = list(sock_info)
						sock_path = os.path.join(os.path.realpath(PROC_CWD % pid), sock_info_list[0])
						sock_info_list[0] = sock_path
						sock_info = tuple(sock_info_list)
					except:
						pass
					else:
						if len(sock_info) > 0 and sock_info[1] == 0x01 and sock_info[2] == 0x01:
							new_node.attrs.USOCKETS.append(sock_info)
						else:
							new_node.attrs.CSOCKETS.append(sock_info)


	def clear(self):
		"""
		Clear all tree node
		Delete root node
		"""
		if not self.valid:
			return
		self.removeChild(self.root)
		del self.root


	def refresh(self):
		"""
		Make new process information tree
		Assertion may occur if there isn't proc file system(/proc/###)
		"""
		self.clear()
		process_list = glob.glob("/proc/[0-9]*")
		assert len(process_list) > 0, "this system has no proc file system!(/proc/###)"

		self.sock_dict = self.getTcpSockInfo()
		self.usock_dict = self.getUnixSockInfo()
		for process in process_list:
			process_temp = process.split("/")
			pid = process_temp[-1]
			if process == ROOT_PROC:
				self.root = ProcInfoNode(pid)
			else:
				self.addChildByPID(pid)
		

	def getTcpSockInfo(self):
		"""
		"""
		fp = open(PROC_NET_TCP, "r")
		tcplines = fp.readlines()
		fp.close()
		sock_dict = {}
		for line in tcplines[1:]:
			fields = line.replace(":", " ").strip().split(None, 14)
			sock_sl, sock_local_address, sock_local_port, sock_rem_address, sock_rem_port, sock_st, \
					sock_tx_queue, sock_rx_queue, sock_tr, sock_tm, sock_retrnsmt, sock_uid, \
					sock_timeout, sock_inode, sock_etc = fields
			sock_local_address = socket.inet_ntoa(binascii.unhexlify(sock_local_address)[::-1])
			sock_local_port = int(sock_local_port, 16)
			sock_rem_address = socket.inet_ntoa(binascii.unhexlify(sock_rem_address)[::-1])
			sock_rem_port = int(sock_rem_port, 16)
			sock_st = int(sock_st, 16)
			sock_info = (sock_local_address, sock_local_port, sock_rem_address, sock_rem_port, sock_st, sock_uid)
			sock_dict[sock_inode] = sock_info
		return sock_dict
	getTcpSockInfo = classmethod(getTcpSockInfo)


	def getUnixSockInfo(self):
		"""
		"""
		fp = open(PROC_NET_UNIX, "r")
		unixlines = fp.readlines()
		fp.close()
		sock_dict = {}
		for line in unixlines[1:]:
			fields = line.strip().split()
			try:
				sock_num, sock_refcount, sock_protocol, sock_flags, sock_type, sock_st, sock_inode, sock_path = tuple(fields)
			except:
				continue
			sock_type = int(sock_type, 16)
			sock_st = int(sock_st, 16)
			sock_dict[sock_inode] = (sock_path, sock_type, sock_st, sock_num, sock_refcount, sock_flags)
		return sock_dict
	getUnixSockInfo = classmethod(getUnixSockInfo)


	def getNodesByProcName(self, proc_name, nodes=[], pnode=None, cmd=None):
		"""
		Find process information use process command line ( or some word of command line )
		""" 
		if not self.valid:
			return []
		if not pnode:
			pnode = self.root

		match_nodes = []
		if " ".join(pnode.attrs.CMD).lower().find(proc_name.lower()) > -1:
			if not cmd:
				match_nodes.append(pnode)
			elif len(pnode.attrs.CMD) > 0 and pnode.attrs.CMD[0] == cmd:
				match_nodes.append(pnode)

		for child in pnode.children:
			match_nodes.extend(self.getNodesByProcName(proc_name, match_nodes, child, cmd))

		return match_nodes


	# Alias member function for ProcessList
	addProcInfo = BaseTree.addChild
	getChildByPID = BaseTree.getChildByName
	removeNodeByPID = BaseTree.removeNodeByName



if __name__=="__main__":
	# TEST CODE
	pl = ProcInfoTree()
#	print "Full list of processes:"
# 	print pl	# pl = pl.getSubTree() -> process tree
	print "Python processes:"
	proc_nodes = pl.getNodesByProcName("python")
	for proc_node in proc_nodes:
		print proc_node, "ASDFASDF", proc_node.attrs.CMD[0]
		print proc_node.attrs.SSOCKETS,
		print proc_node.attrs.USOCKETS,
		print proc_node.attrs.ETIME
#EOF

