#!/usr/bin/env python
# coding: utf8
import happybase

import time
import random

BLOG_HADOOP = ["10.35.51.202", "10.35.51.203", "10.35.51.204", "10.35.51.205", "10.35.51.206", "10.35.51.207", "10.35.51.208", "10.35.51.209", "10.35.51.210", "10.35.51.211", "10.35.51.212", "10.35.51.213", "10.35.51.214", "10.35.51.215", "10.35.51.216", "10.35.51.217", "10.35.51.218", "10.35.51.219", "10.35.51.220", "10.35.51.221", "10.35.51.222" ]

import socket 

socket.setdefaulttimeout(5)


FIELD_MATCH = dict()
FIELD_MATCH["urlinfo:type"] = "sc"
FIELD_MATCH["urlinfo:visit_count"] = "visit_count"
FIELD_MATCH["urlinfo:first_visit"] = "first_visit"
FIELD_MATCH["urlinfo:last_visit"] = "last_visit"
FIELD_MATCH["urlinfo:domain_name"] = "domain_name"
FIELD_MATCH["urlinfo:domain_url"] = "domain_url"
FIELD_MATCH["urlinfo:domain_id"] = "domain_id"

INT_FIELD = ["visit_count","first_visit", "domain_id"]

def getHConnection(num=None):
	while 1:
		try:
			if num == None:
				num = random.randint(0,20)
			server = BLOG_HADOOP[num]
			connection = happybase.Connection(server)
			connection.open()
			return connection
		except Exception,msg:
			time.sleep(1)
			num = None


def useFilter():

	#connection = happybase.Connection('10.35.51.215')
	#connection = happybase.Connection('10.35.32.41')
	#connection.open()
	connection = getHConnection()
	print connection.tables()
	
	table = connection.table("blog_urls")
	#table = connection.table("img_urls")
	st = time.time()
	total_count = 0
	"""
	row = table.row("http://cfile24.uf.tistory.com/image/220C163453DF5DEB3790D2")

	for dk in row:
		print dk
		#print row["docinfo:title"], row["imginfo:src"]
	print st
	"""
	r_count = 0

	for key, data_dict in table.scan(batch_size=10000):
		print key, data_dict["postinfo:visit_count"], data_dict["postinfo:crawl_time"], data_dict["postinfo:visit_count"]
		a = dict()
		a["postinfo:first_crawl_time"] =  data_dict["postinfo:crawl_time"]
		table.put(key, a)
		total_count += 1
	et = time.time()
	print et-st
	print total_count

	connection.close()

def getURL(url):
	#connection = happybase.Connection('10.35.51.202')
	connection = getHConnection()
	connection.open()

	table = connection.table("blog_urls")
	row = table.row(url)

	if row:
		print row["postinfo:first_crawl_time"] ,  row["postinfo:title"], row["postinfo:visit_count"], row["postinfo:crawl_time"], row["postinfo:write_time"], "body :", row["postinfo:body"]


	connection.close()

def getOldPostInfo(url, table):

	row = table.row(url)
	if row:
		try:

			visit_count = int(row["postinfo:visit_count"])
			visit_time = int(row["postinfo:crawl_time"])
			if "postinfo:first_crawl_time" in row:
				first_visit =  int(row["postinfo:first_crawl_time"])
			else:
				first_visit = visit_time
			return first_visit, visit_time, visit_count
		except exception, msg:
			pass

	return 0, 0, 0


def getUrlInfo(url, table):

	result_dict = dict()
	row = table.row(url)

	if row:
		try:
			for h_key, field in FIELD_MATCH.items():
				if h_key in row:
					if field in INT_FIELD:
						result_dict[field] = int(row[h_key])
					else:
						result_dict[field] = row[h_key]
			return result_dict
		except exception, msg:
			pass
	return result_dict

		

def getImage(url):
	connection = happybase.Connection('10.35.51.202')
	connection.open()

	table = connection.table("img_urls")
	row = table.row(url)

	print url
	"""
	for key in row:
		print key
	"""
	if row:	
		for key in row:
			print key
		#print  row["docinfo:url"], row["docinfo:pos"], row["docinfo:title"], row["docinfo:write_time"], row["imginfo:filesize"]
	else:
		print "no result"
	connection.close()


def getAllUrls(url=None):
	connection = happybase.Connection('10.35.51.202')
	connection.open()

	table = connection.table("all_urls")
	d_count = 0

	if url:
		
		row = table.row(url)
		if row:
			for key, val in row.items():
				print key, val
			

	else:
		for key, data_dict in table.scan(batch_size=100):

			print key, 
			for d_key, value in data_dict.items():
				print d_key, value

	connection.close()




def getSiteUrls():
	connection = happybase.Connection('10.35.51.202')
	connection.open()

	table = connection.table("site_urls")
	d_count = 0
	for key, data_dict in table.scan(row_prefix="r:http://kr.studiotiel", batch_size=100):

		print key, 
		for d_key, value in data_dict.items():
			print d_key, value

		d_count += 1
		if d_count == 100:
			break

	connection.close()


def insertData():
	
	connection = happybase.Connection('10.35.32.41')
	connection.open()
	table = connection.table("out_links")


	data_dict = dict()

	url = ""
	domain = ""
	anchor = ""
	doc_id = ""
	base_url = ""
	read_count = 0
	reply_count = 0
	site_id = 0
	write_time = 0
	
	data_dict["url_info:domain"] = domain
	data_dict["url_info:anchor"] = anchor
	data_dict["url_info:url"] = url
	data_dict["url_info:doc_id"] =doc_id 
	
	data_dict["parent_info:url"] = base_url
	data_dict["parent_info:read"] = str(read_count)
	data_dict["parent_info:reply"] = str(reply_count)
	data_dict["parent_info:site_id"] = str(site_id)
	data_dict["parent_info:write_time"] = str(write_time)

	table.put(url, data_dict)


if __name__ == "__main__":
	import sys
	if len(sys.argv) > 1:
		url = sys.argv[1]
		#getImage(url)

		getAllUrls(url)

	else:
		getAllUrls()

	#getImage("http://postfiles4.naver.net/20140831_195/soul7682_1409485215101IWvCg_JPEG/P20140831_200500632_948DB331-1D27-495C-ACD0-7FA878D81514.JPG?type=w1")
	#getImage("http://postfiles7.naver.net/20131129_294/tourcodi05_1385704505191UL8lw_JPEG/resortcompany_co_kr_20131129_142201.jpg?type=w2")
	#getImage("http://postfiles3.naver.net/20141009_82/dyoong1_1412843060322REuVP_PNG/MAPLESTORY_2014-10-09_16-37-31-348.png?type=w2")
	#getSiteUrls()
	#getAllUrls()
	#getImage("http://postfiles8.naver.net/20090705_167/akasw_1246797136100pJeut_jpg/dsc01571_akasw.jpg?type=w3")
	#useFilter()

	#deleteOldData()
	#getNthKey()
