#coding: utf8
from urllib import unquote

from common_func import getDBCursor
from parser import HttpParser, makePerfectURL
from rss_parser import rssParser
from utils.log import getLogger
from date_handler import DateFactory
from field_transformer import FieldTransformer
from urlpattern import getTopDomain

INT_FIELD = ["read_count","read2","reply_count", "recommend_count", "readCount", "replyCount", "recommendCount", "answerRecommedCount", "answerCount"]


def checkInt(value):
	value = int(value.replace(" ","").replace(",","").replace("/","").strip() )
	return value

def getDataByHtml(encoded_html, pattern, name):

	try:
		pieces = pattern.split("****")
		prefix = pieces[0]
		s_cur = 0
		total = 0
		max_length = 1000
		value = None

		if pattern.strip().endswith("****"):
			s_cur = encoded_html.find(prefix.strip())
			e_cur = encoded_html.find("\n", s_cur+len(prefix))
			if s_cur >= 0:
				value = encoded_html[s_cur+len(prefix):e_cur]
				if name in INT_FIELD:
					try:
						value = checkInt(value)
					except Exception, msg:
						return None
				return value
		lines = prefix.split("\n")
		if len(lines) == 1:
			total = encoded_html.count(prefix.rstrip())
			max_length = 500
		else:
			total = encoded_html.count(lines[0].strip())

		for i in range(0,min(total, 10000)):
			succes = True
			if len(lines) == 1:
				s_cur = encoded_html.find(prefix, s_cur+1)
				if s_cur < 0:
					break
				else:
					s_cur = s_cur + len(prefix)
			else:
				for line in lines:
					p_cur = encoded_html.find(line.strip(), s_cur+1)
					if p_cur>=0:

						if s_cur != 0 and p_cur - s_cur - len(line) < 10:
							s_cur = p_cur+len(line.strip())
						elif s_cur == 0:
							s_cur = p_cur+len(line.strip())
						else:   
							succes = False 
							s_cur = p_cur+len(line.strip())
							break   


			if not succes: 
				continue
			suffix = pieces[1]
			if len(suffix.strip()) == 0:
				e_cur = encoded_html.find("\n", s_cur)
				value = encoded_html[s_cur:e_cur] 
			lines = suffix.split("\n")
			p_cur = encoded_html.find(lines[0].strip(),s_cur)
			if p_cur >= 0:
				value = encoded_html[s_cur:p_cur]
				if name in INT_FIELD:
					try:
						value = checkInt(value)
						return value
					except Exception, msg:
						value = None
						continue
				else:
					value = value.strip()
					if len(value) > max_length:
						continue
				if name == "title" and value.find("&nbsp;") >=0:
					value = value.replace('<img src="/images/phone_icon.png" style="padding-bottom:5px"/>&nbsp;',"")
				
				tmp_cur = value.rfind(prefix)
				if tmp_cur >= 0:
					return value[tmp_cur+len(prefix):]
			return value

	except Exception, msg:
		getLogger().error(msg)
		pass

	return value


def getNextText(t_list, text):

	cbit = 0
	for t_text in t_list:
		if t_text.strip() == text:
			cbit = 1
		elif cbit == 1:
			return t_text
	return None



class NodeRule:
	def __init__(self, field, type, value):
		self.field = field 
		self.type = type
		self.value = value
		self.nth = 0
		self.offset = 0	
		self.children_rules = dict()

	

def handleStringfilter(org_string, filter):

	ret_text = org_string

	try:
		if filter.startswith("****"):
			sep = filter[4:].strip()
			if org_string.count(sep) > 0:
				ret_text = org_string[:org_string.rfind(sep)]
		elif filter.endswith("****"):
			sep = filter[:-4].strip()
			if org_string.count(sep) > 0:
				ret_text = org_string[org_string.find(sep)+len(sep):].strip()
		elif filter.count("****") > 0:
			pieces = filter.split("****")
			s_cur = org_string.find(pieces[0].strip())
			e_cur = org_string.rfind(pieces[1].strip())
			if s_cur >=0 and e_cur > s_cur:
				ret_text = org_string[s_cur+len(pieces[0].strip()):e_cur]
		else:
			del_pieces = filter.split(";")
			for piece in del_pieces:
				org_string = org_string.replace(piece, "")
			ret_text = org_string
	except Exception, msg:
		getLogger().error(msg)

	return ret_text.strip()
	


class HostParserRule:
	def __init__(self):
		self.rules = dict()


class ParsingRuleManager:
	def __init__(self, mode="service"):
		self.rules = dict()
		self.mode = mode
		self.string_filter = dict()
		self.id_dict = dict()
		self.template_rules = dict()
		

	def makeRule(self, db_cursor=None):

		#db, cursor = getDBCursor(host="10.35.50.116", user="blogmoa",passwd="blogcrawler", db="pado")
		#db, cursor = getDBCursor(host="10.35.50.116", user="blogmoa",passwd="blogcrawler", db="pado")

		if db_cursor:
			cursor = db_cursor
		else:
			db, cursor = getDBCursor(host="10.35.31.229", user="domanager",passwd="domanagerA!", db="domanager")

		code_dict = dict()
		query = "SELECT idx, name from codes where code_category = 4"
		cursor.execute(query)
		results = cursor.fetchall()
		code_dict = dict()
		for idx, name in results:
			code_dict[idx] = name
			
		query = "SELECT a.idx, domainID, domain, activeYN, b.idx, field, ruleCode, ruleVal, b.parentRuleID FROM pado.parsers a, pado.parser_rules b where a.idx = b.parserID"

		cursor.execute(query)
		results = cursor.fetchall()

		# http://clien.net/cs2/bbs/board.php?bo_table=park&wr_id=35767059
		# http://mlbpark.donga.com/mbs/articleVC.php?mbsC=bullpen2&mbsIdx=1954295
		# http://marumaru.in/b/free/76251
		# parsing rule type : meta, next_text, class, id, html, class_count
		children_rules = dict()
		offset_dict = dict()
		all_rules = dict()

		for parser_id, domain_id, host, activeYN, idx, field, rule_code, value, parent_id in results:

			if self.mode == "service" and activeYN != "Y":
				continue
			if rule_code in code_dict:
				type = code_dict[rule_code]
			else:
				type = ""

			if rule_code == 60:
				self.template_rules[host] = int(value), parser_id
				continue

			if not host in self.rules:
				self.rules[host] = dict() 
				current_rule = HostParserRule()
				self.rules[host][parser_id] = current_rule
			else:
				if parser_id in self.rules[host]:
					current_rule = self.rules[host][parser_id]
				else:
					current_rule = HostParserRule()
					self.rules[host][parser_id] = current_rule

			if parent_id:
				parent_id = int(parent_id)
				if type == "offset":
					if parent_id in current_rule.rules:
						current_rule.rules[parent_id].offset = value
					elif parent_id in all_rules:
						all_rules[parent_id].offset = value
					else:
						offset_dict[parent_id] = value
				elif parent_id in current_rule.rules:
					current_rule.rules[parent_id].children_rules[idx] = NodeRule(field, type, value)

					if idx in offset_dict:
						current_rule.rules[parent_id].children_rules[idx].offset = offset_dict[idx]
					all_rules[idx] = current_rule.rules[parent_id].children_rules[idx]
				else:
					if parent_id  not in children_rules:
						children_rules[parent_id] = dict()
					children_rules[parent_id][idx] = NodeRule(field, type, value)
					if idx in offset_dict:
						children_rules[parent_id][idx].offset = offset_dict[idx]
					all_rules[idx] = children_rules[parent_id][idx]

			if type == "delete":
				self.string_filter[parent_id] = value
			else:
				#   class[3] 
				if parent_id:
					pass
				else:
					t_rule = NodeRule(field, type, value)
					all_rules[idx] = t_rule
					if idx in offset_dict:
						t_rule.offset = offset_dict[idx]
					current_rule.rules[idx] = t_rule
				if idx in children_rules:
					for c_idx, child_rule in children_rules[idx].items():
						if child_rule.type == "offset":
							current_rule.rules[idx].offset = child_rule.value
						else:
							current_rule.rules[idx].children_rules[c_idx] = child_rule
			self.id_dict[parser_id] = current_rule

		if db_cursor == None:
			cursor.close()	



	def getParseRule(self, host):
		if host in self.rules:
			return self.rules[host]
		else:
			host_pieces = host.split(".")
			if len(host_pieces)>3:
				t_host = ".".join(host_pieces[1:])
				if t_host in self.rules:
					return self.rules[t_host]
			t_host = getTopDomain("http://%s/"%host)
			if t_host in self.rules:
				return self.rules[t_host]

		return None


def getParsingScore(parse_dict):
	try:
		score = 10
		if "write_timestamp" in parse_dict and parse_dict["write_timestamp"] != 0:
			score += 10000
		if "title" in parse_dict and  parse_dict["title"] != "":
			score += 50
	except Exception, msg:
		print msg

	return score


class CustomParser:
	def __init__(self, type="service"):
		self.prm = None
		self.parser = HttpParser()
		self.rss_parser = rssParser()
		self.df = DateFactory()
		self.field_transformer = FieldTransformer()
		self.type = type

	def setRules(self, cursor=None):
		self.prm = ParsingRuleManager(self.type)
		self.prm.makeRule(cursor)

	def getLinks(self,  header, html, url):
		if url.find("/feed/") > 0 or url.find("feeds.feedburner.com") > 0 or url.endswith(".xml") or url.find("=rss") > 0 or url.find("/rss") > 0:
			ret_dict = self.rss_parser.parse(url, html)
		else:	
			ret_dict = self.parser.plugParser(header,  html, url)
		if "links" in ret_dict:
			return ret_dict["links"]
		return dict()

	def parse(self, header, html, url, parser_id=None):

		if self.prm == None:
			self.setRules()

		ret_dict = self.parser.plugParser(header,  html, url)

		result_dict = dict()
		if parser_id != None and parser_id in self.prm.id_dict:
			try:
				host_rule = self.prm.id_dict[parser_id]
				result_dict = self.getDataByRule(host_rule, ret_dict, url)
				result_dict["parser_id"] = parser_id
				return result_dict
			except Exception, msg:
				getLogger().error(msg)

		domain = ret_dict["domain"]
		if domain in self.prm.template_rules:
			try:
				t_parser_id, org_parser_id  = self.prm.template_rules[domain]
				host_rule = self.prm.id_dict[t_parser_id]
				result_dict = self.getDataByRule(host_rule, ret_dict, url)
				result_dict["parser_id"] = org_parser_id
				return result_dict
			except Exception, msg:
				getLogger().error(msg)
			

		host_rules = self.prm.getParseRule(domain)

		if host_rules == None:
			return result_dict


		key_count = 0
		result_score = 0
		for parser_id, host_rule in host_rules.items():
			try:

				t_result_dict = self.getDataByRule(host_rule, ret_dict, url)
				parsing_score = getParsingScore(t_result_dict)
				if parsing_score > result_score:
					result_dict = t_result_dict
					result_dict["parser_id"] = parser_id
					result_score = parsing_score
			except Exception, msg:
				getLogger().error(msg)

		#all_links = ret_dict["links"]
		result_dict["all_links"] = ret_dict["links"]
		return result_dict


	def getDataByRule(self, host_rule, ret_dict, url):

		parsing_result = dict()

		r_html = ret_dict["html"]

		ret_tree = ret_dict["tree"]
		meta_dict = ret_dict["meta_info"]

		embed_links = ret_dict["embed_links"]

		all_links = ret_dict["links"]


		"""
		try:
			h_ret_dict = findData(r_html, host_rule.html_rules)
		except Exception, msg:
			pass
		"""
		for r_idx, rule  in host_rule.rules.items():

			if rule.type == "meta":
				if rule.value in meta_dict:
					parsing_result[rule.field] = meta_dict[rule.value]
					if rule.field == "imgSrc":
						parsing_result["imgs"] = list()
						parsing_result["imgs"].append(meta_dict[rule.value])
						parsing_result["imageCount"] = 1
			elif rule.type == "next_text":
				r_text = getNextText(ret_tree.text_list, rule.value)
				if r_text:
					if rule.field in INT_FIELD:
						try:
							value = checkInt(r_text)
							parsing_result[rule.field] = value
						except Exception, msg:
							pass
					else:
						parsing_result[rule.field] = r_text
			elif rule.type == "class_count":
				t_count= ret_tree.getCount(rule.value)
				parsing_result[rule.field] = t_count
			elif rule.type == "html_count":
				t_count= r_html.count(rule.value)
				parsing_result[rule.field] = t_count
			elif rule.type == "html_title":
				try:
					html_title = ret_dict["title"].strip()
					if rule.value == "****":
						ret_text = html_title
					else:
						ret_text = handleStringfilter(html_title, rule.value)
						if ret_text == html_title:
							ret_text = ""

					if r_idx in self.prm.string_filter:
						ret_text = handleStringfilter(ret_text, self.prm.string_filter[r_idx] )

					"""
					if rule.value.startswith("****"):
						sep = rule.value[4:].strip()
						if len(sep) > 0:
							ret_text = html_title[:html_title.rfind(sep)]
					elif rule.value.endswith("****"):
						sep = rule.value[:-4].strip()
						if len(sep) > 0:
							ret_text = html_title[html_title.find(sep)+len(sep):].strip()
					"""
					if ret_text != "":
						parsing_result[rule.field] = ret_text
				except Exception, msg:
					print msg
			elif rule.type == "html":
				try:
					ret_text = getDataByHtml(r_html, rule.value, rule.field)
					if ret_text:
						if r_idx in self.prm.string_filter:
							ret_text = handleStringfilter(ret_text, self.prm.string_filter[r_idx] )
						parsing_result[rule.field] = ret_text
				except Exception, msg:
					print msg

			elif rule.type == "class_list":
				nodes = ret_tree.getNode(rule.type, rule.value)
				ret_list = list()
				for node_id in nodes:

					sub_dict = dict()
					for c_idx, c_rule in rule.children_rules.items():
						try:
							if c_rule.type == "r_offset":
								t_node = ret_tree.getNode("node_id", node_id+int(c_rule.value) )
							elif c_rule.type == "r_xpath":
								tmp_node = ret_tree.getNode("node_id", node_id)
								t_node = ret_tree.getNode("xpath", tmp_node.path+"/"+c_rule.value)
								if t_node:
									pass
								else:
									t_node = tmp_node.getSubNode(c_rule.value)

							if t_node:
								t_text = t_node.getTextUnderNode()[0].strip()
								if c_idx in self.prm.string_filter:
									t_text = handleStringfilter(t_text, self.prm.string_filter[c_idx])
								if c_rule.field == "link":
									link_url = t_node.attr_dict["href"]
									sub_dict[c_rule.field] = link_url
								elif c_rule.field.endswith("_url"):
									t_url = t_node.getAttr("href")
									sub_dict[c_rule.field] = t_url
									sub_dict[c_rule.field.replace("_url", "")] = t_text
								else:
									sub_dict[c_rule.field] = t_text
						except Exception, msg:
							getLogger().error(msg)
					ret_list.append(sub_dict)
						
				parsing_result[rule.field] = ret_list

			elif rule.type == "xpath_list" or rule.type == "class_children_list":
				if rule.type == "xpath_list":
					node = ret_tree.getNode("xpath", rule.value)
				else:
					node = ret_tree.getNode("class", rule.value)
				ret_list = list()
				if node:
					for child in node.children:
						sub_dict = dict()
						for c_idx, c_rule in rule.children_rules.items():
							try:
								if c_rule.type == "r_offset":
									t_node = ret_tree.getNode("node_id", child.id+int(c_rule.value) )
								elif c_rule.type == "r_xpath":
									t_node = ret_tree.getNode("xpath", child.path+"/"+c_rule.value)
									if t_node == None:
										t_node = child.getSubNode(c_rule.value)
								if t_node:
									t_text = t_node.getTextUnderNode()[0].strip()
									if c_idx in self.prm.string_filter:
										t_text = handleStringfilter(t_text, self.prm.string_filter[c_idx] )
									if c_rule.field == "link":
										link_url = t_node.attr_dict["href"]
										sub_dict[c_rule.field] = link_url
									elif c_rule.field.endswith("_url"):
										t_url = t_node.getAttr("href")
										sub_dict[c_rule.field] = t_url
										sub_dict[c_rule.field.replace("_url", "")] = t_text
									else:
										sub_dict[c_rule.field] = t_text
							except Exception, msg:
								getLogger().error(msg)
						ret_list.append(sub_dict)
				parsing_result[rule.field] = ret_list
			else:	
				r_node = ret_tree.getNode(rule.type, rule.value, rule.offset)
				if r_node:
					if rule.field == "imageArea":
						parsing_result["imgs"] = list()
						res = r_node.getTextHtmlWithPosition()
						core, rest, links, imgs, core_len, t_text_list = res
						for img in imgs:
							if img[0] == "#" or img.lower().find("mailto") >= 0  or img.lower().find("javascript:") >= 0:
								continue
							parsing_result["imgs"].append(makePerfectURL(img, url))
						parsing_result["imageCount"] = len(parsing_result["imgs"])
					elif rule.field == "body":    # body에 offset이 직접 붙으면 시작점도 변경 
						sid = 0
						eid = 100000000

						for c_idx, c_rule in rule.children_rules.items():

							if c_rule.type == "r_offset" and c_rule.field == "body_start":
								sid = r_node.id + int(c_rule.value)
							else:
								t_node = ret_tree.getNode(c_rule.type, c_rule.value, c_rule.offset)
								if t_node:
									if c_rule.field == "body_end":
										eid = t_node.id
									elif c_rule.field == "body_start":
										sid = t_node.id


						res = r_node.getTextHtmlWithPosition(sid, eid)
						core, rest, links, imgs, core_len, t_text_list = res

						if "body" in parsing_result and len( parsing_result["body"]) > len(core):
							continue

						parsing_result["body"] = core
						parsing_result["bodyHtml"] = rest
						parsing_result["bodyLinks"] = dict()

						if "imgs" not in parsing_result:
							parsing_result["imgs"] = list()
						parsing_result["videoLinks"] = set()

						
						for link in links:
							try:
								if link == "" or link[0] == "#" or link.lower().find("mailto") >= 0  or link.lower().find("javascript:") >= 0:
									continue
								final_url = makePerfectURL(link, url)
								if final_url in all_links:
									try:
										link_data = all_links[final_url][0]

										if link_data.tag == "IFRAME":


											if final_url.startswith("http://www.instiz.net/iframe_filter.htm"):
												try:
													final_url = unquote(final_url[final_url.find("url=")+4:] )
												except Exception, msg:
													pass

											if final_url.find("youtube.com/") > 0 or final_url.startswith("http://obsnews.co.kr/vod3/vodplayer.html?video=") or final_url.find("/public/video/?") >= 0:
												parsing_result["videoLinks"].add(final_url)
											elif final_url.find("tagstory.com/player/iframe/") > 0:
												parsing_result["videoLinks"].add(final_url)
											elif final_url.find("/vodplayer.jsp?cid=") >= 0:
												parsing_result["videoLinks"].add(final_url)
											elif final_url.find("http://player.vimeo.com/video/") >= 0:
												parsing_result["videoLinks"].add(final_url)
											elif final_url.find("http://videofarm.daum.net/controller/video/viewer/Video.html") >= 0:
												parsing_result["videoLinks"].add(final_url)

										parsing_result["bodyLinks"][final_url] = link_data
									except Exception, msg:
										print msg
							except Exception, msg:
								getLogger().error(msg)

						for link, e_data in embed_links.items():
							if e_data.flashvar.find("type=video") >= 0 and e_data.flashvar.find("file=") >= 0:
								parsing_result["videoLinks"].add(link)

						for img in imgs:
							try:
								if img[0] == "#" or img.lower().find("mailto") >= 0  or img.lower().find("javascript:") >= 0:
									continue
								parsing_result["imgs"].append(makePerfectURL(img, url))
							except Exception, msg:
								pass
						parsing_result["imageCount"] = len(parsing_result["imgs"])
						parsing_result["videoCount"] = len(parsing_result["videoLinks"])

					else:

						try:
							t_text = r_node.getTextUnderNode()[0].strip()
							if r_idx in self.prm.string_filter:
								t_text = handleStringfilter(t_text, self.prm.string_filter[r_idx] )



							if rule.field in INT_FIELD:
								try:
									value = checkInt(t_text)
									parsing_result[rule.field] = value
								except Exception, msg:
									pass
							else:
								parsing_result[rule.field] = t_text

							for c_idx, c_rule in rule.children_rules.items():
								if c_rule.type == "r_attr":
									t_text = r_node.getAttr(c_rule.value)
									parsing_result[c_rule.field] = t_text
								elif c_rule.type == "r_xpath":
									t_node = ret_tree.getNode("xpath", r_node.path+"/"+c_rule.value)
									if t_node:
										pass
									else:
										t_node = tmp_node.getSubNode(c_rule.value)
									if t_node:
										t_text = t_node.getTextUnderNode()[0].strip()
										if c_rule.field in INT_FIELD:
											try:
												value = checkInt(t_text)
												parsing_result[c_rule.field] = value
											except Exception, msg:
												pass
										else:
											parsing_result[c_rule.field] = t_text
						except Exception, msg:
							print msg



		for meta_key in ["keywords", "description", "og:description"]:
			if meta_key in meta_dict:
				parsing_result[meta_key] = meta_dict[meta_key]

		try:
			parsing_result = self.field_transformer.transform(parsing_result)
		except Exception, msg:
			print msg
		return parsing_result
			
	

if __name__ == "__main__":


	import sys
	from downloader import Downloader
	from urlpattern import UrlFactory


	uf = UrlFactory()

	if len(sys.argv) >1:
		url = sys.argv[1]
	else:
		url = "http://mlbpark.donga.com/mbs/articleV.php?mbsC=bullpen2&mbsIdx=2203518"
		url = "http://mlbpark.donga.com/mbs/articleV.php?mbsC=bullpen2&mbsIdx=2203532"
		url = "http://mlbpark.donga.com/mbs/articleV.php?mbsC=bullpen2&mbsIdx=2203496"

	test_urls = ["http://mlbpark.donga.com/mbs/articleV.php?mbsC=bullpen2&mbsIdx=1954295"] 


	opener = Downloader()
	real_URL, http_header, http_content = opener.open(url)


	print real_URL, http_header
	"""
	opener = urllib2.build_opener()
	req = urllib2.Request(url)
	req.add_header("User-agent", USER_AGENT)
	#req.add_header("User-agent", "wget")
	rs = opener.open(req)

	http_header = str(rs.info())
	http_content = rs.read()
	real_URL = rs.url
	"""

	res = uf.getGuid(url)
	if res:
		type, guid, other_dict = res

	c_parser = CustomParser("test")



	if url.find("/feed/") > 0 or url.find("feeds.feedburner.com") > 0 or url.endswith(".xml") :
		ret_dict = c_parser.getLinks(http_header, http_content, real_URL)
	else:
		ret_dict = c_parser.parse(http_header, http_content, real_URL)
		if "body" not in ret_dict:
			if "all_links" in ret_dict:
				links = ret_dict["all_links"]
				for link in links:

					res = uf.getGuid(link)
					if res:
						t_type, t_guid, t_other_dict = res
						if t_type == "iview":
							parser_id = None
							if "parser_id" in t_other_dict:
								parser_id = int(t_other_dict["parser_id"])
							t_URL, t_http_header, t_http_content = opener.open(link)
							new_ret_dict = c_parser.parse(t_http_header, t_http_content, t_URL, parser_id)
							for t_field in ["body", "bodyHtml", "imgs", "bodyLinks", "videoLinks", "imageCount", "videoCount"]:
								if t_field in new_ret_dict:
									ret_dict[t_field] = new_ret_dict[t_field]
		
	for key, val in ret_dict.items():
		if key.lower().endswith("_list"):
			print key
			for c_dict in val:
				for s_key, s_val in c_dict.items():
					print s_key , " : ", s_val
		else:
			if key.lower() == "all_links":
				pass
			else:
				print key , " : ", val



