#coding: utf8
"""
General Parser for HTTP
Support list:
	text/html document
"""

import re
import urlparse
from urllib import quote
import string
import time
from HTMLParser import HTMLParser, HTMLParseError

from uniconv import convertToUTF8
from utils.log import getLogger
from rep_script import rep_javascript
from downloader import USER_AGENT


# node id seed
NODE_ID = 0
MIN_LENGTH = 30

#ALL_TAG = ['AREA','BASE','BASEFONT','BR','COL','FRAME','HR','IMG','INPUT','ISINDEX','LINK','META','PARAM','BODY','COLGROUP','DD','DT','HEAD','HTML','LI','OPTION','P','TBODY','TD','TFOOT','TH','THEAD','TR','A','ABBR','ACRONYM','ADDRESS','APPLET','B','BDO','BIG','BLOCKQUOTE','BUTTON','CAPTION','CENTER','CITE','CODE','DEL','DFN','DIR','DIV','DL','EM','FIELDSET','FONT','FORM','FRAMESET','H1','H2','H3','H4','H5','H6','I','IFRAME','INS','KBD','LABEL','LEGEND','MAP','MENU','NOFRAMES','NOSCRIPT','OBJECT','OL','OPTGROUP','PRE','Q','S','SAMP','SCRIPT','SELECT','SMALL','SPAN','STRIKE','STRONG','STYLE','SUB','SUP','TABLE','TEXTAREA','TITLE','TT','U','UL','VAR', 'EMBED', 'SECTION', 'TIME']

ALL_TAG = ["A","ABBR","ACRONYM","ADDRESS","APPLET","AREA","ARTICLE","ASIDE","AUDIO","B","BASE","BASEFONT","BDI","BDO","BIG","BLOCKQUOTE","BODY","BR","BUTTON","CANVAS","CAPTION","CENTER","CITE","CODE","COL","COLGROUP","DATALIST","DD","DEL","DETAILS","DFN","DIALOG","DIR","DIV","DL","DT","EM","EMBED","FIELDSET","FIGCAPTION","FIGURE","FONT","FOOTER","FORM","FRAME","FRAMESET",'H1','H2','H3','H4','H5','H6',"HEAD","HEADER","HR","HTML","I","IFRAME","IMG","INPUT","INS","KBD","KEYGEN","LABEL","LEGEND","LI","LINK","MAIN","MAP","MARK","MENU","MENUITEM","META","METER","NAV","NOFRAMES","NOSCRIPT","OBJECT","OL","OPTGROUP","OPTION","OUTPUT","P","PARAM","PRE","PROGRESS","Q","RP","RT","RUBY","S","SAMP","SCRIPT","SECTION","SELECT","SMALL","SOURCE","SPAN","STRIKE","STRONG","STYLE","SUB","SUMMARY","SUP","TABLE","TBODY","TD","TEXTAREA","TFOOT","TH","THEAD","TIME","TITLE","TR","TRACK","TT","U","UL","VAR","VIDEO"]

DEF_TRIM_QUERY = ["checksum","sessid","sessionid","session_id","tssess","dummy","tsession","jsessionid","cspchd"]
DEF_IMG_EXT = ["gif","jpg","jpeg","png","tif","tiff","bmp","pcx"]


RESERVED = ":/?#[]@!$&'()*+,;=\\%~"

DEF_DISCARD_EXT = ["exe","wma","avi","fla","mp3","alz", "mp4", "m4v", "wmv"]
zeroTags = ["OPTION", "SCRIPT", "STYLE",  "SELECT", "MAP"]  
negativeTags = ["A" , "BUTTON"] #, "LINK"]

#ignoreTags = ["FONT",  "B", "EM", "H1","H2", "H3", "H4", "H5", "H6", "STRONG", "PARAM", "REMOVE"]  #BR
ignoreTags = ["REMOVE"]  #BR

BIN_EXT = [".pdf",".doc",".xls",".ppt",".rtf",".hwd",".mht",".chm",".hwp", ".xlsx", ".dot",".hwt"]
IMG_EXT = [".jpg",".png",".gif",".thumb", ".jpeg"]
MOV_EXT = [".mpg",".asf",".mov",".mpg", ".mpeg", ".rm", ".ram", ".viv",".vob"]

tag_map = dict()
DEL_TAG = ["TR", "TABLE","BODY","SPAN","SELECT","PARAM","OPTION","MAP","BGSOUND","MARQUEE","ZEROBOARD","OBJECT","DOCTYPE","FORM","BASE","B", "TH", "FONT", "STRONG"]   # 1
LINK_TAG = ["A","AREA", "EMBED"]      #  2
KILL_TAG = ["BR","COL","HR"] # 날릴 태그  3
SADD_TAG = ["INPUT", "LINK"]    # /가 필요한 태그들 4
FRAME_TAG = ["IFRAME" ,"FRAME","IMG"]  #  5
DIV_TAG = ["DIV", "P", "SPAN", "UL", "TD", "LI"]

IGNORE_TYPES = ["image", "bitmap", "apple", "audio", "mpeg", "asf", "exe", "flash", "mp3", "riff",  "autocad", "midi", "compressed", "realmedia", "binary", "gameboy", "icon resource", "smaf"] 
XML_TYPES = ["XML"]
BINARY_TYPES = ["pdf document", "microsoft office document", "hangul (korean) word processor file", "postscript" , "access database"]
COMP_TYPES = ["rar", "zip", "lha", "tar", "archive data"]



def removeComments(text):
	"""
	remove comments in the text
	"""


	text = text.replace('<! --', '<!--').replace('-- >', '-->').replace('< ! --','<!--')

	comment_open_pos = text.find('<!--')
	comment_close_pos = text.find('-->')
	while comment_open_pos >= 0 and comment_close_pos > 0:
		tt = text[comment_open_pos+4:comment_close_pos]
		text = text[:comment_open_pos] +"<REMOVE>"+ text[comment_close_pos+3:]
		comment_open_pos = text.find('<!--')
		comment_close_pos = text.find('-->', comment_open_pos)

	if comment_open_pos >= 0:
		end_pos = text.find("<",comment_open_pos+1)
		st_pos = text.find(">",comment_open_pos)
		if  end_pos < st_pos:
			text = text[:comment_open_pos] + "<REMOVE>"+ text[end_pos:]
		else:
			text = text[:comment_open_pos] + "<REMOVE>"+ text[st_pos+1:]
	return text


def checkType(type):
	try:
		content_type = type.split(",")[0]
		if content_type.find(":") >= 0:
			content_type = content_type.split(":")[0]

		s_type = content_type.lower()
		for t in IGNORE_TYPES:
			if s_type.find(t) >= 0:
				return "IGN"
		for t in XML_TYPES:
			if type.find(t) >= 0:
				return "XML"
		for t in COMP_TYPES:
			if s_type.find(t) >= 0:
				return "CMP"
		for t in BINARY_TYPES:
			if s_type.find(t) >= 0:
				return "BIN"
		return "DOC"
	except Exception, msg:
		return "DOC"

def getSubmatchKey(subkey, class_dict, type="one"):

	result_list = []

	if subkey.find("****") >= 0:
		pieces = subkey.split("****")
		if len(pieces) == 2:
			start_text, end_text = pieces
			for key in class_dict:
				try:
					if key.startswith(start_text) and key.endswith(end_text):
						if type == "one":
							return key
						result_list.append(key)
				except Exception, msg:
					print msg

		if len(result_list) > 0:
			return result_list
		else:
			return None
	else:
		return subkey




class ParseTree:
	"""
	html DOM Tree class
	"""
	def __init__(self, tag="", attrs=[], base_url=""):
		self.root = ParseNode(tag, attrs)
		self.base_url = base_url
		self.idMatch = dict()
		self.path_dict = dict()
		self.attrkey_dict = dict()

		#self.div_position_dict = dict()

		self.id_position_dict = dict()
		self.class_position_dict = dict()

		self.image_position_dict = dict()
		self.link_position_dict = dict()
		
		self.text_list = list()


	def getNode(self, type, name, offset=0):

		idx = -1
		if type == "class":  
			try:
				m = re.search("\[(\d+?)\]\Z", name)
				nth = 0
				if m:
					nth = int(m.group(1))
					name = name.replace("[%s]"%nth, "")

				if nth == 0 and name.find("****") >= 0:
					class_list = getSubmatchKey(name, self.class_position_dict, "class_list")

					if class_list:
						min_idx = 100000
						for c_name in class_list:
							t_idx = self.class_position_dict[c_name][nth]
							if t_idx < min_idx:
								min_idx = t_idx
						if min_idx != 100000:
							idx = min_idx
				elif name in self.class_position_dict:
					idx = self.class_position_dict[name][nth]

				idx = idx + int(offset)
			except Exception, msg:
				getLogger().error(msg)
		elif type == "node_id":
			idx = name
		elif type == "class_list":

			ret_list = list()
			if name in self.class_position_dict:
				return self.class_position_dict[name]
			return ret_list
		elif type == "id":
			r_key = getSubmatchKey(name, self.id_position_dict)

			if r_key in self.id_position_dict:
				idx = self.id_position_dict[r_key] + int(offset)

		elif type == "link":
			if name in self.link_position_dict:
				idx = self.link_position_dict[name]
		elif type == "image":
			if name in self.image_position_dict:
				idx = self.image_position_dict[name] + int(offset)
		elif type == "xpath":
			r_key = getSubmatchKey(name, self.path_dict)
			if r_key in self.path_dict:
				idx = self.path_dict[r_key] + int(offset)
		elif type == "attrs":
			pass
		if idx in self.idMatch:
			return self.idMatch[idx]
		return None

	def getCount(self, name, type="class"):
		if type == "class":
			if name in self.class_position_dict:
				return len(self.class_position_dict[name])
		elif type == "link":
			if name in self.link_position_dict:
				return  len(self.link_position_dict[name])
		elif type == "image":
			if name in self.image_position_dict:
				return len(self.image_position_dict[name])
		return 0

	def isSubset(self, node_a, node_b):
		"""
		check if node_a is in the node_b's subset or otherwise
		return (result, base node)
		"""
		assert isinstance(node_a, ParseNode) and isinstance(node_b, ParseNode)
		ret = (False, None)

		if node_a == node_b:
			return (True, node_a)

		if node_a.depth > node_b.depth:
			loop_node = node_a
			basic_node = node_b
		else:
			loop_node = node_b
			basic_node = node_a
		
		parents = []
		while loop_node != self.root:
			parents.append(loop_node.parent_node)
			loop_node = loop_node.parent_node

		if basic_node in parents:
			ret = (True, basic_node)

		return ret


	def getFullText(self):
		"""
		return Full Text in the document
		"""
		return self.root.getTextUnderNode(withTitle=True)

	def _makeLeafScoreDict(self, scoredic, node):
		"""
		make scoredict 
		"""
		if "id" in node.attr_dict:
			id_name = node.attr_dict["id"]
			if id_name not in self.id_position_dict:
				self.id_position_dict[id_name] = node.id
			node.path = "id=%s"%id_name

		if "class" in node.attr_dict:
			class_name = node.attr_dict["class"].strip()
			if class_name not in self.class_position_dict:
				self.class_position_dict[class_name] = list()
			self.class_position_dict[class_name].append(node.id) 

		if "href" in node.attr_dict:
			link_url = node.attr_dict["href"]
			#r_url = makePerfectURL(new_url, self.base_url)

		if "src" in node.attr_dict and node.tag.upper() == "IMG":
			img_src = node.attr_dict["src"]
			if img_src not in self.image_position_dict:
				self.image_position_dict[img_src] = node.id

		if "background" in node.attr_dict:
			img_src = node.attr_dict["background"]
			if img_src.find(".gif") > 0 or img_src.find(".jpg") > 0:
				if img_src not in self.image_position_dict:
					self.image_position_dict[img_src] = node.id
			
		self.path_dict[node.path] = node.id
		if node.isLeaf():
			scoredic[node.score + 1.0/node.id] = node
		else:
			no_dict = dict()
			for child in node.children:
				if child.tag not in no_dict:
					no_dict[child.tag] = 0
					child.path = node.path+"/"+child.tag
				else:
					no_dict[child.tag] += 1
					child.path = node.path+"/"+child.tag+"[%s]"%no_dict[child.tag]
				self.idMatch[child.id] = child
				self._makeLeafScoreDict(scoredic,child)


class ParseNode:
	"""
	DOM Tree node class
	"""
	def __init__(self, tag="", attrs=[], parent_node=None):
		self.children = []
		self.id = self.__make_node_ID__()
		self.tag = tag
		#self.attrs = attrs
		self.score = 0
		self.length = 0
		self.depth = 0
		self.visibilty = True
		self.marked = False
		self.parent_node = parent_node
		self.path = ""

		self.attr_dict = dict()
		for attr in attrs:
			try:
				if tag == "#text":
					self.attr_dict["text"] = attr.replace("|11818|", "&")
				if len(attr) == 2:
					key, val = attr
					self.attr_dict[key.lower()] = val.replace("|11818|", "&").strip()
			except Exception, msg:
				pass

	def __make_node_ID__(self):
		"""
		make new node ID
		"""
		global NODE_ID
		NODE_ID += 1
		return NODE_ID
		

	def reprTree(self, depth=0):
		"""
		representation for the tree
		"""
		ret_str = ""
		text = "["          
		if self.tag == "#text":
			text += self.attr_dict["text"]
		else:
			for key, val in self.attr_dict.items():
				text += "%s=\"%s\" " %(key, val)
					
		text += "]"     
		ret_str = "%s %03d <%s> %d %s\t\t<%s>\n" % (" "*(depth+2), depth+1, self.tag, self.id, text, self.path)
		#print ret_str
		
		"""
		if not self.isLeaf():
			for child in self.children:
				pass
		"""

		if not self.isLeaf():
			for child in self.children:
				child_ret_str = child.reprTree(depth+1)
				#ret_str += "%s %04d %s %s\n" % (" "*(depth+1), depth+1, child_ret_str, self.id)
				#ret_str += "%s %04d <%s> %d %s %s" % (" "*(depth+1), depth+1, self.tag, self.score, text, self.id)
				ret_str += child_ret_str
		return ret_str


	def getTextListUnderNode(self, withAnchor=False):
		text_list = list()
		if self.isLeaf():
			if self.tag == "#text":
				if self.parent_node.tag.upper() != 'TITLE' and len(self.attr_dict) > 0:
					t_text = self.attr_dict["text"]
					if len(t_text) > 0 :
						text_list.append(t_text)

		else:
			if not withAnchor:
				if not self.tag.upper() in ["A","LINK","FRAME","META"]:
					for child in self.children:
						tt = child.getTextListUnderNode(withAnchor)
						text_list.extend(tt)
			else:
				for child in self.children:
					tt = child.getTextListUnderNode(withAnchor)
					text_list.extend(tt)

		return text_list

	def getSubNode(self, xpath):   # 상대 경로로 노드 찾기
		xpaths = xpath.split("/")

		target_tag = xpaths[0]
		m = re.search("\[(\d+?)\]\Z", target_tag)
		nth = 0
		if m:
			nth = int(m.group(1))
			target_tag = target_tag.replace("[%s]"%nth, "")

		t_nth = 0
		for child in self.children:
			if child.tag == target_tag:
				if nth == t_nth:
					if len(xpaths) == 1:
						return child
					else:
						return child.getSubNode("/".join(xpaths[1:]))
				t_nth += 1
		return None


	def getTitleNode(self, alt_title):
		if self.isLeaf():
			if self.tag == "#text": # and not  self.checkParentNode("A"):
				if not self.checkParentNode('TITLE') and len(self.attr_dict) > 0:
					t_text = self.attr_dict["text"]
					if t_text.find(alt_title.strip()) >= 0 and len(t_text) < 180:
						return (self.id, t_text)
		else:
			for child in self.children:
				tt = child.getTitleNode(alt_title)
				if tt[0]:
					return tt
		return (None, None)

	def getAttr(self, attr_key):
		if attr_key in  self.attr_dict:
			return self.attr_dict[attr_key]
		return None


	def getTextUnderNode(self, mark=False, withTitle=False):

		sub_text = ""
		links_in_summary = set()
		if self.isLeaf():
			if self.tag == "#text":
				if self.parent_node.tag.upper() != 'TITLE' and len(self.attr_dict) > 0:
					sub_text += self.attr_dict["text"]
				elif withTitle and self.parent_node.tag.upper() == 'TITLE' and len(self.attr_dict) > 0:
					sub_text += self.attr_dict["text"]
		else:
			if self.tag.upper()  in ["A","LINK","FRAME","META"] and len(self.attr_dict) > 0 and mark:
				for key in  ["href", "src"]:
					if key in self.attr_dict:
						links_in_summary.add(self.attr_dict[key])
			sub_texts = []
			try:
				if self.tag.upper() == "SPAN":
					if   "class" in self.attr_dict and self.attr_dict["class"] == "invisible":
						return sub_text, links_in_summary
			except Exception, msg:
				pass
			for child in self.children:
				if mark:
					if child.tag.upper() in ["A","LINK","FRAME","META"] and len(child.attr_dict) > 0:
						for key in  ["href", "src"]:
							if key in child.attr_dict:
								links_in_summary.add(child.attr_dict[key])

					(text, links)  = child.getTextUnderNode(mark, withTitle)
					sub_texts.append(text)
					links_in_summary = links_in_summary.union(links)
					
				elif not child.marked:
					(text, links)  = child.getTextUnderNode(mark, withTitle)
					sub_texts.append(text)
					links_in_summary = links_in_summary.union(links)
			sub_text = " ".join(sub_texts)

		if mark:
			self.marked = True
		time.sleep(0.0)
		return sub_text , links_in_summary


	def getTextHtmlWithPosition(self, start_id=0, end_id=10000000):
		sub_text = ""
		rest_text = ""
		text_list = list()
		links_in_summary = set()
		imgs_in_summary = list()
		core_length = 0
		if self.isLeaf():
			if self.id >= start_id and self.id <= end_id:
				if self.tag == "#text":
					t_text = self.attr_dict["text"]
					text_list.append(t_text)

					if self.parent_node.tag.upper() != 'TITLE' and len(self.attr_dict) > 0:
						if len(t_text) >= MIN_LENGTH and self.score > 0:
							core_length = len(t_text.strip())
						if t_text.strip() != "":
							sub_text += t_text
							rest_text += t_text
					elif self.parent_node.tag.upper() == 'TITLE' and len(self.attr_dict) > 0:
						sub_text += t_text
						rest_text += t_text
				elif self.tag.upper() in ["LINK","FRAME","META", "IFRAME", "SOURCE"]:
					for key in  ["href", "src"]:
						if key in self.attr_dict:
							links_in_summary.add(self.attr_dict[key])
				elif self.tag.upper()  == "IMG":
					for key in  ["href", "src"]:
						if key in self.attr_dict:
							imgs_in_summary.append(self.attr_dict[key])



				attr_list = list()
				if self.tag != "#text":
					for  k, v in self.attr_dict.items():
						attr_list.append('%s="%s"'%(k, v))

					if len(attr_list) > 0:
						rest_text += "<%s %s>"%(self.tag, " ".join(attr_list) )
					else:
						rest_text += "<%s>"%(self.tag)

		else:
			sub_texts = []
			rest_texts = []
			if len(self.attr_dict) > 0 and self.id >= start_id and self.id <= end_id:
				try:
					r_link = ""
					for key in  ["href", "src"]:
						if key in self.attr_dict:
							r_link = self.attr_dict[key]
					if self.tag.upper()  in ["A"]:
						if len(self.children) >0:
							links_in_summary.add(r_link)
					elif self.tag.upper() in ["LINK","FRAME","META", "IFRAME", "SOURCE"]:
						links_in_summary.add(r_link)
					elif self.tag.upper()  == "IMG":
						imgs_in_summary.append(r_link)

					attr_list = list()
					for  k, v in self.attr_dict.items():
						attr_list.append('%s="%s"'%(k, v))

					if len(attr_list) > 0:
						tag_text = "<%s %s>"%(self.tag, " ".join(attr_list) )
					else:
						tag_text = "<%s>"%(self.tag)
					rest_texts.append(tag_text)
				except Exception, msg:
					print msg, "AAAAAAAAAAAAA"
					pass
			elif  self.id >= start_id and self.id <= end_id:
				tag_text = "<%s>"%(self.tag)
				rest_texts.append(tag_text)

			for child in self.children:
				(core, rest, links, imgs, core_len, t_text_list)  = child.getTextHtmlWithPosition(start_id, end_id)
				if core.strip() != "":
					sub_texts.append(core)
				rest_texts.append(rest)
				text_list.extend(t_text_list)
				links_in_summary = links_in_summary.union(links)
				isThumb = False
				if child.tag.upper() in ["P", "DIV", "SPAN"]:
					if "class" in child.attr_dict:
						if child.attr_dict["class"] in ["thumb_user"]:
							isThumb = True
				if not isThumb and imgs and len(imgs) > 0:
					try:
						#sub_texts.append("<img src=%s>"%r_link)
						imgs_in_summary.extend(imgs)
					except Exception, msg:
						pass
				core_length += core_len

			rest_texts.append("</%s>"%self.tag)
			"""
			if self.id >= start_id and self.id <= end_id:
				if self.tag.upper() == "P":
					rest_texts.append("\n")
			"""		
					
			sub_text = " ".join(sub_texts)
			rest_text = " ".join(rest_texts)

			"""
			if self.id >= start_id and self.id <= end_id:
				if self.tag.upper() == "P":
					rest_text = "\n\n"+rest_text
			"""
		
		return (sub_text , rest_text,  links_in_summary, imgs_in_summary, core_length, text_list)



	def getTextImageWithPosition(self, start_id=0, end_id=10000000):

		sub_text = ""
		rest_text = ""
		text_list = list()
		links_in_summary = set()
		imgs_in_summary = list()
		core_length = 0
		if self.isLeaf():
			if self.id >= start_id and self.id <= end_id:
				if self.tag == "#text":
					t_text = self.attr_dict["text"]
					text_list.append(t_text)

					if self.parent_node.tag.upper() != 'TITLE' and len(self.attr_dict) > 0:
						if len(t_text) >= MIN_LENGTH and self.score > 0:
							core_length = len(t_text.strip())
						if t_text.strip() != "":
							sub_text += t_text
							rest_text += t_text
					elif self.parent_node.tag.upper() == 'TITLE' and len(self.attr_dict) > 0:
						sub_text += t_text
						rest_text += t_text
				elif self.tag.upper() == "P":
					rest_text += "\n\n"
				elif self.tag.upper() == "BR":
					rest_text += "\n"
				elif self.tag.upper() in ["LINK","FRAME","META", "IFRAME"]:
					for key in  ["href", "src"]:
						if key in self.attr_dict:
							links_in_summary.add(self.attr_dict[key])
				elif self.tag.upper()  == "IMG":
					for key in  ["href", "src"]:
						if key in self.attr_dict:
							imgs_in_summary.append(self.attr_dict[key])
							rest_text += "<\fimg src=%s\f>\n"%self.attr_dict[key]
		else:
			sub_texts = []
			rest_texts = []
			if len(self.attr_dict) > 0 and self.id >= start_id and self.id <= end_id:
				try:
					r_link = ""
					for key in  ["href", "src"]:
						if key in self.attr_dict:
							r_link = self.attr_dict[key]
					if self.tag.upper()  in ["A"]:
						if len(self.children) >0:
							links_in_summary.add(r_link)
					elif self.tag.upper() in ["LINK","FRAME","META", "IFRAME"]:
						links_in_summary.add(r_link)
					elif self.tag.upper()  == "IMG":
						imgs_in_summary.append(r_link)
						rest_texts.append("\n<\fimg src=%s\f>\n"%r_link)
				except Exception, msg:
					pass


			for child in self.children:
				(core, rest, links, imgs, core_len, t_text_list)  = child.getTextImageWithPosition(start_id, end_id)
				if core.strip() != "":
					sub_texts.append(core)
				rest_texts.append(rest)
				text_list.extend(t_text_list)
				links_in_summary = links_in_summary.union(links)
				isThumb = False
				if child.tag.upper() in ["P", "DIV", "SPAN"]:
					if "class" in child.attr_dict:
						if child.attr_dict["class"] in ["thumb_user"]:
							isThumb = True
				if not isThumb and imgs and len(imgs) > 0:
					try:
						#sub_texts.append("<img src=%s>"%r_link)
						imgs_in_summary.extend(imgs)
					except Exception, msg:
						pass

				core_length += core_len
			"""
			if self.id >= start_id and self.id <= end_id:
				if self.tag.upper() == "P":
					rest_texts.append("\n")
			"""		

					
			sub_text = " ".join(sub_texts)
			rest_text = " ".join(rest_texts)
			if self.id >= start_id and self.id <= end_id:
				if self.tag.upper() == "P":
					rest_text = "\n\n"+rest_text
		
		return (sub_text , rest_text,  links_in_summary, imgs_in_summary, core_length, text_list)

	def findStartNode(self):
		if self.isLeaf():
			if self.tag == "#text" and self.score > 0:
				return self.id
			else:
				return None
		else:
			for child in self.children:
				r_id  = child.findStartNode()
				if r_id:
					return r_id

		return None

	# score가 0이 아닌 마지막 노드 
	def findEndNode(self):
		
		if self.isLeaf():
			if self.tag == "#text" and self.score > 0:
				return self.id
			else:
				return None
		else:
			k = list(self.children)
			k.reverse()
			for child in k:
				r_id  = child.findEndNode()
				if r_id:
					del k
					return r_id
			del k

		return None


	# 마지막 노드 
	def findENode(self):
		if self.isLeaf():
			return self.id
		else:
			k = list(self.children)
			k.reverse()
			for child in k:
				r_id  = child.findENode()
				if r_id:
					del k
					return r_id
			del k

		return None




	def getMaxScore(self):
		if self.isLeaf():
			return (self.score, self)
		else:
			max_score = self.score
			max_node = self
			for child in self.children:
				(t_score, t_node) = child.getMaxScore()
				if t_score > max_score:
					max_score = t_score
					max_node = t_node

			return (max_score, max_node)
				
	
	def isRoot(self):
		"""
		check if this node is root of the tree
		"""
		if self.parent_node is None:
			return True
		else:
			return False


	def isLeaf(self):
		"""
		check if this node is leaf of the tree
		"""
		if len(self.children) == 0:
			return True
		else:
			return False


	def append(self, node):
		"""
		add a node to child node of this node
		"""
		node.setParent(self)
		node.depth = self.depth + 1
		self.children.append(node)


	def setParent(self, parent_node):
		"""
		set parent node
		"""
		self.parent_node = parent_node
	

	def getParent(self):
		"""
		get parent node
		"""
		return self.parent_node

		
	def setScoreTextNum(self):
		"""
		set text score to all nodes
		"""
		self.score = 0
		penal = 0

		if self.isLeaf():
			if self.tag == "#text" and "text" in self.attr_dict:
				text = self.attr_dict["text"]
				self.score = getScore(text)
				self.length = text

				if self.score < MIN_LENGTH:
					self.score = 0
				else:
					t_count = 0
					penal = 0
					if penal > len(text) * 0.4 or (t_count > 1 and len(text) < 180):
						#self.score = -100
						self.score = 0

					penal = 0
					t_count = 0

					if penal > len(text) * 0.3 or (t_count > 4 and len(text) < 350) or (t_count > 6 and len(text) < 400):
						self.score = 0
		else:
			for child in self.children:
				try:
					t_score = child.setScoreTextNum()
					self.score += t_score
				except Exception, msg:
					getLogger().error(msg)


		upper_tag = self.tag.upper()

		if upper_tag in zeroTags:
			self.score = 0
		elif upper_tag in negativeTags:
			self.score = 0
			self.makeSubTreeScoreZero()
		elif upper_tag == 'TEXTAREA':
			if self.checkParentNode('FORM'):
				self.makeSubTreeScoreZero()
		elif upper_tag == 'TITLE':
			self.makeSubTreeScoreZero()

		return self.score


	def makeSubTreeScoreZero(self):
		if self.score > 0:
			self.score = 0
		for ch in self.children:
			ch.makeSubTreeScoreZero()


	def checkParentNode(self, tag):
		t_node = self.parent_node
		while t_node != None:
			if t_node.tag.upper() == tag:
				return True
			else:
				t_node = t_node.parent_node
		return False


	def makeSubTreeToInvisible(self):
		self.visibilty = False
		for ch in self.children:
			ch.makeSubTreeToInvisible()


	def maxScoredChild(self):
		"""
		get maximum score node
		"""
		if self.isLeaf():
			return None 
		nodeptr = self.children[0]
		for child in self.children:
			if child.score > nodeptr.score:
				nodeptr = child 
		return nodeptr 		



class LinkURL:
	"""
	Simple LinkURL object
	contains url, tag, inout("IN"|"OUT"), text
	"""

	def __init__(self, tag, inout, text, title, id):
		self.tag = tag.upper()
		self.inout = inout.upper()
		self.text = text
		self.id = id
		self.title = ""
		self.writer = ""
		self.pubdate = 0
	
	def __repr__(self):
		"""
		x.__repr__() <==> repr(x)
		"""
		ret_str = "(%s, %s, %s, %s, %s" % (self.id, self.tag, self.inout, self.text, self.title)
		ret_str += ")"
		return ret_str


	def __str__(self):
		"""
		x.__str__() <==> str(x)
		"""
		text = self.text[:50]
		if len(self.text) > 50:
			text += "...(%d bytes long)" % len(self.text)
		ret_str = "(%s, %s, %s, '%s')" % (self.id, self.tag, self.inout, text)
		return ret_str


class HttpParser:
	"""
	General Parser for HTTP Response data
	"""

	def __init__(self, charset=None):
		self.default_charset = charset
		self.charset = None
		self.html_parser = None
		self.content_type = None
		self.alt_title = None
		self.content = None
		self.encoded_html = None


	def Init(self):
		self.charset = None
		self.html_parser = None
		self.content_type = None
		self.mc_type = None
		self.content = None
		self.encoded_html = None

	def plugParser(self, http_header, http_content, base_url):
		"""
		[external access function]
		Parse content and return the results

		results :
			(title_text, full_text, core_text, rest_text, [LinkURL(url, tag, in/out, text), ...])
		"""

		parsed_result = None
		self.Init()
		try:
			parsed_result = self._parse(http_header, http_content, base_url)
		except NotImplementedError, msg:
			getLogger().error("Not Implemented")
		except Exception, msg:
			getLogger().debug("parsing error : [%s]" , msg)
			
		if not parsed_result:
			# resultReturn
			parsed_result = ["", "", "", "", [], dict(meta_data=("", ""), alt_title="", image_count=0, core_length=0, data_type="")]

		return parsed_result


	def _parse(self, header, content, base_url):
		"""
		parse content and return the results
		results :
			(title_text, full_text, core_text, rest_text, [LinkURL(url, tag, in/out, text), ...])
		"""
		self.header = header
		self.content = content

		try:
			self._classifyContentType(base_url)
		except Exception, msg:
			getLogger().error("%s",msg)

		self.html_parser = None

		if (self.content_type and self.content_type.startswith("text")) or self.content_type == None:


			t_content = removeComments(content)
			self.getMetaData(t_content)
			full_html = self.encodeToUTF8(content, base_url)
			self.encoded_html = full_html


			if self.content_type == None:
				self.content_type = "text/html"
			if self.content_type == "text/plain" and content.startswith("<!DOCTYPE HTML"):
				self.content_type = "text/html"

			if content.find('text/html') >= 0:
				self.content_type = "text/html"
			if self.content_type.find("html") >=0 :
				self.content_type = "text/html"
			if self.content_type.lower() == "text/html":
				# text/html parser

				parser = HtmlParser(self.charset, base_url)
				self.html_parser = parser
				return parser.parse(full_html, False)
			elif self.content_type == "text/plain":
				title = ""
				fulltext = " ".join(self.content.split())
				core_text = fulltext
				rest_text = ""
				# resultReturn
				return [title, fulltext, core_text, rest_text, [], dict(meta_data=("", ""), alt_title="", image_count=0, core_length=len(core_text), data_type="txt")]
		else:
			return None


	def encodeToUTF8(self, content, base_url):

		import codecs 

		if content.startswith(codecs.BOM_UTF8):
			self.charset = "utf8"
		elif content.startswith(codecs.BOM_UTF16):
			self.charset = "utf16"
		elif content.startswith(codecs.BOM_UTF32):
			self.charset = "utf32"



		try:
			if self.charset is None or len(self.charset.strip()) == 0:
				self.charset = getEncoding(content)
				if base_url.find("coolenjoy.net") >= 0:
					self.charset = 'utf8'
			else:
				self.charset = self.charset.lower().replace("charset", "")

			if self.charset in ['8859_1', 'euc-kr']:
				self.charset = 'cp949'
			if self.charset.lower() in ['gb2312', 'gb1980', 'gbk']:
				self.charset = 'gbk'

			full_html = content.decode(self.charset, "replace").encode("utf8", "xmlcharrefreplace")
		except Exception,msg:
			getLogger().error("parse Exception : %s %s", msg, self.charset)
			try:
				self.charset = getEncoding(content)
				full_html = content.decode(self.charset, "replace").encode("utf8", "xmlcharrefreplace")
			except Exception, msg:
				getLogger().error("parse Exception : %s", msg)

		return full_html


	def getMetaData(self, text):
		
		tag = "meta"
		k = re.compile("<\s*%s.*?>"%(tag), re.I | re.S)  
		
		pp = k.findall(text)
		for ss in pp:
			low_ss = ss.lower()
			if low_ss.find("content-type") >= 0:
				l = re.compile("content\s*=\s*[\"\'](.*?)[\'\"]", re.I | re.S)
				rr = l.search(ss, 1)
				if rr: 
					m = rr.group(1)
					pieces = m.split(";")
					content_type = pieces[0]	
					if self.content_type == None:
						self.content_type = content_type
					if len(pieces) == 2 and pieces[1].lower().find("charset"):
						charset = pieces[1].lower().replace("charset","").replace("=", "")
						if self.charset == None:
							self.charset = charset.strip()

			if low_ss.find("charset") >= 0:
				try:
					l = re.compile("charset\s*=\s*[\"\'](.*?)[\'\"]", re.I | re.S)
					rr = l.search(ss, 1)
					if rr: 
						m = rr.group(1)
						if self.charset == None:
							self.charset = m
				except Exception, msg:
					pass

	def _classifyContentType(self, base_url):
		"""
		classify Content-Type from HTTP header
		"""
		header = self.header

		try:
			charset = None
			ext = None
			http_code = None
			if header:
				lines = header.split("\n")
				s_charset = None
				for line in lines:
					try:
						if line.lower().startswith("http/"):
							http_code = line.split()[1]
					except Exception, msg:
						getLogger().error(msg)
					if http_code and not http_code.startswith("30") and line.lower().find("content-type:") >= 0:
						pieces = line.split(';')
						self.content_type = pieces[0].lower().split()[1]
						if len(pieces) >= 2 and pieces[1].lower().find("charset") >= 0:
							charset = pieces[1].replace("charset","").replace("=","").strip()
					elif line.lower().find("content-type:") >= 0:
						pieces = line.split(';')
						self.content_type = pieces[0].lower().split()[1]
						if len(pieces) >= 2 and pieces[1].lower().find("charset") >= 0:
							s_charset = pieces[1].replace("charset","").replace("=","").strip()

					if line.lower().find("content-disposition") >= 0:
						hcur = line.lower().find("filename")
						if hcur:
							ext_cur = line[hcur:].rfind(".")
							if ext_cur:
								try:
									ext = line[hcur:][ext_cur:].lower().strip()
								except:
									ext = None

			if self.charset == None and s_charset != None:
				self.charset = s_charset

		except Exception, msg:
			getLogger().error(msg)

		try:
			parsed_url = urlparse.urlparse(base_url)
			extend_cur = parsed_url[2].rfind(".")
			if not ext and extend_cur >= 0:
				ext = parsed_url[2][extend_cur:].lower().strip()

			# content_type : 'text', 'image', 'audio', 'video', 'message', 'multipart', 'application'
			if ext == None:
				pass
			elif ext in BIN_EXT:
				self.content_type = "application"
			elif ext in IMG_EXT:
				self.content_type = "image"
			elif ext in MOV_EXT:
				self.content_type = "video"
			elif ext in ".xml":
				self.content_type = "text/xml"

			if self.content_type:
				if self.content_type.find("multipart") >=0:
					self.content_type = "text/html"
				
				if self.content_type.startswith('file'):
					self.content_type = "application"

				if self.content_type == "doesn/matter":
					self.content_type = "application"

		except Exception, msg:
			getLogger().error(msg)

def getText(nodelist):
	rc = ""
	for node in nodelist:
		rc += node.data
	return rc

class EmbedData:
	def __init__(self):
		self.width = 0
		self.height = 0
		self.type = None
		self.flashvar = ""

class ImageData:
	def __init__(self):
		self.width = 0
		self.height = 0
		self.alt = ""
		self.title = ""
		self.sub_text = ""
		self.node_ids = list()


class HtmlParser(HTMLParser):
	"""
	HTML Parser for Web Crawler
	"""
	def __init__(self, charset=None, base_url=""):

		self.links_in_summary = set()
		self.base_url = base_url
		self.text_list = list()
		self.lang = ""
		self.scripts = []
		self.script_src = []
		self.script_call = dict()
		self.text = ""
		self.nurl = ""
		self.nurl_id = 0
		self.t_title = ""
		self.ncall = ""
		self.anc_text = ""
		self.links = dict()
		self.img_links = dict()
		self.embed_links = dict()
		self.charset = charset
		self.meta_dict = dict()
		

		self.invisible = False
		self.invisible_depth = None
		self.depth = 1
		self.title = ""
		self.start_tags = dict()
		self.tree = ParseTree("root", [("name", "root")])
		self.point = self.tree.root
		self.robots_exclusion = {'INDEX':True, 'FOLLOW':True}
		self.script_pattern = None
		self.opt_links = set()

		HTMLParser.__init__(self)

		try:
			self.s_domain = urlparse.urlparse(self.base_url).netloc
			n_cur = self.s_domain.find("@")
			if n_cur >= 0:
				self.s_domain = self.s_domain[n_cur+1:]
			self.s_domain = self.s_domain.replace("www.","")

		except Exception,msg:
			self.s_domain = self.base_url
	

	def setScriptPattern(self, s_pattern):
		self.script_pattern = s_pattern


	def getImageList(self):
		pass


	def removeStag(self, text):
		"""
		remove SCRIPT, STYPE tags in the text
		"""

		if len(text) < 1000000:
			text = text.replace("</script-->", "</script>")
			text = text.replace("</script---->", "</script>")
			text = text.replace("</script//-->", "</script>")
			text = text.replace("<!--/SCRIPT>", "</script>")
			text = text.replace("<!--script", "<script")
			text = text.replace("<!--link", "<link")

			text = self.removeTAG(text, "script")

	
		if text.lower().find("<script") >= 0:
			text = self.removeScript(text, "script")



		text = text.replace("<!--[if", "<!--")
		"""
		text = text.replace("<!--[if lte IE 7]>", "")
		text = text.replace("<!-- <![endif]-->", "")
		"""
		text = text.replace("<![endif]-->", "-->")
		text = self.removeTAG(text, "noscript")


		text = self.removeTAG(text, "style")
		text = self.removeTAG(text, "noframes")
		text = removeComments(text)

		if len(text) < 400000:
			text = self.removeTAG(text, "iframe")

		return text


	def getTextList(self):
		try:
			rv = self.tree.root.getTextListUnderNode()
			return rv
		except Exception, msg:
			return None


	def getHTML(self):
		return self.full_html
	

	def getOrgHTML(self):
		return self.org_html


	def getCharset(self):
		return self.charset	

	def addLinksFromOption(self):
		for url in self.opt_links:
			self.addLink(url, "option", "")

	def removeTAG(self, text, tag):
		try:
			MAX_REG_LENGTH = 1024 * 60 ##60Kb

			if tag == "iframe":
				k = re.compile("<\s*%s\s+.*?>(.*?)</\s*%s\s*>"%(tag, tag), re.I | re.S)
			elif tag == "style":
				k = re.compile("<\s*(%s .*?>|%s>)(.*?)</\s*%s\s*>"%(tag, tag, tag), re.I | re.S)
			else:
	#			k = re.compile("<\s*%s.*?>.*?</\s*%s\s*>"%(tag, tag), re.I | re.S)  
				k = re.compile("<\s*%s.{0,%d}?>.{0,%d}?</\s*%s\s*>"%(tag, MAX_REG_LENGTH, MAX_REG_LENGTH, tag), re.I | re.S)  


			if tag == "iframe":
				pp = k.findall(text)
				for ss in pp:
					if len(ss.strip()) > 5:
						text = text.replace(ss, "")
				return text
			elif tag == "script":
				text = text.replace('write("</script>' , 'write("</sss>')
				pp = k.findall(text)
				for ss in pp:
					cur = ss.find("<")
					ecur = ss.find(">", cur)
					src = getValue(ss[cur:ecur], "src")
					if src:
						url = "=".join(src.split("=")[1:]).replace('"',"")
						self.script_src.append(url)
					self.scripts.append(ss)

				text = k.sub("<REMOVE>", text)
				return text
			else:
				text = k.sub("<REMOVE>", text)
				return text
		except Exception, msg:
			return text


	def removeComments(self, text):
		"""
		remove comments in the text
		"""


		text = text.replace('<! --', '<!--').replace('-- >', '-->').replace('< ! --','<!--')

		comment_open_pos = text.find('<!--')
		comment_close_pos = text.find('-->')
		while comment_open_pos >= 0 and comment_close_pos > 0:
			tt = text[comment_open_pos+4:comment_close_pos]
			text = text[:comment_open_pos] +"<REMOVE>"+ text[comment_close_pos+3:]
			comment_open_pos = text.find('<!--')
			comment_close_pos = text.find('-->', comment_open_pos)

		if comment_open_pos >= 0:
			end_pos = text.find("<",comment_open_pos+1)
			st_pos = text.find(">",comment_open_pos)
			if  end_pos < st_pos:
				text = text[:comment_open_pos] + "<REMOVE>"+ text[end_pos:]
			else:
				text = text[:comment_open_pos] + "<REMOVE>"+ text[st_pos+1:]
		return text


	def removeScript(self, text, tag):
		open_pos = text.lower().find("<%s"%tag)
		close_pos = text.lower().find("</%s>"%tag, open_pos)
		pieces = list()
		s_point = 0
		while open_pos >=0 and close_pos > 0:
			pieces.append(text[s_point:open_pos])
			s_point = close_pos + 3 + len(tag)
			open_pos = text.lower().find("<%s"%tag, close_pos)
			close_pos = text.lower().find("</%s>"%tag, open_pos)

		pieces.append(text[s_point:])
			
		text = " ".join(pieces)
		return text

	
	def parse(self, full_html, includeIMG=False):
		"""
		parsing html text and make DOM Tree
		"""
		logger = getLogger()
		self.org_html = full_html
		# write to file for debug
		#	
		# make debug html file

		if len(full_html) > 3000000:
			t_cur = full_html.find(" ", 3000000)
			if t_cur > 0:
				full_html = full_html[:t_cur]

		b_t = time.time()
		full_html = self.preProcessTags(full_html)
		full_html = full_html.replace("<REMOVE>\n","").replace("<REMOVE>", "")

		a_t = time.time()
#		print "preprocessing Time: ", a_t - b_t

		self.full_html = full_html

		time.sleep(0.0001)

		try:
			self.feed(full_html)
		except  HTMLParseError, msg:
			logger.error("parsing error : %s ", msg)
		except Exception, msg:
			logger.error("parsing error : %s, base_url=%s", msg, self.base_url)
		
		b_t = time.time()
#		print "parsing Time: ", b_t - a_t

		#
		# parsing results
		#
	
		b_t = time.time()
		try:
			self.setTreeScore()
		except Exception, msg:
			logger.error(msg)
		try:
			leafScores = dict()
			self.tree._makeLeafScoreDict(leafScores, self.tree.root)
			title = None
		except Exception, msg:
			logger.error(msg)


		full_text = self.tree.getFullText()
		full_text = " ".join(full_text[0].split())

		start_with_title = True

		for script in self.scripts:
			for line in script.split("\n"):
				if not line.strip().startswith("/"):
					try:
						if line and line != None and line != "None":
							t_url = self.getLink(line)
							if t_url:
								self.addLink(t_url, "script", "", "", 0)	

					except Exception, msg:
						logger.error("%s, %s", msg, t_url)

		full_text = " ".join(full_text.replace("\0","").split())

		try:
			title = removeCtrChar(self.title).replace("&gt;", ">").replace("&lt;","<")
			full_text = full_text.replace("&gt;", ">").replace("&lt;","<")
		except Exception, msg:
			getLogger().error("%s",msg)

		self.tree.text_list = self.text_list

		ret_dict = dict()
		ret_dict["title"] = title
		ret_dict["full_text"] = full_text
		ret_dict["tree"] = self.tree
		ret_dict["links"] = self.links
		ret_dict["embed_links"] = self.embed_links
		ret_dict["images"] = self.img_links
		ret_dict["meta_info"] = self.meta_dict
		ret_dict["domain"] = self.s_domain
		ret_dict["html"] = self.org_html

		return ret_dict

	def getLink(self, line):
		
		low_line = line.lower()

		if line.startswith("http://"):
			return line

		hcur = low_line.find("location.href")
		if hcur >= 0:
			new_url = getValueOnly(line[hcur+13:], "=")
			if not new_url:
				new_url = getValueOnly(line[hcur+13:], "(")
			if new_url:
				if new_url.find(";") >= 0:
					return None
				else:
					try:
						new_cc = line[line.find(new_url)+len(new_url)+1:]
						if new_cc.strip()[0] ==  "+":
							return None
					except:
						pass
					if new_url.count("+") > 0 or new_url.find("(") >=0 or new_url.find(")") >= 0 or new_url =="href":
						return None
					n_url = self.makePerfectURL(new_url)
					return n_url
				
				
		else:
			hcur = low_line.find("location")
			if hcur >= 0:
				new_url = getValueOnly(line[hcur+8:], "=")
				if new_url:
					if new_url.find(";") >= 0 or new_url.find(",") >= 0:
						return None
					else:
						try:
							new_cc = line[line.find(new_url)+len(new_url)+1:]
							if new_cc.strip()[0] == "+":
								return None
						except:
							pass
						if new_url.count("+") > 0 or new_url.find("(") >=0 or new_url.find(")") >= 0 or new_url =="href":
							return None
						n_url = self.makePerfectURL(new_url)
						return n_url
			
		hcur = low_line.find("window.open")
		if hcur >= 0:
			k = re.compile("\(\s*\'.*?\'\||\(\s*\".*?\"").search(line[hcur+10:], 1)
			if k:
				tt = k.group()
				if tt.count("+") == 0 :
					nn = re.sub('[\'\";\(\)]', "", tt).strip()
					if len(nn) > 0:
						n_url = self.makePerfectURL(nn)
						return n_url
		hcur = low_line.find("location.replace")
		if hcur >= 0:
			k = re.compile("\(.*?\)\s*;").search(line[hcur+15:], 1)
			if k:
				tt = k.group()
				if tt.count("+") == 0 :
					nn = re.sub('[\'\";\(\)]', "", tt).strip()
					if len(nn):
						n_url = self.makePerfectURL(nn)
						return n_url

		return None


	def handle_starttag(self, tag, attrs):
		"""
		start tage handle for HTMLParser
		"""

		tag_upper = tag.upper()	
		if tag_upper in ignoreTags:
			return 
		if self.depth > 900:
			return 
		

		self.text = self.handle_text(self.text)
		if len(self.text) > 0:
			tNode = ParseNode("#text", [self.text])
			if self.checkParentNode(self.point, "SELECT"):
				tNode.attrs = []
			else:
				self.text_list.append(self.text)
			self.point.append(tNode)
			self.text = ""
		self.depth += 1
		tNode = ParseNode(tag, attrs)
		tmp_url = ""

		attr_dict = tNode.attr_dict
		if tag_upper in ["A", "TD", "AREA"]:
			if "href" in attr_dict:
				if self.nurl != "" :
					if not self.invisible:
						self.addLink(self.nurl, tag, self.anc_text, "", self.nurl_id)
					self.anc_text = ""
				self.nurl = attr_dict["href"]
				self.nurl_id = tNode.id

			if "onclick"  in attr_dict:
				self.ncall = attr_dict["onclick"]
				t_url = self.getLink(self.ncall)
				if t_url:
					self.nurl = t_url
					self.nurl_id = tNode.id

			if "title" in attr_dict:
				self.t_title =attr_dict["title"]
				
			if len(attrs) == 0 and tag_upper == "A":
				return 
			if tag_upper == "TD":
				if self.nurl != "" and self.point.parent_node.tag.upper() == "A":
					self.addLink(self.nurl, tag, self.anc_text, "", self.nurl_id )
					self.anc_text = ""
					self.start_tags["A"] -= 1
					self.point = self.point.parent_node		
					self.depth -= 1
				table_node = self.checkTableParentNode(self.point)
				if table_node and table_node.tag.upper() == "TD":
					self.start_tags["TD"] -= 1
					self.point = table_node.parent_node
					self.depth = table_node.depth
		elif tag_upper == 'HTML':
			for attr in attrs:
				if attr[0].lower().strip() == 'lang':
					self.lang = attr[1].strip()
		elif tag_upper in ["IFRAME","FRAME"]:
			if "src" in attr_dict:
				tmp_url = attr_dict["src"]
				tmp_text = ""
				if "alt" in attr_dict:
					tmp_text = attr_dict["alt"]
				self.addLink(tmp_url, tag, tmp_text, "", tNode.id)
		elif tag_upper in ["IMG"]:
			tmp_url = ""
			t_title = ""
			t_alt = ""
			t_width = None
			t_height = None

			for t_key, t_value in attrs:
				if t_key == "src" and t_value is not None:
					tmp_url = t_value.strip()
					if self.nurl != "":
						self.anc_text += tmp_url
				if t_key == "alt" and t_value != None:
					t_alt = t_value.strip()
				if t_key == "title" and t_value != None:
					t_title = t_value.strip()
				if t_key == "width" and t_value != None:
					try:
						t_width = int(t_value.replace(",", "").strip())
					except:
						pass
				if t_key == "height" and t_value != None:
					try:
						t_height = int(t_value.replace(",", "").strip())
					except:
						pass

			if tmp_url != "":
				tmp_url = self.makePerfectURL(tmp_url)
				if tmp_url in self.img_links:
					t_image_data = self.img_links[tmp_url]
				else:
					t_image_data = ImageData()
					self.img_links[tmp_url] = t_image_data
				t_image_data.node_ids.append(tNode.id)
				t_image_data.title = t_title
				t_image_data.alt = t_alt
				if t_width:
					t_image_data.width = t_width
				if t_height:
					t_image_data.height = t_height
				

		elif tag_upper in ["EMBED"]:
			t_type = "U"
			t_width = 0
			t_height = 0
			t_flashvar = ""
			for t_key, t_value in attrs:
				if t_key == "type":
					if t_value.find("x-mplayer2") >= 0:
						t_type = "M"
					elif t_value.find("x-java-applet") >= 0:
						t_type = "J"
					elif t_value.find("x-shockwave-flash") >= 0:
						t_type = "V"
				if t_key == "src" and t_value is not None:
					tmp_url = t_value.strip()
				if t_key == "width" and t_value != None:
					try:
						t_width = int(t_value.replace(",", "").strip())
					except:
						pass
				if t_key == "height" and t_value != None:
					try:
						t_height = int(t_value.replace(",", "").strip())
					except:
						pass
				if t_key == "flashvars":
					t_flashvar = t_value


			if tmp_url != "":
				try:

					if t_flashvar.find("type=video") >= 0 and t_flashvar.find("file=") >=0:
						if tmp_url.count("?") > 0:
							tmp_url = tmp_url+ t_flashvar
						else:
							tmp_url = tmp_url+"?"+ t_flashvar
					elif t_flashvar and t_flashvar.startswith("file=") and tmp_url.find("/mediaplayer.swf") >= 0:
						tt_cur =  t_flashvar.rfind(".flv")
						if tt_cur >= 0:
							tmp_url = t_flashvar[5:tt_cur+4]

					tmp_url = self.makePerfectURL(tmp_url)

					if tmp_url.find("copyurl.swf") >= 0 or tmp_url.find("clipboard.swf") >= 0:
						pass

					else:
						if tmp_url in self.embed_links:
							t_embed_data = self.embed_links[tmp_url]
						else:
							t_embed_data = EmbedData()
							self.embed_links[tmp_url] = t_embed_data
						t_embed_data.width = t_width
						t_embed_data.height = t_height
						t_embed_data.flashvar = t_flashvar
						t_embed_data.type = t_type
				except Exception, msg:
					getLogger().error(msg)

				self.addLink(tmp_url, tag, None)

		elif tag_upper in ["DIV","P","SPAN", "TEXTAREA"]:
			if self.nurl != "":
				if not self.invisible:
					self.addLink(self.nurl, "A", self.anc_text, "", self.nurl_id )
					self.nurl = ""
					self.nurl_id = 0
				self.anc_text = ""

			for attr in attrs:
				if attr[0].lower() == 'style':
					if attr[1].lower().find("display:none") >= 0 or attr[1].lower().find("visibility:hidden") >= 0:
						self.invisible =True
						if self.invisible_depth == None:
							self.invisible_depth = self.point.depth + 1

				if attr[0].lower() == "class":
					if attr[1].lower() in ["ad_area1"]:
						self.invisible = True
						if self.invisible_depth == None:
							self.invisible_depth = self.point.depth + 1
				if attr[0].lower() == "id":
					if attr[1].startswith("IAMCOMMENT"):
						self.invisible = False
						self.invisible_depth == None

		self.point.append(tNode)
		if tag_upper in ["BR", "INPUT"]:
			pass
		else:
			self.point = tNode
		if tag_upper in self.start_tags:
			self.start_tags[tag_upper] += 1
		else:
			self.start_tags[tag_upper] = 1
		###print self.depth , "START TAG" ,tag
	

	def checkParentNode(self, node, tag):
		t_node = node
		while t_node != None and t_node.parent_node != None:
			if t_node.tag.upper() == tag:
				return True
			t_node = t_node.parent_node
		return False

	def checkTableParentNode(self, node):
		t_node = node
		while t_node != None and t_node.parent_node != None:
			if t_node.tag.upper() in ["TR", "TD", "TABLE"]:
				return t_node
			else:
				t_node = t_node.parent_node
		return None
		


	def handle_text(self, text):
		"""
		if text[-1] == " " or text[-1] == "\t":
			text = " ".join(text.split()) + " "
		else:
		"""

		text = removeCtrChar(text)
		if len(text) == 0:
			return ""
		elif len(text.strip()) == 0:
			return ""

		if text[0] in string.whitespace:
			pre = " "
		else:
			pre = ""
		if text[-1] in string.whitespace:
			suf = " "
		else:
			suf = ""
		text = text.replace(u"\uFEFF".encode("utf8"), "")
		text = " ".join(text.split()) 
		text = pre + text + suf
		return text

	def handle_endtag(self, tag):
		"""
		end tag handle for HTMLParser
		"""
		tag_upper = tag.upper()	
		#self.text = " ".join(self.text.split())
		if tag_upper in ignoreTags:
			return
		if tag_upper in self.start_tags:
			if self.start_tags[tag_upper] > 0:
				if len(self.text) > 0:
					self.text = self.handle_text(self.text)
					if len(self.text) > 0:
						tNode = ParseNode("#text", [self.text])
						# form 밑에 TEXTAREA인 경우 추가 안함 
						"""
						if self.point.tag.upper() == "TEXTAREA":
							if self.checkParentNode(self.point,"FORM") :
								tNode.attrs = []
						"""
						if self.checkParentNode(self.point, "SELECT"):
							tNode.attrs = []
						if tag_upper != "TITLE":
							self.text_list.append(self.text)
								
						self.point.append(tNode)
				if tag_upper == "TITLE" and len(self.title) == 0:
					self.title = self.text
				elif tag_upper in ["A", "TD", "AREA"] :
					del_anc = False

					if self.nurl != "":
						self.anc_text = " ".join(self.anc_text.split())
						if self.nurl.find("fromscript:") >= 0:
							self.nurl = self.nurl[11:]
							self.addLink(self.nurl, "script", self.anc_text, self.t_title, self.nurl_id)
						else:
							self.addLink(self.nurl, tag, self.anc_text, self.t_title, self.nurl_id)
						self.nurl = ""
						self.nurl_id = 0
						self.t_title = None
						del_anc = True
					if self.ncall != "":
						self.anc_text = " ".join(self.anc_text.split())
#						self.script_call[self.ncall] = self.anc_text
						del_anc = True
						self.ncall = ""
					if del_anc:
						self.anc_text = ""
					
				loop_point = self.point
				
				i_count = 0

				while loop_point.tag != "root" and loop_point.tag.upper() != tag_upper:
					loop_point = loop_point.parent_node
					i_count += 1

				if i_count == 0 and loop_point.tag != "root":
					self.start_tags[tag_upper] -= 1
					self.point = self.point.parent_node
					self.depth -= 1
				elif loop_point.tag != "root":
					loop_point = self.point
					while loop_point.tag.upper() != tag_upper:
						if loop_point.tag.upper() == 'A':
							if self.nurl != "":
								tmp_text = loop_point.getTextUnderNode()[0]
								self.anc_text = " ".join(self.anc_text.split())
								if len(self.anc_text) > len(tmp_text):
									tmp_text = self.anc_text
								if self.nurl.find("fromscript:") >= 0:
									self.nurl = self.nurl[11:]
									self.addLink(self.nurl, "script", tmp_text)
								else:
									self.addLink(self.nurl, "A", tmp_text)
								self.nurl = ""
								self.nurl_id = 0
								self.anc_text = ""
						self.start_tags[loop_point.tag.upper()] -= 1
						loop_point = loop_point.parent_node
						self.depth -= 1
					self.start_tags[tag_upper] -= 1
					self.point = loop_point.parent_node		
					self.depth -= 1

				if tag_upper in ["DIV","P", "SPAN", "TEXTAREA"]:
					if self.invisible_depth > self.point.depth:
						self.invisible = False
						self.invisible_depth = None

				self.text = ""

			if tag_upper in ["TD", "TR", "TABLE"]:
				if self.point.tag.upper() == 'A':
					del_anc = False
					if self.nurl != "":
						tmp_text = self.point.getTextUnderNode()[0]
						self.anc_text = " ".join(self.anc_text.split())
						if len(tmp_text) < len(self.anc_text) :
							tmp_text = self.anc_text

						if self.nurl.find("fromscript:") >= 0:
							self.nurl = self.nurl[11:]
							self.addLink(self.nurl, "script", tmp_text)
						else:
							self.addLink(self.nurl, "A", tmp_text)
						self.nurl = ""
						self.nurl_id = 0
						self.anc_text = ""
						self.text = ""
					self.point = self.point.parent_node
					self.depth -= 1
					self.start_tags["A"] -= 1
		else:
			if tag_upper in ["TD", "TR", "TABLE"]:

				chk_point = self.point.parent_node
				if chk_point.tag.upper() == 'A':
					del_anc = False
					if self.nurl != "":
						tmp_text = chk_point.getTextUnderNode()[0]
						self.anc_text = " ".join(self.anc_text.split())
						if len(tmp_text) < len(self.anc_text) :
							tmp_text = self.anc_text

						if self.nurl.find("fromscript:") >= 0:
							self.nurl = self.nurl[11:]
							self.addLink(self.nurl, "script", tmp_text)
						else:
							self.addLink(self.nurl, "A", tmp_text)
						self.nurl = ""
						self.nurl_id = 0
						self.anc_text = ""
						self.text = ""
					self.point = chk_point.parent_node
					self.depth -= 2
					self.start_tags["A"] -= 1


	def handle_startendtag(self, tag, attrs): 
		"""
		start + end tag handler for HTMLParser
		"""

		tag_upper = tag.upper()	
		self.text = " ".join(self.text.split())
		self.text = self.handle_text(self.text)
		if len(self.text) > 0:
			tNode = ParseNode("#text",[self.text])
			self.text_list.append(self.text)
			self.point.append(tNode)
			self.text = ""

		tNode = ParseNode(tag,attrs)
		self.point.append(tNode)

		attr_dict = tNode.attr_dict

		if tag_upper == "META": 

			self.handle_meta(attr_dict)

		elif tag_upper == "AREA":
			if "href" in attr_dict:
				self.nurl = attr_dict["href"]
				if self.nurl != "":
					if self.nurl.find("fromscript:") >= 0:
						self.nurl = self.nurl[11:]
						self.addLink(self.nurl, "script", self.text)
					else:
						self.addLink(self.nurl, tag, self.text)
					self.nurl = ""
					self.nurl_id = 0
		elif tag_upper == "LINK":
			isRSS = False
			text = ""
			if "href" in attr_dict:
				self.nurl = attr_dict["href"]
			elif "type" in attr_dict and attr_dict["type"].lower() in [ "application/rss+xml", "application/atom+xml"]:
				isRSS = True
			elif "title" in attr_dict:
				text = attr_dict["title"]
					
			if self.nurl != "":
				if isRSS:
					self.addLink(self.nurl, tag, self.anc_text, "", self.nurl_id)
				else:
					try:
						if self.nurl not in self.links:
							self.addLink(self.nurl, tag, self.anc_text, "", self.nurl_id)
							#self.links[self.nurl] = list()
						self.links[self.nurl].append(LinkURL(tag, "IN|R", text, "", tNode.id))
					except:
						pass
				self.nurl = ""
		elif tag_upper in ["IFRAME","FRAME"]:
			if "src" in attr_dict:
				tmp_url = attr_dict["src"]
				tmp_text = ""
				if "alt" in attr_dict:
					tmp_text = attr_dict["alt"]
				self.addLink(tmp_url, tag, tmp_text, "", tNode.id)


	def handle_meta(self,attr_dict):
		"""
		process meta tag 
		"""
		try:

			if "content" in attr_dict:
				#key = ""
				value = attr_dict["content"]
				if "name" in attr_dict:
					key = attr_dict["name"].lower() 
				elif "property" in attr_dict:
					key = attr_dict["property"].lower()
				elif "http-equiv" in attr_dict:
					key = attr_dict["http-equiv"].lower()
				elif "itemprop" in attr_dict:
					key = attr_dict["itemprop"].lower()

				if key == "content-type":
					try:
						find_position = value.lower().find("charset=")
						if find_position > 0:
							char_set = value[find_position+len("charset="):]
							self.meta_dict["charset"] = char_set
					except Exception, msg:
						getLogger().error(msg)
				elif key == "refresh":
					try:
						find_position = value.lower().find("url=")
						if find_position > 0:
							ret_url = value[find_position+len("url="):]

							#self.addLink(ret_url, "REFRESH")
							ret_url = self.makePerfectURL(ret_url)
							self.meta_dict["refresh_url"] = ret_url
					except Exception, msg:
						getLogger().error(msg)
				elif key in ["robots", "zumbot"]:
					robots_exclusion = value.upper()
					if robots_exclusion.find("NONE") >= 0:
						self.robots_exclusion["INDEX"] = self.robots_exclusion["FOLLOW"] = False
					else:
						if robots_exclusion.find("NOINDEX") >= 0:
							self.robots_exclusion["INDEX"] = False
						if robots_exclusion.find("NOFOLLOW") >= 0:
							self.robots_exclusion["FOLLOW"] = False
				else:
					self.meta_dict[key] = value
		except Exception, msg:
			getLogger().error("may key error %s",str(attr_dict))


	def handle_data(self, data):
		"""
		data handler for HTMLParser
		"""
		if data != None:
			data = data.replace("|11818|", "&")
			#if not self.invisible:
			self.text += data
			if self.nurl != "":
				self.anc_text += data
			elif self.ncall != "":
				self.anc_text += data

	def feed(self, full_html):
		"""
		feed html to HTMLParser
		"""
		HTMLParser.feed(self, full_html)


	def setTreeScore(self):
		"""
		set Tree score
		"""
		self.tree.root.setScoreTextNum()


	def cleanQuery(self, query, trimargs=None, board_type=None):
		"""
		clear trimargs in query
		this function will be outside for manage URL item
		"""

		if trimargs == None:
			trimargs = DEF_TRIM_QUERY
		
		q_cur = query.find("#")
		if q_cur > 0:
			query = query[:q_cur]
		query_segments = query.split("&")
		querys = []
		for segment in query_segments:
			items = segment.split("=")
			if len(items) > 1:
				querys.append((items[0], items[1]))
			else:
				querys.append((items[0], None))
		
		querys.sort()

		new_query = []

		for key, value in querys:
			add_value = True
			if value == None:
				new_query.append("%s" % key)
			else:
				for trimarg in trimargs:
					if key.lower().find(trimarg.lower()) >= 0 or \
						value.lower().find(trimarg.lower()) >= 0 or \
						key.lower() == "ssid":
						add_value = False
						break

				if add_value:
					new_query.append("%s=%s" % (key, value))

		return "&".join(new_query)


	
	def makePerfectURL(self, url):
		try:
			url = removeCtrChar(url).strip()

			if len(url.strip()) == 0:
				return ""
			if self.charset:
				url = quote(url.strip().decode("utf8", "replace").encode(self.charset, "xmlcharrefreplace"), safe=RESERVED)
			else:
				url = quote(url.strip(), safe=RESERVED)
			url = url.replace("\\","/")
			if url.startswith("?"):
				parsed_url = urlparse.urlparse(self.base_url) 
				page = parsed_url[2]
				url = page + url

			url = urlparse.urljoin(self.base_url, url).replace("&amp;", "&")

			if url.startswith("http://clickstat.hanafos.com"):
				tt = url.find("hdurl=")
				if tt > 0:
					url = url[tt+6:]
			url = url.replace("http://http://", "http://")
			url = url.replace("http://https://", "http://")
			while url.find("/../") >= 0:
				url = url.replace("/../","/")
			
			while url.find("/./") >= 0:
				url = url.replace("/./","/")
			
			parsed_url = urlparse.urlparse(url)
			nn_cur= parsed_url[1].find("@")
			if nn_cur >= 0:
				new_pp = parsed_url[1][nn_cur+1:]
			else:
				new_pp = parsed_url[1]
			url = urlparse.urlunparse((parsed_url[0], new_pp, parsed_url[2].replace("//", "/"), parsed_url[3], parsed_url[4],''))
			if parsed_url[2] == "":
				url += "/"

			return url
		except Exception, msg:
			getLogger().error("%s %s", msg, url)
			return url


	def addLink(self, url, tag,  url_desc=None, title=None, node_id=None):
		"""
		find link and add url link
		"""

		url = url.strip().replace("|11818|", "&")
		if url == "":
			return 

		try:
			if url[0] == "#" or url.lower().find("mailto") >= 0  or url.lower().find("javascript:") >= 0:
				if url.lower().find("javascript:") >= 0:
					t_cur = url.find("(")
					if t_cur >= 11:
						func_name = url[11:t_cur]
						if func_name.startswith("window.") or func_name.startswith("void") or func_name.startswith("document") or func_name.startswith("alert") or func_name.find("mail") >=0 or func_name.startswith("menu"):
							pass
						elif url_desc and len(url_desc) > 5:
							if func_name in self.script_call:
								self.script_call[func_name] += 1
							else:
								self.script_call[func_name] = 1
				return 
			url = self.makePerfectURL(url)

			if not url.startswith("http"):
				return 

			parsed_url = urlparse.urlparse(url)
			
			page = parsed_url[2]
			pcur = page.find("//")
			while pcur >= 0:
				page = page.replace("//", "/")
				pcur = page.find("//")
			parameters = self.cleanQuery(parsed_url[4])

			n_url = urlparse.urlunparse((parsed_url[0], parsed_url[1], page, parsed_url[3], parameters, ""))

			cur = parsed_url[2].rfind(".")
			is_added = False

			# check null link
			if url.strip() == "about:blank" or url.strip().startswith("java"):
				is_added = True

			# step 1 : check in/out
			if parsed_url[1].find("@") >= 0:
				is_added = True
			if parsed_url[1].endswith("."+self.s_domain) or parsed_url[1] == self.s_domain:
				url_inout = "IN"
			else:
				url_inout = "OUT"

			# step 2 : check anchor text
			anchor_text = ""
			if url_desc and len(url_desc.strip()) > 0:
				anchor_text = url_desc.strip()
				
			# check link is image
			if not is_added and cur >= 0:
				ext_name = parsed_url[2][cur+1:].strip()
				if ext_name.lower() in DEF_DISCARD_EXT:
					is_added = True
				elif ext_name.lower() in DEF_IMG_EXT :
					#self.img_links.add(LinkURL(url, tag, url_inout, anchor_text, ""))
					is_added = True
				"""
				if tag.lower() == 'img' and not is_added:
					self.img_links.add(LinkURL(url, tag, url_inout, anchor_text, ""))
					is_added = True
				"""

				# normal urls
			if not is_added:
				if url not in self.links:
					self.links[url] = list()
				self.links[url].append(LinkURL(tag, url_inout, anchor_text, title, node_id)  )
		except Exception, msg:
			getLogger().error("%s %s", msg, url)


	def quoteCheck(self, tstr):

		"""
			need to add META handle
		"""
		
		s_quote_open = False
		d_quote_open = False
		tag_open = False
		skipping = False

		n_str = ""

		#tstr = tstr.replace("“",  '"').replace(" ? ", " ")
		old_a = ""

		for a in tstr:
			if tag_open:  # 태그 내 일때 
				if not d_quote_open and not s_quote_open: # "나 '가 열리지 않은 경우
					if skipping:
						if a == ">":
							n_str += a
							tag_open = False
							skipping = False
					elif old_a in ["'",'"'] and not a in [" ", "\t", ">", "\n", ")", ";", ","]:
						skipping = True
					elif a == "<":           
						n_str += "&lt;"
					elif a == '>':
						tag_open = False
						n_str += a
					elif a == "'":
						if old_a in [" ", "\t", "=", "(", '\n', ","]:
							s_quote_open = True
							n_str += a
					elif a == '"':
						if old_a in [" ", "\t", "=", "(", '\n', ","]:
							d_quote_open = True
							n_str += a

					elif old_a == ";" and not a in  [" ", "\t", ">", "\n", ")" ]:
						n_str += a
					else:
						n_str += a
				elif d_quote_open: # "가 열린 경우 
					if a == '"': #and old_a != '\\':
						d_quote_open = False
						n_str += '"'
					else:
						n_str += a
				elif s_quote_open:
					if a == "'": # and old_a != '\\':
						s_quote_open = False
						n_str += "'"
					else:
						n_str += a
				else:
					n_str += a
			else:
				if a == "<":
					tag_open = True
				n_str += a

			old_a = a

		n_str = n_str[1:-1]
		
		if tag_open:
			return (False, "")
		else:
			return (True, n_str)
	
	def handleMeta(self, substance):

		try:
			k = re.search("""http-equiv([ ]*)=([ ]*)"([^"]*)"|http-equiv([ ]*)=([ ]*)'([^']*)'|http-equiv([ ]*)=([ ]*)([^"'\s]*)""",substance,re.I|re.X)
			if not k:
				k = re.search("""name([ ]*)=([ ]*)"([^"]*)"|name([ ]*)=([ ]*)'([^']*)'|name([ ]*)=([ ]*)([^"'\s]*)""",substance,re.I|re.X)
			if not k:
				k = re.search("""property([ ]*)=([ ]*)"([^"]*)"|property([ ]*)=([ ]*)'([^']*)'|property([ ]*)=([ ]*)([^"'\s]*)""",substance,re.I|re.X)
			if k:
				p = re.search("""content([ ]*)=([ ]*)"([^"]*)"|content([ ]*)=([ ]*)'([^']*)'""",substance,re.I|re.X)
				if p:
					if k.group().find( "description") < 0:
						ccc = p.group().replace("&lt;", "<").replace("&gt;",">")
						piece = "META "+k.group() +" "+  ccc + " /"
						return piece
					elif k.group().find("og:description") >=0:
						ccc = p.group().replace("&lt;", "<").replace("&gt;",">")
						piece = "META "+k.group() +" "+  ccc + " /"
						return piece
		except Exception,msg:
			logger.error("meta tag error %s", msg)
		return substance


	def preProcessTags(self, text):
		"""
		pre processing for several tags
		"""
		
		a = time.time()

		text = text.replace("&#8203;", "")
		text = self.removeStag(text)


		

		text = convertToUTF8(text)
		text = text.replace("&nbsp;"," ")
		text = text.replace("&nbsp"," ")
		text = text.replace("&amp;nbsp;", " ")
		text = text.replace("&amp;","|11818|")
		text = text.replace("\xe3\x80\x80", " ")
		text = text.replace("
", "")
		text = text.replace("&#124;","|")
		text = text.replace("&#160;"," ")
		text = text.replace("&#8211;","-")
		text = text.replace("&#.", "")
		text = text.replace("&#39;", "'")
		text = text.replace("&#40;", "(")
		text = text.replace("&#41;", ")")
		text = text.replace("&#x28;", "(")
		text = text.replace("&#x29;", ")")
		text = text.replace("&#63;", "?")
		text = text.replace("&#58;", ":")
		text = text.replace("&#039;", "'")
		text = text.replace("&#xA;", "'")
		text = text.replace("&#060;", "<")
		text = text.replace("&#062;", ">")
		text = text.replace("&#62;", ">")
		text = text.replace("&#60;", "<")
		text = text.replace("&mdash;", "-")
		text = text.replace("&", "|11818|")


		b = time.time()
		cur = text.find("<")
		ecur = text.find(">",cur)
		chkcur = text.find("<", cur+1)
		length = len(text)
		logger = getLogger()

		piece_list = list()
		loop_count = 0
		while cur >=0 and ecur >= 0 and cur < length :
			if loop_count % 500 == 0: time.sleep(0.00001)
			loop_count += 1

			if not text[cur+1] in string.ascii_lowercase and not text[cur+1] in string.ascii_uppercase and not text[cur+1] in '!/?':  # 태그 시작이 알파벳이나 !, / 가 아니면 무시
				if chkcur > 0:
					substance = text[cur:chkcur]
				else:
					substance = text[cur:]
				#substance = substance.replace("<"," &lt;").replace(">"," &gt;")
				piece = substance 
				piece_list.append(piece)
				cur = chkcur
				ecur = text.find(">", cur)
				chkcur = text.find("<", cur+1)

			else:
				substance = text[cur+1:ecur]
				(result, substance) = self.quoteCheck(text[cur:ecur+1])
				while not result:
					n_ecur = text.find(">", ecur+1)
					(result, substance) = self.quoteCheck(text[cur:n_ecur+1])
					ecur = n_ecur
					chkcur = text.find("<", ecur)

				if len(substance) > 0 :
					k = substance.split()
					tag_upper = k[0].upper()
					st = tag_map.get(tag_upper, -1)
					if tag_upper == "META":
						substance = self.handleMeta(substance)
					if tag_upper == "A":
						substance = rep_javascript(substance, self.base_url)
					if tag_upper in ALL_TAG:
						piece_list.append("<"+substance+">")
					elif k[0].find("=") >=0 :
						pass
					elif k[0][0] == "%" :
						pass
					elif k[0][0] == "/":
						try:
							if k[0] == "/":
								if len(k) > 1:
									tmp_str = k[1]
								else:
									tmp_str = ""
							else:
								tmp_str = k[0][1:]
							# 닫는 태그의 경우 ALL_TAG에 있으면 남기고 없으면 날림 
							if  tmp_str.upper() in ALL_TAG:
								piece =  "</" + tmp_str + ">" 
								piece_list.append(piece)
						except Exception, msg:
							logger.error(msg)

				if chkcur >= 0:
					if chkcur > ecur:
						chkcur = text.find("<", ecur)
					piece_list.append(text[ecur+1:chkcur])
				cur = chkcur
				ecur = text.find(">",cur)
				chkcur = text.find("<",ecur+1)

		text = "".join(piece_list)	
		return text



def removeQuery(url):
	try:
		t_dict = dict()
		parsed_url = urlparse.urlparse(url)
		parameter = parsed_url[4]

		page = parsed_url[1]+parsed_url[2]
		if page.startswith("www."):
			page = page[4:]

		if len(parameter) == 0:
			return parsed_url[0] + "://" + page
		para_pieces = parameter.split("&")
		for para in para_pieces:
			res = para.split("=")
			para_name = res[0]
			para_value = ''
			t_dict[para_name] = para_value

		remove_key = set()
		for para in DEF_TRIM_QUERY:
			for key in t_dict:
				if key.lower().find(para) >= 0:
					remove_key.add(key)

		for key in remove_key:
			if key in t_dict:
				del t_dict[key]

		paras = ""
		items = t_dict.items()
		items.sort()
		for k,v in items:
			if len(paras) == 0:
				paras = k + "=" + v
			else:
				paras = paras + "&" + k + "=" + v
		if len(paras) == 0:
			n_url = parsed_url[0] + "://"+ page
		else:
			n_url = parsed_url[0] + "://"+ page +"?"+paras
		return n_url
	except Exception,msg:
		return url


def getScore(text):
	
	score = 0 
	text = text.strip()
	for c in text:
		if c in string.punctuation + string.whitespace:
			pass	
		else:
			score += 1

	return score


def getValueOnly(text, skey):
	quote_type = ""
	hcur = text.find(skey)

	if hcur >= 0 and hcur < 4:
		candi = text[hcur+1:].strip()
		if candi[0] == "'":
			quote_type = "'"
			lin = candi[1:].strip().split("'")
		elif candi[0] == "\"":
			quote_type = "\""
			lin = candi[1:].strip().split("\"")
		else:
			quote_type = "\""
			lin = candi.split()
		if len(lin[0]) > 0:
			return lin[0]
	return None

def getValue(text, key, only_val=False):
	"""
	retrive value in the text
	"""
	quote_type = ""
	text = text.replace("\n"," ")
	hcur = text.lower().find(" "+key+"=")
	if hcur < 0:
		hcur = text.lower().find(" "+key+" =")
		if hcur < 0:
			hcur = text.lower().find(" "+key+"\t=")
	
	if hcur >= 0:
		try:
			candi = text[hcur+len(key)+1:].strip()
			if candi[0] == "=" and len(candi) > 2:
				candi = candi[1:].strip()
				if candi[0] == "'":
					quote_type = "'"
					lin = candi[1:].strip().split("'")
				elif candi[0] == "\"":
					quote_type = "\""
					lin = candi[1:].strip().split("\"")
				else:
					quote_type = "\""
					lin = candi.split()
				if len(lin[0]) > 0:
					ret_val = key + "=" + quote_type + lin[0] + quote_type
					if only_val:
						return lin[0]
					else:
						return ret_val
		except Exception, msg:
			getLogger().error("%s",msg)
			return ""
	return ""

		
		
def getEncoding(full_html):
	support_encodings = ["utf8", "euckr", "cp949", "shift_jis", "eucjp"]  #, "big5", "big5hkscs", "cyrillic", "koi8_r", "utf_16", "cp1361", "arabic", "cp950", "cp500"]
	try:
		unicode_html = unicode(full_html, "utf8")
		return "utf8"
	except:
		decoded = False
		for trial_encoding in support_encodings:
			try:
				unicode_html = unicode(full_html, trial_encoding)
				return trial_encoding
			except:
				decoded = False
				continue
			else:
				decoded = True
				break
		if not decoded:
			return "euckr"

def makePerfectURL(url, base_url, charset=None):
	try:
		url = removeCtrChar(url)
		if charset:
			url = quote(url.strip().decode("utf8", "replace").encode(self.charset, "xmlcharrefreplace"), safe=RESERVED)
		else:
			url = quote(url.strip(), safe=RESERVED)
		url = url.replace("\\","/")

		if url.startswith("?"):
			parsed_url = urlparse.urlparse(base_url) 
			page = parsed_url[2]
			url = page + url
		url = urlparse.urljoin(base_url, url).replace("&amp;", "&")
		url = url.replace("http://http://", "http://")
		url = url.replace("http://https://", "http://")

		while url.find("/../") >= 0:
			url = url.replace("/../","/")

		parsed_url = urlparse.urlparse(url)
		nn_cur= parsed_url[1].find("@")
		if nn_cur >= 0:
			new_pp = parsed_url[1][nn_cur+1:]
		else:
			new_pp = parsed_url[1]
		url = urlparse.urlunparse((parsed_url[0], new_pp, parsed_url[2].replace("//","/"), "", parsed_url[4],''))
		if parsed_url[2] == "" and parsed_url[3] == "" and parsed_url[4] == "":
			url += "/"
		return url
	except Exception, msg:
		getLogger().error(msg)
		return url


def removeCtrChar(text):
	try:
		text = text.translate(MAP, TRUNCMAP)
		return text
	except Exception, msg:
		return text

MAP = "".join([chr(cc) for cc in range(256)])
TRUNCMAP = "".join([chr(cc) for cc in range(32)])


__all__ = ["HttpParser", "LinkURL"]

if __name__ == "__main__":

	import socket 
	import sys
	import optparse
	import urllib2

	socket.setdefaulttimeout(10)
	url = "http://zum.com/"

	cmdparser = optparse.OptionParser(usage='%prog [options] [args]' )
	cmdparser.add_option('-d', '--debug', dest='isdebug', default=False, action='store_true',
							help='debug mode. (disabled by default)')

	cmdparser.add_option('-t', '--showtree', dest='showtree', default=False, action='store_true',
							help='show tree mode. (disabled by default)')

	cmdparser.add_option('-k', '--showhtml', dest='showhtml', default=False, action='store_true',
							help='show html mode. (disabled by default)')

	cmdparser.add_option('-o', '--showorghtml', dest='orghtml', default=False, action='store_true',
							help='show orghtml mode. (disabled by default)')

	cmdparser.add_option('-l', '--showlink', dest='showlink', default=False, action='store_true',
							help='show link mode. (disabled by default)')


	cmdparser.add_option('-e', '--showheader', dest='showheader', default=False, action='store_true',
							help='show header mode. (disabled by default)')


	(cmdoptions, cmdargs) = cmdparser.parse_args()
	isDebug = cmdoptions.isdebug
	if len(cmdargs) > 0:
		url = cmdargs[0]

	
	# for test using Document class
	#url = quote(url.strip().decode("utf8", "replace").encode("euckr", "xmlcharrefreplace"), safe=RESERVED)
#	referer_url = "http://" + urlparse.urlparse(url)[1] + "/"

	opener = urllib2.build_opener(urllib2.HTTPRedirectHandler(), urllib2.HTTPCookieProcessor() )
	#opener = urllib2.build_opener()
	req = urllib2.Request(url)
	#req.add_header("User-agent", "Mozilla/5.0 (Windows NT 6.1; Trident/7.0; WOW64; compatible; http://help.zum.com/inquiry) like Gecko")
	#req.add_header("User-agent", "Mozilla/5.0 (Windows NT 6.1;WOW64;Trident/7.0;rv:11.0) like Gecko")
	req.add_header("User-agent", USER_AGENT)
	rs = opener.open(req)

	http_header = str(rs.info())
	http_content = rs.read()
	real_URL = rs.url
	print rs.code, real_URL, http_header

	#http_content = rs.read()
	id = None
	attr_dict = dict()

	logger = getLogger()
	logger.debug("OK Parsing Start... [%s] id : %s", real_URL, id)

	#
	# start parser test 
	#
	script_pattern = None
	parser = HttpParser()

	print "SCRIPT PATTERN : ", script_pattern

	t1 = time.time()

	try:
		ret_dict = parser.plugParser(http_header, http_content, real_URL)

		if cmdoptions.showheader:
			print http_header.decode("euc-kr").encode("utf8")
		if cmdoptions.orghtml:
			print parser.html_parser.getOrgHTML()
		if cmdoptions.showhtml:
			print parser.html_parser.getHTML()

#		print http_content
	except Exception, msg:
		getLogger().error( "Exception ::%s" % msg)

	sys.stdout.write("Parser spent: %.3f seconds.\n" % (time.time()-t1))
	sys.stdout.flush()


	from urlpattern import UrlFactory
	a = UrlFactory()


	domain = ret_dict["domain"]

	for key, val in ret_dict.items():
		print key
		if key == "tree":
			tree = val
	
			if cmdoptions.showtree:
				print tree.root.reprTree()
			"""
			for id, node_list in tree.id_position_dict.items():
				print id, node_list
			for id, node_list in tree.class_position_dict.items():
				print id, node_list
			"""
		elif key == "images":
			images = val
			"""
			for image in images:
				print image, images[image].node_ids
			"""
		elif key == "html":
			pass
			"""
			images = val
			for image in images:
				print image, images[image].node_ids
			"""
		elif key == "links":  # and cmdoptions.showlink:
			for url, u_list in val.items():
				ret = a.getGuid(url)
				if ret:
					(u_type, guid, sub_info) = ret 
					print u_type, guid, sub_info
				else:
					print url
				print u_list

		elif key == "embed_links":  # and cmdoptions.showlink:
			for url, embed_data in val.items():
				print url, embed_data.width


		else:
			print key, val

	

