#!/bin/env python
# coding: utf8
"""
Management for URL
"""

import re, os
from urlparse import urlparse
from bbspattern import removeGarbage


URL_TABLE_NAME = "url_%02x"


URL_TYPE_TP = "TP" # Site Top
URL_TYPE_DR = "DR" # Site Directory
URL_TYPE_ST = "ST" # Static page
URL_TYPE_DY = "DY" # Dynamic URL
URL_TYPE_BC = "BC" # BBS control page
URL_TYPE_BT = "BT" # BBS list top page
URL_TYPE_BL = "BL" # BBS list page
URL_TYPE_BV = "BV" # BBS view page
URL_TYPE_SR = "SR" # Search Result
URL_TYPE_ES = "ES" # Expect Search Result
URL_TYPE_E1 = "E1" # Expect Search Result E1
URL_TYPE_E2 = "E2" # Expect Search Result E2
URL_TYPE_E3 = "E3" # Expect Search Result E3 (/tag/ type)
URL_TYPE_SP = "SP" # Shopping


DEF_SEARCH_RESULT_DOMAINS = ("tab.search.daum.net","book.daum.net","search.daum.net",\
						"web.search.daum.net","search.empas.com","website.search.daum.net",\
						"searchplus.nate.com","staff.eco.or.kr","help.empas.com",\
						"froogle.google.com","rd.empas.com","krd.empas.com","irana.dreamwiz.com",\
						"pie.daum.net","www.yesform.com","www.rollyo.com","dr.movist.com",\
						"maps.google.com","search.nate.com","chinese.gg.go.kr","search.allblog.net",\
						"search.interpark.com","froogle.com","alltheweb.com","japanese.gg.go.kr",\
						"english.gg.go.kr","kr.srd.yahoo.com","japanese.kg21.net","nrd.empas.com",\
						"clickstat.hanafos.com","www.yes24.com", "www.google.co.kr",\
						"search.moneytoday.co.kr", "ws.daum.net", "hanaro.digitalnames.net",\
						"gimai.com", "www.infomaster.co.kr", "dns.paran.com")
DEF_SEARCH_RESULT_QUERY_KEYS = ("keyword", "kw", "q", "query", "qu", "searchword")


TLDs = ["com", "net","org","edu","int", "biz", "info","name", "pro", "museum", "gov", "mil", "art", "aero","coop", "co", "mobi", "asia", "travel", "jobs"]
ccTLDs = ["bn","mm","li","pt","gb","jo","es","py","ki","ne","ar","tg","bf","mu","la","lv","ro","fm","cc","tw","im","me","rw","pl","tn","kz","sy","gs","mz","ir","vn","ie","ga","ml","pr","au","bv","kh","tc","lr","bo","td","cr","ci","er","gi","mt","jp","nf","do","tt","vg","si","il","md","az","to","ru","pe","gr","cz","��.ac","ky","id","no","cy","at","gy","bh","mo","am","nr","eu","ch","ua","kw","bw","lt","ng","pk","np","io","ee","kp","kg","bg","tl","fo","ca","it","gq","hm","mg","ae","ht","aw","tz","cx","sb","co","nu","bi","gg","mn","al","de","et","sn","ph","wf","re","pm","fi","cg","mw","uy","gh","sa","tr","lu","vi","sg","in","mf","ad","tm","hu","ug","kn","us","gp","nz","cn","ao","gf","rs","na","cw","pa","sm","bq","bj","mq","sj","fj","om","cf","vc","ke","ba","mv","hk","ma","dm","eg","hr","ag","tj","ni","km","by","gw","cm","uz","um","bz","st","aq","cv","su","dz","ge","mp","an","br","sl","gn","dj","se","bb","my","fr","tk","iq","tp","mh","af","ss","gu","eh","bl","cu","pg","qa","va","cl","ms","lc","ai","gd","nc","ve","sk","bs","bd","mc","dk","pf","je","ws","fk","vu","gm","mx","so","uk","sh","sd","mk","lk","th","jm","ps","sr","gt","bm","ls","ck","sx","pn","bt","ly","as","tf","ec","kr","be","mr","ax","tv","cd","sz","hn","gl","lb","sv","sc","pw","nl","is"]
SLDs = ["co","go","ac","nm","ne","or","re","kg","es","ms","hs","sc","pe"]



class URLUtil:
	"""
	URL management class
	"""

	def __init__(self, url=None):
		"""
		initialize instance
		"""
		self.url = None
		self.scheme = None
		self.netloc = None
		self.path = None
		self.params = None
		self.query = None
		self.fragment = None

		self.re_bc = None
		self.re_sr = None
		self.re_sp = None

		self.url_type = None

		if url:
			self._setURL(url)


	def _createRE(self):
		"""
		Regular expression을 이용하여 url type detect하기 위한 re 생성
		"""
		# 게시글을 조작하는 형태 "BC": action=, mode= 파라미터 중 특정한 값을 갖는 경우
		self.re_bc = re.compile('^.*?(action|mode|command|query|type)=(write|write_form|edit|mod|del|remove|reply|add|input|email|.*email$).*?$')

		# 쇼핑 페이지
		self.re_sp = re.compile('^.*?(store|mall|shopp?)(ing)?$')

		# 검색형 페이지
		# 2009-04-15 start with 'search' 
		self.re_sr = re.compile('(search)(ing)?(web)?(\..*)?$')


	def _setURL(self, url):
		assert type(url) in (str, unicode)

		# 2009-03-10
		# bbspattern 적용 (bbs list top, list, view detect)
		refine_url, self.url_type = removeGarbage(url)
		if refine_url != url:
			url = refine_url

		self.url = url.strip().lower()
		(self.scheme, self.netloc, self.path, self.params, self.query, self.fragment) = urlparse(self.url)

		self.query_fields = self.query.split("&")
		self.query_dict = dict([(query_field.split("=")[0], query_field.split("=")[1]) for query_field in self.query_fields if len(query_field.split("=")) > 1])


	def __str__(self):
		return self.__repr__()


	def __repr__(self):
		template = """
				URL : %(url)s
				SCHEME : %(scheme)s
				NETLOC : %(netloc)s
				PATH : %(path)s
				PARAMS : %(params)s
				QUERY : %(query)s
				FRAGMENT : %(fragment)s
				""".replace("\t", "")

		return template % {"url": self.url,
							"scheme": self.scheme,
							"netloc": self.netloc,
							"path": self.path,
							"params": self.params,
							"query": self.query,
							"fragment": self.fragment}


	def getURLTypeRSS(self, url=None):
		"""
		x.getURLType(...) -> URL_TYPE_STRING

		doc_ID가 어떤 URL인지 return

		"TP" : Site Top (Web Search)
		"ST" : Static Page (확장자가 있고, 쿼리가 없을 경우)
		"DY" : ETC (default)
		"""
		if url:
			self._setURL(url)

		assert self.url != None

		if self._isSiteTop():
			# Site Top
			type_name = URL_TYPE_TP
		elif self._isStaticRSS():
			# Static Page
			type_name = URL_TYPE_ST
		else:
			# Dynamic Page
			type_name = URL_TYPE_DY

		return type_name


	def getURLType(self, url=None, title=None):
		"""
		x.getURLType(...) -> URL_TYPE_STRING

		doc_ID가 어떤 URL인지 return

		"NA" : Not available
		"BC" : BBS control page
		"CD" : Cafe (UCC search)
		"BL" : Blog (UCC search)
		"DY" : Dynamic URL (Web Search)
		"SR" : Search Result (Web Search)
		"ES" : Expect Search Result
		"E#" : Expect Search Result Extended (0~9)
		"SP" : Shopping
		"TP" : Site Top (Web Search)
		"DR" : Site Directory (Web Search)
		"ST" : Static Page
		"""
		if url:
			self._setURL(url)

		assert self.url != None

		if not self.re_bc or not self.re_sr or not self.re_sp:
			self._createRE()

		if self._isDynamicURL():
			if self._isSearchResult():
				# Search Result
				type_name = URL_TYPE_SR
			elif self._isExpectSearchResult():
				# Expect Search Result
				type_name = URL_TYPE_ES
			elif self._isExpectedSearchE1Result(title):
				# Expect Search Result E1
				type_name = URL_TYPE_E1
			elif self._isExpectedSearchE2Result():
				# Expect Search Result E2
				type_name = URL_TYPE_E2
			elif self._isExpectedSearchE3Result():
				# Expect Search Result E3
				type_name = URL_TYPE_E3
			elif self._isBBSControl():
				# BBS Control
				type_name = URL_TYPE_BC
			elif self.url_type in ["listtop", "list", "view"]:
				# BBS pattern
				if self.url_type == "listtop":
					type_name = URL_TYPE_BT
				elif self.url_type == "list":
					type_name = URL_TYPE_BL
				else:
					type_name = URL_TYPE_BV
			elif self._isShopping():
				# Shopping Page
				type_name = URL_TYPE_SP
			else:
				# Dynamic URL
				type_name = URL_TYPE_DY
		else:
			if self._isSiteTop():
				# Site Top
				type_name = URL_TYPE_TP
			elif self._isExpectSearchResult():
				# Expect Search Result
				type_name = URL_TYPE_ES
			elif self._isExpectedSearchE3Result():
				# Expect Search Result E3
				type_name = URL_TYPE_E3
			elif self._isSiteDirectory():
				# Site Directory
				type_name = URL_TYPE_DR
			elif self._isNumberPage():
				# Dynamic URL
				type_name = URL_TYPE_DY
			else:
				# Static Page
				type_name = URL_TYPE_ST

		return type_name

	
	def _isNumberPage(self):
		"""
		self._isNumberPage() -> BOOL

		self.url이 숫자로된 페이지 일 경우 Dynamic
		"""
		paths = self.path.split("/")
		if len(paths) > 0 and paths[-1].isdigit():
			return True
		return False


	def _isBBSControl(self):
		"""
		self._isBBSControl() -> BOOL

		self.url이 BBS 조작형태인지 판단
		"""
		for query in self.query.split("&"):
			if self.re_bc.match(query) is not None:
				return True
		if len(self.query) > 0 and self.path.find("print.") >= 0:
			return True
		return False


	def _isShopping(self):
		"""
		self._isShopping() -> BOOL

		self.url이 Shopping형태인제 판단
		"""
		if self.query <> "":
			for sub_netloc in self.netloc.split("."):
				if self.re_sp.match(sub_netloc) is not None:
					return True
		return False
		

	def _isDynamicURL(self):
		"""
		self._isDynamicURL() -> Bool

		self.url이 동적URL인지 판단
		"""
		#if len("".join([self.params, self.query, self.fragment])) > 0:
		if len("".join([self.params, self.query])) > 0:
			return True

		return False


	def _isSearchResult(self):
		"""
		self._isSearchResult() -> Bool

		self.url이 검색 결과 URL인지 판단
		"""
		# step1: 기존 방법
		if self.netloc in DEF_SEARCH_RESULT_DOMAINS or \
			self.netloc.find("search") >= 0 or \
			self.netloc.find("incruit.com") >= 0:
			for search_query_key in DEF_SEARCH_RESULT_QUERY_KEYS:
				if self.query_dict.has_key(search_query_key) and len(self.query_dict[search_query_key]) > 0:
					return True

		# step2: RE 방법
		if self.query <> "":
			for sub_netloc in self.netloc.split("."):
				if self.re_sr.match(sub_netloc) is not None:
					return True

		for sub_path in self.path.split("/"):
			if self.re_sr.match(sub_path) is not None:
				return True

		return False


	def _isExpectSearchResult(self):
		"""
		self._isExpectSearchResult() -> Bool

		self.url이 검색 결과 URL로 의심되는지 판단
		"""
		# 2008-11-05 추가
		if self.path.find("search") >= 0:

			for search_query_key in DEF_SEARCH_RESULT_QUERY_KEYS:
				if self.query_dict.has_key(search_query_key) and len(self.query_dict[search_query_key]) > 0:
					return True

		# 2008-11-28 추가
		# netloc + path부분에 "search"가 있고, url 전체에 "search"가 3번 이상 나올경우
		if (self.netloc +" "+ self.path).find("search") >= 0:
			if self.url.count("search") >= 3:
				return True

		# 2009-04-02 추가
		# path부분에 "search"가 있고, path 맨 뒤에 "%"가 있는 경우
		paths = self.path.split("/")
		if "search" in paths and paths[-1].find("%") >= 0:
			return True

		return False


	def _isExpectedSearchE1Result(self, title):
		"""
		self._isExpectedSearchE1Result() -> BOOL

		sel.url이 검색 결과 URL로 의심되는지 판단 (weak detection)
		"""
		# 2009-01-15
		if title:
			if title.find("검색") >= 0 and self.query.find("%") >= 0:
				return True
		return False


	def _isExpectedSearchE2Result(self):
		"""
		self._isExpectedSearchE2Result() -> BOOL

		sel.url이 검색 결과 URL로 의심되는지 판단 (weak detection)
		logic: path[-1][:6] == "result" && query.find("keyword=") >= 0
		"""
		# 2009-03-13
		paths = self.path.split("/")
		if paths[-1][:6] == "result":
			for search_query_key in DEF_SEARCH_RESULT_QUERY_KEYS:
				if self.query_dict.has_key(search_query_key) and len(self.query_dict[search_query_key]) > 0:
					return True
			
		return False


	def _isExpectedSearchE3Result(self):
		"""
		self._isExpectedSearchE3Result() -> BOOL

		sel.url이 검색 결과 URL로 의심되는지 판단 (/tag/ type)
		logic: 
				"tag" in paths[:-1] || "tags" in paths[:-1]
				paths[-1] == "" && len(query) == 1 && "tag" in keys
				paths[-1] == "" && len(query) == 2 && "tag" in keys && "page" in keys
		"""
		# 2009-03-30
		paths = self.path.split("/")

		if "tag" in paths[:-1] or "tags" in paths[:-1]:
			return True
		if paths[-1] == "":
			if self.query_dict.has_key("tag"):
				query_dic_len = len(self.query_dict)
				if query_dic_len == 1:
					return True
				if query_dic_len == 2 and self.query_dict.has_key("page"):
					return True
			
		return False


	def _isSiteTop(self):
		"""
		self._isSiteTop() -> Bool

		self.url이 site의 top url인지 판단
		"""
		#if len("".join([self.params, self.query, self.fragment])) == 0:
		if len("".join([self.params, self.query])) == 0:
			if self.path in ("", "/"):
				return True

		return False


	def _isSiteDirectory(self):
		"""
		self._isSiteDirectory() -> Bool

		self.url이 site의 directory url인지 판단
		"""
		#if len("".join([self.params, self.query, self.fragment])) == 0:
		if len("".join([self.params, self.query])) == 0:
			paths = self.path.split("/")
			is_dr = True
			for path in paths:
				if path.isdigit() or path.find("%") >= 0:
					is_dr = False
					break

			if is_dr and self.path.endswith("/"):
				return True

		return False


	def _isStaticRSS(self):
		"""
		self._isStaticRSS() -> Bool

		self.url이 RSS의 Static인지 판단 
		(확장자가 있고, 쿼리가 없으면)
		"""
		(root, ext) = os.path.splitext(self.path)
		if ext != "" and len("".join([self.params, self.query])) == 0:
			return True

		return False

		

	def isPriorityURL(new_url, old_url, alt_title=None, dup_alt_title=None):
		"""
		URLUtil.isPriorityURL(...) -> Bool

		new_url의 저장 우선순위가 old_url보다 높은지 판단
		step:
			0. alttile이 있을 경우
			1. static page > dynamic page
			2. path depth 적을수록
			3. query, params 적을수록
			4. url 짧을 수록
		"""
		(n_scheme, n_netloc, n_path, n_query, n_params, n_fragment) = urlparse(new_url)
		(o_scheme, o_netloc, o_path, o_query, o_params, o_fragment) = urlparse(old_url)

		# step 0 : 2008-09-23
		if dup_alt_title and dup_alt_title != "":
			if alt_title and alt_title != "":
				# 둘 다 있을 경우
				pass
			else:
				# 예전것만 있을 경우
				return False
		else:
			if alt_title and alt_title != "":
				# 신규만 있을 경우
				return True
			else:
				# 둘 다 없을 경우
				pass

		# step 1
		if len("".join([n_query, n_params])) == 0 and len("".join([o_query, o_params])) > 0:
			return True

		# step 2
		n_len = len(n_path.split("/"))
		o_len = len(o_path.split("/"))
		if n_len < o_len:
			return True
		elif n_len > o_len:
			return False

		# step 3
		n_len = len((n_query+"&"+n_params).split("&"))
		o_len = len((o_query+"&"+o_params).split("&"))
		if n_len < o_len:
			return True

		# step 4
		n_len = len(new_url)
		o_len = len(old_url)
		if n_len < o_len:
			return True
		elif n_len > o_len:
			return False

		# step 5
		if new_url < old_url:
			return True

		return False


	isPriorityURL = staticmethod(isPriorityURL)


def getDomainCandi(domain):   # domain 매치를 위한 domain 후보 리스트 추출
	ret_list = []
	if domain.startswith("www."):
		domain = domain[4:]
	ret_list.append(domain)
	domain_pieces = domain.split(".")
	top_domain = getTopDomain(domain, "domain")
	if len(domain_pieces) > 3:
		t_host = ".".join(domain_pieces[1:])
		ret_list.append(t_host)
		if t_host != top_domain:
			ret_list.append(top_domain)
	else:
		if top_domain != domain:
			ret_list.append(top_domain)
	return ret_list
	




def getTopDomain(url, type="url"):

	if type == "url":
		parsed_url = urlparse(url)
		netloc = parsed_url.netloc
	else:
		netloc = url
	pieces = netloc.split(".")
	if len(pieces) < 2:
		return None
	elif len(pieces) == 2:
		if pieces[1] in TLDs:
			return netloc
		elif pieces[1] == "kr":
			if pieces[0] in SLDs:
				return None
			else:
				return netloc
	else:
		if len(pieces) == 4:
			try:
				k = int(pieces[-1])
				return None
			except Exception, msg:
				pass

		if pieces[-1] in TLDs:
			return ".".join(pieces[-2:])

		elif pieces[-1] in ccTLDs:
			if pieces[-2] in SLDs:
				return ".".join(pieces[-3:])
			else:
				return ".".join(pieces[-2:])
		else:
			return ".".join(pieces[-3:])



def getBlogMobileUrl(url):

	parsed_url = urlparse(url)

	try:
		if parsed_url.netloc == "blog.naver.com":
			url = url.replace("http://blog.","http://m.blog.")
			return url
		elif parsed_url.netloc == "blog.daum.net":
			url = url.replace("http://blog.","http://m.blog.")
			return url
		elif parsed_url.netloc.find(".tistory.com") >= 0:
			url = "http://%s/m/post%s"%(parsed_url.netloc, parsed_url.path)
			return url
	except Exception, msg:
		return ""

	return ""


def getMobileUrl(url, bbs_id=None):
	parsed_url = urlparse(url)
	
	r_url = ""
	netloc = parsed_url.netloc.replace("www.","")
	if netloc in ["mlbpark.donga.com", "instiz.net", "gigglehd.com", "ko.goldenears.net", "djuna.cine21.com", "ppuang.com", "nbamania.com", "kdbmania.net", "82cook.com", "pgr21.com", "playwares.com", "ossue.com", "becle.net" ]:
		r_url = url
	elif netloc in ["inven.co.kr"]:
		r_url = url.replace("www.inven.co.kr", "m.inven.co.kr").replace("/board/","/powerbbs/")
	elif netloc in ["clien.net"]:
		r_url = "http://m.clien.net/cs3/board?"+parsed_url[4]+"&bo_style=view"

	elif netloc in ["theappl.com"]:
		r_url = url.replace("http://","http://m.").replace("/bbs/","/")
	elif netloc in ["rgrong.kr", "simsimhe.com", "baedalnet.com"]:
		r_url = url.replace("/bbs/", "/m/")
	elif netloc in ["applclub.com"]:
		r_url = url.replace("/bbs/","/m/bbs/")
	elif netloc == "fun.jjang0u.com":
		r_url = url.replace("fun.jjang0u.com", "m.jjang0u.com")

	elif netloc == "ppomppu.co.kr":
		r_url = "http://m.ppomppu.co.kr/new/bbs_view.php?"+parsed_url[4]

	elif netloc == "gasengi.com":
		r_url = url.replace("/main/", "/m/bbs/")

	elif netloc == "soccerline.co.kr":
		r_url = "http://m.soccerline.co.kr/bbs/totalboard/view.html?"+parsed_url[4]

	elif netloc == "todayhumor.co.kr":
		r_url = "http://m.todayhumor.co.kr/view.php?"+parsed_url[4]

	elif netloc in ["hungryapp.co.kr", "ezday.co.kr", "luckyworld.net"]:
		r_url = url.replace("www.","m.")

	elif netloc == "pann.nate.com":
		r_url = url.replace("http://pann.","http://m.pann.")

	elif netloc == "fun.pullbbang.com":
		r_url = "http://m.pullbbang.com/contents/cttView.pull?"+parsed_url[4].replace("code=","bd_code=")
		if bbs_id:
			if bbs_id == 9494:
				r_url = r_url+"&category1_code=1&category2_code=3"
			elif bbs_id == 9493:
				r_url = r_url+"&category1_code=1&category2_code=1"
	elif netloc == "gall.dcinside.com":
		r_url = "http://m.dcinside.com/view.php?"+parsed_url[4]

	elif netloc == "bobaedream.co.kr":
		pieces = parsed_url[4].split("&")
		board = ""
		no = ""
		for piece in pieces:
			parts = piece.split("=")
			if parts[0] == "code":
				board = parts[1]
			elif parts[0] == "No":
				no = parts[1]
		if board != "" and no != "":
			r_url = "http://m.bobaedream.co.kr/board/bbs_view/%s/%s"%(board, no)
	elif netloc.find("telzone.daum.net") >= 0:
		r_url = url.replace("http://","http://m.").replace("/gaia/do/","/gaia/do/mobile/")
	elif netloc.find("web.humoruniv.com") >= 0:
		r_url = url.replace("http://web.","http://m.").replace("/board/humor/", "/board/")
	return r_url




def getReverseUrl(url):
	
	parsed_url = urlparse(url)
	tmp_netloc = parsed_url.netloc
	if tmp_netloc.startswith("www."):
		tmp_netloc = tmp_netloc[4:]


	pieces = tmp_netloc.split(".")
	pieces.reverse()
	r_netloc = ".".join(pieces)
	
	if len(parsed_url.query)>0:
		return_url = "r:%s://%s%s?%s"%(parsed_url.scheme, r_netloc, parsed_url.path, parsed_url.query)
	else:
		return_url = "r:%s://%s%s"%(parsed_url.scheme, r_netloc, parsed_url.path)
	return return_url


if __name__ == "__main__":
	
	print "> URL Test Start"

	url = "http://search.daum.net/cgi-bin/nsp/search.cgi?nil_profile=g&nil_search=btn&sw=tot&q=@%B1%E8%BC%F8%C8%AF"
	url = "http://www.google.co.kr/search?complete=1&hl=ko&newwindow=1&q=%EA%B9%80%EC%88%9C%ED%99%98&lr="
	#url = "http://www.google.co.kr/"

	title = None
	import sys
	if len(sys.argv) > 1:
		url = sys.argv[1]
		if len(sys.argv) > 2:
			title = sys.argv[2]


	print "> URL Type Test"

	url_util = URLUtil(url)
	print url_util
	print "W:", url_util.getURLType(title=title)
	print "R:", url_util.getURLTypeRSS()


	print getReverseUrl("http://www.mirror.enha.kr/index.html")

	print getTopDomain("http://naver.com/")
	print getTopDomain("http://mirror.enha.kr/")
	print getTopDomain("http://mirror.co.kr/")

	print getMobileUrl("http://clien.net/cs2/bbs/board.php?bo_table=park&wr_id=23778529")

	print getMobileUrl("http://gall.dcinside.com/board/view/?id=stock_new&no=3761937")

	print getMobileUrl("http://rgrong.kr/bbs/view.php?id=100&no=3247&category=1")
	print getMobileUrl("http://fun.jjang0u.com/chalkadak/view?db=160&no=163673")
	print getMobileUrl("http://www.ppomppu.co.kr/zboard/view.php?id=ppomppu&page=1&sn1=&divpage=30&sn=off&ss=off&sc=off&select_arrange=headnum&search_type=&desc=asc&no=164723")
	print getMobileUrl("http://www.gasengi.com/main/board.php?bo_table=sports&wr_id=138425&w10=")

	print getMobileUrl("http://www.soccerline.co.kr/slboard/view.php?code=totalboard&uid=1990350841")

	print getMobileUrl("http://www.todayhumor.co.kr/board/view.php?table=bestofbest&no=125473&s_no=125473&page=1")

	print getMobileUrl("http://www.hungryapp.co.kr/bbs/bbs_view.php?pid=221011&bcode=free&page=1")
	print getMobileUrl("http://www.bobaedream.co.kr/board/bulletin/view.php?code=strange&No=636166")
	print getMobileUrl("http://bbs1.telzone.daum.net/gaia/do/board/photo/read?bbsId=A000010&articleId=581396")
	print getMobileUrl("http://theappl.com/bbs/board.php?bo_table=talk_03&wr_id=458464")
	print getMobileUrl("http://fun.pullbbang.com/fun/funView.pull?code=15041338", 9493)

	print getBlogMobileUrl("http://asb.tistory.com/123")

