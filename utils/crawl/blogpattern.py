#coding: utf8
import urlparse
import ahocorasick
import urllib2 
import re
from common_func import getDBCursor
from app_blog_guid_gen import BlogUrlFactory

def makeDict(query):

	t_dict = dict()
	para_pieces = query.split("&")
	for para in para_pieces:
		res = para.split("=")
		para_name = res[0]
		if len(res) < 2:
			para_value = None
		else:
			if len(res[1]) > 0:
				para_value = "=".join(res[1:])
				t_dict[para_name] = para_value
			else:
				para_value = ''
	return t_dict

def getBlogMobileUrl(url):

	parsed_url = urlparse(url)
	if parsed_url.netloc == "blog.naver.com":
		url = url.replace("http://blog.","http://m.blog.")
		return url
	return ""


def handleYoutubeUrl(url):

	parsed_url = urlparse.urlparse(url)

	if parsed_url[1] == "www.youtube.com":
		page =  parsed_url[2].replace("/embed/","/v/")

		pieces = page.split("/")
		if pieces[1] == "v" and len(pieces[2]) == 11:
			url = "http://www.youtube.com/watch?v="+pieces[2]
			return url
		elif pieces[1] == "watch":
			param_dict = makeDict(parsed_url[4])
			
			if "v" in param_dict:
				vid = param_dict["v"]
				url = "http://www.youtube.com/watch?v=%s"%vid
				return url
	return None




def handleNBUrl(url, org_id=None, type="key"):

	# type : key, trackback, blogid
	# http://blog.naver.com/PostView.nhn?blogId=zera80&logNo=50113077803	
	# http://blog.naver.com/sohcool1?Redirect=Log&LogNo=150112150871 
	# http://exboifriend.blog.me/70101722381
 	# http://blog.naver.com/sejong_jang/109389607
	# http://fogopa.com/192711840

	blogId = ""
	logNo = ""
	parsed_url = urlparse.urlparse(url)

	if parsed_url[1] in ["blog.naver.com", "m.blog.naver.com"]:
		pieces = parsed_url[2].split("/")
		params = parsed_url[4].split("&")
		for param in params:
			para = param.split("=")
			if para[0] == "blogId":
				blogId = para[1]
			elif para[0] == "logNo":
				try:
					logNo = int(para[1])
				except Exception, msg:
					pass

		if pieces[1] == "PostView.nhn":
			pass
		elif pieces[1] == "PostRelayList.nhn":
			type = "trackback"
			pass
		elif pieces[1] == "GoPost.nhn":
			pass
		elif parsed_url[2].find(".nhn") >= 0:
			pass
		elif len(pieces) >= 2:
			blogId = pieces[1]
			if len(pieces) == 3:
				try:
					logNo = int(pieces[2])
				except Exception, msg:
					pass

	elif parsed_url[1].find(".blog.me")>= 0:
		blogId = parsed_url[1].split(".")[0]
		try:
			logNo = int(parsed_url[2].split("/")[1])
		except Exception, msg:
			pass
	elif org_id:
		try:
			logNo = int(parsed_url[2].split("/")[1])
		except Exception, msg:
			pass
		blogId = org_id
	
	if blogId != "" and  logNo != "":
		blogId = blogId.lower()
		if type == "key":
			n_url = "http://blog.naver.com/PostView.nhn?blogId="+blogId+"&logNo="+str(logNo)
			return n_url
		elif type == "trackback":
			n_url = "http://blog.naver.com/PostRelayList.nhn?blogId="+blogId+"&logNo="+str(logNo)
			return n_url
		elif type == "blogid":
			return blogId, logNo

	if blogId != "" and type == "channel":
		return "http://blog.rss.naver.com/%s.xml"%blogId

	return ""

def getSiteInfo(domain):
	if domain == "blog.naver.com":
		return "네이버 블로그", "http://blog.naver.com/"
	elif domain == "blog.daum.net":
		return "다음 블로그", "http://blog.daum.net/"	
	elif domain == "tistory.com":
		return "티스토리", "http://tistory.com/"
	elif domain == "blog.aladin.co.kr":
		return "알라딘 블로그", "http://blog.aladin.co.kr/"
	elif domain == "blog.dreamwiz.com":
		return "드림위즈 블로그", "http://blog.dreamwiz.com/"
	elif domain == "booklog.kyobobook.co.kr":
		return "교보문고 북로그", "http://booklog.kyobobook.co.kr/"
	elif domain == "blog.moneta.co.kr":
		return "부자마을 블로그", "http://blog.moneta.co.kr/"
	elif domain == "blog.yes24.com":
		return "YES 블로그", "http://blog.yes24.com/"
	elif domain == "book.interpark.com":
		return "북피니언", "http://book.interpark.com/"
	elif domain == "blog.donga.com":
		return "저널로그", "http://blog.donga.com/"
	elif domain == "blogspot.com":
		return "Google Blogger", "http://www.blogger.com/"
	elif domain == "wordpress.org":
		return "워드프레스", "http://wordpress.org/"
	elif domain == "textcube.org":
		return "텍스트큐브", "http://textcube.org/"
	elif domain == "wordpress.com":
		return "워드프레스", "http://wordpress.com/"
	else:
		return "", "http://%s/"%domain


def handleBlogUrls(url, domain, id=None):

	if domain == "blog.naver.com" or domain.find(".blog.me") >= 0:
		return  handleNBUrl(url, id)
	elif domain == "blog.daum.net":
		return handleDBUrl(url, id)


def makeChannelUrl(blog_type, blog_id):
	if blog_type == "blog.naver.com":
		return "http://blog.rss.naver.com/%s.xml"%blog_id
	elif blog_type == "blog.daum.net":
		return "http://blog.daum.net/xml/rss/%s"%blog_id
	elif blog_type == "tistory.com":
		return "http://%s.tistory.com/rss"%blog_id
	elif blog_type == "blog.aladin.co.kr":
		return "http://blog.aladin.co.kr/%s/rss"%blog_id


def makeViewUrl(domain, blog_id, no):
	if domain == "blog.naver.com":
		return "http://blog.naver.com/%s/%s"%(blog_id, no)
	elif domain == "blog.daum.net":
		return "http://blog.daum.net/%s/%s"%(blog_id, no)
	elif len(domain) > 4: 
		return "http://%s/%s"%(domain, no)
	return ""


def getChannelFromGuid(guid):
	parsed_url = urlparse.urlparse(guid)
	netloc = parsed_url.netloc
	if netloc == "blog.naver.com":
		blog_id, no = handleNBUrl(guid, type="blogid")
		return makeChannelUrl("blog.naver.com", blog_id), "blog.naver.com", blog_id, no
	elif netloc == "blog.daum.net":
		blog_id, no = detectDaumBlog(guid)
		return makeChannelUrl("blog.daum.net", blog_id), "blog.daum.net", blog_id, no
	elif netloc.find(".tistory.com") > 0:
		blog_id, no = handleTistory(guid, type="blogid")
		return makeChannelUrl("tistory.com", blog_id), "tistory.com", blog_id, no
	return "", "", "", 0




def getNaverBlog(url):
	parsed_url = urlparse.urlparse(url)
	if parsed_url[1] == "blog.naver.com":
		num = parsed_url[2].count("/")
		if num ==  1:
			return "http://blog.naver.com"+parsed_url[2]
	elif parsed_url[1].find(".blog.me") >= 0:
		blogId = parsed_url[1].split(".")[0]
		return "http://blog.naver.com/"+blogId
	return ""

def getNaverBlogID(url):
	parsed_url = urlparse.urlparse(url)
	if parsed_url[1] == "blog.naver.com":
		num = parsed_url[2].count("/")
		if num ==  1:
			if parsed_url[2][1:].find(".") > 0:
				return ""
			if parsed_url[2][1:].find("%") > 0:
				return ""
			return parsed_url[2][1:]
		elif num == 2:
			try:
				pieces= parsed_url[2].split("/")
				no = int(pieces[2])
				return pieces[1]
			except Exception, msg:
				pass
	elif parsed_url[1].find(".blog.me") >= 0:
		blogId = parsed_url[1].split(".")[0]
		return blogId
	return ""


def handleTistory(url, id=None, type="key"):
	parsed_url = urlparse.urlparse(url)

	netloc = parsed_url.netloc
	if not netloc.endswith(".tistory.com"):
		pieces = parsed_url[2].split("/")
		try:
			no = int(pieces[1])
			return "http://%s.tistory.com/%s"%(id, no)
		except Exception, msg:
			pass
	else:
		id = netloc[:netloc.find(".")]
		pieces = parsed_url[2].split("/")
		try:
			no = int(pieces[1])
			if type == "blogid":
				return id, no
			return "http://%s.tistory.com/%s"%(id, no)
		except Exception, msg:
			pass
	return ""



def checkDaumPost(path):

	pieces = path.split("/")
	if len(pieces) > 2:
		try:
			id = pieces[1]
			no = int(pieces[2])
			return "http://blog.daum.net/%s/%s"%(id, no)
		except Exception, msg:
			return ""
	else:
		return ""
		

			

def detectDaumBlog(url, withTmpID=False):
	parsed_url = urlparse.urlparse(url)
	if parsed_url[1] == "blog.daum.net":
		pieces = parsed_url[2].split("/")
		if len(pieces) >= 2:
			blogId = pieces[1]
			if len(pieces) == 3:
				try:
					articleno = int(pieces[2])
					if withTmpID:
						req = urllib2.Request(url)
						f = urllib2.urlopen(req)
						html = f.read()
						m = re.search("blogid=(.+?)&amp;", html)
						if m:
							tmp_id = m.group(1)
							return blogId, tmp_id
					else:
						return blogId, articleno
				except:
					pass

	return "",""

	
def handleDBUrl(url, tmp_id=None, type="key"):

	# http://blog.daum.net/tourcodi/8128482   
	# return Data
	# http://blog.daum.net/_blog/BlogTypeView.do?blogid=07Qbe&articleno=13419044    
	# http://blog.daum.net/_blog/hdn/ArticleContentsView.do?blogid=0Gagz&articleno=8128486

	parsed_url = urlparse.urlparse(url)
	if parsed_url[1] == "blog.daum.net":
		pieces = parsed_url[2].split("/")
		if len(pieces) >= 2:
			blogId = pieces[1]
			if len(pieces) == 3:
				try:
					articleno = int(pieces[2])
					if not tmp_id:
						try:
							req = urllib2.Request(url)
							f = urllib2.urlopen(req)
							html = f.read()
							m = re.search("blogid=(.+?)&amp;", html)
							if m:
								tmp_id = m.group(1)
						except Exception, msg:
							pass
					if tmp_id and articleno and type == "down":			
						return "http://blog.daum.net/_blog/BlogTypeView.do?articleno=%s&blogid=%s"%(articleno, tmp_id), "http://blog.daum.net/_blog/hdn/ArticleContentsView.do?articleno=%s&blogid=%s"%(articleno, tmp_id)
					elif tmp_id and articleno and type == "trackback":
						return "http://blog.daum.net/_blog/getTrackbacks.do?articleno=%s&blogid=%s"%(articleno, tmp_id)
					elif blogId != "" and type == "key":
						return "http://blog.daum.net/%s/%s"%(blogId, articleno)
				except Exception, msg:
					pass

			if blogId != "" and type == "channel":
				return "http://blog.daum.net/rss/xml/%s"%blogId
	return ""



	
def getBlogType(netloc, cursor):


	if netloc.startswith("www."):
		another_netloc = netloc.replace("www.","")
	else:
		another_netloc = "www."+netloc
	
	query = "SELECT blog_domain, blog_id FROM blogdb.hosts where netloc in (%s, %s)"
	cursor.execute(query, [netloc, another_netloc])
	result = cursor.fetchone()
	if result:
		return result[0], result[1]
	return None, None


def getGuid(url, cursor=None):

	parsed_url = urlparse.urlparse(url)
	netloc = parsed_url.netloc
	if netloc in ["blog.naver.com", "m.blog.naver.com"] or netloc.endswith(".blog.me"):
		return  handleNBUrl(url)
	elif netloc == "blog.daum.net":
		return checkDaumPost(parsed_url[2])
	elif netloc.endswith(".tistory.com"):
		return handleTistory(url)
	else:
		if cursor == None:
			try:
				DBHOST = "10.35.50.116"  
				(db, n_cursor) = getDBCursor(host=DBHOST, user='blogmoa', passwd='blogcrawler', db='blogdb') 
			except Exception, msg:
				return None
		else:
			n_cursor = cursor
		type, id = getBlogType(netloc, n_cursor)
		if  type:
			if type == "tistory.com":
				return handleTistory(url, id)
			elif type == "blog.naver.com":
				return handleNBUrl(url, id)
		return None
	return None


class GuidChecker:
	def __init__(self):
		self.getCursor()
		self.tmp_dict = dict()
		self.bf = BlogUrlFactory()

	def getCursor(self):
		DBHOST = "10.35.50.116"  
		(self.db, self.cursor) = getDBCursor(host=DBHOST, user='blogmoa', passwd='blogcrawler', db='blogdb') 

	def getGuid(self, url):
		parsed_url = urlparse.urlparse(url)
		netloc = parsed_url.netloc
		if netloc == ["blog.naver.com", "m.blog.naver.com"] or netloc.endswith(".blog.me"):
			return  handleNBUrl(url)
		elif netloc == "blog.daum.net":
			return checkDaumPost(parsed_url[2])
		elif netloc.endswith(".tistory.com"):
			return handleTistory(url)
		else:
			if netloc in self.tmp_dict:
				type, id = self.tmp_dict[netloc]
			else:
				type, id = getBlogType(netloc, self.cursor)
			if  type:
				self.tmp_dict[netloc] = type, id
				if type == "tistory.com":
					return handleTistory(url, id)
				elif type == "blog.naver.com":
					return handleNBUrl(url, id)
			else:
				self.tmp_dict[netloc] = "", ""

			guid = self.bf.getGuid(url)
			if guid:
				return guid
			return ""

	def getChannel(self, url):
		return self.bf.getChannelUrl(url)


	def close(self):
		self.db.close()
		self.cursor.close()


if __name__ == "__main__":

	#print makeEncryptDB('222.231.51.83','edi','wwwcrawler','sitedb','sitedb09')
	#print makeEncryptDB('222.231.51.82','edi','wwwcrawler','sitedb','sitedb09')

#	Init()
#	print garbage_set, len(garbage_set)
	garbage_set = {}
	import sys
	 
	a  = GuidChecker()
	if len(sys.argv) > 1:
	 	url = sys.argv[1]
		print url, a.getGuid(url), a.getChannel(url)
		exit(0)



	print handleDBUrl("http://blog.daum.net/songjumwoo/616")
	print handleDBUrl("http://blog.daum.net/eliakim/123")
	print handleDBUrl("http://blog.daum.net/songjumwoo/616", type="trackback")

	print handleTistory("http://abc.com/616", "asdb")
	print handleTistory("http://abc.tistory.com/entry/이런젠장616#asdfa")


	print "GUID TEST"
	print a.getGuid("http://blog.naver.com/sejong_jang/109389607")
	print a.getGuid("http://blog.naver.com/sohcool1?Redirect=Log&logNo=150112150871")
	print a.getGuid("http://iln.blog.me/220042816612")
	print a.getGuid("http://blog.naver.com/PostView.nhn?blogId=zera80&logNo=50113077803")
	print a.getGuid("http://blog.daum.net/songjumwoo/616/asdf")
	print a.getGuid("http://blog.daum.net/asdfa.asdf?did=asdf")
	print a.getGuid("http://abc.tistory.com/16#asdfa")
	print a.getGuid("http://abc.com/616")
	print a.getGuid("http://abc.com/1111")
	print a.getGuid("http://abc.com/61asdf")

	print a.getGuid("http://aboutchun.com/861")
	print a.getGuid("http://blog.busan.go.kr/3284")
	print a.getGuid("http://0jin0.com/2309")

	print a.getGuid("http://0jin0.com/1209")
	print a.getGuid("http://blog.aladin.co.kr/editors/2965338")
	print a.getGuid("http://blog.yes24.com/blog/blogMain.aspx?blogid=hanmunhwa0&artSeqNo=4855342")
	print a.getGuid("http://book.interpark.com/blog/lsy6025/1792019")
	print a.getGuid("http://blog.aladin.co.kr/editors/2965338")

	print a.getGuid("http://blog.enes.kr.pe/50194110072")

	"""
	print getGuid("http://0jin0.com/3309")
	print getGuid("http://m.blog.naver.com/220111546908")
	print getGuid("http://wosence.blog.me/220110837078%A4%BD")
	print getGuid("http://blog.naver.com/MultimediaFLVPlayer.nhn?blogId=iamschoool&logNo=220107215777&vid=E7C1165AAE7B00A989D23E18CECAAE499F65&width=512&height=321&ispublic=true")
	print getGuid("http://nootl.com/?p=13"), "nootl.com", "AAAAAAA"
	"""

	print handleNBUrl("http://iln.blog.me/220042816612", type="channel")
	print handleNBUrl("http://iln.blog.me/", type="channel")
	print handleDBUrl("http://blog.daum.net/songjumwoo/616", type="channel")
	print handleDBUrl("http://blog.daum.net/songjumwoo/", type="channel")
	print handleDBUrl("http://blog.daum.net/", type="channel")

	print handleYoutubeUrl("http://www.youtube.com/embed/2HbMzu1aQW8")
	print handleYoutubeUrl("http://www.youtube.com/embed/C8xPKPqzhHk?feature=player_embedded")
