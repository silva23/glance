#/usr/bin/env python
# coding: utf8

"""
크롤결과로 얻은 Raw Data로부터 필드별 속성에 따라 최종 정제된 값을 얻을 수 있는 모듈

- 지원필드 : 날짜 문자열 데이터, 기자 및 email 문자열 

Made by. shiwoo park 2015 - silva23@naver.com
"""
import re

from date_handler import DateFactory
from field_filter import WriterFilter
from utils.log import getLogger


def getListStr(_list):
	if not _list:
		return ""
	s = "["
	for elem in _list:
		if s == "[":
			s += elem
		else :
			s += ", "+elem
	s += "]"
	return s

def getDicStr(_dic, newline=False):
	if not _dic:
		return ""
	newlineChar = ""
	if newline:
		newlineChar = "\n"
	s = "{%s"%newlineChar
	for k,v in _dic.items() :
		s += "%s:%s, %s"%(k,v,newlineChar)
	s += "}"
	return s

class FieldTransformer():

	def __init__(self):
		self.numberFields = []
		self.timeFields = []
		self.logger = getLogger()

	def transform(self, dataDic):
		if "guid" not in dataDic:
			dataDic["guid"] = "None"
		dateFactory = DateFactory()
		writerFilter = WriterFilter()
		
		for key, val in dataDic.items():
			try :
				# 시간
				if key.endswith("TimeText"):
					dataDic[key[:-4]] = dateFactory.getUnixTimestamp(val)
				elif key in self.timeFields :
					dataDic[key] = self.getTimestamp(val)
	
				# 숫자(미사용)
				#elif key.endswith("Count")  :
				#	dataDic[key[:-4]] = self.getNumber(val)
				#elif key in self.numberFields :
				#	dataDic[key] = self.getNumber(val)

				# 기자명
				elif key == "authorText":
					authorName, authorEmail = writerFilter.getWriterAndEmail(val)
					if authorName and ("authorName" not in dataDic):
						dataDic["authorName"] = authorName
					if authorEmail and ("authorEmail" not in dataDic):
						dataDic["authorEmail"] = authorEmail
	
				# Email
				elif key.endswith("Email") :
					dataDic[key] = self.getEmail(val)

				# 뉴스SC 크롤매체 표기
				elif key == "type" :
					if val == "NEWS" :
						dataDic["sourceType"] = 4 

			except Exception, msg :
				self.logger.error("Field [%s] transform error at GUID [%s] : %s"%(key, dataDic["guid"], msg) )

		return dataDic

	def getEmail(self, inputStr):
		m = re.match(".*?(\w+@[\w\.]+).*", inputStr)
		if m :
			return m.group(1)
		return ""

	def getNumber(self, inputStr):
		m = re.match(".*?(\d+).*", inputStr)
		if m :
			return m.group(1)
		return 0


if __name__ == "__main__":
	# TEST SETTING ------------------------------------------------------------------------------ 
	testURL = "http://enewstoday.co.kr/news/articleView.html?idxno=375306"
	dirPath = ""

	from resource_writer import WRITER_DATA

	import optparse
	
	cmdParser = optparse.OptionParser(usage="")
	(cmdOptions, cmdArgs) = cmdParser.parse_args()

	# TEST 할 데이터만 주석제거 --------------------------------------------------------------
	#testData = TIME_AGODATA  # 한달 전
	#testData = TIME_DATEDATA  # 2014 Mar 7 3:12:30 PM
	testData = WRITER_DATA # 박시우 인턴기자
	#testData = "댓글 수 [3524]" # 직접지정

	# 필드단위 TEST ------------------------------------------------------------------------------ 
	writerFilter = WriterFilter()
	fieldTransformer = FieldTransformer()
	
	for inputStr in testData.split("\n") :
		if inputStr.strip() :
			# 테스트 할 필드만 주석 제거
			print "INPUT STR : ",inputStr
			#print "getEmail : ",fieldTransformer.getEmail(inputStr)
			#print "getNumber : ",fieldTransformer.getNumber(inputStr)
			#print "getTimestamp : ",fieldTransformer.getTimestamp(inputStr)
			print "getWriterAndEmail : ",getListStr( writerFilter.getWriterAndEmail(inputStr) )
			print "#######################################"
			

	# 문서단위 TEST ------------------------------------------------------------------------------
	
	testDic = dict()
	testDic["createTimeText"] = "108명 읽음2개 덧글5 시간 전"
	testDic["replyCountText"] = "댓글 5"
	testDic["modifiedTimeText"] = "12/11/2013 10:30:48 PM"
	testDic["authorText"] = "박성준 기자 |  natalirk@nate.com"
	#testDic["authorName"] = "박시우"
	testDic["companyEmail"] = "silva23@naver.com"
	
	# 페이지 단위------------------------------------------------------------------------------
"""
	from custom_parser import CustomParser
	from urlpattern import UrlFactory
	
	if len(cmdArgs) > 0 :
		testURL = cmdArgs[0]

	#DOWNLOAD
	opener = urllib2.build_opener()
	req = urllib2.Request(testURL)
	#req.add_header("User-agent", "Mozilla/5.0 (compatible; Windows NT 6.1?; ZumBot/1.0; http://help.zum.com/inquiry)")
	req.add_header("User-agent", "wget")
	rs = opener.open(req)

	http_header = str(rs.info())
	http_content = rs.read()
	real_URL = rs.url
	
	urlFac = UrlFactory()
	parser = CustomParser()
	testDic = parser.parse(http_header, http_content, real_URL)
	urlInfo = urlFac.getGuid(testURL)

	if "guid" in urlInfo :
		testDic["guid"] = urlInfo["guid"]
	else :
		testDic["guid"] = testURL

	fieldTransformer.transform(testDic)
	#print getDicStr(testDic,True)

	xmlPrinter = XMLPrinter(dirPath)
	#xmlPrinter.printXML([testDic], "testData")
"""
