#coding: utf8

import time
import socket
import os
from data_producer import makeOutputDict

MAYFLY_INSERT_QUERY = dict()

MAYFLY_FIELDS = dict()

MAYFLY_FIELDS["common"] = ["guid", "mode",  "channelId", "firstCrawlTime", "crawlTime", "visitCount", "webLink", "title",  "body", "createTime", "server", "pid" ]
MAYFLY_FIELDS["know"] = ["seedId", "bodyHtml", "channelName", "imageCount", "readCount", "recommendCount", "answerCount", "answerRecommendCount", "isSolved", "linkCount" ]
MAYFLY_FIELDS["news"] = ["seedId", "authorName", "imageKey", "bodyHtml", "channelName", "imageCount", "videoCount", "imageThumbnailSignature"]
MAYFLY_FIELDS["oi"] = ["siteId", "siteName", "imageKey", "bodyHtml", "channelName", "imageCount"]
MAYFLY_FIELDS["bbs"] = ["authorName", "imageKey", "bodyHtml", "channelName", "imageCount", "videoCount", "readCount", "replyCount", "recommedCount", "siteId", "siteName"]
UPDATE_FIELDS = ["crawlTime", "visitCount", "mode", "title", "imageKey", "server", "pid", "imageThumbnailSignature", "body", "bodyHtml", "readCount", "recommendCount", "answerCount", "answerRecommendCount", "isSolved", "linkCount", "channelName"]


class DummyData:
	def __init__(self, id):
		self.id = id

def insertDocToMayfly(sc, document_data, cursor):

	server = socket.gethostname()
	pid = os.getpid()

	table = "%sdb.documents_in_"%sc+time.strftime("%Y%m%d", time.localtime(document_data.write_time) )

	domain_data = document_data.domain_data
	if document_data.seed_data:
		seed_data = document_data.seed_data
	else:
		seed_data = DummyData(0)

	read_count = 0
	recommend_count = 0
	answer_count = 0

	result_dict = makeOutputDict(document_data)
	result_dict["server"] = server
	result_dict["pid"] = pid
	result_dict["channelId"] = domain_data.id
	result_dict["seedId"] = seed_data.id
	result_dict["firstCrawlTime"] = document_data.first_visit
	result_dict["visitCount"] = document_data.visit_count

	if sc == "know":
		try:
			query = "INSERT INTO "+ table +"(guid, channelId, seedId, firstCrawlTime, crawlTime, visitCount, mode, title, webLink, createTime, body, server, pid, bodyHtml, channelName, imageCount, readCount, recommendCount, answerCount, answerRecommendCount, isSolved, linkCount) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE crawlTime = %s, visitCount = %s, mode = %s, title = %s, server = %s, pid = %s, body = %s, bodyHtml = %s, readCount = %s, recommendCount = %s, answerCount = %s, answerRecommendCount = %s, isSolved = %s, linkCount = %s"
			cursor.execute(query, (result_dict["guid"], domain_data.id, seed_data.id, document_data.first_visit, result_dict["crawlTime"], document_data.visit_count, result_dict["mode"], result_dict["title"], result_dict["webLink"], result_dict["createTime"], result_dict["body"], server, pid, result_dict["bodyHtml"], result_dict["channelName"], result_dict["imageCount"], result_dict["readCount"], result_dict["recommendCount"], result_dict["answerCount"], result_dict["answerRecommendCount"],result_dict["isSolved"], result_dict["linkCount"],  result_dict["crawlTime"], document_data.visit_count, result_dict["mode"], result_dict["title"], server, pid, result_dict["body"], result_dict["bodyHtml"] , result_dict["readCount"], result_dict["recommendCount"],   result_dict["answerCount"], result_dict["answerRecommendCount"], result_dict["isSolved"], result_dict["linkCount"]        ) )
			cursor.execute("commit")
		except Exception, msg:
			print msg
	elif sc == "bbs":
		try:
			result_dict["siteId"] = domain_data.id
			result_dict["siteName"] = domain_data.name
			result_dict["channelId"] = seed_data.id
			result_dict["channelName"] = seed_data.name
			if "image_key" in document_data.image_data:
				result_dict["imageKey"] = document_data.image_data["image_key"]
			makeQuery("bbs", table, result_dict, cursor)
		except Exception, msg:
			print msg
	elif sc == "oi":
		try:
			result_dict["siteId"] = domain_data.id
			result_dict["channelId"] = seed_data.id
			if "image_key" in document_data.image_data:
				result_dict["imageKey"] = document_data.image_data["image_key"]
			makeQuery("oi", table, result_dict, cursor)
		except Exception, msg:
			print msg

def makeQuery(sc, table, result_dict, cursor):
	INSERT_FIELD = MAYFLY_FIELDS["common"] + MAYFLY_FIELDS[sc]
	fields = []

	values = []
	updates = []

	value_list = []
	update_list = []

	for field in INSERT_FIELD:
		if field in result_dict:
			fields.append(field)
			values.append("%s")
			value_list.append(result_dict[field])
			if field in UPDATE_FIELDS:
				updates.append(field +" = " + "%s" )
				update_list.append(result_dict[field])

	value_list += update_list
	try:
		query = "INSERT INTO " + table +"( " + ",".join(fields) +") values( " + ",".join(values) + ")  ON DUPLICATE KEY UPDATE " + ",".join(updates)
		cursor.execute(query, value_list)
		cursor.execute("commit")
	except Exception, msg:
		print query, msg


if __name__ == "__main__":
	#makeQuery("news")
	pass
		

