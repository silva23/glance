#coding: utf8
import urlparse
import ahocorasick
import urllib2 
import re


class regdata:

	TRIM_PARA = ['sessid','sessionid','session_id','tssess','dummy','tsession','jsessionid','cspchd', 'sid_t', "phpsessid"]

	EXCEPT_NETLOC = ['login.', 'mail.', 'ftp.', 'advert.']
	EXCEPT_PATH = ['quickviewarticleview.html','current_connect.php','naeilsearch.html', '404.htm','preans','predel','preup','trouble','modify','delete','login','write','del_','vote','passwd', 'password','edit','reply.','admin','/del.','/error','pagenot','nopage','notfound','suspended','missing','forbidden','calendar','calender', 'Calendar','.wmv','.avi','.exe','.swf','.asf','.fla','.mp3','.rar','.zip','.000','.001','.002','.alz','.mpa','.rm', '.tar', '.gz','.gif','.mp4','.mpg' , '.css','.js', '.thumb', '.tgz', 'modform', 'delform', 'post_rating.php', "dispmemberfindaccount","dispmembersignupform", "dispboardtaglist","add_wish", "p_teacher_data_ans_insert", ".jpg", "traceback", "remove.php", "/move/", "board_secret_f.php", "/bbs/link.php", "cj_dm_new.htm", "/board_secret", "member_join.php", "membermodify.asp", "check_passwd", "search_daum_openapi.php", "/openapi/search.", "/search/openapi", "openapisearch", "/apinaver.php", "/find_result_total.jsp", "/mailSend.jsp", "PassCheck.jsp", "dispboardbgcommentlistbox","dispboardwrite", "refuse.php", "search.php", "/search/", "bbs_mod.php", "/print_paper.php", "/united-search/", "/download.", "memo_del.asp", "recomm.asp"]
	EXCEPT_PARA = ['act=post', '=login', 'mode=del','type=del', '=modify', '=delete','=edit', '=reply', '=write','=comment_del', 'action=input', 'action=print_view', 'act=dispboardreplycomment', 'act=dispboardwrite', 'act=dispmemberloginform' , 'act=dispmembersignupform', 'act=dispboardtaglist', 'act=dispmemberfindaccount','act=dispmember', 'act=procfiledownload', 'u_mode=trash', 'navi=LOGIN', '.mp4', '.flv', '.mpg', '.mp3', '.tgz', '.gz','.wmv', "mode=pwd", "name=Spambot_Killer", "_delete"]

	view_patterns = dict()
	list_patterns = dict()

	t_view_patterns = dict()
	t_list_patterns = dict()

	cmd_list = ["cmd" , "command" ,"action", "exe", "exec" , "ncmode", "query", "mode", "bset", "kids" ,"act", "job" ,"g_job", "at", "AM", "imode", "m", "s_work", "o", "BDOC", "st", "c", "flag", "q", "Work"]
	view_cmd = ["view", "body", "read", "detail", "V", "view_article", "v", "board.read"]
	list_cmd = ["list", "L", "board.list"]

	view_patterns["/board.php"] = [["bbs","no", "s_work"], ["cmd","db", "key", "no"], ["tbnum","mode", "number"], ["mode", "keyno", "db"], ["tb", "id"], ["board", "command", "no"], ["board","act", "no"], ["cmd", "tbname","no"],["tbname","action","no"], ["bo_table", "wr_id"], [ "articleNo","id" ], ["bId","mode", "no"], ["tbname", "no"], ["db", "number", "j"], ["db", "mode", "idx"], ["db", "no", "c"], ["id", "no"], ["board","n"] ]
	view_patterns["/zboard.php"] = [ ["id", "no"]] 
	view_patterns["/"] = [ ["document_srl","mid"],["board","n"], ["bo_table","doc","wr_id" ], ["code", "no", "o"], ["code","id","p"],["bset","code", "tot_code" ], ["bset", "tot_code", "bpage"], ["BbsCode","kids","seq"], ["mode","pg", "no"], ["CATE","CATE2","CATE3"], ["bc", "code"], ["m", "bid", "readno"], ["mode" ,"board", "go", "idx"], ["bbs_table", "menu_table", "ref", "eb_idx"], ["bid","idx"], ["BDOC","BID", "BIDX" ], ["GO", "mode", "no"], ["GO","no"], ["bo_table", "wr_id"] ]
	view_patterns["/board.cgi"] = [ ["db", "idx"], ["action",  "gul", "id"], ["id","action","l" ], ["action", "bname", "unum"] ]
	view_patterns["/ezboard.cgi"] = [ ["action","db","dbf"] ]
	view_patterns["/ttboard.cgi"] = [["act", "bname", "code"], ["act","idx","db"]]
	view_patterns["/ezboard.exe"] = [ ["action","db","dbf"] ]
	view_patterns["/kisboard.html"] = [ ["code", "num", "page"] ]
	view_patterns["/board_main.html"] = [ ["NC_id", "TB_id", "ncmode"] ]
	view_patterns["/Board.mi"] = [ ["cmd", "boardId", "articleId"]]
	view_patterns["/vx2.php"] = [ ["id", "no"]]
	view_patterns["/view.asp"] = [ ["tbName", "menu", "idx"] ,["bbslist_id", "master_id"],["rid", "gubun"], ["seq", "tbcode" ], ["boardcode","idx"],["code","strBoardID"], ["code", "h_Board"], ["bbs_id", "i_number","bca"], ["table", "id"], ["board_idty", "list_seq"], ["BID", "idx"], ["tn", "idx"], ["bbsid", "seq"] ]
	view_patterns["/view.htm"] = [["num", "table"] ]
	view_patterns["/view.html"] = [ ["gid", "id", "subcode"], ["catenum", "item_index"], ["cate", "pid"],["idx", "menutree"], ["qid"],["tb", "code", "num"], ["code" ,"id"], ["code", "num"], ["ID", "code"], ["bid", "ano"], ["board_id", "uid"],["blog_board", "log_no"], ["bid", "gid", "pid"], ["category", "no", "section"], ["n"]]
	view_patterns["/view.ga"] = [["id", "row_no"]] 
	view_patterns["/view.php"] = [["tbname" , "no"], ["board_id", "game_no", "no"], ["id", "code", "url"] , ["uid","Board_num","boardnum"],["boardid", "uid"], ["id", "no"], ["bbs_code", "bd_num"], ["bbs_id", "doc_num"], ["code","db", "n" ], ["bn", "no" ], ["bbid","no"], ["id","bid"],  ["db", "id"], ["bid","num"],["board", "id", "category"], ["board","id"], ["table", "uid"], ["uid","code"] , ["db", "no"], ["bbs", "idx"], ["id","category"],  ["tn", "id"], ["bcode","no"], ["table" ,"no"], ["board", "MK"], ["code", "No"], ["gcode","gid"], ["board", "seq"]  ]
	view_patterns["/view.do"] = [["bseq", "pseq"]]
	view_patterns["/View.jsp"] = [["menu_id", "seq"]]
	view_patterns["/View.asp"] = [["BoardIdx" , "Board_Id"]]
	view_patterns["/View.aspx"] = [["BoardType","BoardNo"],["bid" , "idx"], ["index", "num"]]
	view_patterns["/view.aspx"] = [["id", "rno"]]
	view_patterns["/bbs.do"] = [["act","bcd","msg_no"]]

	view_patterns["/move.html"] = [ ["gid", "id", "subcode"]]

	view_patterns["/bbs.asp"] = [["exe", "board_id", "idx_num", "group_name"], ["exe", "idx_num", "group_name"] ]
	view_patterns["/bbs.php"] = [["query", "table", "uid"], ["l", "query", "table" ], ["id", "q", "uid", "category" ], ["id","q", "uid"], ["id", "no"] ]
	view_patterns["/bbs.php3"] = [["query", "table", "uid"], ["l", "query", "table" ]]
	view_patterns["/bbs.html"] = [["mode","uid","Table" ], ["imode", "D", "num"],["mode", "bcode", "no"], ["bbs_code","mode","bbs_no"]]
	view_patterns["/bbs.htm"] = [["dbname" ,"mode" ,"seq" ]]
	view_patterns["/bbs5.htm"] = [[ "id", "code"]]
	view_patterns["/bbs_view.php"] = [["No", "code"], ["code", "idx"], ["pid","bcode" ] ]
	view_patterns["/bbs_read.html"] = [["board_id", "uid"] ,["id","no"], ["code", "uid"], ["bbs_id","code"]]

	view_patterns["/ezbbs.php"] = [["eb_seq", "ebcf_id", "g_job" ], ["eb_seq", "ebcf_id", "job" ]]
	view_patterns["/powerbbs.php"] = [["come_idx", "l"]]

	view_patterns["/index.php"] = [ ["cmd", "key", "no", "pagekey"], ["cmd", "code","dbt" ], ["blog_code","article_id"], ["CATE","CATE2","CATE3"], ["boardid","mode", "no"], ["document_srl","mid"] ]
	view_patterns["/index.asp"] = [["file","menu","IDX"]]
	view_patterns["/index.cgi"] = [["action", "number"]]
	view_patterns["/index.html"] = [["id" ,"no"], ["id", "code"], ["section","flag", "code"]]
	view_patterns["/index.htm"] = [["uid","page_name","menu_key"]]
	view_patterns["/index.jsp"] = [["cmd","code", "no"]]
	view_patterns["/pb.php"] = [["id","no"]]

	view_patterns["/cboard.php"] = [["board", "no"]]
	view_patterns["/CyberCarView.php"] = [["gubun", "number"]]
	view_patterns["/articleView.html"] = [["idxno"]]
	view_patterns["/search.daum"] = [["q"] ]
	view_patterns["/product.php"] = [["product_no"]]
	view_patterns["/nread.nhn"] = [["artclNo", "bbsNo"]]
	view_patterns["/nx.aspx"] = [["n4ArticleSN", "maskPageType"]]


	view_patterns["/content.asp"] = [["code", "num", "ref", "tb" ], ["table", "board_idx"] , ["tb_name","board_idx"], ["board","grp", "num"],  ["board", "num"], ["tb", "num"],["idx", "tname"], ["cate", "idx"] ]   # ref는 의미 없지만 없으면 안 나오심. ㅜㅜ
	view_patterns["/content.php"] = [["ct", "no" ], ["tid", "mode", "n"], ["table", "idx"], ["board", "id" ]]


	view_patterns["/board_show.asp"] = [["board" , "idx"]]
	view_patterns["/ezboard.asp"] = [["id","idx", "mode" ]]
	view_patterns["/board.comm"] = [["action", "iid","num"], ["action", "kid", "num"]] 
	view_patterns["/column.comm"] = [["action", "iid","num"], ["action", "kid", "num"] ]
	view_patterns["/board.html"] = [["code","lock","num1","num2","type"]]
	view_patterns["/board.preexistence.html"] = [["code","lock","num1","num2","type"]]
	view_patterns["/view_body.php"] = [["code", "number"]]
	view_patterns["/view_board.html"] = [["q_id_board", "q_sq_board" ], ["q_id_info", "q_sq_board"]]
	view_patterns["/PolicyTypeView.jsp"] = [["kindSeq", "menuCd" , "menuSeq", "writeDateChk"]]
	view_patterns["/hb_boardview.php"] = [[ "bseq", "page_seq", "seq"]]
	view_patterns["/sb_read.php"] = [["b_id","num"]]
	view_patterns["/b_read.php"] = [["b_id","num"]]

	view_patterns["/read"] = [["articleId","bbsId", "itemId"], ["articleId","bbsid"]]
	view_patterns["/read.asp"] = [["id", "ref", "step"], ["type", "id"], ["arti_id"], ["board_name", "board_idx"], ["q_db", "idx" ] ]
	view_patterns["/read.php"] = [["no","table"], ["serial", "table"], ["number", "table"], ["rowid","table"], ["board","seq"], ["board","scode"], ["board_code", "num"], ["board", "id"], ["tname", "uid" ], ["bn","num"] ]
	view_patterns["/read.php3"] = [["no","table"], ["serial", "table"], ["number", "table"], ["rowid","table"], ["board","seq"], ["board","scode"], ["board_code", "num"], ["board", "id"], ["tname", "uid" ], ["code", "number"] ]
	view_patterns["/read.htm"] = [["num", "table"], ["num","t" ]]
	view_patterns["/read.html"] = [[ "table", "number"], ["code" ,"number"], ["id", "num"], ["table" , "no"] ]
	view_patterns["/read.cgi"] = [ ["board", "y_number"] ]

	view_patterns["/board_read.php"] = [["bbs_no", "index_no", "mart_id"], ["board_no", "no"] ]
	view_patterns["/board_read.asp"] = [ ["board_name", "board_type","m_id","number"],["id","section", "idx"], ["code", "num", "mode"], ["code","id"], ["code","num"], ["bbsid", "b_num"], ["bbsid","num"], ["Id","ref", "step"]]
	view_patterns["/board_read.jsp"] = [ ["id", "num"], ["db","seq"] ]
	view_patterns["/board_read_new.php"] = [["board_no", "no"]]
	view_patterns["/board_r.asp"] = [["board_id", "bm_idx"]]

	view_patterns["/board_view.htm"] = [["scode", "seq","sub_seq"]]
	view_patterns["/board_view.html"] = [["menu_num", "num"]]
	view_patterns["/board_view.asp"] = [[ "ctcode", "num"], ["doc_no" ,"datekey" ], ["boardname", "id" ], ["contentNo", "listNo"],["bidx" , "strBoardID"] ]
	view_patterns["/board_view.jsp"] = [["tbnum", "num"], ["boardid", "seq"] ]
	view_patterns["/board_view.ws"] = [["boardId" , "docsId"]] 
	view_patterns["/board_view.php"] = [["board", "uid"], ["article_id", "category"], ["db", "number", "mode"], ["board", "num", "tnum"], ["bbs_code", "bbs_number"]]

	view_patterns["/boardview.asp"] = [["code", "num"], ["code","idx"], ["BoardName", "Num"] ]

	view_patterns["/board_view2.jsp"] = [["boardid", "seq"]]

	view_patterns["/BoardView.asp"] = [["IDNo" , "Table"]]
	view_patterns["/BoardView.aspx"] = [["BoardID" ,"ID"],["type", "mno", "bno"] ]
	view_patterns["/BoardView.php"] = [["nSeq" , "nBlogCateSeq1" ]]

	view_patterns["/iBoardView.phtml"] =  [["bbsid", "iArticleId"]]
	view_patterns["/Article.jsp"] = [["table", "articleIndex" ]]
	#view_patterns["/articleV.php"] = [["mbsC", "mbsIdx"]]
	view_patterns["/articleVC.php"] = [["mbsC", "mbsIdx"]]
	view_patterns["/newCommonView.do"] = [["newsId"]]
	view_patterns["/news_view.asp"] = [["NewsNum"]]
	view_patterns["/sub_detail.html"] = [["brandKey", "goodsCode"]]
	view_patterns["/download.php"] = [["id", "no","filenum"]]
	view_patterns["/Trend.asp"] = [["vBoard","intIdx"]]
	view_patterns["/TrendView.asp"] = [["board", "bseq"] , ["num"] ]
	view_patterns["/newsRead.php"] = [["no", "year"]]
	view_patterns["/abbr_view"] = [["m_temp1"]]
	view_patterns["/default.aspx"] = [["act", "id", "pid"]]
	view_patterns["/review_read.php"] = [["table", "seq" ,"no"], ["table_name", "number"] ,["no"] ]
	view_patterns["/Detail.aspx"] = [["BoardID", "MAEULNO", "no", "ref"], ["BoardID", "MAEULNO", "no"]]
	view_patterns["/showthread.php"] = [["tid","threadid",  "t", "p" ]] 
	view_patterns["/pdsRead.jsp"] = [["name", "number"]]
	view_patterns["/show.htm"] = [["bn", "pkid"]]
	view_patterns["/outBoardDoc.asp"] = [[ "cidx" ,"idx" ], ["GrpID", "LstID"]]
	view_patterns["/mboard.asp"] = [["exe", "board_id", "idx_num", "group_name"],["exec","strBoardID", "intSeq"],["Action","strBoardID", "intSeq"], ["board_id", "idx_num", "group_name"], ["exe", "idx_num", "group_name"]]
	view_patterns["/allnews_view.asp"] = [["seq"]]
	view_patterns["/fsboard.asp"] = [["id", "idx", "mode"]] 
	view_patterns["/community.php"] = [["id", "mode", "num"]]
	view_patterns["/Default.asp"] = [["mode" ,"board", "go", "idx"]]
	view_patterns["/iboard.asp"] = [["code", "mode", "num"]]
	view_patterns["/story"] = [[ "at", "azi" ]]
	view_patterns["/ques_view.html"] = [[ "dnum", "qnum" ]]
	view_patterns["/viewbody.html"] = [["category","code", "number"]]
	view_patterns["/article.html"] = [["imode", "D", "num", "cate", "d_category"] , ["imode", "D", "num"]]
	view_patterns["/UserBBS.asp"] = [["aid", "mode", "ReadSn" ]]
	view_patterns["/BbsContentView.asp"] = [["seq"]]  # directory로 게시판 구분  
	view_patterns["/fifaonline_board.nwz"] = [["bserial" ,"ano",  "act"]]
	view_patterns["/DBWork.asp"] = [["itemIDT", "aID"]]
	view_patterns["/board.asp"] = [["id", "mode", "idx"], ["id", "idx"],["id","no","mode"], ["id", "no"], ["board", "id"], ["bname","ct","num"]]
	view_patterns["/Detail.asp"] = [["nsCode"]]
	view_patterns["/channel_read.asp"] = [["bid", "idx"]]
	view_patterns["/bbs2.nhn"] = [["bbsid", "docid", "m" ]]
	view_patterns["/bbs.message.view.screen"] = [[ "bbs_id", "message_id"]]
	view_patterns["/etc_list.php"] = [["code", "id"]]
	view_patterns["/view.mbc"] = [["bid", "list_id"]]
	view_patterns["/view"] = [["db", "no"], ["articleID", "category"], ["code","No"]]
	view_patterns["/viewArticle.do"] = [["artiNo","listCateNo"]]
	view_patterns["/GoodRest_view.asp"] = [["id"]]

	# moneta 
	view_patterns["/bbs.normal.qry.screen"] = [["p_bbs_id", "p_message_id", "sub", "depth", "top"]]
	view_patterns["/bbs.anony.qry.screen"] = [["p_bbs_id", "p_message_id", "sub", "depth", "top"]]

	view_patterns["/free_view.htm"] = [["uid"], ["no"] ]
	view_patterns["/free_read.htm"] = [["id"]]

	# 쇼핑 
	view_patterns["/goods_view.php"] = [["goodsno"] ]
	view_patterns["/good_detail.php"] = [["goodcd"]]
	view_patterns["/shop_detail.asp"] = [["idx", "cm", "ct"], ["idx"]] 
	view_patterns["/shop.php"] = [["SHOP_TBL", "sno" ]]
	view_patterns["/merchan_details.asp"] = [["num"]]
	view_patterns["/shopdetail.html"] = [["brandcode"], ["branduid"]]

	view_patterns["/bbs.daum"] = [["code", "articleId"]]

	view_patterns["/list.pull"] = [["AM", "bcode", "pb1_code"], ["AM", "bcode"]  ] 
	view_patterns["/list.html"] =  [["idxno", "no" ], ["idxno","table"], ["num","code"], ["idx", "st", "bo_table"] ]
	view_patterns["/list.asp"] = [["ct", "bname","num"]]
	view_patterns["/list.php"] = [["id", "no"], ["db", "idxno"], ["action","forum","seq"] ]
	view_patterns["/board.list"] = [["act","id", "mcode"]]
	view_patterns["/board.read"] = [["act","id", "mcode"]]
	view_patterns["/userbbs.aspx"] = [["bn", "mode", "no"]]
	view_patterns["/kboard.php"] = [["act","board","no"]]

	VIEW_HINT = ["cmd=view" ,"action=read", "action=view", "mode=view", "mode=read", "act=view","act=read",  "command=body", "act=bbs_view", "query=view", "mode=view"] 



	list_patterns["/kboard.php"] = [["board","page"]]
	list_patterns["/userbbs.aspx"] = [["bn", "page"]]
	list_patterns["/bbs.message.list.screen"] = [["bbs_id", "current_page"]] 
	list_patterns["/enti.php"] = [["bn","page"]]
	list_patterns["/cboard.php"] = [["board", "page"]]
	list_patterns["/bbs2.nhn"] = [["bbsid", "page"]]
	list_patterns["/DTWork.asp"] = [["itemIDT", "page"]]
	list_patterns["/UserBBS.asp"] = [["aid", "page"]] 
	list_patterns["/community.php"] = [["id", "mode", "page"]]
	list_patterns["/fsboard.asp"] = [["id", "page"]]
	list_patterns["/gallery.php"] = [["id","page"]]

	list_patterns["/pdsView.jsp"] = [[ "name" ,"page"]] 
	list_patterns["/board.php"] = [ ["db", "cmd", "page"], ["tbnum","mode", "page"], ["tbname", "cmd",  "page_num"],["board", "command", "exe"], ["board","page"], ["bo_table","page"], ["id","page"],["tbname", "action","page"], ["tbname","page"], ["b_id","page" ], ["bId", "pageNo"], ["db","pg"], ["board_code", "page"], ["db", "page"] ]
	list_patterns["/list.htm"] = [["page", "table"], ["bn", "thisPage"]]
	list_patterns["/list.php"] = [ ["tn", "pagenum"] ,["action","forum", "page" ],  ["board", "page", "page_start"], ["board_id" ,"game_no", "pagenum"], ["id", "page"] , ["boardid", "startPage"], ["bbs_code" , "page"], ["bbs_id", "page"], ["db", "code", "page"],["table", "start"], ["page","table"], ["bid", "page"], ["field", "start"], ["tbname", "page"], ["gb", "page"], ["bd_no", "page"], ["board","position"], ["bbs", "cpage"], ["code", "page"], ["board","page" ], ["id", "start"], ["bcode", "page"], ["db", "page"], ["tname","page"], ["forum","page"], ["gcode","p"] ]
	list_patterns["/mlist.php"] = [["id","page"]]
	list_patterns["/sb.php"] = [["b_id", "page"]]
	list_patterns["/zboard.php"] = [ ["id", "page"] ]
	list_patterns["/"] = [["mid", "page"] ,["bo_table", "doc", "page"], ["code", "p", "page" ],["bset", "page", "tot_code" ],  ["BbsCode","kids","page"], ["board","page"], ["bc", "pg"], ["m", "bid", "page"], ["mode" ,"board" ,"go", "gotopage"], ["bbs_table", "menu_table", "ref", "page"], ["bid", "page"], ["BDOC","BID", "npage" ], ["bo_table", "page"], ["GO","position"] ]
	list_patterns["/index.php"] = [ ["pagekey", "page"], ["nPage", "nBlogCateSeq1"], ["boardid", "mode", "start"], ["page","mid"]]
	list_patterns["/board.cgi"] = [ ["db", "page"], ["id","action","p"], ["id", "page"] ]
	list_patterns["/ezboard.cgi"] = [ ["db", "page", "action"] ]
	list_patterns["/ezboard.exe"] = [ ["db", "page", "action"] ]
	list_patterns["/list.asp"] = [ ["tbcode", "page"], ["tbName", "menu", "curpage"], ["bname", "cpage" ], ["page","tb"], ["tb_name", "page"], ["master_id", "page"], ["master_id", "Page"], ["bbs_id", "bca", "page"], ["big", "middle", "small", "page"], ["table", "page"], ["board_idty", "page"], ["tname", "page"], ["BID", "page"], ["tn", "gotopage"], ["bbsid", "page"]]

	list_patterns["/list.aspx"] = [["id", "page"]]
	list_patterns["/m_list.asp"] = [["table", "page"]]
	list_patterns["/kisboard.html"] = [ ["code", "start"] ]
	list_patterns["/board_main.html"] = [ ["NC_id", "page"] ]
	list_patterns["/list.html"] = [ ["page", "subcode"], ["table_name","page"], ["code", "category", "page"], ["category_id", "page"], ["menutree", "page"],["tb", "code", "page"], ["code", "page"], ["code", "start"], ["table", "page"], ["table", "pg"], ["c", "p"],["bid","gid", "page"],  ["bid", "p"], ["board_id", "cline"], ["blog_board", "cline"], ["menu2","start" ] ]
	list_patterns["/bbs.php"] = [["p" , "table" ], ["bbid","page"], ["id", "p", "category"], ["id", "page"] ]
	list_patterns["/bbs.php3"] = [["p" , "table" ]]
	list_patterns["/bbs.html"] = [["Table", "page"], ["imode", "D", "start"], ["bcode", "page"], ["bbs_code","page"]]
	list_patterns["/main.cgi"] = [["board", "number"]] 
	list_patterns["/articleList.html"] = [["page"]]
	list_patterns["/articleL.php"] = [["mbsC","cpage"]]
	list_patterns["/board_list.php"] = [["board_no", "page"], ["bbs_no", "mart_id", "page"], ["code", "GotoPage"] ]
	list_patterns["/board_list.ws"] = [["boardId" , "pageNum"]] 

	#list_patterns["/nx.aspx"] = [["n4PageNo"]]
	list_patterns["/pb.php"] = [["id","page"]]
	list_patterns["/list.mbc"] = [["bid","page"]]

	# shopping
	list_patterns["/shopbrand.html"] = [["xcode","page"]]
	list_patterns["/category.php"] = [["cate_no", "page"]] 
	list_patterns["/goods_list.php"] = [["category", "page"]]
	list_patterns["/good_align.php"] = [["refrefcd", "page"], ["refcatcd", "page"], ["refvircd","page"] ]
	list_patterns["/good_virture_align.php"] = [["refvircd", "page"]]
	list_patterns["/good_theme.php"] = [["thecd", "page"]]

	list_patterns["/channel_list.asp"] = [["bid","gotopage"]]

	list_patterns["/BBSList.jsp"] = [["table", "page"]]
	list_patterns["/listArticle.do"] = [["cateNo", "page"]]

	list_patterns["/board.html"] = [["code", "page"]]
	list_patterns["/board.preexistence.html"] = [["code", "page"]]
	list_patterns["/board.asp"] = [["code", "tb", "page"],["board", "grp","page"], ["board", "page"], ["db","mode","page"], ["id", "Mode", "page"], ["board_name", "page"] ,["id","page", "pageblock", "cnt"], ["id", "page" ], ["bname", "cpage"]]
	list_patterns["/ezboard.asp"] = [["id","mode", "page"]]
	list_patterns["/list.ga"] = [["id","page"]]
	list_patterns["/BoardView.asp"] = [["IDNo" , "Page"]]
	list_patterns["/hb_boardlist.php"] = [[ "page_seq", "page" , "seq"]]
	list_patterns["/index.asp"] = [["file", "menu", "page"], ["boardcode", "page"]]
	list_patterns["/board.htm"] = [["scode", "page"]]
	list_patterns["/bbs_list.php"] = [["code", "page"]]
	list_patterns["/CyberCar.php"] = [["gubun", "page"]]
	list_patterns["/iBoardList.phtml"] = [["bbsid","iPage"] ]
	list_patterns["/boardlist.asp"] = [["BoardName", "ScrollAction"], ["BoardName", "Page"]]
	list_patterns["/board_list.asp"] = [["bbsid", "page"], ["ctcode", "page"], ["boardname", "page"], ["id" ,"page" , "section"], ["pageNow", "listNo"], ["Id","page"]]
	list_patterns["/board_list.jsp"] = [["tbnum", "req_pg"], ["boardid", "page"], ["id", "Req_pg"] ]
	list_patterns["/ttboard.cgi"] = [["bname", "page"], ["db","page"]]
	list_patterns["/Trend.asp"] = [["vBoard","intPage"]]
	list_patterns["/index.cgi"] = [["action", "page"]]
	list_patterns["/list"] = [["bbsId","itemId", "pageIndex"], ["db", "page"], ["category","page"], ["code", "page"]]
	list_patterns["/board.comm"] = [["iid", "pageNo" ] ]
	list_patterns["/view_board.html"] = [["q_id_board", "page" ]]
	list_patterns["/index.jsp"] = [["cmd", "code", "page_no" ]]
	list_patterns["/list.do"] = [["boardName", "curPage"], ["manageIdx", "menuCode" , "go"]]
	list_patterns["/review_list.php"] = [["table", "page"] , ["cate", "page"],["tb", "page"], ["pageNo"] ]
	list_patterns["/List.aspx"] = [["BoardType", "PageNo"], ["BoardID", "MAEULNo", "page"], ["index", "page"], ["bid", "pNo"] ]
	list_patterns["/powerbbs.php"] = [["come_idx", "p"]]
	list_patterns["/forumdisplay.php"] = [["f", "page"], ["forumid","page"], ["forumid", "pagenumber"] ]
	list_patterns["/list.pull"] = [["AM", "page"]]
	list_patterns["/list_new_know.html"] = [["dnum", "start"], ["start"]]
	list_patterns["/bbs.htm"] = [["dbname", "mode", "page"]] 
	list_patterns["/index.html"] = [["page_name", "menu_key","start"], ["category","page","section"]]
	list_patterns["/index.htm"] = [["page_name", "menu_key","start"]]
	list_patterns["/lstBoardDoc.asp"] = [["GrpID", "hdnPage"], ["cidx", "page"]] 
	list_patterns["/mboard.asp"] = [["exe" , "board_id", "group_name", "page" ], ["strBoardID", "intPage"], ["exe", "group_name", "page" ], ["board_id", "group_name", "page"]  ]
	list_patterns["/bbs.asp"] = [["exe" , "board_id", "group_name", "page" ],  ["exe", "group_name", "page" ]] 
	list_patterns["/BBS.asp"] = [["Board_Id", "Page"]]
	list_patterns["/freeb.php"] = [["code", "page"]]
	list_patterns["/Default.asp"] = [["mode" ,"board" ,"go", "gotopage"]]
	list_patterns["/BoardList.aspx"] = [["type", "mno"], ["CurrentPage", "ID" ]]
	list_patterns["/iboard.asp"] = [["code", "page"]]
	list_patterns["/bbs.normal.lst.screen"] = [["p_bbs_id", "p_page_num" , "p_current_sequence", "p_start_page", "sub", "depth", "top"], ["p_bbs_id", "p_page_num", "sub","depth", "top" ]]
	list_patterns["/bbs.normal.anony.screen"] = [["p_bbs_id", "p_page_num" , "p_current_sequence", "p_start_page", "top"],["p_bbs_id", "p_page_num", "top" ]]
	list_patterns["/column.comm"] = [["iid", "pageNo"]]
	list_patterns["/Board.mi"] =  [["boardId", "page" ]]
	list_patterns["/ques_solu_home_a.html"] = [["dnum", "start"]]
	list_patterns["/index.do"] = [["bseq", "currpage"]] 
	list_patterns["/BbsListView.asp"] = [["pageno"]]
	list_patterns["/DiscourseList.asp"] = [["cCode" , "n4_Page"]]
	list_patterns["/SectionTagList.nhn"] = [["tagname", "currentPage"]] 
	list_patterns["/yardList.jsp"] = [["yyear", "pg"]]
	list_patterns["/board.list"] = [["act","page", "mcode"]]
	list_patterns["/etc_list.php"] = [["code", "page"]]
	list_patterns["/GoodRest_list.asp"] = [["pg", "mn"]]
	list_patterns["/srh_board.html"] = [["q_id_info","srh[page]"]]
	list_patterns["/mini.php"] = [["id","page"]]
	list_patterns["/list.nhn"] = [["bbsNo","page"]]

	t_view_patterns["board.nwz"] = [["ano", "bserial", "act"]]
	t_view_patterns["_view.html"] = [["hm_idx"]]
	t_view_patterns["_view.htm"] = [["uid"]]
	t_list_patterns["board.nwz"] = [["bserial", "page"]]

#	t_list_patterns["_list.htm"] = [["Ncode", "page"] , ["page"]]
	#t_list_patterns["_list.html"] = [["page"]]


	pageset = ["p", "page", "iPage", "startPage", "Page", "req_pg", "position", "curPage", "pageIndex", "pageNo", "cpage", "page_num", "npage", "pg", "page_no", "bpage", "nPage", "pNo", "gotopage", "GotoPage", "p_page_num", "p_start_page", "currpage", "n4_Page", "currentPage", "current_page", "curpage", "GoPage" , "pagenow", "intPage", "PageNo", "pno", "srh[page]", "n4pageno", "nowpage", "n4PageNo", "pageno", "pageNum"]
	startpoint = ["number", "start" , "cline"]

	noset = ["row_id", "no" , "num", "idx", "bbslist_id", "l", "wr_id", "number", "ano", "keyno", "kid", "ReadSn", "pid", "qnum", "uid", "articleId", "gul", "document_srl", "No", "pk", "seq", "rno", "articleIndex", "q_sq_board", "nSeq", "BoardNo", "Seq", "code", "bcode", "BNum", "bIdx", "articleID", "contID", "DocId", "artiNo", "articlekey", "mbsIdx", 'n', "q_sq_board", "n4ArticleSN", "artclNo", "docid", "gid", "oidarticle"]

	exset = ["maskPageType", "c_reply_num"]

def removeGarbage(url, view_patterns=None, list_patterns=None):

	if view_patterns ==None:
		view_patterns=regdata.view_patterns
	if list_patterns ==None:
		list_patterns=regdata.list_patterns
	parsed_url = urlparse.urlparse(url)
	page = parsed_url[2][parsed_url[2].rfind("/"):]
	query = parsed_url[4]
	qdict = makeDict(query)
	
	pattern_set = dict()

	pattern = None
	type = None

	if page in view_patterns:
		for t_pattern in view_patterns[page]:
			isFullyChecked = True
			for param in t_pattern:
				if not param in qdict.keys():
					isFullyChecked = False
					continue
				elif param in regdata.cmd_list:
					if qdict[param] not in regdata.view_cmd and len(t_pattern) >=3:
						isFullyChecked = False
						continue
					
			if isFullyChecked:
				pattern = t_pattern
				type = "view"
				break
	
	if page in list_patterns:
		if pattern == None:
			for t_pattern in list_patterns[page]:
				isFullyChecked = True
				missed_param = None

				# 전체 매치와 부분 매치를 따로 하는 이유 
				# pattern내의 파라미터가 전부 존재하는 경우 
				for param in t_pattern:
					# 파라미터가 없거나 있더라도 value가 없는 경우 
					if not param in qdict.keys() or not (qdict[param] and len(qdict[param]) > 0):
						isFullyChecked = False
						break
					# 커맨드형 파리미터인데 list_cmd가 아닌 경우 
					elif param in regdata.cmd_list:
						if qdict[param] not in regdata.list_cmd:
							isFullyChecked = False
							break
					# page가 0이나 1이여서 listtop인 경우
					elif param in regdata.pageset and (qdict[param] == "1" or qdict[param] == "0"):
						if url.find("web.humoruniv.") >= 0:
							if  qdict[param] == "0":
								missed_param = param
						else:
							missed_param = param
				if isFullyChecked:
					pattern = t_pattern
					if missed_param:
						type = "listtop"
					else:
						type = "list"
					break
			if pattern  == None:
				# pattern내의 page파라미터가 없는 경우
				for t_pattern in list_patterns[page]:
					isFullyChecked = True
					missed_param = None
					for param in t_pattern:
						if not param in qdict.keys():
							if (param in regdata.pageset or param in regdata.startpoint) and not missed_param:
								missed_param = param	
							else:
								isFullyChecked = False
								break
						elif param in regdata.cmd_list and qdict[param] not in regdata.list_cmd:
							isFullyChecked = False
							break
						elif not (param in qdict.keys() and qdict[param] and len(qdict[param]) > 0):
							isFullyChecked = False
							break
					if isFullyChecked:
						pattern = t_pattern
						if missed_param:
							type = "listtop"
						else:
							type = "list"
						break

	if pattern == None:
		for t_pp in regdata.t_view_patterns:
			if parsed_url[2].find(t_pp) >= 0:
				for t_pattern in regdata.t_view_patterns[t_pp]:
					isFullyChecked = True
					for param in t_pattern:
						if not param in qdict.keys():
							isFullyChecked = False
							continue
						elif param in regdata.cmd_list:
							if qdict[param] not in regdata.view_cmd:
								isFullyChecked = False
								continue
							
					if isFullyChecked:
						pattern = t_pattern
						type = "view"
						break

		if pattern  == None:
			for t_pp in regdata.t_list_patterns:
				if parsed_url[2].find(t_pp) >= 0:
					for t_pattern in regdata.t_list_patterns[t_pp]:
						isFullyChecked = True
						missed_param = None
						for param in t_pattern:
							if not param in qdict.keys():
								if (param in regdata.pageset or param in regdata.startpoint) and not missed_param:
									missed_param = param	
								else:
									isFullyChecked = False
									continue
							elif param in regdata.cmd_list and qdict[param] not in regdata.list_cmd:
									isFullyChecked = False
									continue
							elif not (param in qdict.keys() and qdict[param] and len(qdict[param]) > 0):
								isFullyChecked = False
								continue
						if isFullyChecked:
							pattern = t_pattern
							if missed_param:
								type = "listtop"
							else:
								type = "list"
							break

				
	candiView = False
	for param in qdict:
		if param in regdata.noset:
			if url.find("zboard") >= 0 and param == "idx":
				pass
			elif url.find("thinkpool.com") >= 0 and  param == "code":
				pass
			elif qdict[param].isdigit() and url.find("list") < 0:
				candiView = True

	for kk in regdata.VIEW_HINT:
		if query.lower().find(kk) >=0:
			candiView = True
			break

	if type == "listtop" and candiView:
		type = None
		pattern =None

	if pattern == None:
		# xe보드에 대한 처리 
		try:
			total_page = parsed_url[2]
			if len(parsed_url[4]) == 0 or (len(qdict) == 1 and "PHPSESSID" in qdict):
				hcur = total_page.find("/bbs/")
				if hcur < 0:
					hcur = total_page.find("/xe/")
				if hcur < 0:
					hcur = total_page.find("/zbxe/")
				if hcur < 0:
					hcur = total_page.find("/board/")

				if hcur >= 0:
					ppage = total_page[hcur:]
					if ppage.count("/") == 5:
						pieces = ppage.split("/")
						
						if pieces[4] == "page":
							ppage = "/".join(pieces[:4])
							rr_url = parsed_url[0] + "://" + parsed_url[1] + total_page[:hcur] + ppage
							return (rr_url, "view")
					elif ppage.count("/") == 3:
						pieces = ppage.split("/")
						try:
							num = int(pieces[3])
							rr_url = parsed_url[0] + "://" + parsed_url[1] + parsed_url[2]
							return (rr_url, "view")
						except:
							pass
					elif ppage.count("/") == 4:
						pieces = ppage.split("/")
						if pieces[3] == "page":
							return (url, "list")
					elif ppage.count("/") == 2:
						pieces = ppage.split("/")
						try:
							num = int(pieces[2])
						except:
							if len(pieces[2]) > 1:
								if pieces[2].find(".") >= 0:
									return (url, None)
								board = "mid=%s"%pieces[2]
								ppage = "/".join(pieces[:2])
								if url.find("/zbxe/") >= 0:
									rr_url = parsed_url[0] + "://" + parsed_url[1] + total_page[:hcur] + ppage +"/index.php?%s"%board
								else:
									rr_url = parsed_url[0] + "://" + parsed_url[1] + total_page[:hcur] + ppage +"/?%s"%board
								if url.find("/xe/") >= 0 or url.find("/zbxe/") >= 0:
									return (rr_url, "listtop")
								"""
								else:
									return (url, "listtop")
								"""
		except:
			pass

		L = qdict.items()
		L.sort(key=lambda item:item[0])

		pp = list()
		
		for key, val in L:
			pp.append("%s=%s"%(key,val) )
			
		param = "&".join(pp)
		if len(param) == 0:
			r_url = parsed_url[0] + "://" +  parsed_url[1] + parsed_url[2]
		else:
			r_url = parsed_url[0] + "://" +  parsed_url[1] + parsed_url[2] + "?" +param
		return (r_url, None)

	else:
		n_dict = dict()
		new_query = list()
		pattern.sort()
		for param in pattern:
			try:
				if param in qdict and qdict[param] and not  (type == "listtop" and param in regdata.pageset):
					new_query.append(param+"="+qdict[param])
			except Exception, msg:
				print (msg)
				new_query = ""
		if len("&".join(new_query)) == 0:
			return (parsed_url[0] + "://" +  parsed_url[1] + parsed_url[2] , type)
		return (parsed_url[0] + "://" +  parsed_url[1] + parsed_url[2] + "?" +"&".join(new_query) , type)


def makeURLPattern(results):
	
	global regdata

	view_patterns = regdata.view_patterns
	list_patterns = regdata.list_patterns

	for type, page, params in results:
		if type == "view":
			if page in view_patterns:
				view_patterns[page].insert(0, params.split("*") )
			else:
				view_patterns[page] = [params.split("*")]
		elif type == "list":
			if page in list_patterns:
				list_patterns[page].insert(0, params.split("*") )
			else:
				list_patterns[page] = [params.split("*")]
	return view_patterns, list_patterns





def makeDict(query):

	t_dict = dict()
	para_pieces = query.split("&")
	for para in para_pieces:
		res = para.split("=")
		para_name = res[0]

		if para_name in regdata.TRIM_PARA:
			continue
		if len(res) < 2:
			para_value = None
		else:
			if len(res[1]) > 0:
				para_value = "=".join(res[1:])
				t_dict[para_name] = para_value
			else:
				para_value = ''
	return t_dict


def handleBlogUrls(url, domain, id=None):

	if domain == "blog.naver.com" or domain.find(".blog.me") >= 0:
		return  handleNBUrl(url, id)
	elif domain == "blog.daum.net":
		return handleDBUrl(url, id)

def detectDaumBlog(url, withTmpID=False):
	parsed_url = urlparse.urlparse(url)
	if parsed_url[1] == "blog.daum.net":
		pieces = parsed_url[2].split("/")
		if len(pieces) >= 2:
			blogId = pieces[1]
			if len(pieces) == 3:
				try:
					articleno = int(pieces[2])
					if withTmpID:
						req = urllib2.Request(url)
						f = urllib2.urlopen(req)
						html = f.read()
						m = re.search("blogid=(.+?)&amp;", html)
						if m:
							tmp_id = m.group(1)
							return blogId, tmp_id
					else:
						return blogId, ""
				except:
					pass

	return "",""

	
def handleDBUrl(url, tmp_id=None, key="key"):

	# http://blog.daum.net/tourcodi/8128482   
	# return Data
	# http://blog.daum.net/_blog/BlogTypeView.do?blogid=07Qbe&articleno=13419044    
	# http://blog.daum.net/_blog/hdn/ArticleContentsView.do?blogid=0Gagz&articleno=8128486

	parsed_url = urlparse.urlparse(url)
	if parsed_url[1] == "blog.daum.net":
		pieces = parsed_url[2].split("/")
		if len(pieces) >= 2:
			blogId = pieces[1]
			if len(pieces) == 3:
				try:
					articleno = int(pieces[2])
					if not tmp_id:
						try:
							req = urllib2.Request(url)
							f = urllib2.urlopen(req)
							html = f.read()
							m = re.search("blogid=(.+?)&amp;", html)
							if m:
								tmp_id = m.group(1)
						except Exception, msg:
							pass
					if tmp_id and articleno and key == "key":			
						return "http://blog.daum.net/_blog/BlogTypeView.do?articleno=%s&blogid=%s"%(articleno, tmp_id), "http://blog.daum.net/_blog/hdn/ArticleContentsView.do?articleno=%s&blogid=%s"%(articleno, tmp_id)
					elif tmp_id and articleno and key == "trackback":
						return "http://blog.daum.net/_blog/getTrackbacks.do?articleno=%s&blogid=%s"%(articleno, tmp_id)

				except Exception, msg:
					pass
	return ""

def handleNBUrl(url, org_id=None, type="key"):
	# http://blog.naver.com/PostView.nhn?blogId=zera80&logNo=50113077803	
	# http://blog.naver.com/sohcool1?Redirect=Log&LogNo=150112150871 
	# http://exboifriend.blog.me/70101722381
 	# http://blog.naver.com/sejong_jang/109389607

	# http://fogopa.com/192711840

	blogId = ""
	logNo = ""
	parsed_url = urlparse.urlparse(url)


	if parsed_url[1] == "blog.naver.com":
		pieces = parsed_url[2].split("/")

		params = parsed_url[4].split("&")
		for param in params:
			para = param.split("=")
			if para[0] == "blogId":
				blogId = para[1]
			elif para[0] == "logNo":
				logNo = para[1]
		if pieces[1] == "PostView.nhn":
			pass
		elif pieces[1] == "PostRelayList.nhn":
			type = "trackback"
			pass
		elif pieces[1] == "GoPost.nhn":
			pass
		elif len(pieces) >= 2:
			blogId = pieces[1]
			if len(pieces) == 3:
				logNo = pieces[2]
	elif parsed_url[1].find(".blog.me")>= 0:
		blogId = parsed_url[1].split(".")[0]
		logNo = parsed_url[2].split("/")[1]


	elif org_id:
		logNo = parsed_url[2].split("/")[1]
		blogId = org_id

	
	if blogId != "" and  logNo != "":
		if type == "key":
			n_url = "http://blog.naver.com/PostView.nhn?blogId="+blogId+"&logNo="+logNo
			return n_url
		elif type == "trackback":
			n_url = "http://blog.naver.com/PostRelayList.nhn?blogId="+blogId+"&logNo="+logNo
			return n_url
	return ""



def getSiteData(url):
	from test_util import getSiteId
	from common_func import getDBCursor

	DBHOST = "bbsdb-mst.s0.crawl.web.search"
	(db, cursor) = getDBCursor(host=DBHOST, user='bbsmoa', passwd='bbscrawler', db='bbsdb') 

	id = getSiteId(url, cursor)


	if id:

		print id, url
		query = "SELECT type, page, param FROM url_patterns where site_id = %s"
		cursor.execute(query, id)
		results = cursor.fetchall()

		if results:
			return  makeURLPattern(results)

	return None, None

if __name__ == "__main__":

	#print makeEncryptDB('222.231.51.83','edi','wwwcrawler','sitedb','sitedb09')
	#print makeEncryptDB('222.231.51.82','edi','wwwcrawler','sitedb','sitedb09')

#	Init()
#	print garbage_set, len(garbage_set)
	garbage_set = {}
	import sys
	 
	if len(sys.argv) > 1:
	 	url = sys.argv[1]
		(view_pattern, list_pattern) = getSiteData(url)
		if view_pattern:

			print view_pattern, list_pattern
			print removeGarbage(url, view_pattern, list_pattern)
		else:
			print removeGarbage(url) #, view_pattern, list_pattern)
		 
	else:
		urls = ["http://www.gobada.co.kr/bbs/board.php?bo_table=a003&wr_id=&page=3", "http://www.vote2007.or.kr/bbs/board.php?bo_table=report&wr_id=837&sfl=&stx=&sst=wr_hit&sod=desc&sop=and&page=8" , "http://mclaren.kr/board/bbs/board.php?bo_table=news_column&wr_id=41&page=&doc_page=2", "http://action.jinbo.net/zbxe/?mid=guest&document_srl=6552&search_target=regdate&search_keyword=200807" , "http://cineast.kr/bbs/board.php?bo_table=co_free&wr_id=58155&sfl=&stx=&sst=wr_hit&sod=desc&sop=and&page=7", "http://naver.com/?fdasdf.ff", "http://therain.co.kr/bbs/view.php?bn=notice&no=173&PHPSESSID=8a51457fff69fcfedcd43061c35cdbe5", "http://www.kisboard.com/kisboard/kisboard.html?code=opensource&page=view&num=95", "http://www.spcgi.com/spboard/board.cgi?id=pds&action=view&gul=100&page=1&go_cnt=1", "http://wow.gamemeca.com/special/section/html_section/wow/focus/info_board/view.html?subcode=c0583&page=1&search_kind=&search_text=&id=9841&gid=9621&head="]

		for url in urls:
			print removeGarbage(url)
		print removeGarbage("http://www.clubauto.co.kr/board/view.php?db=maintenance&id=31469&page=1")
		print removeGarbage("http://www.thisisgame.com/board/view.php?id=145073&board=0&page=4&category=744&subcategory=&best=&searchmode=title&search=&orderby=")

		
		print removeGarbage("http://bbs3.agora.media.daum.net/gaia/do/story/list?bbsId=K161")
		print removeGarbage("http://news.sidaesori.co.kr/bbs.html?Table=ins_bbs75&mode=view&uid=10530&page=1")
		print removeGarbage("http://ggsw.com/bbs/view.php?id=photo&no=36")
		print removeGarbage("http://happycj.org/technote6/board.php?board=m09com4&command=body&no=536")
		print removeGarbage("http://gooddaysports.co.kr/news/?tset=list&bot_code=269&asdf="), "AAAAAAA"

		print removeGarbage("http://nbbs3.sbs.co.kr/index.jsp?cmd=read&code=tb_temptation01&top_index=&field=&keyword=&category=&star=&no=35840&page_no=1")
		print removeGarbage("http://nbbs3.sbs.co.kr/index.jsp?cmd=list&code=tb_temptation01&top_index=&field=&keyword=&category=&star=&page_no=3")

		print removeGarbage("http://gall.dcinside.com/list.php?page=10&id=panasonic")
		print removeGarbage("http://gall.dcinside.com/list.php?id=cain&no=7894")

		print removeGarbage("http://ruliweb4.nate.com/ruliboard/list.htm?main=nds&table=discuss_leisure&left=e&db=2")

		print removeGarbage("http://action.or.kr/home/bbs/board.php?bo_table=action_news&page=1")
		print removeGarbage("http://218.38.16.224/~yeongju/lend/zboard.php?id")

		print removeGarbage("http://www.mukmool.co.kr/ttboard/ttboard.cgi?category=&search_method=&search_mode=&search_word=&act=view&code=22199&bname=GUEST&page=2&SearchBlock=1")
		print removeGarbage("http://www.daum.net/index.cgi?action=list&number=113&a=1" )

		print removeGarbage("http://artsupply.co.kr/shop/goodalign/good_align.php?refrefcd=10")
		print removeGarbage("http://dvdprime.paran.com/bbs/list.asp?major=&minor=&master_id=22&bbsfword_id=&master_sel=&fword_sel=&SortMethod=0&SearchCondition=0&SearchConditionTxt=&Page=2")


		print removeGarbage("http://keben.kbench.com/qna/?bc=21&pg=1&total=177472&code=2342458")
		print removeGarbage("http://action.or.kr/home/bbs/board.php?bo_table=action_news&page=2&page=1")

		print removeGarbage("http://ruliweb2.nate.com/ruliboard/list.htm?table=img_pm")
		print removeGarbage("http://bbs.danawa.com/BoardView.php?nSeq=968103&nPage=47&nBlogCateSeq1=28")
		print removeGarbage("http://www.devpia.com/MAEUL/Contents/List.aspx?page=2&BoardID=69&MAEULNo=911")
		print removeGarbage("http://board.pullbbang.com/list.pull?AM=L&pb1_bcate=1&pb2_bcate=1&pb_cat=B&page=2")
		print removeGarbage("http://www.gim1000.net/sanggaro/?mode=list&cid=66&dong=&field=&query=&page=3")
		print removeGarbage("http://www2.ppomppu.co.kr/zboard/view.php?id=wow&page=8&sn1=&divpage=6&sn=off&ss=on&sc=on&&select_arrange=headnum&desc=asc&no=28865")
		print removeGarbage("http://bogildo.pe.kr/bbs/bbs.htm?dbname=B0002&mode=view&premode=list&seq=558")
		print removeGarbage("http://www.doremihouse.com/bbs/b_humor/128289/page/3")

		(view_patterns, list_patterns) = makeURLPattern([("view", "/view.php", "a_id")])

		print removeGarbage("http://www.twar.co.kr/tbattle/view.php?a_id=35386&tag=10009", view_patterns, list_patterns)
		print removeGarbage("http://poplez.net/xe/daysbbs/6591559")
		print removeGarbage("http://cytic.com/zbxe/gnj")
		print removeGarbage("http://mapthesoul.com/board/freetalk/177011")
		print removeGarbage("http://mapthesoul.com/board/freetalk?page=2&id=asdf&asdfg=")

		print handleNBUrl("http://blog.naver.com/sejong_jang/109389607")
		print handleNBUrl("http://blog.naver.com/sohcool1?Redirect=Log&logNo=150112150871")
		print handleNBUrl("http://iln.blog.me/220042816612")

		print handleDBUrl("http://blog.daum.net/songjumwoo/616")
		print handleDBUrl("http://blog.daum.net/songjumwoo/616", key="trackback")


