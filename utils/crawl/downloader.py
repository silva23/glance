#!/usr/bin/env python
#coding: utf8
import time
import urllib2
import socket

import mechanize
from utils.log import getLogger

socket.setdefaulttimeout(15)

#USER_AGENT = "Mozilla/5.0 (compatible; ZumBot/1.0; http://help.zum.com/inquiry;WOW64;Trident/7.0;rv:11.0) like Gecko"


USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; ZumBot/1.0; http://help.zum.com/;WOW64;Trident/7.0;rv:11.0) like Gecko"

class Downloader:

	def __init__(self, user_agent=USER_AGENT, type="urllib", proxy="N", retry=3):
		self.type = type
		self.USER_AGENT = USER_AGENT
		self.useProxy = proxy
		self.retry = retry

		if type == "urllib":
			if proxy == 'Y':
				proxy = urllib2.ProxyHandler({'http':'10.35.31.136:8008' } )
				self.opener = urllib2.build_opener(urllib2.HTTPRedirectHandler(), urllib2.HTTPCookieProcessor(), proxy )
			else:
				self.opener = urllib2.build_opener(urllib2.HTTPRedirectHandler(), urllib2.HTTPCookieProcessor() )
		elif type == "mechanize":
			self.opener = mechanize.build_opener(mechanize.HTTPRefreshProcessor,)

	def open(self, url):

		http_code, r_url, response = self.getResponse(url)
		if http_code < 400:
			html = response.read()
			header = str(response.info())
			return r_url, header, html
		else:
			return None

	def getHtml(self, response):

		retry_count = 0
		while retry_count < self.retry:
			try:
				html = response.read()
				return html
			except Exception, e:
				getLogger().error(e)
				time.sleep(1)
				retry_count += 1

		return ""


	def getResponse(self, url):

		retry_count = 0
		if self.type == "urllib":
			req = urllib2.Request(url)
			if self.useProxy == "Y":
				req.add_header("User-agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2; OfficeLiveConnector.1.4; OfficeLivePatch.1.3)")
			else:
				req.add_header("User-agent", self.USER_AGENT)

		elif self.type == "mechanize":
			req = mechanize.Request(url)
			if self.useProxy == "Y":
				req.set_proxy("10.35.31.136:8008", "http")
			req.add_header("User-agent", self.USER_AGENT)

		while retry_count < self.retry:
			try:
				response = self.opener.open(req)
				break
			except urllib2.HTTPError, e:
				getLogger().error("%s %s",url, e)
				return e.code, url, str(e)
			except urllib2.URLError, e:
				getLogger().error("%s %s",url, e)
				if str(e).find("Name or service not known") >= 0:
					time.sleep(1)
					retry_count += 2
				else:
					time.sleep(3)
					retry_count += 1
			except Exception, e:
				getLogger().error(e)
				time.sleep(3)
				retry_count += 1

		if retry_count == self.retry:
			return 700, url, e
		if self.type == "urllib":
			return response.code, response.url, response
		else:
			return response.code, response.geturl(), response


if __name__ == "__main__":
	import sys
	#a = Downloader(type="mechanize", proxy="Y")
	#a = Downloader('Y')
	a = Downloader(type="mechanize")
	if len(sys.argv) > 1:
		url = sys.argv[1]
	else:
		url = "http://almien.co.uk/m/tools/net/ip/"
		url = "http://zum.com/"

	http_code, r_url, response = a.getResponse(url)

	if http_code  < 400:
		print http_code, r_url
		print "###################"
		print response.info()
		print response.read()
	else:
		print "CCCCCCCC"
		print http_code, response
	"""
	(a, b, c) = a.open(url)
	print b
	"""
