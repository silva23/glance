#!/usr/bin/env python
#coding:utf8

"""
Replace
JavaScript
"""
import re
import urlparse



GENERAL_FUNC = ["logView4"]

PATTERN = {}
PATTERN["logView4"] = {
'all':{'replace':[('scrap','view')],
	'query':['blogId','category'],
	'rex':'new_a = re.sub("javascript:logView4\(\'(.+)\',\'.+\'.*(class=.+\>)", new_base + "&logId=\g<1> \g<2>", a_tag)'},
}
PATTERN["goView"] = {
'www.migame.tv':{'replace':[('list.asp','view.asp')],
				'query':[],
				'rex':'new_a = re.sub("javascript:goView\((.+), (\d+)\)\;\\" (class=\\".+\\".*>)", new_base + "?bd=\g<2>&msg=\g<1>\\" \g<3>", a_tag)'},
'sewc.ac.kr':{'replace':[('list.asp','view.asp')],
			'query':['menuCode', 'BBS_ID'],
			'rex':'new_a = re.sub("javascript:goView\((.+)\)", new_base + "&SEQ_NO=\g<1>", a_tag)'},
'foodjoa.co.kr':{'replace':[('list.php','view.php')],
			'query':[],
			'rex':'new_a = re.sub("javascript:goView\(.+, (.+),.+,.+\)", new_base + "?seq=\g<1>", a_tag)'},
'sulwhasoo.co.kr':{'replace':[('list.jsp','view.jsp')],
			'query':[],
			'rex':'new_a = re.sub("javascript:goView\(\'(.+)\', \'(.+)\', \'(.+)\'\)", new_base + "?bbsSeq=\g<1>&bbsCd=\g<2>&tpCd=\g<3>", a_tag)'},
'enterfund.com':{'replace':[('list.asp','view.asp')],
			'query':['fundkind'],
			'rex':'new_a = re.sub("javascript:goView\(\'(.+)\'\);", new_base + "&seq=\g<1>", a_tag)'},

'korean.visitkorea.or.kr': {'query': [], 'rex': 'new_a = re.sub("goView\(\'(.*?)\'\);" , new_base + "?func_name=view&articleId=\\g<1>" ,a_tag)', 'replace': []},

}

PATTERN["GoPage"] = {
'bookworld24.co.kr':{'replace':[],
					'query':[],
					'rex':'new_base = "http://" + netloc; new_a = re.sub("JAVASCRIPT:GoPage\(\'(.+)\'\);", new_base + "\g<1>", a_tag)'},
'booksaetong.co.kr':{'replace':[],
					'query':[],
					'rex':'new_base = "http://" + netloc; new_a = re.sub("JAVASCRIPT:GoPage\(\'(.+)\'\)", new_base + "\g<1>", a_tag)'},
}

PATTERN["go_goods"] = {
'fashionplus.co.kr':{'replace':[('im_list.asp','goods.asp')],
					'query':[''],
					'rex':'new_a = re.sub("javascript:go_goods\(([^,]+),.*\)", new_base + "?goods_id=\g<1>", a_tag)'},
}


PATTERN["goList"] = {
'fishnet.co.kr':{'replace':[],
					'query':['p_mid'],
					'rex': 'new_a = re.sub("javascript:goList\(\'(.+)\',\'(.+)\'\);" , new_base + "&nowpage=\g<1>" ,a_tag)'},
}


PATTERN["goTo"] = {
'encar.com':{'replace':[],
					'query':[],
					'rex': 'new_a = re.sub("javascript:goTo\((.+)\)" , new_base + "?pageNo=\g<1>" ,a_tag)'},
}

PATTERN["goMstory"] = {
'melon.com':{'replace':[],
					'query':[''],
					'rex': 'new_base = "http://" + netloc; new_a = re.sub("javascript:goMstory\(\'(.+)\',\'(.+)\'\);" , new_base + "/musicstory/inform.htm?mstorySeq=\g<1>" ,a_tag)'},
}

PATTERN["product_view"] = {
'arambook.co.kr':{'replace':[('view.asp','detail.asp')],
				'query':[],
				'rex':'new_a = re.sub("javascript:product_view\(\'(.+)\'\)", new_base + "?product_number=\g<1>", a_tag)'},
'purplerecord.com':{'replace':[],
				'query':[],
				'rex':'new_base = "http://" + netloc;new_a = re.sub("javascript:product_view\(\'(.+)\'\)", new_base + "/product_html/product_detail.asp?product_number=\g<1>", a_tag)'},
}
PATTERN["winOpenDetail"] = {
'book.nawayo.com':{'replace':[],
				'query':[],
				'rex':'new_base = "http://" + netloc; new_a = re.sub("javascript:winOpenDetail\(\'(.+)\'\);", new_base + "/Goods/Window/Price.asp?G_Cod=\g<1>", a_tag)'},
}

PATTERN["goPage"] = {
'know.seromedu.com' : {'replace':[],
						'query':["triangles"],
						'rex':'new_a = re.sub("javascript:goPage\(\'(.+)\',\'(.+)\'\)" , new_base + "?page=\g<1>" ,a_tag)'},
'fun.pullbbang.com': {'query': [], 'rex': 'new_base = "http://" + netloc; new_a = re.sub("javascript:goPage\\(\\\'(.+)\\\'\\);" , new_base + "/fun/funView.pull?code=\\g<1>" ,a_tag)', 'replace': []}, 

'aion.plaync.com': {'query': ['category'], 
'rex': 'new_a = re.sub("javascript:goPage\\((.+)\\)" , new_base + "&page=\g<1>" ,a_tag)', 
'replace': []}, }  


PATTERN["loadPage"] = {
'clubaudition.ndolfin.com' : {'replace':[],
						'query':['BID'],
						'rex':'new_a = re.sub("javascript:loadPage\((.+)\)" , new_base + "&page=\g<1>" ,a_tag)'},
}

PATTERN["viewPage"] = {
'aceonline.ndolfin.com' : {'replace':[],
						'query':['id'],
						'rex':'new_a = re.sub("javascript:viewPage\((.+)\);" , new_base + "&idx=\g<1>&mode=view" ,a_tag)'},
}


PATTERN["check_p"] = {
'yangsan.go.kr' : {'replace':[],
						'query':[],
						'rex':'new_a = re.sub("Onclick=\\"javascript:check_p\(\'(.+)\',\'(.+)\',\'(.+)\',\'(.+)\',\'(.+)\',\'(.+)\'.\'(.+)\',\'(.+)\',\'(.+)\',\'(.+)\',\'(.+)\',\'(.+)\'\)\\"" , "location=\'\g<1>\';" ,a_tag)'},
}

PATTERN["viewLink"] = {
'story.mgame.com' : {'replace':[("freeboard.mgame" ,"freeboard_view.mgame")],
						'query':['gameCode', 'bname'],
						'rex':'new_a = re.sub("javascript:viewLink\((.+)\)" , new_base+"&idx=\g<1>" ,a_tag)'},
'mgame.com' : {'replace':[("freeboard.mgame" ,"freeboard_view.mgame"), ("tip.mgame", "tip_view.mgame"), ("opinion.mgame" ,"opinion_view.mgame")],
						'query':['game_id'],
						'rex':'new_a = re.sub("javascript:viewLink\((.+)\)" , new_base+"&idx=\g<1>" ,a_tag)'},
}

PATTERN["pageLink"] = {
'mgame.com' : {'replace':[],
						'query':['game_id'],
						'rex':'new_a = re.sub("javascript:pageLink\((.+)\)" , new_base+"&page=\g<1>" ,a_tag)'},
}


PATTERN["read"] = {
'cms.hangame.com' : { 'replace':[],
						'query':['bbsid'],
						'rex':'new_a = re.sub("javascript:read\(\'(.+)\'\);" , new_base+"&docid=\g<1>" ,a_tag)'},


'lineage2.plaync.co.kr': {'query': ['classid', 'boardid', 'serverId'], 'rex': 'new_a = re.sub("javascript:read\\(\\\'(.+)\\\'\\)" , new_base + "&bid=\\g<1>" ,a_tag)', 'replace': [('/linkedbbs/qna/list', '/linkedbbs/qna/view')]}, 
}

PATTERN["sendView"] = {
'www.hungryapp.co.kr': {'query': ['bcode'], 'rex': 'new_a = re.sub("javascript:sendView\\((.+) *, *\\\'\\\'\\)" , new_base + "&pid=\g<1>" ,a_tag)', 'replace': [('/bbs/list.php', '/bbs/bbs_view.php')]},
}

PATTERN["goArticleDetail"] = {
'nongmin.com': {'query': [], 'rex': 'new_a = re.sub("javascript:goArticleDetail\\(\\\'([^,]+)\\\'\\)" , new_base + "?ar_id=\g<1>" ,a_tag)', 'replace': [('/article/ar_list_today.htm', '/article/ar_detail.htm')]},
}

PATTERN["go_page"] = {
'nongmin.com': {'query': [], 'rex': 'new_a = re.sub("go_page\\(\\\'(.+)\\\'\\)" , new_base + "?page=\g<1>" ,a_tag)', 'replace': []},
}

PATTERN['sendList'] = {
'www.hungryapp.co.kr': {'query': ['bcode'], 'rex': 'new_a = re.sub("javascript:sendList\\((.+)\\)" , new_base + "&page=\\g<1>" ,a_tag)', 'replace': []},
}
PATTERN['page'] = {
'www.inven.co.kr': {'query': ['come_idx'], 'rex': 'new_a = re.sub("javascript:page\\(\\\'(.+)\\\'\\);" , new_base + "&p=\\g<1>" ,a_tag)', 'replace': []},
'news.joinsland.joins.com': {'query': [], 'rex': 'new_base = "http://" + netloc + "/total/total.asp"; new_a = re.sub("page\\((.+)\\); return false;" , new_base + "?page=\\g<1>" ,a_tag)', 'replace': []},

}


PATTERN['list_go'] = {'www.battlepage.com': {'query': ['menu'], 'rex': 'new_a = re.sub("javascript:list_go\\(\\\'(.+)\\\'\\)" , new_base + "&mode=view&no=\\g<1>" ,a_tag)', 'replace': []}, }

FIXED_PATTERN = dict()

FIXED_PATTERN["ttboard.cgi"] = dict()

FIXED_PATTERN["ttboard.cgi"]["read_article"] = { 1 : {"*": { "fixed" : "db", "additional" : "act=read", "changed" : { "idx" : 0} }}}
FIXED_PATTERN["ttboard.cgi"]["read_article"] = { 1 : {"*": { "fixed" : "db", "additional" : "act=read", "changed" : { "idx" : 0} }}}



def key_cmp(a, b):
	return cmp(len(a), len(b))


def make_baseurl(url, reged_pattern):
	url_tup = urlparse.urlparse(url)
	query_list = sorted(url_tup.query.split("&"))
	essential_key = reged_pattern['query']
	
	remix_query_list = []
	for query in query_list:
		query_key = query.split("=")[0]
		if query_key in essential_key:
			remix_query_list.append(query)
		else:
			False
	temp_url_list = list(url_tup)
	temp_url_list[4] = "&".join(remix_query_list)
	new_url = urlparse.urlunparse(tuple(temp_url_list))
	replace_args = reged_pattern['replace']
	for replace_from, replace_to in replace_args:
		new_url = new_url.replace(replace_from, replace_to)
	
	return new_url



def makeURL(base_url, r_args, pattern):
	
	parsed_url = urlparse.urlparse(base_url)
	query_list = sorted(parsed_url.query.split("&"))
	if 'fixed' in pattern:
		fixed_param = pattern['fixed']
	else:
		return None
		
	
	remix_query_list = []
	t_count = 0
	for query in query_list:
		query_key = query.split("=")[0]
		if query_key in fixed_param:
			remix_query_list.append(query)
			t_count += 1
	if t_count != len(fixed_param):
		return None
	
	for para in pattern['changed']:
		if r_args[pattern['changed'][para]].strip() != "":
			remix_query_list.append(para+"="+r_args[pattern['changed'][para]])
		else:
			return None
	
	if len(pattern['additional']) > 0:
		remix_query_list.append(pattern['additional'])

	temp_url_list = list(parsed_url)
	temp_url_list[4] = "&".join(remix_query_list)
	new_url = urlparse.urlunparse(tuple(temp_url_list))
	replace_args = pattern['replace']
	for replace_from, replace_to in replace_args:
		try:
			rep_cur = replace_to.find("$(") 
			if rep_cur >= 0:
				end_cur = replace_to.find(")", rep_cur)
				if end_cur >=0:
					rep_a = replace_to[rep_cur:end_cur+1]
					rep_b = r_args[int(replace_to[rep_cur+2:end_cur])]
					replace_to = replace_to.replace(rep_a, rep_b)
					new_url = new_url.replace(replace_from, replace_to)
			else:
				new_url = new_url.replace(replace_from, replace_to)
		except Exception, msg:
			print msg
	return new_url


def makeURLFromPattern(base_url, r_args, pattern):
	
	parsed_url = urlparse.urlparse(base_url)
	query_list = sorted(parsed_url.query.split("&"))
	fixed_param = pattern['fixed']
	
	remix_query_list = []
	
	fixed_count = 0
	for query in query_list:
		query_key = query.split("=")[0]
		if query_key in fixed_param:
			remix_query_list.append(query)
			fixed_count += 1
	if fixed_count != len(fixed_param):
		return None
		
	for para in pattern['changed']:
		if r_args[pattern['changed'][para]].strip() != "":
			remix_query_list.append(para+"="+r_args[pattern['changed'][para]])
	
	if len(pattern['additional']) > 0:
		remix_query_list.append(pattern['additional'])

	temp_url_list = list(parsed_url)
	temp_url_list[4] = "&".join(remix_query_list)
	new_url = urlparse.urlunparse(tuple(temp_url_list))
	replace_args = pattern['replace']
	for replace_from, replace_to in replace_args:
		new_url = new_url.replace(replace_from, replace_to)

	return new_url



def extract_right_pattern(netloc, func, pattern_dic):
	check_pattern_list = pattern_dic.keys()
	right_pattern = None
	if 'all' in check_pattern_list:
		right_pattern = pattern_dic['all']
	else:
		for check_pattern in check_pattern_list:
			if check_pattern == netloc:
				right_pattern = pattern_dic[check_pattern]
		if right_pattern == None:
			for check_pattern in check_pattern_list:
				if check_pattern in netloc:
					right_pattern = pattern_dic[check_pattern]
	return right_pattern	


def getUrl(a_tag):
	s_cur = a_tag.find('"')
	if s_cur >= 0:
		e_cur = a_tag.find('"', s_cur+1)
		h_cur = a_tag.find(';', s_cur)
		if e_cur >= 0 :
			if h_cur > 0:
				return a_tag[s_cur+1:h_cur]
			else:
				return a_tag[s_cur+1:e_cur]
	return a_tag


def makeVarDict(args):
	arg_dict = dict()
	r_args = args.split(",")
	arg_index = 0
	for i in r_args:
		if len(i) > 0:
			i = i.strip()
			if i[0] == "'" and i[-1] == "'":
				r_val = i[1:-1]
			elif i[0] == '"' and i[-1] == '"':
				r_val = i[1:-1]
			else:
				r_val = i
			arg_dict[arg_index] = r_val
		else:
			arg_dict[arg_index] = ""
		arg_index += 1
	return arg_dict

def findProperPattern():
	pass

def getUrlFromScript(func_call, base_url, pattern):
	"""
		netloc -> funcName -> num of args -> base_page -> real_pattern
		                                     general

	"""

	(funcName, args, semi) = getFuncName(func_call)

	if funcName == None:
		return None
	r_args = makeVarDict(args)
	args_count = len(r_args)

	parsed_url = urlparse.urlparse(base_url)
	base_page = parsed_url[2]

	new_url = None

	try:
		if funcName in GENERAL_FUNC:
			netloc = "all"
		else:
			netloc = urlparse.urlparse(base_url).netloc.replace("www.", "")
	except ex:
		print "wrong url!! : ", ex
		return False

	if "*" in pattern:
		try:
			func_pattern = pattern[netloc]
			if funcName in func_pattern:
				proper_pattern = func_pattern[funcName]
				new_url = makeURL(base_url, r_args, proper_pattern)
		except Exception, msg:
			print msg
	elif netloc in pattern:
		try:
			func_pattern = pattern[netloc]
			if funcName in func_pattern:
				args_pattern = func_pattern[funcName]
				if args_count in args_pattern:
					page_pattern = args_pattern[args_count]
					if base_page in page_pattern:
						real_pattern = page_pattern[base_page]
					else:
						real_pattern = page_pattern["*"]
					new_url = makeURL(base_url, r_args, real_pattern)
		except Exception, msg:
			pass
	else:
		n_cur = netloc.find(".")
		if n_cur >= 0:
			netloc = netloc[n_cur+1:]

			if netloc in pattern:
				try:
					func_pattern = pattern[netloc]
					if funcName in func_pattern:
						args_pattern = func_pattern[funcName]
						if args_count in args_pattern:
							page_pattern = args_pattern[args_count]
							if base_page in page_pattern:
								real_pattern = page_pattern[base_page]
							else:
								real_pattern = page_pattern["*"]
							new_url = makeURL(base_url, r_args, real_pattern)

				except Exception, msg:
					print msg
			else:
				new_url = makeURL(base_url, r_args, FIXED_PATTERN)
				
				
				

	return new_url


def replaceFuncToURL(func_call, base_url, pattern):
	
	(funcName, args, semi) = getFuncName(func_call)

	if funcName == None:
		return None
	r_args = makeVarDict(args)

	new_url = None
	try:
		if funcName in GENERAL_FUNC:
			netloc = "all"
		else:
			netloc = urlparse.urlparse(base_url).netloc.replace("www.", "")
	except ex:
		print "wrong url!! : ", ex
		return False


	if "*" in pattern:
		try:
			func_pattern = pattern[netloc]
			if funcName in func_pattern:
				proper_pattern = func_pattern[funcName]
				new_url = makeURL(base_url, r_args, proper_pattern)
		except Exception, msg:
			print msg
	elif netloc in pattern:
		try:
			func_pattern = pattern[netloc]
			if funcName in func_pattern:
				proper_pattern = func_pattern[funcName]
				new_url = makeURL(base_url, r_args, proper_pattern)
		except Exception, msg:
			print msg
	else:
		n_cur = netloc.find(".")
		if n_cur >= 0:
			netloc = netloc[n_cur+1:]

			if netloc in pattern:
				try:
					func_pattern = pattern[netloc]
					if funcName in func_pattern:
						proper_pattern = func_pattern[funcName]
						new_url = makeURL(base_url, r_args, proper_pattern)
				except Exception, msg:
					print msg

	return new_url


def new_rep_javascript(a_tag, base_url, pattern):

	
	if a_tag.count(";") > 1:
		s_cur = a_tag.find(";")
		e_cur = a_tag.rfind(";")
		a_tag = a_tag[:s_cur] + a_tag[e_cur:]

	new_a = a_tag

	(funcName, args, semi) = getFuncName(a_tag)
	try:
		if funcName in GENERAL_FUNC:
			netloc = "all"
		else:
			netloc = urlparse.urlparse(base_url).netloc.replace("www.", "")
	except ex:
		print "wrong url!! : ", ex
		return False
	
	if netloc in pattern:
		try:
			func_pattern = pattern[netloc]

			if funcName in  func_pattern:
				proper_pattern = func_pattern[funcName]

				if proper_pattern != None:
					new_base = make_baseurl(base_url, proper_pattern)
					rex = proper_pattern['rex']
					exec(rex)
		except Exception, msg:
			print msg

	if new_a != a_tag:
		new_url = getUrl(new_a)
		if new_url != new_a:
			new_a = 'a href="%s"'%new_url

	return new_a	


def rep_javascript(a_tag, base_url, pattern=PATTERN ):

	a_tag = a_tag.replace("$('#btype').val('');$('#bcode2').val('');","")
	new_a = a_tag
	
	new_base = ''
	right_pattern = None
	netloc = None

	try:
		netloc = urlparse.urlparse(base_url).netloc
	except ex:
		print "wrong url!! : ", ex
		return False

	# 등록된 PATTERN 의 키를 긴 문자열 순으로 정렬하여 매칭
	func_list = pattern.keys()
	func_list.sort(key_cmp, reverse=True)
	
	for func in func_list:
		if func in a_tag:
			right_pattern = extract_right_pattern(netloc, func, pattern[func])
			if right_pattern != None:
				new_base = make_baseurl(base_url, right_pattern)
				"""
				try:
					rex = pattern[func]['rex']
					exec(rex)
				except Exception, ex:
					print "ERROR", ex
				"""
			else:
				#print "pattern not found!! : ", netloc
				continue
				#return False
			rex = right_pattern['rex']
			exec(rex)
			break
	return new_a


def getFuncName(a_tag):
	j_cur = a_tag.lower().find("javascript:")
	if j_cur >= 0:
		s_point = j_cur+11
	else:
		s_point = 0
	e_cur = a_tag.find("(")
	if e_cur > 0:
		func = a_tag[s_point:e_cur]
		qq_cur = func.rfind('"')
		sq_cur = func.rfind("'")
		if qq_cur >= 0 and sq_cur >= 0:
			if sq_cur > sq_cur: 
				func = func[sq_cur+1:]
			else:   
				func = func[qq_cur+1:]
		elif qq_cur >= 0:
			func = func[qq_cur+1:]
		elif sq_cur >=0:
			func = func[sq_cur+1:]

		ee_cur = a_tag.find(")", e_cur)

		if ee_cur >= 0:
			args = a_tag[e_cur+1:ee_cur]
			try:
				if a_tag[ee_cur+1] == ";":
					return (func, args, ";")
				else:
					return (func, args, "")
			except:
				return (func, args, "")
	return (None, None, None)



def findPattern(a_tag, base_url, r_url):

	# func_name , netloc, page_replace, needed_para

	kk_templelet = """%s\(%s\)%s"""
	nq_arg = "(.+)"
	sq_arg = "\\'(.+)\\'"

	r_parsed_url = urlparse.urlparse(r_url)
	b_parsed_url = urlparse.urlparse(base_url)
	b_netloc = b_parsed_url[1]

	r_page = r_parsed_url[2]
	b_page = b_parsed_url[2]
	page_replace = []
	if r_page == b_page:
		page_replace = []
	else:
		page_replace = [(b_page, r_page)]

	r_para = r_parsed_url[4]
	b_para = b_parsed_url[4].strip()

	b_para_dict = dict()
	r_para_dict = dict()

	for pp in  b_para.split("&"):
		kk = pp.split("=")
		if len(kk) == 2:
			b_para_dict[kk[0]] = kk[1]
	for pp in r_para.split("&"):
		kk = pp.split("=")
		if len(kk) > 1:
			r_para_dict[kk[0]] = kk[1]

	additional_para = set()
	needeed_para = list()
	mode_para = set()

	for para in r_para_dict:
		if para in b_para_dict:
			if r_para_dict[para] == b_para_dict[para]:
				needeed_para.append(para)
			else:
				mode_para.add(para)
		else:
			additional_para.add(para)

	(funcName, args, semi) = getFuncName(a_tag)
	args_dict = dict()
	cc = 1
	
	args_form = []

	if args:
		r_args = args.split(",")
		for i in r_args:
			if len(i) > 0:
				i = i.strip()
				if i[0] == "'" and i[-1] == "'":
					r_val = i[1:-1]
					args_dict[r_val] = str(cc)+"|'"
					args_form.append(sq_arg)
				elif i[0] == '"' and i[-1] == '"':
					r_val = i[1:-1]
					args_dict[r_val] = str(cc)+'|"'
				else:
					r_val = i
					args_dict[r_val] = str(cc)+"|"
					args_form.append(nq_arg)
			cc += 1

	args_tmp = " *, *".join(args_form)
		
	reverse_dict = dict()

	param_form = []
	for para in additional_para:
		val = r_para_dict[para]
		if val in args_dict:
			kk =  "%s=\\g<%s>"%(para, args_dict[val].split("|")[0])
			param_form.append(kk)
	for para in mode_para:
		val = r_para_dict[para]
		param_form.append("%s=%s"%(para, val) )

	a = kk_templelet%(funcName, args_tmp, semi)
	b = "&".join(param_form)
	if len(needeed_para) == 0:
		regular_templelet = """new_a = re.sub("javascript:%s" , new_base + "?%s" ,a_tag)"""%(a, b)
	else:
		regular_templelet = """new_a = re.sub("javascript:%s" , new_base + "&%s" ,a_tag)"""%(a, b)

	test_pattern = dict()
	bb = dict()
	bb["replace"] = page_replace
	bb["query"] = needeed_para
	bb["rex"] = regular_templelet
	kk = dict()
	kk[b_netloc] = bb
	test_pattern[funcName] = kk

	return test_pattern


if __name__ == "__main__":
	test_set = list()
	
	print rep_javascript("""<a href="javascript:logView4('3805229','N')" class="04com_darkblue13">""", "http://blog.moneta.co.kr/blog.log.scrap.screen?blogId=bs0717&folderType=1&category=742371&listType=4&logNumber=-1")
	print rep_javascript("""<a href="javascript:goView(332012, 99);" class="gray12px"  >""", "http://www.migame.tv/section/video/list.asp?bd=99&bt=C&ct=PSP&ttl=u3")
	print rep_javascript("""<a href="javascript:goView(327848, 117);" class="gray12px"  >""", "http://www.migame.tv/section/video/list.asp?bt=C&ct=PS3&ttl=2137")
	print rep_javascript("""<a href="javascript:goView(-32680)" >""", "http://www.sewc.ac.kr/board/board_list.asp?menuCode=02_01_04&BBS_ID=P0000004&goPage=&QRY_GBN=&QRY_CONTENT=")
	print rep_javascript("""<a href="javascript:goView(1, 70, '', '')">""", "http://foodjoa.co.kr/maniaRe/happyletter_list.php")
	print rep_javascript("""<a href="javascript:goView('3772', 'PHOTO', 'N')">""", "http://sulwhasoo.co.kr/community/maven_event_list.jsp")
	print rep_javascript("""<a href="javascript:goView('21869');">""", "http://enterfund.com/commu/bbs/catalog_list.asp?fundkind=43")
	print rep_javascript("""<a href="JAVASCRIPT:GoPage('/product/product_view.asp?in_id=2000030804');">""","http://bookworld24.co.kr/product/new_list.asp?in_sdate=20000308")
	print rep_javascript("""<a href="javascript:logView(3791482);" class="newlist">""","http://blogreporter.chosun.com/blog.screen?blogId=6246&listType=3&menuId=127022")
	print rep_javascript("""<a href="javascript:logView(413225);" class="newlist">""","http://blog.chosun.com/blog.screen?blogId=18850&listType=3&menuId=69471")
	print rep_javascript("""<a href="javascript:go_goods(7624850,  0)">""", "http://www.fashionplus.co.kr/mall/goods/im_list.asp?brand_id=8594&brand_name=GGPX%28DONGA%29&itemModel_code=3008&itemModel_name=%C0%E7%C5%B6&mall_id=25&pn=507")
	print rep_javascript("""<A href="javascript:product_view('43420')">""", "http://arambook.co.kr/product_html/product_view.asp?ac_number=1&ac_type=1&bc_number=33&gubun_number=794&list_type=4")
	print rep_javascript("""<A href="javascript:winOpenDetail('0100008200737');">""", "http://book.nawayo.com/Goods/SmmList.asp?insFlag=1&intLarCod=164&intMidCod=1376&intSmmCod=9238")
	print rep_javascript("""<a HREF="javascript:winOpenDetail('0100006230194');" CLASS="TITLE">""", "http://book.nawayo.com/Goods/SpecialList.asp?intLarCod=156&intMidCod=1448&intRow_Count=15")
	print rep_javascript("""<a href="JAVASCRIPT:GoPage('/product/product_view.asp?in_id=2000102722')" style="FONT-SIZE: 10pt; FONT-FAMILY: 굴림">""","http://booksaetong.co.kr/product/series_list.asp?in_mid=004332")
	print rep_javascript("""<a href=javascript:product_view('26301')>""", "http://purplerecord.com/product_html/product_s_view.asp?ac_number=1&bc_number=14&cc_number=186&list_type=8")


	print rep_javascript("""<a href="javascript:storyRead(24523338);">""" , "http://story.hangame.com/bbs.nhn?bbsid=119")
	print rep_javascript("""<a href="javascript:view_content('635611');">""" ,  "http://tr.nopp.co.kr/Community/Free/Free_list.asp")
	print rep_javascript("""<a href="javascript:goPage(1)">""" , "http://tr.nopp.co.kr/Community/Free/Free_list.asp?page=4&type=&search_type=&search_word=&term=")
	print rep_javascript("""<a href="javascript:doRead(1,19203753,'0')" class="notice" onfocus='this.blur();'>""" , "http://aaa.pmang.com/fifaonline_board.nwz?act=list&ssn=321&bserial=3&lbserial=3&isgroup=&page=1&s_ano=&ano=&no=&findtype=0&keyword=")
	print rep_javascript("""<a href="javascript:pageView('130465');" class="board_list_free">""",  "http://www.windyzone.com/sub_main/menu/board/view/board_list.ws?boardId=humorBoard")
	print rep_javascript("""<span onClick="pageMove(3)" style="cursor:hand"> """, "http://aceonline.ndolfin.com/community/board.asp?id=user&mode=&svr=&bpage=1&firstidx=27771&lastidx=27749&page=2&idx=")

	print rep_javascript("""<span style='cursor:hand'  Onclick="javascript:check_p('view.php?id=dis_free&num=5380&c=&action=&field=&adminfield=&s_name=&category=&stype=&view=&page_now=&page=','1','1','1','','0','','','','','N','','5380','dis_free','','D','','0')" onkeypress='' onMouseOver='return onText("상세보기");' onfocus='' onMouseOut='StatusOff();' onblur=''>""", "http://map.yangsan.go.kr/kor/program/board/main/list.php?id=dis_free")
	print rep_javascript("""<a href='javascript:viewLink(26044)' >""", "http://story.mgame.com/common_board/freeboard/freeboard.mgame?gameCode=hero&bname=F")   # http://story.mgame.com/common_board/freeboard/freeboard_view.mgame?bname=F&gameCode=hero&idx=26044

	print rep_javascript(""" <a href="javascript:goPage('2','21')">""", "http://know.seromedu.com/index.html?page=1&triangles=exchange"), "AAAAAAAAAA"


	print rep_javascript("""<a href="javascript:goPage('15032774');">""", "http://fun.pullbbang.com/fun/funBoardMain.pull")
	print rep_javascript("""<a href="#9" class="numBox" onclick="page(9); return false;" >""","http://news.joinsland.joins.com/total/total.asp") , "CCCCCCCCCCCCCCCCCCCCCC"


	print getFuncName("javascript:goPage(1)")

#	print new_rep_javascript("""<a href="javascript:storyRead(24523338);return false;">""" , "http://story.hangame.com/bbs.nhn?bbsid=119", pattern) , " new BBBBBBBBBB"

	findPattern("""<a href="javascript:goView(332012, 99);" class="gray12px"  >""", "http://www.migame.tv/section/video/list.asp?bd=99&bt=C&ct=PSP&ttl=u3", "http://www.migame.tv/section/video/view.asp?bd=99&msg=332012")
	findPattern("""<a href="javascript:goPage(1)">""" , "http://tr.nopp.co.kr/Community/Free/Free_list.asp?page=4&type=&search_type=&search_word=&term=", "http://tr.nopp.co.kr/Community/Free/Free_list.asp?page=1")
	findPattern("""<a href="javascript:doRead(1,19203753,'0')" class="notice" onfocus='this.blur();'>""" , "http://aaa.pmang.com/fifaonline_board.nwz?act=list&ssn=321&bserial=3&lbserial=3&isgroup=&page=1&s_ano=&ano=&no=&findtype=0&keyword=","http://aaa.pmang.com/fifaonline_board.nwz?bserial=1&ano=19203753&act=read")
	findPattern("""<a href="javascript:goView('3772', 'PHOTO', 'N')">""", "http://sulwhasoo.co.kr/community/maven_event_list.jsp", "http://sulwhasoo.co.kr/community/maven_event_view.jsp?bbsSeq=3772&bbsCd=PHOTO&tpCd=N")
	print findPattern("""<a href="javascript:pageView('130465');" class="board_list_free">""",  "http://www.windyzone.com/sub_main/menu/board/view/board_list.ws?boardId=humorBoard", "http://www.windyzone.com/sub_main/menu/board/view/board_view.ws?boardId=humorBoard&docsId=130465")
	print findPattern("""<a href='javascript:viewLink(26044)' >""", "http://story.mgame.com/common_board/freeboard/freeboard.mgame?gameCode=hero&bname=F", "http://story.mgame.com/common_board/freeboard/freeboard_view.mgame?bname=F&gameCode=hero&idx=26044")
	
	print findPattern("""<a href='javascript:sendList(8)' class='off'>""", "http://www.hungryapp.co.kr/bbs/list.php?bcode=puzdragon", "http://www.hungryapp.co.kr/bbs/list.php?bcode=puzdragon&page=8")

	print findPattern("""<a href='javascript:sendView(8, '')' class='off'>""", "http://www.hungryapp.co.kr/bbs/list.php?bcode=puzdragon", "http://www.hungryapp.co.kr/bbs/list.php?bcode=puzdragon&page=8")


	print findPattern("""<a href="javascript:$('#btype').val('');$('#bcode2').val('');sendView(3002,'')" >""", "http://www.hungryapp.co.kr/bbs/list.php?bcode=puzdragon", "http://www.hungryapp.co.kr/bbs/list.php?bcode=puzdragon&no") , "NNNNN"


	print findPattern("""<A HREF=javascript:page('6');>""", "http://www.inven.co.kr/board/powerbbs.php?come_idx=1492", "http://www.inven.co.kr/board/powerbbs.php?come_idx=1492&p=6")

	print findPattern("""<a href="javascript:read('1949647')">""", "http://lineage2.plaync.co.kr/linkedbbs/qna/list?classid=6&serverId=1&boardid=21&page=15", "http://lineage2.plaync.co.kr/linkedbbs/qna/view?classid=6&boardid=21&bid=1949647&serverId=1")


	print findPattern("""<a href="javascript:goPage(5)">""", "http://aion.plaync.com/board/server/list?page=6&category=3", "http://aion.plaync.com/board/server/list?page=5&category=3")

	print findPattern("""<a href="javascript:list_go('733')" >""", "http://www.battlepage.com/index.php?menu=b_gameroom", "http://www.battlepage.com/index.php?menu=b_gameroom&mode=view&no=733")
	

	print findPattern("""<a href="javascript:goPage('15032774');">""", "http://fun.pullbbang.com/fun/funBoardMain.pull", "http://fun.pullbbang.com/fun/funView.pull?code=15032774") 
