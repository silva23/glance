import time
import  subprocess

import mechanize
from utils.log import getLogger

THUMB_DIR = "/home/zumse/data/images/thumb/"
MEDIA_SERVER = "121.78.244.249"
MEDIA_SERVER = "10.35.21.251"
MEDIA_SERVER_M = "10.35.21.169"
IMAGE_CHECK_API = "http://%s/"%MEDIA_SERVER+"check/%s"
IMAGE_UPLOAD_API = "http://%s/"%MEDIA_SERVER+"upload/single/key/%s"



BAD_IMAGE_DOMAIN = ["blogimgs.naver.net", "www.google.com", "cafe.daum.net", "planet.daum.net", "hanmail.net", "daumcdn.net", "media.daum.net", ".yahoo.co.kr", "kr.img.blog.yahoo.com", "empas.com", "tfile.nate.com", ".go.kr/", "paran.com", "textcube.com"]


BAD_IMAGE_HINT = ["1x1", "icon.gif", "sns.gif", "twitter.gif","me2day.gif", "/sns_", "/skin4/", "/login_", "_btn_sns_", "/ui_logo.jpg", "/mailimg/", "img.inven.co.kr/image/www/common/", "img.inven.co.kr/image/site_image"]


def isBadImage(img):
	
	for bd in BAD_IMAGE_DOMAIN + BAD_IMAGE_HINT:
		if img.find(bd) >= 0:
			return True
	if  img.find("/emoticon/") >= 0 or img.find("/sticker/") >= 0 or img.find("/banner/") > 0:
		return True
	if img.find("http://agoda114.me/") >= 0 or img.find("http://check-in.name/") >= 0:
		return True
	if img.find(".ru/") >= 0 or img.find(".doubleclick.") >= 0 or img.find("techneedle.com") >= 0 or img.find("1.1.1") > 0 or img =="/blank.gif" or img.find(".json") > 0 or img.find("/dot.gif") > 0 or img.find("netpia.png") > 0 or img.find("like.png") > 0 or img.find("/btn_") > 0 or img.find("compia.com") >= 0:
		return True
	if img.startswith("data:") or img.strip().startswith("file://"):
		return True

	return False



def downloadImage(img_url):
	rq = mechanize.Request(img_url)
	try:
		rs = mechanize.urlopen(rq)
		http_content = rs.read()
		header = rs.info()
		return http_content
	except Exception, msg:
		try:
			getLogger().error("%s %s", img_url, msg)
			time.sleep(1)
			rs = mechanize.urlopen(rq)
			http_content = rs.read()
			return http_content
		except Exception, msg:
			getLogger().error("%s %s", img_url, msg)
			return str(msg)


def isOldImage(chk_api, hash_key):

	try:
		cmd = "curl %s --connect-timeout 5 --max-time 10 "%chk_api
		exist = True
		fd = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		for line in fd.stdout.readlines():
			results = line.strip()
			if results.find("404") >= 0:
				exist = False
				break
			if results.find(hash_key.upper()) >= 0:
				getLogger().info("same hash")
				return True
	except Exception, msg:
		getLogger().error(msg)
	return False

def uploadImage(upload_url, file):
	retry = 0
	results = ""
	m_t = time.time()
	while retry < 3:
		try:
			cmd = "curl --upload-file %s %s --connect-timeout 5 --max-time 10 --header 'Expect:' "%(file, upload_url+"/fileext/jpg")
			fd = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			for line in fd.stdout.readlines():
				results = line.strip()
				if results.startswith("OK"):
					e_t = time.time()
					getLogger().info("%s upload time :  %s", upload_url, e_t - m_t)
					return "OK"
		except Exception, msg:
			getLogger().error(msg)
		retry += 1
	return results 

if __name__ == "__main__":

	uploadImage("d956d0e9a4871edaf15ff8d8a500975f744c6968", "/home/zumse/data/images/thumb/d956d0e9a4871edaf15ff8d8a500975f744c6968.jpg")
