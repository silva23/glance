#!/usr/bin/env python
#coding: utf8


import socket
import urlparse
import string
import os

from log import getLogger
import MySQLdb
from utils import hasher

PP = [u"은", u"는", u"이",u"가",u"에", u'의', u'과', u'도', u'을', u'를', u'랑', u'두', u'만', u'로', u'와', u'나'u'라', u'며', u'인', u'루', u'씨', u'들', u'때', u'랑']
TPP = [u"에게", u"에서",u"에도", u"으로", u"까지", u"이라", u"이나",u"이네",u"이냐" u"보다", u"인데", u"이랑", u"라고", u"하는", u"하고", u"하러", u"한테", u"였다", u"이다", u"한다", u"같은", u"같이", u"같다", u"하긴", u"할수", u"일까", u"이면", u"했어"]
TRPP = [ u'입니다', u'이지만', u'랍니다',u"합니다", u"라는데", u"하려면", u"하나요",u"하네요", u"하라고",u"한지요", u"한가요", u"에서는", u"에서도", u"이라면", u"이라고", u"이네요", u"만큼의", u"인가요", u"이란게", u"하니까", u"했는데", u"이었다", u"하세요", u"있는데",u"이예요", u"하래요"]
FOPP = [u"했습니다", u"했었는데", u"드립니다", u"라면서요", u"이거든요", u"해야하냐", u"했을까요"]
FIPP = [u"이었습니다", u"이라면서요"]
delch = u'~‘’`_{}[]…!·@#$.,”-+=)“(*&/?:•;\'\"'


DELETE_SIGN = ["선택하신 글이 삭제 또는 이동되었습니다","삭제 또는 이동되었습니다, 신고하려면 여기를 클릭하세요", "찾으시려는 페이지의 주소가 잘못 입력되었거나", "선택하신 게시물이 존재하지 않습니다", "해당 게시물은 삭제 되었습니다", "해당 글이 존재 하지 않습니다.", "없는 게시물 번호입니다.", "요청하신 페이지의 사용권한이 없습니다", "블라인드 처리된 게시물입니다."] 

SCRIPT_MESSAGE = ["잘 못된 접근 입니다", "글이 존재하지 않습니다","잘못된 요청입니다","alert('해당게시물은 존재하", "<script>alert('이미 삭제된 게시글 입니다.');", "글을 읽을 권한이 없습니다."]

HTML_DEAD = ["window.alert('삭제된 글 입니다.\n새로고침후 이용해 주세요.');"]



def getIp(domain):
	try:
		(a, b, ips) = socket.gethostbyname_ex(domain)
	except Exception, msg:
		try:
			(a, b, ips) = socket.gethostbyname_ex("www."+domain)
		except Exception, msg:
			pass
			return "0.0.0.0"
	ips.sort()
	return ips[0]



def checkDead(core_text,full_text,  html):

	if len(core_text) == 0:
		for text in SCRIPT_MESSAGE:
			if html.find(text) >= 0:
				return True
	else:
		for text in DELETE_SIGN:
			if full_text.find(text) >= 0:
				return True
	for text in HTML_DEAD:
		if html.find(text) >= 0:
			return True


	if core_text.find("이 글을 보려면 로그인이 필요합니다 (This board is only for members. Please sign up and Upgrade to level 8)") >= 0:
		return True

	if len(full_text) > 3000 and len(core_text) == 0:
		return False

	return False

def deleteWWW(url):
	cur = url.find("/www.")
	if cur >=0 and cur < 8:
		url_nowww = url[:cur+1] + url[cur+5:]
	else:
		url_nowww = url.replace("/www1.", "/").replace("/www2.", "/").replace("/www3.","/")

	if url.startswith("https://"):
		url_nowww = url.replace("https://", "http://")

	return url_nowww


def getPage(url):
	parsed_url = urlparse.urlparse(url)
	
	page = parsed_url[1]+parsed_url[2]
	if page.startswith("www."):
		page = page[4:]
	
	return page

def getDBCursor(host='175.118.124.41' ,user='bbsprime',passwd='bbscrawler',db='bbsdb'):
	try:
		db = MySQLdb.connect(host ,user, passwd, db)
		db.set_character_set('utf8')
		cursor = db.cursor()
	except Exception,msg:
		print msg
		return None
	return db, cursor

def deleteFile(file):
	try:
		os.remove(file)
	except Exception, msg:
		getLogger().error("%s delete error : %s", file, msg)




def getProperAnchor(anchor):

	rv = ""
	try:
		pieces = anchor.split("||")
		min_diff = 100
		for piece in pieces:
			try:
				candi = ""
				if len(piece) > 150:
					continue
				elif piece.startswith("http://"):
					continue
				elif piece.startswith("by "):
					continue
				elif len(piece) < 15:
					if piece.lower().find("comment") >= 0:
						continue
					for c in piece.strip():
						if not c in string.punctuation :
							candi += c
					try:
						num =  int(candi)	
						candi = candi
					except Exception, msg:
						pass
					else:
						continue
					if "".join(candi.split()) in ["조회수","추천수", "이전","다음", "새창", "Offline", "인증메일재발송", "제목순", "날짜순", "인기순", "좋아요순", "ReadMore", "조회", "등록일", "상위항목", "최신순", "정확도순", "평점순", "조회순"]:
						continue
					else:
						candi = piece
				else:
					candi = piece
				
				t_diff = abs(len(candi) - 45)
				if t_diff < min_diff:
					if len(rv) > 0 :
						if abs(len(rv) - 45) > t_diff:
							rv = candi
					else:
						rv = candi
			except Exception, msg:
				getLogger().error(msg)
		rv = rv.replace("«","").strip()
	except Exception, msg:
		getLogger().error(msg)
		
	return rv


def getBoardKey(url):
	pass


def getNewHash(text):
	
	new_text = ""
	for c in text:
		if c in string.digits or c in string.punctuation or  c in string.whitespace:
			pass	
		else:
			new_text += c

	hash = hasher.Hasher()
	h_val = hash.sha256(new_text)

	return  h_val

INT_FIELD = ["read","read2","reply", "recommend"]

def findData(encoded_html, attr_dict):


	return_dict = dict()

	for name in attr_dict:
		try:
			pattern = attr_dict[name]
			pieces = pattern.split("****")
			prefix = pieces[0]
			s_cur = 0
			total = 0
			max_length = 1000

			if pattern.strip().endswith("****"):
				s_cur = encoded_html.find(prefix.strip())
				e_cur = encoded_html.find("\n", s_cur+len(prefix))
				if s_cur >= 0:
					value = encoded_html[s_cur+len(prefix):e_cur]
					if name in INT_FIELD:
						try:
							value = int(value.replace(",","").replace("/","").strip() )
						except Exception, msg:
							continue
					return_dict[name] = value
					continue

			lines = prefix.split("\n")

			if len(lines) == 1:
				total = encoded_html.count(prefix.rstrip())
				max_length = 200
			else:
				total = encoded_html.count(lines[0].strip())

			for i in range(0,min(total, 10000)):
				succes = True
				if len(lines) == 1:
					s_cur = encoded_html.find(prefix, s_cur+1)
					if s_cur < 0:
						break
					else:
						s_cur = s_cur + len(prefix)
				else:
					for line in lines:
						p_cur = encoded_html.find(line.strip(), s_cur+1)
						if p_cur>=0:

							if s_cur != 0 and p_cur - s_cur - len(line) < 10:
								s_cur = p_cur+len(line.strip())
							elif s_cur == 0:
								s_cur = p_cur+len(line.strip())
							else:   
								succes = False 
								s_cur = p_cur+len(line.strip())
								break   
				if not succes: 
					continue
				suffix = pieces[1]
				if len(suffix.strip()) == 0:
					e_cur = encoded_html.find("\n", s_cur)
					value = encoded_html[s_cur:e_cur] 

				lines = suffix.split("\n")

				p_cur = encoded_html.find(lines[0].strip(),s_cur)
				if p_cur >= 0:
					value = encoded_html[s_cur:p_cur]
					if name in INT_FIELD:
						try:
							value = int(value.replace(",","").replace("/","").strip() )
						except Exception, msg:
							continue
					else:
						value = value.strip()
						if len(value) > max_length:
							continue
					if name == "title" and value.find("&nbsp;") >=0:
						value = value.replace('<img src="/images/phone_icon.png" style="padding-bottom:5px"/>&nbsp;',"")
					return_dict[name] = value
					break
		except Exception, msg:
			print msg
			pass

	return return_dict

def oldFindData(encoded_html, attr_dict):
	return_dict = dict()
	for name in attr_dict:
		try:
			pattern = attr_dict[name]
			pieces = pattern.split("****")
			prefix = pieces[0]
			lines = prefix.split("\n")
			s_cur = 0
			total = 0
			max_length = 1000
			if len(lines) == 1:
				total = encoded_html.count(prefix.rstrip())
				max_length = 200
			else:
				total = encoded_html.count(lines[0].strip())
			for i in range(0,min(total, 20)):
				succes = True
				if len(lines) == 1:
					s_cur = encoded_html.find(prefix, s_cur+1)
					if s_cur < 0:
						break
					else:
						s_cur = s_cur + len(prefix)
				else:
					for line in lines:
						p_cur = encoded_html.find(line.strip(), s_cur+1)
						if p_cur>=0:
							if s_cur != 0 and p_cur - s_cur - len(line) < 10:
								s_cur = p_cur+len(line.strip())
							elif s_cur == 0:
								s_cur = p_cur+len(line.strip())
							else:   
								succes = False 
								s_cur = p_cur+len(line.strip())
								break   
				if not succes: 
					continue
				suffix = pieces[1]
				if len(suffix.strip()) == 0:
					e_cur = encoded_html.find("\n", s_cur)
					value = encoded_html[s_cur:e_cur] 

				lines = suffix.split("\n")

				p_cur = encoded_html.find(lines[0].strip(),s_cur)
				if p_cur >= 0:
					value = encoded_html[s_cur:p_cur]
					if name in ["read","read2","reply"]:
						try:
							value = int(value.replace(",","").replace("/","").strip() )
						except Exception, msg:
							continue
					else:
						value = value.strip()
						if len(value) > max_length:
							continue
					if name == "title" and value.find("&nbsp;") >=0:
						value = value.replace('<img src="/images/phone_icon.png" style="padding-bottom:5px"/>&nbsp;',"")
					return_dict[name] = value
					break
		except Exception, msg:
			pass

	return return_dict




if __name__ == "__main__":
	attr_dict = dict()
	attr_dict["write_dttm"] = """ <td><font color="#878787">20****</font></td> """
	attr_dict["read"] = """  Hit : **** ,  Vote : """
	attr_dict["read2"] = """<font color="666666">조회수

        <span class="A12oreng"><strong>****</strong></span></font>
"""


	html = """     <td height="8"></td>
										  </tr>
										  <tr>
											<td>
											  <table border="0" cellspacing="0" cellpadding="0">
												<tr>
												  <td><font color="#07a6ac">idee</font></td>
												  <td width="15" align="center"><img src="/Common/nvIMG/Board_nvimg/line_02.gif"></td>
												  <td><font color="#878787">업종:카페</font></td>
												  <td width="15" align="center"><font color="#878787"><img src="/Common/nvIMG/Board_nvimg/line_02.gif"></font></td>
												  <td><font color="#878787">상권:대전</font></td>
												  <td width="15" align="center"><font color="#878787"><img src="/Common/nvIMG/Board_nvimg/line_02.gif"></font></td>
												  <td><font color="#878787">조회:27</font></td>
												  <td width="15" align="center"><font color="#878787"><img src="/Common/nvIMG/Board_nvimg/line_02.gif"></font></td>
												  <td><font color="#878787">2011.07.12</font></td>
												</tr>
											  </table>
											</td>
										  </tr>
										</table></td>
							"""
	html = """
	
 <!-- 게시판 시작 -->
        <div class="board_head">
          <!-- 게시물 총 갯수
          <p class="total_count"><img src="../skin/board/cheditor/img/icon_board_total.gif" title="total" /> 1,083,739</p> -->
          <!-- 관리, rss, 북마크 -->
          <ul class="head_btn">
                                    <li class="btn_bookmark"><span id="my_menu_add"></span></li>
          </ul>
          <!-- 게시판 카테고리 시작  <li>[PDA][272]</li> -->
		          </div>









        <!-- 게시판 메인 -->
        <div class="board_main">
          <div class="view_head">
            <p class="user_info"> <span class='member'>절정남아님</span> </p>
            <p class="post_info"> 2012-02-06 13:51 ,  Hit : 79 ,  Vote : 0</p>
          </div>
          <div class="view_title">
            <div>
                           <h4><span>시크릿가든 정주행중입니다 ㅋ</span></h4>
            </div>
            <p class="title_btn"> <a href="javascript:scaleFont(+1);"><img src="../skin/board/cheditor/img/btn_board_plus.gif" title="plus" /></a> <a href="javascript:scaleFont(-1);"><img src="../skin/board/cheditor/img/btn_board_minus.gif" title="minus" /></a></p>
          </div>


          <div class="view_content">

          <p class="view_content_btn"> 		  		  		  		  </p>
           


		   <div id="resContents" class="resContents">
						
		   <span id="writeContents" class="ct lh">스카이라이프가 처음으로 좋다고 느껴지네요 ㅋㅋ<br />
근데. 오스카랑 김사랑이랑 테마곡이 감수성 노래라 ㅡㅡ<br />
슬픈데 웃겨요 ㅋㅋㅋㅋ<br />
완전 잼나네요 ㅋㅋ. 처음보는데 ㅋㅋ<br />
<br />
:: 나도끌량ⓣ ::</span>
	"""
	html = """
<table width="702" border="0" cellspacing="0" cellpadding="0">

<tr>

  <td><img src="http://image.donga.com/mlbpark/img200807/sub_line_w_702.gif" width="702" height="4"></td>

</tr>

<tr>

  <td height="44" align="center" background="http://image.donga.com/mlbpark/img200807/sub_line_w_702bg.gif">

    <table width="99%" border="0" cellspacing="0" cellpadding="0">

    <tr>

      <td width="82%" height="16" class="D14">

        &nbsp;&nbsp;&nbsp;<strong>가카와 강만수가 또 합작했네요.</strong> <span class="A12oreng"><strong></strong></span>

      </td>

      <td width="18%" align="right" class="D11"><font color="666666"><div id="nik_543532_1" style="position:absolute;z-index:9999;width:100;margin-top:10px;margin-left:10px;display:none;border-width:3px;border-style:solid;border-color:orange;background-color:ffffff;" onblur="javascript:hideE('543532_1');"><ul><li onclick="MlbNewWindow('http://mlbpark.donga.com/mypage/memo_write.php?mode=send&id=seoulutd')" style="cursor:pointer">쪽지보내기</li><li onclick="MlbNewWindow2('http://mlbpark.donga.com/mbs/userIntroV.php?mbsUid=seoulutd','530','390')" style="cursor:pointer">자기소개</li><li onclick="UserAja('seoulutd','G');" style="cursor:pointer">등급확인</li><li onclick="UserAja('seoulutd','R');" style="cursor:pointer">가입일</a></li><li onclick="MlbNewWindow2('http://mlbpark.donga.com/mypage/my_bulletin2011.php?mbsUid=seoulutd','550','500')" style="cursor:pointer">게시물보기</li></ul></div><a href="javascript:userInfo('543532_1','block');" title='쪽지보내기'><font color='666666'>비★가</font></a></font><img src="http://mlbpark.donga.com/data/emoticon/0.gif" hspace="6" align="absmiddle"></td>

    </tr>

    </table>

  </td>

</tr>

<tr>

  <td align="center" background="http://image.donga.com/mlbpark/img200807/sub_line_702bg.gif">

    <table width="99%" border="0" cellpadding="0" cellspacing="0"style="MARGIN: 14 0 0 0px">

    <tr>

      <td width="32%" class="D11">

        &nbsp;&nbsp;<font color="666666">글번호 </font><font color="999999">543532</font>

        <font color="cdcdcd">|</font> <font color="999999">2012-02-01 08:55:06</font>

      </td>

      <td width="46%" class="D11">

        <font color="666666">IP</font> <font color="999999">183.96.***.249</font>

      </td>

      <td width="11%" class="D11">

        <font color="666666">조회수

        <span class="A12oreng"><strong>1545</strong></span></font>

      </td>



      <td width="11%" class="D11">

        <font color="666666">추천수

        <span class="A12oreng"><strong>1</strong></span></font>

      </td>

    </tr>

    </table>

    <table width="702" border="0" cellpadding="0" cellspacing="0" style='table-layout:fixed; word-break:break-all'>

    <tr>

      <td class="G13">

        <table >



        </table>

        <table>

        <tr>

          <td  class="G13">

            <br>



            <div align="justify" style="width:100%;text-overflow:ellipsis;table-layout:fixed;margin:20px,20px,20px,20px;">

              <head><style>body{font-family: Tahoma,Verdana,Arial;font-size: 11px;color: #555555;margin: 0px}td{font-size :11px; font-family: Tahoma,Verdana,Arial;}p{margin-top:1px;margin-bottom:1px;}</style>




"""
	print findData(html, attr_dict)
	print  getProperAnchor("보르헤르트「적설」||0 댓글||보르헤르트「적설」||0 댓글||« 보르헤르트「적설」||0 댓글||응답 취소")
	print  getProperAnchor("우디르||우디르")

	print getPage("http://naver.com/view.php?asdf=asdf")
	print getIp("zum.com")
	print deleteWWW("https://www3.asdf.com/")

