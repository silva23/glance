#!/usr/bin/env python
#coding: utf8
import urlparse

from downloader import Downloader
from common_func import findData
from parser import HttpParser
from rss_parser import rssParser
from app_blog_guid_gen import BlogUrlFactory
from utils.log import getLogger


def getRssInfo(rss):
	downLoader = Downloader()
	try:
		(t_url, header, html) = downLoader.open(rss) 
		print "download ", rss
	except Exception, msg:
		getLogger().error("feed download error : %s %s", msg, rss)
		return None

	try:
		parser = rssParser()
		ret_dict = parser.parse(t_url, html)
	except Exception, msg:
		getLogger().error("feed parsing error : %s %s", msg, rss)
		return None
	return ret_dict


def checkGenType(generator):
	blog_type = ""
	if generator.lower().find("tistory.com") >= 0:
		blog_type = "tistory.com"
	elif generator.lower().find("naver blog") >= 0:      
		blog_type = "blog.naver.com"
	elif generator.lower().find("daum blog") >= 0:
		blog_type = "blog.daum.net"
	elif generator.lower().find("textcube") >= 0:
		blog_type = "textcube.org"
	elif generator.lower().find("wordpress.com") >= 0:
		blog_type = "wordpress.com"
	elif generator.lower().find("wordpress") >= 0:
		blog_type = "wordpress.org"
	elif generator.lower().find("blogger") >= 0:
		blog_type = "blogspot.com"
	elif generator.lower().find("xpressengine") >= 0:
		blog_type = "xpressengine.com"
	return blog_type

def getTistoryId(url):
	downLoader = Downloader()
	attr_dict = dict()
	attr_dict["tid"] = "livere_blogurl = '****.tistory.com';"
	attr_dict["tid2"] = """__addParam("author","****");"""
	try:
		(t_url, header, html) = downLoader.open(url) 
		print "download", url
	except Exception, msg:
		getLogger().error("feed download error : %s %s", msg, rss)
		return None
	ret_dict = findData(html, attr_dict)
	if "tid" in ret_dict:
		tistory_id = ret_dict["tid"]
		if len(tistory_id) > 0:
			return tistory_id
	elif "tid2" in ret_dict:
		tistory_id = ret_dict["tid2"]
		if len(tistory_id) > 0:
			return tistory_id
	return None


def getTistoryIdFromHTML(html):
	attr_dict = dict()
	attr_dict["tid"] = "livere_blogurl = '****.tistory.com';"
	attr_dict["tid2"] = """__addParam("author","****");"""
	ret_dict = findData(html, attr_dict)
	if "tid" in ret_dict:
		tistory_id = ret_dict["tid"]
		if len(tistory_id) > 0:
			return tistory_id
	elif "tid2" in ret_dict:
		tistory_id = ret_dict["tid2"]
		if len(tistory_id) > 0:
			return tistory_id
	return None





class BlogChecker:
	
	def __init__(self):
		self.downLoader = Downloader()
		self.attr_dict = dict()
		self.attr_dict["tid"] = """__addParam("author","****");"""
		self.attr_dict["r_url"] = """    """
		# http://cfs.tistory.com/custom/named/bw/bwell/rss.xml
		self.domain_match = dict()
		self.http_parser = HttpParser()
		self.bf = BlogUrlFactory()

	def checkDomain(self, domain):

		#header, url, html = getDownloadData(domain, self.opener)
		url, header, html = self.downLoader.open(domain)
		print "download ", domain
		if url == "ERROR" or header == None:
			return None, None

		parsing_result = self.http_parser.plugParser(header, html, url)
		links = self.http_parser.html_parser.links
		isTistory = False
		naver_id = ""

		if html.find("TOP_SSL_URL = 'https://www.tistory.com';") >= 0:
			isTistory = True
			

		feed_urls = set()

		for tt_url in links:
			try:
				link = links[tt_url]
				if isTistory and tt_url.find(domain) >= 0:
					try:
						path = urlparse.urlparse(tt_url).path[1:]
						postno = int(path)
						tistory_id = getTistoryId(tt_url)
						if tistory_id:
							return "tistory.com", tistory_id
						isTistory = False
					except Exception, msg:
						pass

				if link.inout.find("R") > 0 and tt_url.find("/response") < 0 and tt_url.find("atom") < 0 and tt_url.find("comments") < 0:
					feed_urls.add(tt_url)
					status, response = self.downLoader.getResponse(tt_url)

					t_url = response.url
					if t_url != link.url and t_url.find("tistory.com") >= 0 and t_url.endswith("/rss.xml") :
						end_cur = t_url.rfind("/")
						tistory_id = t_url[t_url[:end_cur].rfind("/")+1:end_cur]
						return "tistory.com", tistory_id

					if t_url.startswith("http://blog.rss.naver.com/") and  t_url.endswith(".xml") and len(links) < 5:
						return "blog.naver.com", t_url.replace("http://blog.rss.naver.com/","").replace(".xml", "")

				if link.tag == "REFRESH":
					netloc = urlparse.urlparse(tt_url).netloc
					if tt_url.find(".tistory.com/") >= 0:
						tistory_id = netloc[:netloc.find(".")]
						return "tistory.com", tistory_id
					else:
						return self.checkDomain("http://%s/"%netloc)
					
				if len(links) < 3:
					if link.url.find("blog.naver.com") >= 0:     # http://baljak.com/
						try:
							ret_dict = self.bf.getAllDataDic(link.url)
							print link.url
							if ret_dict and ret_dict["gen"] == "blog.naver.com":
								naver_id = ret_dict["cid"]
								if naver_id not in ["PostList"]:
									return "blog.naver.com", naver_id
						except Exception, msg:
							print msg

			except Exception, msg:
				getLogger().error(msg)

		ret_dict = findData(html, self.attr_dict)

		if "tid" in ret_dict:
			tistory_id = ret_dict["tid"]
			if len(tistory_id) > 0:
				return "tistory.com", tistory_id
		if html.find("<meta content='blogger' name='generator'/>") >= 0:
			return "blogspot.com", None

		if len(feed_urls) < 3:
			for feed in feed_urls:
				if feed.endswith("/rss") or feed.endswith("feed=rss2"):
					result = self.checkFeed(feed)
					if result:
						if len(result) == 2:
							return result
						else:
							return result, feed
		return "etc", feed_urls


	def checkFeed(self, feed):

		ret_dict = getRssInfo(feed)
		blog_type = ""
		if ret_dict:
			if "generator" in ret_dict:
				generator = ret_dict["generator"]
				if generator.lower().find("tistory.com") >= 0:
					blog_type = "tistory.com"
				elif generator.lower().find("naver blog") >= 0:      
					blog_type = "blog.naver.com"
				elif generator.lower().find("daum blog") >= 0:
					blog_type = "blog.daum.net"
				elif generator.lower().find("textcube") >= 0:
					blog_type = "textcube.org"
				elif generator.lower().find("wordpress.org") >= 0:
					blog_type = "wordpress.org"
				elif generator.lower().find("blogger") >= 0:
					blog_type = "blogspot.com"
				elif generator.lower().find("xpressengine") >= 0:
					blog_type = "xpressengine.com"
				else:
					blog_type = "etc"

				if blog_type == "tistory.com":
					if "links" in ret_dict:
						domains = set()
						post_url = ""
						for link in ret_dict["links"]:
							netloc = urlparse.urlparse(link).netloc
							
							domains.add(netloc)
							post_url = link
						if len(domains) == 1:
							tistory_id = getTistoryId(post_url)
							return "tistory.com", tistory_id
					return "tistory.com", None
				return blog_type
		return None

	def checkUrl(self, url):

		# 예외 str  :   /response/ , /comment/, 
		parsed_url = urlparse.urlparse(url)
		domain = parsed_url.netloc
		blog_type, blog_id = self.checkDomain("http://"+domain+"/")
		rc_url = ""
		if blog_type == "tistory.com":
			rc_url = "http://%s.tistory.com/rss"%blog_id
		elif blog_type == "blog.naver.com":
			rc_url = "http://blog.rss.naver.com/%s.xml"%blog_id
		elif blog_type == "blogspot.com":
			if domain.find(".blogspot.com") >= 0:
				blog_id = domain[:domain.find(".")]
				rc_url = "http://%s.blogspot.com/feeds/posts/default?alt=rss"%blog_id
			else:
				rc_url = "http://%s/feeds/posts/default?alt=rss"%domain
		elif blog_type == "textcube.org":
			rc_url = blog_id
			blog_id = None
		elif blog_type == "wordpress.org":
			rc_url = blog_id
			blog_id = None
			
		return blog_type, blog_id, rc_url


if __name__ == "__main__":

	import sys

	a = BlogChecker()
	if len(sys.argv) > 1:
		url = sys.argv[1]
		print a.checkUrl(url)
		exit(0)
	#print a.checkUrl("http://www.rudolph.kr/")
	#print a.checkUrl("http://tvpop.khan.kr/")
	#print a.checkUrl("http://ljd710.kr/")    # title에 도메인네임 서비스(DNS) 정지
	#print a.checkUrl("http://prisbrary.com/")
	#print a.checkUrl("http://www.finebe.com/71")
	#print a.checkUrl("http://neoearly.net/2467528")
	#print a.checkUrl("http://wacoalblog.com/14")
	#print a.checkUrl("http://foforu.net/")
	#print a.checkUrl("http://www.nuriblog.com/esosi/rss")
	#print a.checkUrl("http://sosori.com/")
	print a.checkUrl("http://smd.or.kr/863")
	#print a.checkUrl("http://isabelstore.net/10152744238")
	#print a.checkUrl("http://baljak.com")
	#print a.checkUrl("http://webple.net")
	#print a.checkUrl("http://bestpaper.wordpress.com/2010/06/01/32bit-risc-processor-krp-%ec%9d%98-verilog%eb%a5%bc-%ec%82%ac%ec%9a%a9%ed%95%9c-rtl-%ec%84%a4%ea%b3%84")

