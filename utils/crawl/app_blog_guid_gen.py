#!/usr/bin/env python
#coding: utf8

import re
import urllib2
from urlparse import urlparse
from common_func import getDBCursor
import db_conf as conf

ESCAPE_STRS = ":/?#[]@!$&'()*+,;=\\%~"

def strToList(s):
	ret = s.split("\n")
	nret = []
	for i in range(0,len(ret)) :
		tmp = ret[i].strip()
		if tmp != "" :
			nret.append(tmp)
	ret = None
	return nret

def isInt(s) :
	try :
		i = int(s)
		return True
	except :
		return False

class BlogUrlFactory:
	def __init__(self, db_cursor=None):
		self.db_dict = dict()
		self.db_cursor = None
		self.db_table = conf.INDIE_BLOG_CHANNEL_TABLE
		is_my_cursor = False

		if db_cursor is None :
			is_my_cursor = True
			db_con, db_cursor = getDBCursor(host=conf.GUID_GEN_DB_HOST, user=conf.GUID_GEN_DB_USER, passwd=conf.GUID_GEN_DB_PWD, db=conf.GUID_GEN_DB_NAME)
		self.db_cursor = db_cursor

		self.blog_fam_list = ["naver","daum","tistory","blogspot","aladin","interpark","dreamwiz","joins","kyobo","chosun","jinbo","ohmynews","moneta","yes24","donga","indie"]
		self.key_list = ["guid","gen","ourl","curl","vurl","cid","pid","fam","trackback"]
		self.pt_dic = None # pattern dic (ex. pt_dic["SITE_NAME"]["PATTERN_NAME"])
		self.indie_data_dic = dict()
		self.cid_str = "##CID##"
		self.pid_str = "##PID##"
		self.initPatterns()
		self.initIndieData()

		if is_my_cursor :
			self.db_cursor.close()

	def initPatterns(self):
		# 1. 항상 더 엄격한 패턴을 먼저 적을 것.(먼저 match가 됨)
		# 2. 마지막 패턴은 끝에 쉼표 붙이지 말것.
		# 3. 채널ID만 알수 있는 패턴도 추가할 것
		# 4. PID가 숫자가 아닌경우에는 tmp_guid라는 필드를 생성하여 guid를 내보낼 것
		# 5. URL 마지막에 '/'가 붙는경우는 항상 제거할 것
		naver_url_patterns = """
			http://blog\.naver\.com/PostView\.nhn\?.*blogId=([\w\-]+).*logNo=(\d+).*
			http://blog\.naver\.com/PostList\.nhn\?.*blogId=([\w\-]+).*
			(http)://blog\.naver\.com/PostView\.nhn\?.*logNo=(\d+).*blogId=([\w\-]+).*
			http://blog\.naver\.com/([\w\-]+)\?.*logNo=(\d+).*
			http://m\.blog\.naver\.com/([\w\-]+)/(\d+).*
			http://blog\.naver\.com/([\w\-]+)/(\d+).*
			http://([\w\-]+)\.blog\.me/(\d+).*
			http://blog\.naver\.com/PostView\.nhn\?.*blogId=([\w\-]+).*
			http://blog\.rss\.naver\.com/([\w\-]+)\.xml
			http://m\.blog\.naver\.com/([\w\-]+)/?$
			http://blog\.naver\.com/([\w\-]+)/?$
			http://([\w\-]+)\.blog\.me.*
			"""
		daum_url_patterns = """
			http://m?.?blog\.daum\.net/([\w\-]+)/(\d+).*
			http://blog\.daum\.net/xml/rss/([\w\-]+)
			http://m?.?blog\.daum\.net/([\w\-]+)/?
			"""
			#http://blog\.daum\.net/_blog/BlogTypeView\.do\?.*blogid=([\w\-]+).*articleno=(\d+).*
			#(http)://blog\.daum\.net/_blog/BlogTypeView\.do\?.*articleno=(\d+).*blogid=([\w\-]+).*
			#http://blog\.daum\.net/_blog/BlogTypeView\.do\?.*blogid=([\w\-]+).*
		tistory_url_patterns = """
			http://([\w\-]+)\.tistory\.com/(\d+).*
			http://([\w\-]+)\.tistory\.com/entry/([가-힣\w%\-]+)/?.*
			http://([\w\-]+)\.tistory\.com.*
			"""
			#http://([\w\-]+)\.tistory\.com/entry/.*,
		blogspot_url_patterns = """
			http://([\w\-]+)\.blogspot\.kr/(\d{4}/\d{2}/[가-힣\w%\-]+\.[hH][tT][mM][lL]).*
			http://([\w\-]+)\.blogspot\.kr.*
			http://([\w\-]+)\.blogspot\.com/(\d{4}/\d{2}/[가-힣\w%\-]+\.[hH][tT][mM][lL]).*
			http://([\w\-]+)\.blogspot\.com.*
			"""
		aladin_url_patterns = """
			http://blog\.aladin\.co\.kr/([\w\-]+)/(\d+).*
			http://blog\.aladin\.co\.kr/([\w\-]+)/?
			"""
		interpark_url_patterns = """
			http://book\.interpark\.com/blog/([\w\-]+)/(\d+).*
			http://book\.interpark\.com/blog/postArticleView\.rdo\?.*blogName=([\w\-]+).*postNo=(\d+).*
			(http)://book\.interpark\.com/blog/postArticleView\.rdo\?.*postNo=(\d+).*blogName=([\w\-]+).*
			http://book\.interpark\.com/blog/postArticleView\.rdo\?.*blogName=([\w\-]+).*
			http://book\.interpark\.com/blog/viewBlogRss\.rdo\?blogName=([\w\-]+).*
			http://book\.interpark\.com/blog/([\w\-]+)/?
			"""
		dreamwiz_url_patterns = """
			http://blog\.dreamwiz\.com/([\w\-]+)/(\d+).*
			http://blog\.dreamwiz\.com/media/index\.asp\?.*uid=([\w\-]+).*list_id=(\d+).*
			(http)://blog\.dreamwiz\.com/media/index\.asp\?.*list_id=(\d+).*uid=([\w\-]+).*
			http://blog\.dreamwiz\.com/media/index\.asp\?.*uid=([\w\-]+).*
			http://blog\.dreamwiz\.com/([\w\-]+)/index\.xml
			http://blog\.dreamwiz\.com/([\w\-]+)/?
			"""
		joins_url_patterns = """
			http://blog\.joins\.com/([\w\-]+)/(\d+).*
			http://blog\.joins\.com/media/folder[Ll]ist[Ss]lide\.asp\?.*uid=([\w\-]+).*list_id=(\d+).*
			(http)://blog\.joins\.com/media/folder[Ll]ist[Ss]lide\.asp\?.*list_id=(\d+).*uid=([\w\-]+).*
			http://blog\.joins\.com/media/folderlistslide\.asp\?.*uid=([\w\-]+).*
			http://blog\.joins\.com/([\w\-]+)/?
			"""
		kyobo_url_patterns = """
			http://booklog\.kyobobook\.co\.kr/([\w\-]+)/(\d+)/?.*
			http://booklog\.kyobobook\.co\.kr/([\w\-]+)/rss/?
			http://booklog\.kyobobook\.co\.kr/([\w\-]+)/?
			"""
		chosun_url_patterns = """
			http://blogs\.chosun\.com/([\w\-]+)/(\d{4}/\d{2}/\d{2}/.+)/?.*
			http://blogs\.chosun\.com/([\w\-]+)/feed/?
			http://blogs\.chosun\.com/([\w\-]+)/?
			"""
		jinbo_url_patterns = """
			http://blog\.jinbo\.net/([\w\-]+)/(\d+).*
			http://blog\.jinbo\.net/([\w\-]+)/rss/?
			http://blog\.jinbo\.net/([\w\-]+)/?
			"""
		ohmynews_url_patterns = """
			http://blog.ohmynews.com/([\w\-]+)/(\d+).*
			http://blog.ohmynews.com/([\w\-]+)/rss/?
			http://blog.ohmynews.com/([\w\-]+)/?
			"""
		moneta_url_patterns = """
			http://blog\.moneta\.co\.kr/([\w\-]+)/(\d+).*
			http://blog\.moneta\.co\.kr/blog\.log\.view\.screen\?.*blogId=([\w\-]+).*logId=(\d+).*
			(http)://blog\.moneta\.co\.kr/blog\.log\.view\.screen\?.*logId=(\d+).*blogId=([\w\-]+).*
			http://blog\.moneta\.co\.kr/blog\.log\.view\.screen\?.*blogId=([\w\-]+).*
			http://blog\.moneta\.co\.kr/blog\.screen\?.*blogId=([\w\-]+).*
			http://blog\.moneta\.co\.kr/rss/blog/([\w\-]+)/.*
			http://blog\.moneta\.co\.kr/([\w\-]+)/?
			"""
		yes24_url_patterns = """
			http://blog\.yes24\.com/blog/blogMain\.aspx\?.*blogid=([\w\-]+).*artSeqNo=(\d+).*
			(http)://blog\.yes24\.com/blog/blogMain\.aspx\?.*artSeqNo=(\d+).*blogid=([\w\-]+).*
			http://blog\.yes24\.com/([\w\-]+)/(\d+).*
			http://blog\.yes24\.com/blog/blogMain\.aspx\?.*blogid=([\w\-]+).*
			http://blog\.yes24\.com/rss/([\w\-]+)\.xml
			http://blog\.yes24\.com/([\w\-]+)/?
			"""
			#http://blog\.yes24\.com/(document)/(\d+).*, 
		donga_url_patterns = """
			http://blog.donga.com/([\w\-]+)/archives/(\d+).*
			http://blog.donga.com/([\w\-]+)/\?p=(\d+).*
			http://blog.donga.com/([\w\-]+)/feed/rss/?
			http://blog.donga.com/([\w\-]+)/feed/?
			http://blog.donga.com/([\w\-]+)/?
			"""
		# 뒤쪽에 String붙는것 : tmp_guid 처리
		wordpress_url_patterns = """
			http://([\w\-]+)\.wordpress\.com/\?p=(\d+).*
			http://([\w\-]+)\.wordpress\.com/(\d{4}/\d{2}/\d{2}/[가-힣\w%\-]+)/?
			http://([\w\-]+)\.wordpress\.com/(\d{4}/\d{2}/[가-힣\w%\-]+)/?
			http://([\w\-]+)\.wordpress\.com/(\d{4}/[가-힣\w%\-]+)/?
			http://([\w\-]+)\.wordpress\.com/feed/?
			http://([\w\-]+)\.wordpress\.com/([가-힣\w%\-]+)/?
			http://([\w\-]+)\.wordpress\.com.*
			"""
		# group(1)=host, group(2)=path, group(3)=postNo
		# tmp_guid가 생성되는 경우는 DB에 해당 채널 정보가 있는경우에만 가능
		indie_url_patterns = """
			http://([\w\-\.]+)(/[\w\-/]+)/(\d+)
			http://([\w\-\.]+)(/[\w\-/]+)/(\d+)\?.*
			http://([\w\-\.]+)(/)\?p=(\d+)
			http://([\w\-\.]+)(/)\?\w+=(\d+)
			http://([\w\-\.]+)(/)(\d{4}/\d{2}/\d{2}/[\w%\-]+)/?
			http://([\w\-\.]+)(/)(\d{4}/\d{2}/[\w%\-]+)/?
			http://([\w\-\.]+)(/)(\d{4}/[\w%\-]+)/?
			http://([\w\-\.]+)(/)(\d+)
			http://([\w\-\.]+)(/[\w\-/]+)/\?.+=(\d+)
			http://([\w\-\.]+)(/.+?)/(\d+)
			http://([\w\-\.]+)/feed/?
			http://([\w\-\.]+)/rss/?
			http://([\w\-\.]+)(/)([가-힣\w%\-]+)/?
			http://([\w\-\.]+)/?
			"""

		if self.pt_dic is None :
			self.pt_dic = dict()
		self.pt_dic["naver"] = dict()
		self.pt_dic["daum"] = dict()
		self.pt_dic["tistory"] = dict()
		self.pt_dic["blogspot"] = dict()
		self.pt_dic["aladin"] = dict()
		self.pt_dic["interpark"] = dict()
		self.pt_dic["dreamwiz"] = dict()
		self.pt_dic["joins"] = dict()
		self.pt_dic["kyobo"] = dict()
		self.pt_dic["jinbo"] = dict()
		self.pt_dic["chosun"] = dict()
		self.pt_dic["ohmynews"] = dict()
		self.pt_dic["moneta"] = dict()
		self.pt_dic["yes24"] = dict()
		self.pt_dic["donga"] = dict()
		self.pt_dic["wordpress"] = dict()
		self.pt_dic["indie"] = dict()

		# 유입 패턴 초기화
		self.pt_dic["naver"]["in"] = strToList(naver_url_patterns)
		self.pt_dic["daum"]["in"] = strToList(daum_url_patterns)
		self.pt_dic["tistory"]["in"] = strToList(tistory_url_patterns)
		self.pt_dic["blogspot"]["in"] = strToList(blogspot_url_patterns)
		self.pt_dic["aladin"]["in"] = strToList(aladin_url_patterns)
		self.pt_dic["interpark"]["in"] = strToList(interpark_url_patterns)
		self.pt_dic["dreamwiz"]["in"] = strToList(dreamwiz_url_patterns)
		self.pt_dic["joins"]["in"] = strToList(joins_url_patterns)
		self.pt_dic["kyobo"]["in"] = strToList(kyobo_url_patterns)
		self.pt_dic["jinbo"]["in"] = strToList(jinbo_url_patterns)
		self.pt_dic["chosun"]["in"] = strToList(chosun_url_patterns)
		self.pt_dic["ohmynews"]["in"] = strToList(ohmynews_url_patterns)
		self.pt_dic["moneta"]["in"]= strToList(moneta_url_patterns)
		self.pt_dic["yes24"]["in"] = strToList(yes24_url_patterns)
		self.pt_dic["donga"]["in"] = strToList(donga_url_patterns)
		self.pt_dic["wordpress"]["in"] = strToList(wordpress_url_patterns)
		self.pt_dic["indie"]["in"] = strToList(indie_url_patterns)

		# 금지된 channel id 리스트
		self.pt_dic["naver"]["bancid"] = []
		self.pt_dic["daum"]["bancid"] = ["_top","_blog"]
		self.pt_dic["tistory"]["bancid"] = []
		self.pt_dic["blogspot"]["bancid"] = []
		self.pt_dic["aladin"]["bancid"] = []
		self.pt_dic["interpark"]["bancid"] = []
		self.pt_dic["dreamwiz"]["bancid"] = []
		self.pt_dic["joins"]["bancid"] = []
		self.pt_dic["kyobo"]["bancid"] = []
		self.pt_dic["jinbo"]["bancid"] = []
		self.pt_dic["chosun"]["bancid"] = []
		self.pt_dic["ohmynews"]["bancid"] = []
		self.pt_dic["moneta"]["bancid"] = []
		self.pt_dic["yes24"]["bancid"] = ["blogmain","blog","document"]
		self.pt_dic["donga"]["bancid"] = []
		self.pt_dic["wordpress"]["bancid"] = []
		self.pt_dic["indie"]["bancid"] = []

		cid = self.cid_str
		pid = self.pid_str

		# View url (Simplest url) 패턴 지정
		self.pt_dic["naver"]["view"] = "http://blog.naver.com/"+cid+"/"+pid
		self.pt_dic["daum"]["view"] = "http://blog.daum.net/"+cid+"/"+pid
		self.pt_dic["tistory"]["view"] = "http://"+cid+".tistory.com/"+pid
		self.pt_dic["blogspot"]["view"] = "http://"+cid+".blogspot.com/"+pid
		self.pt_dic["aladin"]["view"] = "http://blog.aladin.co.kr/"+cid+"/"+pid
		self.pt_dic["interpark"]["view"] = "http://book.interpark.com/blog/"+cid+"/"+pid
		self.pt_dic["dreamwiz"]["view"] = "http://blog.dreamwiz.com/"+cid+"/"+pid
		self.pt_dic["joins"]["view"] = "http://blog.joins.com/"+cid+"/"+pid
		self.pt_dic["kyobo"]["view"] = "http://booklog.kyobobook.co.kr/"+cid+"/"+pid
		self.pt_dic["jinbo"]["view"] = "http://blog.jinbo.net/"+cid+"/"+pid
		self.pt_dic["chosun"]["view"] = "http://blogs.chosun.com/"+cid+"/"+pid
		self.pt_dic["ohmynews"]["view"] = "http://blog.ohmynews.com/"+cid+"/"+pid
		self.pt_dic["moneta"]["view"]= "http://blog.moneta.co.kr/"+cid+"/"+pid
		self.pt_dic["yes24"]["view"] = "http://blog.yes24.com/"+cid+"/"+pid
		self.pt_dic["donga"]["view"] = "http://blog.donga.com/"+cid+"/?p="+pid
		self.pt_dic["wordpress"]["view"] = "http://"+cid+".wordpress.com/?p="+pid

		# guid (= download url) 패턴 지정
		# 조건1. 접근 가능한 동일 포스트의 모든 URL에 대하여 유일해야 함.
		# 조건2. 해당 URL로 접근 시 다운로드 하여 크롤이 가능해야 함. (예외 : daum)
		# 조건3. URL에 Parameter가 존재할 시에는 알파벳 순으로 정렬해 주어야 함.(ex. dreamwiz, yes24)

		self.pt_dic["naver"]["guid"] = "http://blog.naver.com/PostView.nhn?blogId="+cid+"&logNo="+pid

		# daum은 자체 내장 변환 id를 사용(download 불가)
		self.pt_dic["daum"]["guid"] = "http://blog.daum.net/"+cid+"/"+pid
		#self.pt_dic["daum"]["guid"] = "http://blog.daum.net/_blog/BlogTypeView.do?blogid="+cid+"&articleno="+pid

		self.pt_dic["tistory"]["guid"] = self.pt_dic["tistory"]["view"]
		self.pt_dic["blogspot"]["guid"] = self.pt_dic["blogspot"]["view"]
		self.pt_dic["aladin"]["guid"] = self.pt_dic["aladin"]["view"]
		self.pt_dic["interpark"]["guid"] = "http://book.interpark.com/blog/postArticleView.rdo?blogName="+cid+"&postNo="+pid
		self.pt_dic["dreamwiz"]["guid"] = "http://blog.dreamwiz.com/media/index.asp?list_id="+pid+"&uid="+cid
		self.pt_dic["joins"]["guid"] = "http://blog.joins.com/media/folderlistslide.asp?list_id="+pid+"&uid="+cid
		self.pt_dic["kyobo"]["guid"] = self.pt_dic["kyobo"]["view"]
		self.pt_dic["jinbo"]["guid"] = self.pt_dic["jinbo"]["view"]
		self.pt_dic["chosun"]["guid"] = self.pt_dic["chosun"]["view"]
		self.pt_dic["ohmynews"]["guid"] = self.pt_dic["ohmynews"]["view"]
		self.pt_dic["moneta"]["guid"]= "http://blog.moneta.co.kr/blog.log.view.screen?blogId="+cid+"&logId="+pid
		self.pt_dic["yes24"]["guid"] = "http://blog.yes24.com/blog/blogMain.aspx?artSeqNo="+pid+"&blogid="+cid
		self.pt_dic["donga"]["guid"] = self.pt_dic["donga"]["view"]
		self.pt_dic["wordpress"]["guid"] = self.pt_dic["wordpress"]["view"]

		# channel url(=feed) 패턴 지정
		self.pt_dic["naver"]["curl"] = "http://blog.rss.naver.com/"+cid+".xml"
		self.pt_dic["daum"]["curl"] = "http://blog.daum.net/xml/rss/"+cid
		self.pt_dic["tistory"]["curl"] = "http://"+cid+".tistory.com/rss"
		self.pt_dic["blogspot"]["curl"] = "http://"+cid+".blogspot.com/feeds/posts/default?alt=rss"
		self.pt_dic["aladin"]["curl"] = "http://blog.aladin.co.kr/"+cid+"/rss"
		self.pt_dic["interpark"]["curl"] = "http://book.interpark.com/blog/viewBlogRss.rdo?blogName="+cid+"&type=rss_2.0"
		self.pt_dic["dreamwiz"]["curl"] = "http://blog.dreamwiz.com/"+cid+"/index.xml"
		self.pt_dic["joins"]["curl"] = "http://blog.joins.com/"+cid 
		self.pt_dic["kyobo"]["curl"] = "http://booklog.kyobobook.co.kr/"+cid+"/rss"
		self.pt_dic["jinbo"]["curl"] = "http://blog.jinbo.net/"+cid+"/rss"
		self.pt_dic["chosun"]["curl"] = "http://blogs.chosun.com/"+cid+"/feed"
		self.pt_dic["ohmynews"]["curl"] = "http://blog.ohmynews.com/"+cid+"/rss"
		self.pt_dic["moneta"]["curl"]= "http://blog.moneta.co.kr/rss/blog/"+cid+"/-1"
		self.pt_dic["yes24"]["curl"] = "http://blog.yes24.com/rss/"+cid+".xml"
		self.pt_dic["donga"]["curl"] = "http://blog.donga.com/"+cid+"/feed"
		self.pt_dic["wordpress"]["curl"] = "http://"+cid+".wordpress.com/feed"

		# murl 패턴 지정 ingingingingingingingingin
		#self.pt_dic["naver"]["guid"] = "http://blog.naver.com/"+cid+"/"+pid
		self.pt_dic["naver"]["murl"] = "http://m.blog.naver.com/"+cid+"/"+pid
		self.pt_dic["daum"]["murl"] = "http://m.blog.daum.net/_blog/_m/articleView.do?blogid="+cid+"&articleno="+pid
		self.pt_dic["tistory"]["murl"] = "http://"+cid+".tistory.com/m/post/"+pid
		self.pt_dic["blogspot"]["murl"] = "http://"+cid+".blogspot.com/"+pid+"?m=1"
		self.pt_dic["aladin"]["murl"] = "http://blog.aladin.co.kr/m/"+cid+"/"+pid
		self.pt_dic["interpark"]["murl"] = self.pt_dic["interpark"]["guid"]
		self.pt_dic["dreamwiz"]["murl"] = self.pt_dic["dreamwiz"]["guid"]
		self.pt_dic["joins"]["murl"] = self.pt_dic["joins"]["guid"]
		self.pt_dic["kyobo"]["murl"] = "http://booklog.kyobobook.co.kr/"+cid+"/"+pid
		self.pt_dic["jinbo"]["murl"] = self.pt_dic["jinbo"]["guid"]
		self.pt_dic["chosun"]["murl"] = self.pt_dic["chosun"]["guid"]
		self.pt_dic["ohmynews"]["murl"] = "http://m.blog.ohmynews.com/"+cid+"/"+pid
		self.pt_dic["moneta"]["murl"]= self.pt_dic["moneta"]["guid"]
		self.pt_dic["yes24"]["murl"] = self.pt_dic["yes24"]["guid"]
		self.pt_dic["donga"]["murl"] = self.pt_dic["donga"]["guid"]
		self.pt_dic["wordpress"]["murl"] = self.pt_dic["donga"]["guid"]

		# channel link 패턴 지정
		self.pt_dic["naver"]["clink"] = "http://blog.naver.com/"+cid
		self.pt_dic["daum"]["clink"] = "http://blog.daum.net/"+cid
		self.pt_dic["tistory"]["clink"] = "http://"+cid+".tistory.com"
		self.pt_dic["blogspot"]["clink"] = "http://"+cid+".blogspot.com"
		self.pt_dic["aladin"]["clink"] = "http://blog.aladin.co.kr/"+cid
		self.pt_dic["interpark"]["clink"] = "http://book.interpark.com/"+cid
		self.pt_dic["dreamwiz"]["clink"] = "http://blog.dreamwiz.com/"+cid
		self.pt_dic["joins"]["clink"] = "http://blog.joins.com/"+cid
		self.pt_dic["kyobo"]["clink"] = "http://booklog.kyobobook.co.kr/"+cid
		self.pt_dic["jinbo"]["clink"] = "http://blog.jinbo.net/"+cid
		self.pt_dic["chosun"]["clink"] = "http://blogs.chosun.com/"+cid
		self.pt_dic["ohmynews"]["clink"] = "http://blog.ohmynews.com/"+cid
		self.pt_dic["moneta"]["clink"]= "http://blog.moneta.co.kr/"+cid
		self.pt_dic["yes24"]["clink"] = "http://blog.yes24.com/"+cid
		self.pt_dic["donga"]["clink"] = "http://blog.donga.com/"+cid
		self.pt_dic["wordpress"]["clink"] = "http://"+cid+".wordpress.com"

		# generator(host key) 패턴 지정
		self.pt_dic["naver"]["gen"] = "blog.naver.com"
		self.pt_dic["daum"]["gen"] = "blog.daum.net"
		self.pt_dic["tistory"]["gen"] = "tistory.com"
		self.pt_dic["blogspot"]["gen"] = "blogspot.com"
		self.pt_dic["aladin"]["gen"] = "blog.aladin.co.kr"
		self.pt_dic["interpark"]["gen"] = "book.interpark.com"
		self.pt_dic["dreamwiz"]["gen"] = "blog.dreamwiz.com"
		self.pt_dic["joins"]["gen"] = "blog.joins.com"
		self.pt_dic["kyobo"]["gen"] = "booklog.kyobobook.co.kr"
		self.pt_dic["jinbo"]["gen"] = "blog.jinbo.net"
		self.pt_dic["chosun"]["gen"] = "blogs.chosun.com"
		self.pt_dic["ohmynews"]["gen"] = "blog.ohmynews.com"
		self.pt_dic["moneta"]["gen"]= "blog.moneta.co.kr"
		self.pt_dic["yes24"]["gen"] = "blog.yes24.com"
		self.pt_dic["donga"]["gen"] = "blog.donga.com"
		self.pt_dic["wordpress"]["gen"] = "wordpress.com"

	def initIndieData(self) :
		query = "SELECT id, domain, channel_url, org_domain, link FROM "+self.db_table
		self.db_cursor.execute(query)
		result = self.db_cursor.fetchall()
		if len(result) < 1:
			return None

		for parser_id, generator, feed, host, link in result :

			if feed is None or generator is None :
				continue

			khost = host # www 있으면 제거된 host
			if len(host) > 4 and host.startswith("www") :
				khost = host[4:]

			self.indie_data_dic[khost] = dict()
			self.indie_data_dic[khost]["host"] = host
			self.indie_data_dic[khost]["parser_id"] = parser_id
			self.indie_data_dic[khost]["gen"] = generator
			self.indie_data_dic[khost]["curl"] = feed
			self.indie_data_dic[khost]["clink"] = link



	# returns blog site name
	def getBlogFam(self,url):
		for fam in self.pt_dic :
			if fam == "indie" :
				continue
			pt_list = self.pt_dic[fam]["in"] 

			for pt in pt_list :
				result = re.match(pt,url)
				if result :
					reverse = False

					# parameter로 cid,pid가 뒤바뀐경우 'http'를 group(1)로 지정
					try :
						cid = result.group(1)
						if cid == "http" :
							reverse = True
					except Exception, msg :
						cid = None

					try :
						pid = result.group(2)
					except Exception, msg :
						pid = None

					# 나중에 잡히는것이 cid
					if reverse :
						try :
							cid = result.group(3)
						except Exception, msg :
							cid = None

					# 금지된 cid 일경우에 None
					if cid in self.pt_dic[fam]["bancid"]:
						return None
					else :
						return (fam,cid,pid,pt)

		return None

	def parseIndieURL(self, url):
		pt_list = self.pt_dic["indie"]["in"] 

		for pt in pt_list :
			result = re.match(pt,url)
			if result : # (host, path, postID)
				if result.lastindex == 3 :
					return (result.group(1), result.group(2), result.group(3), pt)
				if result.lastindex == 2 :
					return (result.group(1), result.group(2), None, pt)
				if result.lastindex == 1 :
					return (result.group(1), None, None, pt)
		return None

	def getDataFromDB(self, url, path=None, pid=None):
		"""
		독립도메인을 사용하는 블로그의 경우 DB에서 조회
		RETURN : (guid, feedUrl)
		"""

		if self.indie_data_dic is None :
			return None

		host = urlparse(url).hostname
		khost = host # www 있으면 제거된 host
		if len(host) > 4 and host.startswith("www") :
			khost = host[4:]

		data_dic = None
		if khost in self.indie_data_dic:
			data_dic = dict(self.indie_data_dic[khost])
			host = data_dic["host"] # DB의 host를 사용하여 guid 제작
		else :
			return None

		if path :
			if path != "/" and len(path) > 1 : 
				path = path + "/"
		else :
			path = "/"

		data_dic["path"] = path

		guid = None
		if pid :
			data_dic["pid"] = pid
			#TODO 각종 독립 HOST output URL형태 정의
			#if data_dic["gen"] == "blog.naver.com" :
			if data_dic["gen"] == "tistory.com" :
				if isInt(pid) :
					guid = "http://"+host+"/"+pid
				else :
					pid = urllib2.quote(pid, safe=ESCAPE_STRS)
					data_dic["tmp_guid"] = "http://"+host+"/entry/"+pid
			if data_dic["gen"] == "blogspot.com" : # 유일하게 pid가 글자일때 guid 생산
				pid = urllib2.quote(pid, safe=ESCAPE_STRS)
				guid = "http://"+host+"/"+pid
			if data_dic["gen"] == "wordpress.org" :
				if isInt(pid) :
					guid = "http://"+host+path+"?p="+pid
					guid = "http://"+host+"/?p="+pid
				else :
					pid = urllib2.quote(pid, safe=ESCAPE_STRS)
					data_dic["tmp_guid"] = "http://"+host+"/"+pid
				data_dic["fam"] = "wordpress"
			elif data_dic["gen"] == "textcube.org" :
				guid = "http://"+host+path+pid
				guid = "http://"+host+"/"+pid
				data_dic["fam"] = "textcube"
			elif data_dic["gen"] == "xpressengine.com" :
				guid = "http://"+host+path+pid
				guid = "http://"+host+"/"+pid
				data_dic["fam"] = "xpressengine"
			elif data_dic["gen"] == "drupal.org" :
				guid = "http://"+host+path+pid
				data_dic["fam"] = "drupal"
		data_dic["guid"] = guid
		data_dic["vurl"] = guid

		return data_dic

	def getParserID(self,url) :
		data_dic = self.getDataFromDB(url,"","")
		if data_dic :
			if "parser_id" in data_dic :
				return data_dic["parser_id"]
		return None

	# return : generator
	def getGenerator(self,url):
		urlData = self.getAllDataDic(url)
		if urlData :
			if "gen" in urlData :
				return urlData["gen"]
		return None

	# return : matched regex pattern
	def getRegex(self,url):
		urlData = self.getAllDataDic(url)
		if urlData :
			if "regex" in urlData :
				return urlData["regex"]
		return None

	# return : guid(= download_url)
	def getGuid(self,url):
		urlData = self.getAllDataDic(url)
		if urlData :
			if "guid" in urlData :
				return urlData["guid"]
		return None

	# return : view_url
	def getViewUrl(self,url):
		urlData = self.getAllDataDic(url)
		if urlData :
			if "vurl" in urlData :
				return urlData["vurl"]
		return None

	# return : mobile_url
	def getMobileUrl(self,url):
		urlData = self.getAllDataDic(url)
		if urlData :
			if "murl" in urlData :
				return urlData["murl"]
		return None

	# return : feed_url
	def getChannelUrl(self,url):
		urlData = self.getAllDataDic(url)
		if urlData :
			if "curl" in urlData :
				return urlData["curl"]
		return None

	# return : channel_url
	def getChannelLink(self,url):
		urlData = self.getAllDataDic(url)
		if urlData :
			if "clink" in urlData :
				return urlData["clink"]
		return None

	# NOT IN USE
	# return : url (which shows trackback list)
	def getTrackback(self,url):
		urlData = self.getAllDataDic(url)
		if urlData :
			if "trackback" in urlData :
				return urlData["trackback"]
		return None

	# return : dictionary which contains all data (key : regex,guid,ourl,curl,vurl,cid,pid,trackback)
	def getAllDataDic(self,url):
		fam_info = self.getBlogFam(url)
		if fam_info is not None :
			dic = dict()
			(fam,cid,pid,pt) = fam_info 
			dic["fam"] = fam
			dic["ourl"] = url
			dic["gen"] = self.pt_dic[fam]["gen"]
			dic["regex"] = pt

			if cid is not None :
				dic["cid"] = cid
				curl_pt = self.pt_dic[fam]["curl"]
				if fam == "joins" :
					dic["curl"] = curl_pt.replace(self.cid_str,"usr/"+cid[0]+"/"+cid[1]+"/"+cid+"/rss.xml")
				else :
					dic["curl"] = curl_pt.replace(self.cid_str,cid)

				clink_pt = self.pt_dic[fam]["clink"]
				dic["clink"] = clink_pt.replace(self.cid_str,cid)
				#trk_pt = self.pt_dic[fam]["trackback"]
				#dic["trackback"] = trk_pt.replace(self.cid_str,cid).replace(self.pid_str,pid)

			if pid is not None :
				dic["pid"] = pid

			if (cid is not None) and (pid is not None) :
				isNormal = True
				# TODO tmp_guid 만들어 주는 곳
				if fam == "wordpress" :
					if not isInt(pid) :
						isNormal = False
						dic["tmp_guid"] = dic["clink"]+"/"+pid

				if fam == "tistory" : 
					if not isInt(pid) :
						isNormal = False
						dic["tmp_guid"] = dic["clink"]+"/entry/"+pid

				if fam == "blogspot" :
					pid = urllib2.quote(pid, safe=ESCAPE_STRS)

				if isNormal :
					guid_pt = self.pt_dic[fam]["guid"]
					dic["guid"] = guid_pt.replace(self.cid_str,cid).replace(self.pid_str,pid)
					durl_pt = self.pt_dic[fam]["view"]
					dic["vurl"] = durl_pt.replace(self.cid_str,cid).replace(self.pid_str,pid)
					durl_pt = self.pt_dic[fam]["murl"]
					dic["murl"] = durl_pt.replace(self.cid_str,cid).replace(self.pid_str,pid)

			return dic

		else :
			url_data = self.parseIndieURL(url)
			if url_data is None :
				return None
			else :
				host, path, pid, pt = url_data
				data_dic = self.getDataFromDB(url, path, pid)
				if data_dic is not None:
					data_dic["regex"] = pt
					return data_dic

		return None

if __name__ == "__main__":
	import sys
	import optparse

	cmdparser = optparse.OptionParser(usage='%prog [option] [args]')
	cmdparser.add_option('-a', '--all', dest='showall', default=True, action='store_true',
			help='show all data')
	cmdparser.add_option('-d', '--debug', dest='isdebug', default=False, action='store_true',
			help='debug mode')
	cmdparser.add_option('-t', '--test', dest='showtest', default=False, action='store_true',
			help='show test url result (url list in file)')
	cmdparser.add_option('-g', '--guid', dest='showguid', default=False, action='store_true',
			help='show only guid')
	cmdparser.add_option('-v', '--view', dest='showviewurl', default=False, action='store_true',
			help='show view url')
	cmdparser.add_option('-m', '--mobile', dest='showmobileurl', default=False, action='store_true',
			help='show mobile url')
	cmdparser.add_option('-f', '--feed', dest='showfeedurl', default=False, action='store_true',
			help='show feed url')
	cmdparser.add_option('-c', '--channel', dest='showchannelurl', default=False, action='store_true',
			help='show channel url')
	cmdparser.add_option('-p', '--parser', dest='showparserid', default=False, action='store_true',
			help='show parser id of url')
	cmdparser.add_option('-b', '--basic', dest='showbasicparsed', default=False, action='store_true',
			help='show (blog family, channel id, post id) of url')

	(cmdoptions, cmdargs) = cmdparser.parse_args()

	factory = BlogUrlFactory()

	if len(sys.argv) == 1 :
		print "Usage : [option] [args]"
		print "Use '--help' to see all options"
		exit(0)

	#print cmdoptions
	#print cmdargs
	#exit(0)
	use_option = False

	if cmdoptions.showbasicparsed :
		for arg in cmdargs :
			print factory.getBlogFam(arg)

	if cmdoptions.showguid :
		for arg in cmdargs :
			print factory.getGuid(arg)

	if cmdoptions.showfeedurl :
		for arg in cmdargs :
			print factory.getChannelUrl(arg)

	if cmdoptions.showchannelurl :
		for arg in cmdargs :
			print factory.getChannelLink(arg)

	if cmdoptions.showmobileurl :
		for arg in cmdargs :
			print factory.getMobileUrl(arg)

	if cmdoptions.showviewurl :
		for arg in cmdargs :
			print factory.getViewUrl(arg)

	if cmdoptions.showparserid :
		for arg in cmdargs :
			print factory.getParserID(arg)

	if cmdoptions.showall :
		for arg in cmdargs :
			dic = factory.getAllDataDic(arg)
			if dic is not None :
				for item in dic.items() :
					print item[0] + " : " + str(item[1])
				print "============================"
			else :
				print "None"

	if cmdoptions.showtest :

		# cid, pid reversed examples
		urls = """
		http://blog.naver.com/PostView.nhn?logNo=220144100654&blogId=gkachzhfldk
		http://blog.daum.net/_blog/BlogTypeView.do?articleno=336&blogid=0Oys0&admin=
		http://book.interpark.com/blog/postArticleView.rdo?postNo=3827121&blogName=mujin98&listType=B&listSize=3&prdIpTp=20
		http://blog.yes24.com/blog/blogMain.aspx?artSeqNo=7819130&blogid=dontcrymommy&Gcode=011_17_2
		http://blog.joins.com/media/folderlistslide.asp?list_id=13521108&uid=liberum&folder=20
		http://blog.moneta.co.kr/blog.log.view.screen?logId=8018124&blogId=limcloud
		http://blog.dreamwiz.com/media/index.asp?list_id=14000705&uid=estheryoo&folder=0
		"""

		# All examples
		urls2 = """
		http://blog.naver.com/gkachzhfldk/220107453664
		http://blog.naver.com/gkachzhfldk
		http://m.blog.naver.com/gkachzhfldk/220107453664
		http://m.blog.naver.com/gkachzhfldk
		http://exboifriend.blog.me/70101722381
		http://exboifriend.blog.me
		http://blog.naver.com/PostView.nhn?blogId=gkachzhfldk&logNo=220144100654
		http://blog.naver.com/PostView.nhn?blogId=gkachzhfldk
		http://blog.naver.com/PostView.nhn?blogId=gkachzhfldk&logNo=220144100654&redirect=Dlog&widgetTypeCall=true&topReferer=http%3A%2F%2Fblog.naver.com%2FPostList.nhn%3FblogId%3Dgkachzhfldk

		http://blog.daum.net/tkfkdaq/336?t__nil_best=righttxt
		http://blog.daum.net/tkfkdaq
		http://m.blog.daum.net/tkfkdaq/336?t__nil_best=righttxt
		http://blog.daum.net/_blog/BlogTypeView.do?blogid=0Oys0&articleno=336&admin=

		http://jsilva.tistory.com/33
		http://jsilva.tistory.com/entry/이게-무슨-일임니까
		http://jsilva.tistory.com

		http://blog.aladin.co.kr/misshide/7149825
		http://blog.aladin.co.kr/707302156
		http://blog.aladin.co.kr/707302156/3381880
		http://blog.aladin.co.kr/707302156/3381880/asdff
		http://blog.aladin.co.kr/707302156/3381880?asdfasdff


		http://book.interpark.com/blog/mujin98
		http://book.interpark.com/blog/mujin98/3827121
		http://book.interpark.com/blog/postArticleView.rdo?blogName=mujin98&postNo=3827121
		http://book.interpark.com/blog/postArticleView.rdo?blogName=mujin98
		http://book.interpark.com/blog/postArticleView.rdo?blogName=mujin98&listType=B&listSize=3&postNo=3827121&prdIpTp=20

		http://blog.yes24.com/document/7819130
		http://blog.yes24.com/blog/blogMain.aspx?blogid=dontcrymommy&artSeqNo=7819130&Gcode=011_17_2
		http://blog.yes24.com/blog/blogMain.aspx?blogid=dontcrymommy&artSeqNo=7819130
		http://blog.yes24.com/blog/blogMain.aspx?blogid=dontcrymommy

		http://blog.joins.com/liberum
		http://blog.joins.com/liberum/13521108
		http://blog.joins.com/media/folderlistslide.asp?uid=liberum&folder=20&list_id=13521108
		http://blog.joins.com/media/folderlistslide.asp?uid=liberum&list_id=13521108
		http://blog.joins.com/media/folderlistslide.asp?uid=liberum

		http://booklog.kyobobook.co.kr/manido/1393668/#0
		http://booklog.kyobobook.co.kr/manido/1393668
		http://booklog.kyobobook.co.kr/manido

		http://blogs.chosun.com/bowler1/2014/09/29/%EC%89%BC-%EA%B7%B8%EB%A6%AC%EA%B3%A0-%EC%95%8E-%EC%BA%A0%ED%94%84-%EC%B0%B8%EA%B0%80%EC%9E%90%EC%9D%98-%ED%95%9C%EB%A7%88%EB%94%94-%EA%BF%88%EC%9D%84-%EA%BE%B8%EB%8A%94-%EB%93%AF%ED%95%9C/
		http://blogs.chosun.com/bowler1/2

		http://blog.jinbo.net/antiorder/391
		http://blog.jinbo.net/antiorder
		http://blog.jinbo.net/com/476?category=3

		http://blog.moneta.co.kr/limcloud
		http://blog.moneta.co.kr/limcloud/8018124/266143
		http://blog.moneta.co.kr/blog.log.view.screen?blogId=limcloud&listType=4&logId=8018124&folderType=1&category=266143
		http://blog.moneta.co.kr/blog.log.view.screen?blogId=limcloud&logId=8018124

		http://blog.ohmynews.com/chamstory/524876
		http://blog.ohmynews.com/kht0306/189673

		http://blog.dreamwiz.com/estheryoo/14000705
		http://blog.dreamwiz.com/media/index.asp?uid=estheryoo&folder=0&list_id=14000705

		http://blog.donga.com/ranbi/?p=356
		http://blog.donga.com/ranbi/archives/356
		"""
		urls3 = """
		http://aru.in/6
		http://blog.ahnlab.com/ahnlab/1969
		http://bo-da.net/archives/3206
		http://bo-da.net/?p=3203
		http://abc.com/?post=323
		http://freeappl.com/%ec%8b%a4%ec%a0%9c-%ea%b0%99%ec%9d%80-%ec%9a%b4%ec%a0%84-%ec%8b%9c%eb%ae%ac%eb%a0%88%ec%9d%b4%ec%85%98-%ea%b2%8c%ec%9e%84-live-for-speed/
		http://rectamoptionem.com/2014/12/16/%EC%9E%AC%EB%AC%B4%EC%A0%81%EA%B2%80%ED%86%A0-4-rec-%EC%88%98%EC%9D%B5/
		http://abc3.wordpress.com/?p=123
		http://abc1.wordpress.com/2014/12/16/%EC%9E%AC%EB%AC%B4%EC%A0%81%EA%B2%80%ED%86%A0-4-rec-%EC%88%98%EC%9D%B5/
		http://abc2.wordpress.com/%EC%9E%AC%EB%AC%B4%EC%A0%81%EA%B2%80%ED%86%A0-4-rec-%EC%88%98%EC%9D%B5/
		http://21stcentury.co.kr/123
		http://21stcentury.co.kr/2014/12/16/%EC%9E%AC%EB%AC%B4%EC%A0%81%EA%B2%80%ED%86%A0-4-rec-%EC%88%98%EC%9D%B5/
		http://21stcentury.co.kr/%EC%9E%AC%EB%AC%B4%EC%A0%81%EA%B2%80%ED%86%A0-4-rec-%EC%88%98%EC%9D%B5/
		http://21stcentury.co.kr/feed
		http://abc4.wordpress.com/feed
		http://www.valleyinside.com/?p=2521
		http://abc2.tistory.com/entry/%EC%9E%AC%EB%AC%B4%EC%A0%81%EA%B2%80%ED%86%A0-4-rec-%EC%88%98%EC%9D%B5/
		http://abc2.tistory.com/rss
		http://snowkiwi.com/파이썬-플라스크-로-배우는-웹프로그래밍
		http://abc.blogspot.com/2014/03/나이는-먹는가는데-왜-여친은.html
		http://abc.blogspot.com/2014/03/aggressive-shit-끙끙.html
		"""
		urls4 = """
		http://blog.naver.com/abc?Redirect=Log&logNo=1234567890
		http://blog.naver.com/abc?logNo=1234567890
		"""
		url_list = strToList(urls3)
		#url_list = strToList(urls2)
		url_list = strToList(urls4)
		#url = "http://blog.aladin.co.kr/misshide/7149825"
		klist1 = ["ourl","regex","guid","tmp_guid","cid","pid","gen","host","curl","clink","vurl","murl","trackback"]
		klist2 = ["ORIGINAL","REGEX","GUID","TMP GUID","CHANNEL ID","POST ID","GENERATOR","HOST","FEED","CHANNEL URL","VIEW URL","MOBILE URL","TRACKBACK"]
		print "======================================="
		for url in url_list :
			url = url.strip()
			if url == "" :
				continue
			print "URL : "+url+"\n"
			urldata = factory.getAllDataDic(url)
			if urldata is None :
				continue
			for i in range(0, len(klist1)) :
				k = klist1[i]
				if k in urldata :
					v = urldata[k]
					print "%s : %s"%(klist2[i],v)
			print "======================================="
