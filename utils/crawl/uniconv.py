#!/usr/bin/env python
#coding: utf8

def tryToDecodeXmlEntity(src, min_count=20, unknown_char_reaction="ignore"):
	"""
	별루 안길면 그까이꺼 디코드 안한다.. ㅡ_ㅡㅋ
	우린 왜 맨날 이런식이야;;;
	"""
	if len(src) > min_count * 5 and src.count("&#") > min_count:
		return uniUTF8(src)
	else:
		return src


def convertToUTF8(src):
	cur = src.find("&#")
	n_cur = src.find(";", cur)
	rep_set = set()
	try:
		while cur >=0  and n_cur >=0:
			if n_cur - cur > 5 and n_cur - cur < 8:
				rep_set.add(src[cur:n_cur+1])
			cur = src.find("&#", n_cur)
			n_cur = src.find(";", cur)
	except Exception, msg:
		return src

	for kk in rep_set:
		try:
			n = kk[2:-1]
			if n[0] == 'x' or n[0] == 'X':
				new_n = unichr(int(n[1:], 16)).encode("utf8", "ignore")
			else:
				new_n = unichr(int(n) ).encode("utf8", "xmlcharrefreplace")
			src = src.replace(kk, new_n)
		except Exception, msg:
			pass
	
	return src
	
			

def uniUTF8(src, unknown_char_reaction="ignore"):
	"""
	unknown_char_reaction is one of ("ignore", "replace", "error")
	and "error" may cause some UnicodeEncodeError
	주의: 이 함수 많이 쓰면 느려질 수 있음..ㅎㅎ ㅡ_ㅡ;;;;
	주의2: 이 함수를 직접 쓰지 말고 가급적 tryToDecodeXmlEntity()를 사용할 것.
	"""
	if type(src) == unicode:
		return src.encode("utf8", unknown_char_reaction)
	tokens = "__BLANK__".join(src.split()).replace("&#", " &#").replace(";", "; ").split()
	tgt = ""
	for token in tokens:
		if token.endswith(";"):
			try:
				if token.upper().startswith("&#X"):
					tgt += unichr(int(token.upper().strip("&#X;"), 16)).encode("utf8", unknown_char_reaction)
				elif token.startswith("&#"):
					tgt += unichr(int(token.strip("&#;"))).encode("utf8", unknown_char_reaction)
				else:
					tgt += token
			except:
				tgt += token
		else:
			tgt += token
	return tgt.replace("__BLANK__", " ")


def test (s):
	import time
	b_time = time.time()
	for i in range(2000):
		convertToUTF8(s)
	print time.time() - b_time
	


if __name__ == "__main__":
	s = """[&#x10FFFF;] ascii and 한글도;;; 있고 &#xD504;&#xB85C;&#xADF8;&#xB798;adjlaksdjklsaj&#xBA38;&#xB4E4;&#xC740; &#xD2B9;&#xBCC4; 변태문자열&#xD55C; &#xC0AC;&#xB78C;&#xC774;&#xB77C;&#xACE0; &#xD3C9;&#xAC00;&#xB418;&#xB294; &#xAC83;&#xC744; &#xC88B;&#xC544;&#xD55C;&#xB2E4;. &#xC0AC;&#xC2E4;&#xC740; &#xC5B4;&#xB5A4; &#xBAA8;&#xBC94;&#xC801;&#xC778; &#xD504;&#xB85C;&#xADF8;&#xB798;&#xBA38;&#xB4E4;&#xC740; &#xB2E4;&#xB978; &#xD504;&#xB85C;&#xADF8;&#xB798;&#xBA38;&#xB4E4;&#xC758; &#xC774;&#xC0C1;&#xD55C; &#xC810;&#xC744; &#xAC1C;&#xBC1C;&#xC790;&#xB4E4;&#xC758; &#xCEE4;&#xBBA4;&#xB2C8;&#xD2F0; &#xB0B4;&#xC5D0;&#xC11C;&#xB3C4; &#xBC1C;&#xACAC;&#xD55C;&#xB2E4;. &#xC544;&#xB798;&#xC5D0; 10&#xAC00;&#xC9C0; &#51824; 아쉬케론 근처 &#51824;림 &#34;  키브츠에 " 저 호구놈ㅜ &#56352;호 """
	#print uniUTF8(s)
	#print tryToDecodeXmlEntity(s)
	print convertToUTF8(s)


