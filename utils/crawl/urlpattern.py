#coding: utf8
import urlparse
from urllib import quote

from common_func import getDBCursor
from utils.log import getLogger

RESERVED = ":/?#[]@!$&'()*+,;=\\%~"

TLDs = ["com", "net","org","edu","int", "biz", "info","name", "pro", "museum", "gov", "mil", "art", "aero","coop", "co", "mobi", "asia", "travel", "jobs"]
ccTLDs = ["bn","mm","li","pt","gb","jo","es","py","ki","ne","ar","tg","bf","mu","la","lv","ro","fm","cc","tw","im","me","rw","pl","tn","kz","sy","gs","mz","ir","vn","ie","ga","ml","pr","au","bv","kh","tc","lr","bo","td","cr","ci","er","gi","mt","jp","nf","do","tt","vg","si","il","md","az","to","ru","pe","gr","cz","��.ac","ky","id","no","cy","at","gy","bh","mo","am","nr","eu","ch","ua","kw","bw","lt","ng","pk","np","io","ee","kp","kg","bg","tl","fo","ca","it","gq","hm","mg","ae","ht","aw","tz","cx","sb","co","nu","bi","gg","mn","al","de","et","sn","ph","wf","re","pm","fi","cg","mw","uy","gh","sa","tr","lu","vi","sg","in","mf","ad","tm","hu","ug","kn","us","gp","nz","cn","ao","gf","rs","na","cw","pa","sm","bq","bj","mq","sj","fj","om","cf","vc","ke","ba","mv","hk","ma","dm","eg","hr","ag","tj","ni","km","by","gw","cm","uz","um","bz","st","aq","cv","su","dz","ge","mp","an","br","sl","gn","dj","se","bb","my","fr","tk","iq","tp","mh","af","ss","gu","eh","bl","cu","pg","qa","va","cl","ms","lc","ai","gd","nc","ve","sk","bs","bd","mc","dk","pf","je","ws","fk","vu","gm","mx","so","uk","sh","sd","mk","lk","th","jm","ps","sr","gt","bm","ls","ck","sx","pn","bt","ly","as","tf","ec","kr","be","mr","ax","tv","cd","sz","hn","gl","lb","sv","sc","pw","nl","is"]
SLDs = ["co","go","ac","nm","ne","or","re","kg","es","ms","hs","sc","pe"]


def getTopDomain(url):
	
	parsed_url = urlparse.urlparse(url)
	netloc = parsed_url.netloc
	pieces = netloc.split(".")
	if len(pieces) < 2:
		return None
	elif len(pieces) == 2:
		if pieces[1] in TLDs:
			return netloc
		elif pieces[1] == "kr":
			if pieces[0] in SLDs:
				return None
			else:
				return netloc
	else:
		if len(pieces) == 4:
			try:
				k = int(pieces[-1])
				return None
			except Exception, msg:
				pass

		if pieces[-1] in TLDs:
			return ".".join(pieces[-2:])
		elif pieces[-1] in ccTLDs:
			if pieces[-2] in SLDs:
				return ".".join(pieces[-3:])
			else:
				return ".".join(pieces[-2:])
		else:
			return ".".join(pieces[-3:])


class Rule:
	def __init__(self, type, sc_type, priority, idx):
		self.type = type
		self.path_rule = None
		self.param_rule = dict()
		self.host = ""
		self.key_dict = dict()
		self.out_pattern = ""
		self.sc_type = sc_type
		self.idx = idx
		self.priority = priority
		self.other_pattern = ""
		self.build_patterns = dict()


class HostUrlRule:
	def __init__(self):
		self.rules = list()
		self.listtop_patterns = list()
		
		
RESERVED_KEY = ["(BOARD)", "(PAGE)", "(CMD)", "(INT_ID)", "(STR_ID)", "(ANY)"]
INT_KEY = ["PAGE", "NO", "INT"]




def makeUrlFromPattern(patterns, ret_key_dict):
	try:
		for ret_url in patterns:
			anypath_str = ""
			for kk in ret_key_dict:
				if (kk in INT_KEY or kk.startswith("INT_")) and  not isInt(ret_key_dict[kk]) :
					break
				if kk == "ANYPATH" and ret_key_dict[kk].strip() == "":
					ret_url = ret_url.replace("/(ANYPATH)/", "/")
				else:
					ret_url = ret_url.replace("("+kk+")", ret_key_dict[kk])
					if kk == "ANYPATH":
						anypath_str = "/"+ret_key_dict[kk]

			if ret_url.count("(") == 0 and ret_url.count(")") == 0:
				ret_url = quote(ret_url.strip(), safe=RESERVED)
				ret_url = ret_url.replace("://m.www.","://m.").replace(anypath_str, "")
				return ret_url
	except Exception, msg:
		getLogger().error(msg)
	return ""


def getParsedUrl(url):
	
	parsed_url = urlparse.urlparse(url)

	org_netloc = key_netloc = parsed_url.netloc
	if org_netloc.startswith("www."):
		key_netloc = org_netloc.replace("www.", "")

	paths = parsed_url.path.split("/")
	params = parsed_url.query.split("&")
	param_dict = dict()
	for param in params:
		pieces = param.split("=")
		if len(pieces) == 2:
			key, val = pieces
			param_dict[key] = val
		elif len(pieces) == 1 and param != "":
			param_dict["EXCEPTION"] = param
		elif len(pieces) > 2:
			key = pieces[0]
			val = "=".join(pieces[1:])
			param_dict[key] = val

	return org_netloc, key_netloc, paths, param_dict


def makeUrl(org_netloc, key_netloc, paths, param_dict):
	
	L = param_dict.items()
	L.sort(key=lambda item:item[0])

	param = "&".join(key+"="+val for key, val in L)	

	url = "http://"+org_netloc+"/".join(paths)

	if len(param.strip()) > 0:
		url = url+"?"+param
	return url


def isInt(str):
	try:
		k = int(str)
		return True
	except Exception, msg:
		return False

def getKey(str):
	if len(str) > 2 and str[0] == "(" and str[-1] == ")":
		return str[1:-1]
	return None

def comparePath(path_a, path_b, p_i):

	ret_dict = dict()
	matched = True
	for t_p in path_a:
		try:
			t_key = getKey(t_p)
			if t_key:
				if t_key in ret_dict:
					#print t_key , ret_dict, path_b[p_i]
					if ret_dict[t_key] != path_b[p_i]:
						matched = False
						break
				else:
					t_cur = path_b[p_i].find("&")
					if t_cur >= 0:
						ret_dict[t_key] = path_b[p_i][:t_cur]
					else:
						ret_dict[t_key] = path_b[p_i]

			elif p_i == len(path_b):
				matched = False
				break
			elif t_p != path_b[p_i]:      # path 중 일부가 키로 매치된는 경우 
				if t_p.count("(") > 0:
					t_ret_dict = compareStr(t_p, path_b[p_i])
					for key, val in  t_ret_dict.items():
						if key in ret_dict and ret_dict[key] != val:
							matched = False
							break

					if t_ret_dict:
						ret_dict.update(t_ret_dict)
					else:
						matched = False
						break
				else:
					matched = False
					break

			p_i += 1
		except Exception, msg:
			continue

	return matched, ret_dict, p_i


def compareStr(str1, str2):   # (STR1).html    abc.html
	idx = 0
	str1 = str1.replace("(","##").replace(")","##")
	cc = str1.count("##") 
	ret_dict = dict()
	if cc % 2 == 0:
		pieces = str1.split("##")
		old_key = ""
		for k in range(len(pieces)):
			try:
				if k % 2 == 0:
					if pieces[k] == "" and old_key != "":
						ret_dict[old_key] = str2
					elif str2.startswith(pieces[k]):
						str2 = str2[len(pieces[k]):]
					else:
						cur = str2.find(pieces[k])
						if cur >= 0:
							ret_dict[old_key] = str2[:cur]
							#str2 = str2[len(pieces[k]):]
							str2 = str2[cur+len(pieces[k]):]
						else:
							return ret_dict
				else:
					old_key = pieces[k]
			except Exception, msg:	
				#getLogger().error(msg)
				pass
	return ret_dict


		


DEFAULT_RULE = dict()



class UrlFactory:

	def __init__(self):
		self.rules = dict()
		self.url_build_patterns = dict()
		self.parser_dict = dict()
		#self.setRules()

	def setRules(self, db_cursor=None):
		#db, cursor = getDBCursor(host="10.35.31.3", user="krystal",passwd="zmfltmxkf", db="domanager")


		if db_cursor == None:
			db, cursor = getDBCursor(host="10.35.31.229", user="domanager",passwd="domanagerA!", db="domanager")
		else:
			cursor = db_cursor


		query = "SELECT a.idx, priority, urlType, scCode, inputPattern, guidPattern, b.name FROM url_patterns a, codes b where a.scCode = b.idx "
		cursor.execute(query )
		results = cursor.fetchall()
		self.makeRules(results)


		query = "SELECT urlpatternID, parserID FROM url_pattern_parser"
		cursor.execute(query )
		results = cursor.fetchall()
		for idx, parser_id in results:
			self.parser_dict[idx] = parser_id

		query = "SELECT a.guidPattern, a.buildPattern, b.name FROM url_build_patterns a, codes b where a.buildTypeCode = b.idx "
		cursor.execute(query )
		results = cursor.fetchall()

		for guid_pattern, build_pattern, name in results:

			o_netloc, o_key_netloc, o_path, o_params = getParsedUrl(guid_pattern)
			guid_pattern = makeUrl(o_netloc, o_key_netloc, o_path, o_params)


			if not guid_pattern in self.url_build_patterns:
				self.url_build_patterns[guid_pattern] = dict()
			self.url_build_patterns[guid_pattern][name] = build_pattern

		if db_cursor == None:
			cursor.close()
			db.close()

	def makeRules(self, results):
		for idx, priority, type, sc_code, input, output, sc_type in results:
			r_rule = Rule(type, sc_type, priority, idx)
			org_netloc, key_netloc, path, params = getParsedUrl(input)

			domain_pieces = key_netloc.split(".")
			index = 0
			if key_netloc == "(ANYHOST)":
				key_netloc = "ANYHOST"
			else:
				t_key = getKey(domain_pieces[0])
				if t_key:
					key_netloc = ".".join(domain_pieces[1:])
					r_rule.key_dict[t_key] = "host", 0
					r_rule.host = key_netloc
				else:
					r_rule.host = key_netloc

			index = 1
			r_rule.path_rule = path
			page_param = ""

			if "(ANYPATH)" in path:
				p_i = path.index("(ANYPATH)")
				r_rule.path_rule = [path[:p_i],path[p_i+1:]]
				r_rule.key_dict["ANYPATH"] = "path", 1
			else:
				for piece in path[1:]:
					t_key = getKey(piece)
					if t_key:
						r_rule.key_dict[t_key] = "path", index
					index += 1
					
			for key, val in params.items():
				t_key = getKey(val)
				if t_key:
					if t_key in ("PAGE", "INT_START"):
						page_param = key
					r_rule.key_dict[t_key] = "param", key
				else:
					r_rule.param_rule[key] = val
			if key_netloc not in self.rules:
				self.rules[key_netloc] = HostUrlRule()

			o_netloc, o_key_netloc, o_path, o_params = getParsedUrl(output)
			output = makeUrl(o_netloc, o_key_netloc, o_path, o_params)

			if type == "list":
				if page_param != "" and page_param in o_params:
					del o_params[page_param]                            # 페이지 파라미터 제거후 패턴 재생성하여 list_top 추가
					r_rule.other_pattern = makeUrl(o_netloc, o_key_netloc, o_path, o_params)
					t_rule = Rule("list_top", sc_type, 100, idx)
					t_rule.host = r_rule.host
					t_rule.path_rule = r_rule.path_rule
					t_rule.param_rule = o_params
					t_rule.key_dict = r_rule.key_dict
					t_rule.out_pattern = r_rule.other_pattern

					self.rules[key_netloc].listtop_patterns.append(t_rule.out_pattern)
					self.rules[key_netloc].rules.append(t_rule)

			elif type == "list_top":
				self.rules[key_netloc].listtop_patterns.append(output)

			r_rule.out_pattern = output
			self.rules[key_netloc].rules.append(r_rule)
			self.rules[key_netloc].rules.sort(key=lambda item:item.priority)


	def checkKeyNetloc(self, key_netloc, url):

		if key_netloc not in self.rules:
			host_pieces = key_netloc.split(".")
			if len(host_pieces)>3:
				t_host = ".".join(host_pieces[1:])
				if t_host in self.rules:
					key_netloc = t_host
				else:
					key_netloc = getTopDomain(url)
			else:
				key_netloc = getTopDomain(url)

			if key_netloc not in self.rules:
				key_netloc = "ANYHOST"
		return key_netloc


	def getGuid(self, url):

		if len(self.rules) == 0:
			self.setRules()

	
		org_netloc, key_netloc, path, params = getParsedUrl(url)
		org_key_netloc = key_netloc
		key_netloc = self.checkKeyNetloc(key_netloc, url)


		if key_netloc in self.rules:
			host_rules = self.rules[key_netloc]

			for rule in host_rules.rules:
				matched = True
				ret_key_dict = dict()     

				if key_netloc == "ANYHOST":
					ret_key_dict["ANYHOST"] = org_netloc

				"""
				elif rule.host != "" and rule.host != org_key_netloc:
					continue
				"""

				host_match = False

				for key, position in rule.key_dict.items():
					where, val = position
					if matched == False:
						break
					if where == "path":
						try:
							if key == "ANYPATH":
								pre_path = rule.path_rule[0]
								suf_path = rule.path_rule[1]

								p_i = 0
								matched, ret_dict, p_i = comparePath(pre_path, path, p_i)
								if matched == False:
									break
								if ret_dict:
									ret_key_dict.update(ret_dict)
									
								key_count = "/".join(suf_path).count("(") 
								reverse_path = path[p_i:][::-1]
								smatched, ret_dict, t_pi = comparePath(suf_path[::-1], reverse_path, 0)

								if smatched and len(ret_dict) == key_count:
									anypath = "/".join(reverse_path[t_pi:][::-1])
									ret_key_dict[key] = anypath
									if ret_dict:
										ret_key_dict.update(ret_dict)
								else:
									break
							else:
								r_val = path[val]
								if key == "BOARD" and r_val.count(".") > 0:
									break
								else:
									ret_key_dict[key] = r_val

									t_cur = r_val.find("&")
									if t_cur > 0 and key not in ret_key_dict:
										ret_key_dict[key] = r_val[:t_cur]

						except Exception, msg:
							#getLogger().error(msg)
							pass
					elif where == "param" and val in params:
						ret_key_dict[key] = params[val]
					elif where == "host":
						try:
							domain_pieces = org_netloc.split(".")
							r_val = domain_pieces[val]
							ret_key_dict[key] = r_val

							if rule.host != ".".join(domain_pieces[val+1:]):
								matched = False
								break
							host_match = True
						except Exception, msg:
							getLogger().error(msg)

				if not host_match and  rule.host != "" and rule.host != org_key_netloc:
					matched = False
				if matched == False:
					continue
				
				if "ANYPATH" not in ret_key_dict:
					p_i = 0
					if len(rule.path_rule) != len(path):
						continue
					else:
						matched, ret_dict, p_i = comparePath(rule.path_rule, path, p_i)
						if ret_dict:
							ret_key_dict.update(ret_dict)


				if matched == False:
					continue

				for param in rule.param_rule:
					str1 = rule.param_rule[param]
					if param in params:
						str2 = params[param]
						ret_dict = compareStr(str1, str2)
						if len(ret_dict) > 0:
							ret_key_dict.update(ret_dict)
						elif str1 != str2:
							matched = False
							continue
					elif str1.count("(") == 0:
						matched = False
						continue
				
				if matched == False:
					continue

				ret_url = rule.out_pattern
				down_url = rule.out_pattern

				for kk in ret_key_dict:

					if (kk in INT_KEY or kk.startswith("INT_")) and  not isInt(ret_key_dict[kk]) :
						matched = False
						break
					if kk == "ANYPATH" and ret_key_dict[kk].strip() == "":
						ret_url = ret_url.replace("/(ANYPATH)/", "/").replace("/(ANYPATH)", "/")
						down_url = down_url.replace("/(ANYPATH)/", "/").replace("/(ANYPATH)", "/")
					elif kk == "ANYPATH":
						ret_url = ret_url.replace("/(ANYPATH)/", "/").replace("/(ANYPATH)", "/")
						down_url = down_url.replace("("+kk+")", ret_key_dict[kk])
					elif ret_key_dict[kk] == "":
						matched = False
						break
					else:
						down_url = down_url.replace("("+kk+")", ret_key_dict[kk])
						if kk.startswith("ANY_"):
							t_cur =  ret_url.find("("+kk+")")
							try:
								if t_cur > 0:
									s_cur = ret_url.find("/", 10)
									if s_cur > 0:
										while s_cur < t_cur:
											n_cur = ret_url.find("/", s_cur+1)
											if n_cur < t_cur and n_cur > 0:
												s_cur = n_cur
											else:
												break
									e_cur = ret_url.find("/", t_cur+len(kk) )

									if e_cur > 0:
										ret_url = ret_url.replace(ret_url[s_cur+1:e_cur] +"/", "")
									else:
										ret_url = ret_url.replace(ret_url[s_cur+1:], "")
							except Exception, msg:
								print msg
							#ret_url = ret_url.replace("/("+kk+")/", "/")
						else:
							ret_url = ret_url.replace("("+kk+")", ret_key_dict[kk])


				if matched == False:
					continue
				ret_type = rule.type
				if ret_url.count("(") == 0 and ret_url.count(")") == 0:
					if rule.type == "list" and "PAGE" in ret_key_dict and int(ret_key_dict["PAGE"]) == 1:
						ret_type = "list_top"

						if rule.other_pattern:
							ret_url = rule.other_pattern
						for kk in ret_key_dict:
							ret_url = ret_url.replace("("+kk+")", ret_key_dict[kk])

					if ret_type == "view":
						ret_key_dict["listtop_url"] = makeUrlFromPattern(host_rules.listtop_patterns, ret_key_dict)

					if rule.out_pattern in self.url_build_patterns:
						try:
							for name, out_pattern in self.url_build_patterns[rule.out_pattern].items():
								ret_key_dict[name] = makeUrlFromPattern([out_pattern], ret_key_dict)
						except Exception, msg:
							getLogger().error(msg)
					ret_key_dict["idx"] = int(rule.idx)
					ret_key_dict["sc"] = rule.sc_type

					if  ret_key_dict["idx"] in self.parser_dict:
						ret_key_dict["parser_id"] = self.parser_dict[ret_key_dict["idx"]]

					if down_url.find("EXCEPTION=") > 0:
						down_url = down_url.replace("EXCEPTION=", "")
						ret_url = ret_url.replace("EXCEPTION=", "")
					ret_key_dict["down_url"] = down_url
						
					ret_url = quote(ret_url.strip(), safe=RESERVED)
					ret_url = ret_url.replace("://www.","://").replace("/xe/","/")
					return ret_type, ret_url, ret_key_dict

			return None   #, url, {})



if __name__ == "__main__":
	a = UrlFactory()		

	TEST_URLS = ["http://ilwar.com/asdf/123?name=asdf  ", "http://ilwar.com/asdf/123/page/123?name=asdf  ", "http://www.ilwar.com/asdf/page/1?name=asdf  ", "http://ilwar.com/asdf/123/page/1?name=asdf  ",  "http://ilwar.com/asdf", "http://www.ilbe.com/index.php?mid=ilbe&category=123&document_srl=123123",  "http://www.ilbe.com/asdfasd?mid=ilbe&category=123&document_srl=123123", "http://www.ilbe.com/123123", "http://abc.tistory.com/asdf/123123",  "http://abc.tistory.com/asdf", "http://abc.tistory.com/123", "http://naver.com/asdfa/asdfa/asdfa/board.php?bo_table=cm_lego&wr_id=1231&dasd=asdf", "http://todayhumor.co.kr/board/list.php?table=gomin", "http://todayhumor.co.kr/board/list.php?table=gomin&page=4" ]

	import sys

	if len(sys.argv) > 1:
		url = sys.argv[1]
		print url, a.getGuid(url)
	else:

		db, cursor = getDBCursor(host="10.35.31.229", user="domanager",passwd="domanagerA!", db="domanager")
		query = "SELECT down_url, guid FROM url_test "
		cursor.execute(query)
		results = cursor.fetchall()
		fail_count = 0
		for url, guid in results:
			res = a.getGuid(url)
			if res:
				(type, new_guid, other_dict) = res
				if guid != new_guid:
					print url, guid, new_guid , "SOMETHING WRONG"
					fail_count += 1
				else:
					print url, guid, "IT's OK!!"
			else:
				print url, "AAAAAAAA" 
				fail_count += 1

		print 
		print "FAIL COUNT : %s"%fail_count
