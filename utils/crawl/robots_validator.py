#!/usr/bin/env python
#coding: utf8
"""
Robot_validator : check url crawlable by rules of robots.txt

usage :
	robot_validator = RobotsValidator()
	is_crawlable = robot_validator.isCrawlable(url)

"""
import re
import socket
import robotparser
import urlparse
import threading

import mechanize
from utils import log
from url_util import URLUtil, URL_TYPE_TP, URL_TYPE_DR

USER_AGENT_ZUM = "Mozilla/5.0 (compatible; ZumBot/1.0; http://help.zum.com/inquiry)"
USER_AGENT = USER_AGENT_ZUM
USER_AGENT_ZUMBOT = "ZUMBOT"
MAX_KEY_LENGTH = 250
MAX_ROBOTS_CACHE_SIZE = 1024 
socket.setdefaulttimeout(5.0)


class RobotsTextRules:
	"""
	An object has data of robots text
	"""
	def __init__(self):
		self.return_code = 0
		self.robots_file = []
		self.is_useragent_configured = False
		self.allow_paths = []
		self.disallow_paths = []

	def __str__(self):
		#return "%s\nreturn_code=%s\nis_useragent_configured=%s" % ("\n".join(self.robots_file), self.return_code, self.is_useragent_configured)
		s = "----------\n%s\n----------" % "\n".join(self.robots_file)
		s += "\nreturn_code=%s\nis_useragent_configured=%s" % (self.return_code, self.is_useragent_configured)
		s += "\nallow_paths: %s" % (self.allow_paths)
		return s


class RobotsValidator:
	"""
	An object that handles checking if we should fetch and crawl a specific URL.
	This is based on the type of the URL (only crawl http URLs) and robot rules.
	Maintains a cache of robot rules already fetched.
	"""

	def __init__(self, user_agents=[USER_AGENT_ZUMBOT], match_url=".+"):
		"""
		Init RobotsValidator class
		If no argument about match_url, 
		initialize user_agents=[DAUMOA, EDI], match_url=".+"(regular expression)
		The user_agents must be list or tuple.
		"""
		assert type(user_agents) in (list, tuple)
		self.robots = {}	# Dict of robot URLs to robot parsers
		self.match_url = re.compile(match_url)
		self.user_agents = user_agents
		self.lock = threading.Lock()
		self.url_util = URLUtil()
		self.blocked = False
		self.delay = 3

	def __repr__(self):
		"""
		Print user_agents and internal robots cache size
		"""
		return "inputed user_agents: %s\ninternal robots cache size: %d" % (self.user_agents, len(self.robots))

	
	def __str__(self):
		"""
		Print user_agents and internal robots cache size
		"""
		ret_str = ""
		for key in self.robots.keys():
			ret_str += "%s:\n\t%s\n" % (key, "\n\t".join(str(self.robots[key]).split("\n")))
		return ret_str
		

	def flushInternalCache(self):
		"""
		Flush internal cache
		"""
		self.robots.clear()


	def flushAllCache(self):
		"""
		Flush all cache
		"""
		self.flushInternalCache()

	flush = flushAllCache
	
	def isDisallowSite(self, url, verbose=False):
		"""
		robots.txt가 아래 문구를 포함하면 True를 리턴.
			User-agent: * or zumbot
			Disallow: /
		"""
		logger = log.getLogger()
		self.delay = 3
		robots_site_path = urlparse.urljoin(url, "/robots.txt")	# Then the site-level
		# 3. download robots text
		self.blocked = False
		rules = self._parsingRobotsFile(robots_site_path)	# First try site-level
		if self.blocked:
			return True, self.delay
		else:
			return False, self.delay


	def isCrawlable(self, url, verbose=False, detail=False):
		"""
		Returns True if it's OK to crawl the absolute URL provided.
		"""
		logger = log.getLogger()

		# daum.net일 경우 무조건 True
		(scheme, netloc, path, param, query, fragment) = urlparse.urlparse(url)

		try:
			self.lock.acquire()

			ret = None

			if not url.startswith("http") or not self.match_url.match(url):
				if verbose:
					logger.info("not match with 'http' or match_url: %s" % self.match_url.match(url))
				ret = False

			if not ret:
				# clear internal cache if exceed MAX_ROBOTS_CACHE_SIZE
				if len(self.robots) > MAX_ROBOTS_CACHE_SIZE:
					self.robots.clear()
				#logger.debug("++ isCrawlable: A")

				robot_rules = self._getRules(url, verbose)
				#logger.debug("++ isCrawlable: B")
				if verbose:
					try:
						logger.info("URL: %s" % url)
						logger.info("ROBOT_RULES: \n%s" % robot_rules)
						#logger.info(" RULE: %s" % robot_rules.robots_file)
					except Exception, msg:
						logger.info("Exception: %s" % msg)

				parser = robotparser.RobotFileParser()
				parser.parse(robot_rules.robots_file)
				#logger.debug("++ isCrawlable: C")

				if robot_rules.is_useragent_configured:

					is_crawlable = True
					for user_agent in self.user_agents:
						is_crawlable = is_crawlable and parser.can_fetch(user_agent, url)

						if verbose:
							logger.info("maching: %s" % user_agent)
							logger.info("is_crawlable: %s" % is_crawlable)

					if verbose:
						logger.info("return %s" % is_crawlable)
					#return is_crawlable
					ret = is_crawlable
				else:
					#logger.debug("is_crawlable2: %s" % is_crawlable)
					is_crawlable = parser.can_fetch("*", url)

					if verbose:
						logger.info("is_crawlable3: %s" % is_crawlable)
						logger.info("is_crawlable4: %s" % parser.can_fetch("*", url))
						logger.info("return2 %s" % is_crawlable)
					ret = is_crawlable
		except:
			self.lock.release()
			if verbose: 
				logger.debug("++ robots_lock release")
		else:
			self.lock.release()
			if verbose: 
				logger.debug("++ robots_lock release")

		if not ret:
			ret = False

		# URLType이 TOP/Directory일 경우에는 무조건 방문한다(08/07/08)
		# TP가 False에서 True로 바꼈는지 여부를 판단하기 위해서 위치 수정(08/11/26)
		try:
			forced = False
			url_type = self.url_util.getURLType(url)
			if url_type in (URL_TYPE_TP, URL_TYPE_DR) and ret == False:
				logger.debug("**VISIT forced, URLType: %s", url_type)
				ret = True
				forced = True
		except:
			pass

		#logger.debug("++ is_crawlable: FINISH")
		if detail:
			ret = (ret, forced)

		return ret

	
	def _getRules(self, url, verbose=False):
		"""
		Returns the RobotTextRules object for url(site-level or dir-level)
		First:  use internal cache
		Second: use memcache
		Third:  download robots.txt and parsing 
		"""
		logger = log.getLogger()

		# 1. use stored robots dictionary cache
		robots_site_path = urlparse.urljoin(url, "/robots.txt")	# Then the site-level
		if robots_site_path in self.robots:
			if verbose:
				logger.info("robotstxt in local memory: %s", robots_site_path)
			return self.robots[robots_site_path]
		
		# 2. use memcache
		rules = None
		try:
			# 3. download robots text
			rules = self._parsingRobotsFile(robots_site_path)	# First try site-level
			if verbose:
				logger.info("robotstxt downloaded: %s: %s", rules.return_code, robots_site_path)
			self.robots[robots_site_path] = rules

		except:
			pass

		return rules
	

	def _parsingRobotsFile(self, url):
		"""
		Setup internal state after downloading robots.txt
		If urlopen.code in (401, 403), all user-agent is disallowed. -> 삭제
		If urlopen.code >= 400, all user-agent is allowd.
		"""
		domain_name = urlparse.urlparse(url)[1]
		rules = RobotsTextRules()
		#getLogger().debug("++Trying to download: %s", url)
		
		opener = mechanize.build_opener(mechanize.HTTPRefreshProcessor,)

		rq = mechanize.Request(url)
		rq.add_header("User-agent", "Mozilla/5.0 (compatible; Windows NT 6.1?; ZumBot/1.0; http://help.zum.com/inquiry)")


		#shttp.setRequestHeader(user_agent = USER_AGENT)
		rs = None
		try:
			rs = opener.open(rq)
			header = rs.info()
			rules.return_code = rs.code
		except Exception, msg:
			try:
				if not url.startswith("http://www."):
					t_url = url.replace("http://", "http://www.")
					rq = mechanize.Request(t_url )
					rq.add_header("User-agent", "Mozilla/5.0 (compatible; Windows NT 6.1?; ZumBot/1.0; http://help.zum.com/inquiry)")
					rs = opener.open(rq)
					header = rs.info()
					rules.return_code = rs.code
			except Exception, msg:
				return rules
		#getLogger().debug("++Downloaded: %s", url)
		try:

			if rs:
				robottxt_body = rs.read()

				#print robottxt_body
				if header["Content-Type"].find("text/plain") < 0:
					robottxt_body = "User-agent: *\nDisallow:"

				robottxt_body = robottxt_body.replace("\r\n", "\n")
				robottxt_body = robottxt_body.replace("\n\r", "\n")
				robottxt_body = robottxt_body.replace("\r", "\n")
				temptxt_lines = robottxt_body.split("\n")

				robottxt_lines = []
				for robottxt_line in temptxt_lines:
					robottxt_lines.append(robottxt_line.strip())

	# 			if rules.return_code in (401, 403):
	# 				robottxt_lines = ["# 404 robots.txt","User-agent: *","Disallow:/"]
				if rules.return_code >= 400:
					# 400번대는 다 허용
					robottxt_lines = ["# 404 robots.txt","User-agent: *","Disallow:"]
				elif rules.return_code == 200 and robottxt_lines:
					pass
				else:
					return rules

				
				if robottxt_lines[-1] != "":
					robottxt_lines.append("")	#구분을 위해서 빈줄 하나 추가	
				rules = self._parseLines(robottxt_lines, domain_name, rules)
		except Exception, msg:
			log.getLogger().debug("parsing Exception: %s" % msg)
		#getLogger().debug("++End _parsingRobotsFile: %s", url)

		return rules


	def _parseLines(self, lines, domain_name, rules):
		"""
		Strip repeated sequential definitions of same user agent, 
		then return modified lines
		user-agent: 뒤에 아무것도 명시되지 않을 경우 unknown
		예외처리:
			Allow: /
			Disallow: /w3asp/documents/
			Disallow: /w3asp/documents/file.aspx
		"""
		last_ua = ""
		modified_lines = []
		allow_paths = []
		rules.is_useragent_configured = False
		zumbot_except = False
		zumbot_allow = False
		exist_allow_all = re.compile("^allow[\s]*:[\s]*/")
		for line in lines:
			line = line.strip()

			if line.lower().startswith("user-agent"):
				temp_ua = last_ua
				last_ua = line.strip()
				# user-agent: 뒤에 아무것도 명시되지 않을 경우 unknown으로 셋팅 (08/07/01)
				if last_ua.lower().replace("user-agent:","").strip() == "":
					line = "User-Agent: unknown"
					last_ua = line
				if filter(lambda x: x > -1, map(line.lower().find, self._make_check_ua_names(self.user_agents))):
					rules.is_useragent_configured = True
				if last_ua == temp_ua:
					continue	# skip line

			else:
				# allow: / 인지 확인 -> 의미없는 규칙이므로 없애버린다 (09/07/07)
				r = exist_allow_all.findall(line.lower())
				if len(r) > 0:
					path = line[line.find("/"):].strip()
					if path != "/":
						# / 인 경우 잘못 처리될수 있으므로 빼버린다 (10/03/18)
						allow_paths.append(path)
					else:
						continue

				if "".join(last_ua.lower().split()) in ["user-agent:*", "user-agent:zumbot"] :
					t_str = "".join(line.lower().strip().split())
					if t_str == "disallow:/" and not zumbot_allow:
						self.blocked = True
					elif t_str.startswith("disallow:/") and "".join(last_ua.lower().split()) == "user-agent:zumbot":
						zumbot_except = True	
					elif t_str == "disallow:" and "".join(last_ua.lower().split()) == "user-agent:zumbot":
						zumbot_allow = True	
						self.blocked = False
					elif t_str.startswith("allow:/"):
						self.blocked = False
						zumbot_allow = True
					
					if line.lower().startswith("crawl-delay:"):
						try:
							self.delay = int(line[12:])
						except Exception, msg:
							pass

			if line == "":
				last_ua = ""	# reset on blank line

			if line and not line.startswith("#"):
				# edi, daumoa, * 셋다 아니면 저장 하지 않도록 고치자
				modified_lines += [line]	# skip comment

		rules.robots_file = modified_lines 
		rules.allow_paths = allow_paths

		if zumbot_except :
			self.blocked = False
		
		return rules


	def _make_check_ua_names(self, user_agents):
		"""
		Make a list of user agents for modifying the robot.txt	
		Ex. list  = [" edi", ":edi", " daumoa", ":daumoa"]
		"""
		chk_ua_names = []
		for user_agent in user_agents:
			chk_ua_names.append(" %s" % user_agent.lower())
			chk_ua_names.append(":%s" % user_agent.lower())
		return chk_ua_names



if __name__ == "__main__":
	# TEST CODE
	#rv = RobotsValidator(["edi", "daumoa"])
	rv = RobotsValidator([USER_AGENT_ZUMBOT])
	urls = []
	urls = ["http://twitter.com/bigkini"]
	urls += ["http://uncyclopedia.kr/wiki/%EB%B0%B1%EA%B4%B4%EC%82%AC%EC%A0%84:%EA%B4%80%EB%A6%AC%EC%9E%90_%EC%84%A0%EA%B1%B0"]
	urls += ["http://kin.naver.com/opendic/index.nhn"]
	urls += ["http://kin.naver.com/"]
	urls.append("http://www.typemoon.net/bbs/board.php?bo_table=review&wr_id=185923")
	urls.append("http://clien.net/bbs/board.php?bo_table=review&wr_id=185923")
	urls.append("http://clien.net/adff/bbs/aaboard.php?bo_table=review&wr_id=185923")
	import sys
	if len(sys.argv) > 1:
		urls = []
		urls.append(sys.argv[1])

	# test code
	_debug = False

	for url in urls:
		print "*"*20, url, ">isCrawlable ->", rv.isCrawlable(url, _debug, detail=False)
		#print "*"*20, url, "> detail:TRUE ->", rv.isCrawlable(url, _debug, detail=True)
		print "isBlocked : ", rv.isDisallowSite(url)

	"""
	if _debug:
		print "\nprint RobotsValidator object :"
		print "%s" % rv
	"""
