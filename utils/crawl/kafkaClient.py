from kafka.client import KafkaClient
from kafka.producer import SimpleProducer
from utils.log import getLogger


SERVER_LIST = ["10.35.30.31:9092", "10.35.30.32:9092", "10.35.30.33:9092", "10.35.30.34:9092","10.35.30.35:9092","10.35.30.36:9092","10.35.30.37:9092","10.35.30.38:9092","10.35.30.39:9092","10.35.30.40:9092","10.35.30.41:9092", "10.35.30.42:9092","10.35.30.43:9092"]

class KafkaSender:

	def __init__(self):
		try:
			self.client = KafkaClient(",".join(SERVER_LIST))
			self.producer = SimpleProducer(self.client)
		except Exception, msg:
			getLogger().error("can't connect server")

	def sendData(self, data, sc="NEWS"):
		try:

			if sc != "NEWS":
				return "NO SERVICE"

			res = self.producer.send_messages(sc, data)
			if str(res).find("error=0") >= 0:
				return "OK"
			else:
				getLogger().error(str(res))
				return "ERROR"
		except Exception, msg:
			getLogger().error(msg)
			return "ERROR"

if __name__ == "__main__":

	import sys

	if len(sys.argv) > 1:
		file = sys.argv[1]
	else:
		file = "/home/zumse/data/news_docs/document_20150428_133410_7653.xml"
	kc = KafkaSender()

	fp = open(file)
	data = fp.read()
	res = kc.sendData(data)
	

	if str(res).find("error=0") >= 0:
		print "OK"
	else:
		print "ERROR"

