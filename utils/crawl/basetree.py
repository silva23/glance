#!/usr/bin/env python
# coding: utf-8
"""
This module provides some Tree-Data structure functions and classes.
"""

import os, sys
import Queue
import sets

__all__ = []



class BaseTreeNode:
	"""
	Base tree node class
	"""

	def __init__(self, name="", parent_name="", attrs=None):
		"""
		Initialize member value
		Tree child use Set
		"""
		self.name = name
		self.attrs = attrs
		self.parent_name = parent_name 
		self.children = sets.Set()


	def __repr__(self):
		"""
		Return tree node's key
		"""
		if not self.isLeaf(): return self.name + "/"
		else: return self.name
	

	def __str__(self):
		"""
		Return tree node's key
		"""
		return self.__repr__()


	def __getitem__(self, childname):
		"""
		Return tree node's attribute
		"""
		return self.attrs


	def isLeaf(self):
		"""
		Node is Leaf???
		"""
		if len(self.children) > 0: return False
		else: return True 


	def getSubTree(self, path="", depth=256):
		"""
		Return Tree key data use recursion
		"""
		buff = "%s%s\n" % (path, self)
		if depth != 0:
			if not self.isLeaf():
				for child in self.children:
					buff += child.getSubTree(path + str(self), depth-1)
		return buff



class BaseTree:
	"""
	Base Tree class
	"""

	def __init__(self, rootname=""):
		"""
		Initialize tree's root node and node key 
		"""
		self.name = rootname
		self.root = BaseTreeNode(self.name)


	def __repr__(self):
		"""
		Print tree tralverser data
		"""
		return self.__str__()


	def __str__(self):
		"""
		Print tree tralverser data
		"""
		return self.root.getSubTree()


	def __add__(self, child):
		"""
		Overload "+" operation
		tree = tree + node
		"""
		self.addChild(child)
		return self


	def getSubTree(self):
		"""
		Return tree tralverser data
		"""
		if self.root == None:
			return ""
		return self.root.getSubTree()


	def addChild(self, input_child): 
		"""
		Add tree's node use parent_name
		Input child must have parent_name
		"""
		parent_node = self.getChildByName(input_child.parent_name)
		if not parent_node:
			parent_node = self.root
		if parent_node == None:
			return False
		else:
			parent_node.children.add(input_child)
			return True


	def getChildByName(self, childname):
		"""
		Return tree's node use childname ( key )
		"""
		queue = Queue.Queue()
		queue.put(self.root)
		while not queue.empty(): 
			child_node = queue.get()
			if childname == child_node.name:
				result = child_node
				break
			for child in child_node.children:
				queue.put(child)
		else:
			return None
		return result


	def removeNodeByName(self, childname):
		"""
		Delete tree's node use childname ( key )
		"""
		del_child_node = self.getChildByName(childname)
		if del_child_node == None:
			return False
		self.removeChild(del_child_node)
		parent_node = self.getChildByName(del_child_node.parent_name)
		if parent_node != None:
			parent_node.children.remove(del_child_node)
		else:
			del self.root
	

	def removeChild(self, target_node):
		"""
		Delete all subtree of target node
		"""
		if not target_node.isLeaf():
			for child in target_node.children:
				self.removeChild(child)
			target_node.children.clear()
	


__all__.extend(["BaseTree", "BaseTreeNode"])



if __name__ == "__main__":
	tree = BaseTree("secret key")
	tree.addChild(BaseTreeNode("new key","secret key", None))
	tree.addChild(BaseTreeNode("other key","new key", None))
	print tree
	tree.removeNodeByName("new key")
	print tree.getSubTree()

#EOF
